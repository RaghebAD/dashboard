"use strict";

/**
 * Custom configuration values
 */
var titlePrefix = "YP Store";
var mydomainport = location.port;
var protocol = window.location.protocol;
var themeToUse = "store";

// var mydomain = "ypstore.com";
var mydomain = "ypcloud.io";
if (mydomainport && mydomainport !== 80) {
	mydomain += ":" + mydomainport;
}

var whitelistedDomain = ['localhost', '127.0.0.1', 'dashboard-api.' + mydomain];
var apiConfiguration = {
	domain: protocol + '//dashboard-api.' + mydomain,
	key: '9b96ba56ce934ded56c3f21ac9bdaddc8ba4782b7753cf07576bfabcace8632eba1749ff1187239ef1f56dd74377aa1e5d0a1113de2ed18368af4b808ad245bc7da986e101caddb7b75992b14d6a866db884ea8aee5ab02786886ecf9f25e974'
	//key: 'd44dfaaf1a3ba93adc6b3368816188f96134dfedec7072542eb3d84ec3e3d260f639954b8c0bc51e742c1dff3f80710e3e728edb004dce78d82d7ecd5e17e88c39fef78aa29aa2ed19ed0ca9011d75d9fc441a3c59845ebcf11f9393d5962549'
};

// Tenant Key Security: a3ztyNnEVygBLysZrLVWGrtB
// cookie secret : QQjYiEEDUaByyEbrwTW7YjKB
// session secret : DzXDHFTaQdoYbxP4KhQueMfe
// var whitelistedDomain = ['localhost', '127.0.0.1', 'ypstore-dashboard-api.' + mydomain];
// var apiConfiguration = {
// 	domain: protocol + '//ypstore-dashboard-api.' + mydomain,
// 	key: '9b96ba56ce934ded56c3f21ac9bdaddc8ba4782b7753cf07576bfabcace8632eba1749ff1187239ef1f56dd74377aa1e5d0a1113de2ed18368af4b808ad245bc7da986e101caddb7b75992b14d6a866db884ea8aee5ab02786886ecf9f25e974'
// };

var uiModuleDev = 'modules/dev';
var uiModuleQa = 'modules/qa';
var uiModuleStg = 'modules/stg';
var uiModuleProd = 'modules/prod';

var SOAJSRMS = ['controller','urac','oauth','dashboard','proxy', 'gcs'];

var modules = {
	"develop": {
		"dashboard": {
			services: 'modules/dashboard/services/install.js',
			contentBuilder: 'modules/dashboard/contentBuilder/install.js',
			staticContent: 'modules/dashboard/staticContent/install.js',
			githubApp: 'modules/dashboard/gitAccounts/install.js',
			swaggerEditorApp: 'modules/dashboard/swaggerEditor/install.js',
			catalogs: 'modules/dashboard/catalogs/install.js',
			ci: 'modules/dashboard/ci/install.js',
			cd: 'modules/dashboard/cd/install.js'
		}
	},
	"manage": {
		"dashboard": {
			productization: 'modules/dashboard/productization/install.js',
			multitenancy: 'modules/dashboard/multitenancy/install.js',
			members: 'modules/dashboard/members/install.js',
			settings: 'modules/dashboard/settings/install.js'
		}
	},
	"deploy": {
		"dashboard": {
			environments: 'modules/dashboard/environments/install.js'
		}
	},
	"operate": {
		"dev": {
			shoppingCart: uiModuleDev + '/shoppingCart/install.js',
			catalogProfiles: uiModuleDev + '/catalogProfiles/install.js',
			profile: uiModuleDev + '/profile/install.js',
			merchantProducts: uiModuleDev + '/merchantProducts/install.js',
			orders: uiModuleDev + '/orders/install.js',
			// drivers: uiModuleDev + '/drivers/install.js',
			urac: uiModuleDev + '/urac/install.js'
		},
		"qa": {
			shoppingCart: uiModuleQa + '/shoppingCart/install.js',
			catalogProfiles: uiModuleQa + '/catalogProfiles/install.js',
			profile: uiModuleQa + '/profile/install.js',
			merchantProducts: uiModuleQa + '/merchantProducts/install.js',
			orders: uiModuleQa + '/orders/install.js',
			urac: uiModuleQa + '/urac/install.js'
		},
		"prod": {
			shoppingCart: uiModuleProd + '/shoppingCart/install.js',
			catalogProfiles: uiModuleProd + '/catalogProfiles/install.js',
			profile: uiModuleProd + '/profile/install.js',
			merchantProducts: uiModuleProd + '/merchantProducts/install.js',
			orders: uiModuleProd + '/orders/install.js',
			urac: uiModuleProd + '/urac/install.js'
		}
	},
	"common": {
		"dashboard": {
			myAccount: 'modules/dashboard/myAccount/install.js'
		}
	}
};