"use strict";
var ModuleProdCatalog = uiModuleProd + '/catalogProfiles';

var ctModuleProdTranslation = {
	"catalogProfiles": {
		"ENG": "Catalog Profiles",
		"FRA": "Catalog Profiles"
	}
};

for (var attrname in ctModuleProdTranslation) {
	translation[attrname] = ctModuleProdTranslation[attrname];
}

var catalogProfilesModuleProdNav = [
	{
		'id': 'catalogProfiles',
		'label': translation.catalogProfiles[LANG],
		'url': '#/catalogProfiles',
		'scripts': [
			ModuleProdCatalog + '/config.js', ModuleProdCatalog + '/service.js',
			ModuleProdCatalog + '/controller.js'
		],
		'tplPath': ModuleProdCatalog + '/directives/list.tmpl',
		'checkPermission': {
			'service': 'catalog',
			'method': 'get',
			'route': '/owner/catalog/profiles'
		},
		'icon': 'profile',
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'contentMenu': true,
		'mainMenu': true,
		'tracker': true,
		'order': 2,
		'ancestor': [translation.home[LANG]]
	}
];

navigation = navigation.concat(catalogProfilesModuleProdNav);

errorCodes.catalog = {
	"400": {
		"ENG": "Failed to connect to Database",
		"FRA": "Échec de la connexion à la base de données"
	}
};