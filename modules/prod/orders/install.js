"use strict";
var ModuleProdOrdLocation = uiModuleProd + '/orders';

var orderModuleProdTranslation = {
	"userId": { // Duplicate
		"ENG": "User Id",
		"FRA": "User Id"
	},
	"commerceUsersInfo": {
		"ENG": "Commerce Users Info",
		"FRA": "Info des Utilisateurs"
	},
	"viewUserInfo": {
		"ENG": "View userInfo",
		"FRA": "Information de l'utilisateur"
	},
	"usersInfo": {
		"ENG": "Users Info",
		"FRA": "Information des Utilisateurs"
	},
	"customerId": {
		"ENG": "Customer Id",
		"FRA": "Customer Id"
	},
	"dateLastModified": {
		"ENG": "Date Last Modified",
		"FRA": "Date Last Modified"
	},
	"creditCards": {
		"ENG": "Credit Cards",
		"FRA": "Credit Cards"
	},
	"orders": {
		"ENG": "Orders",
		"FRA": "Les Commandes"
	},
	"viewOrder": {
		"ENG": "View Order",
		"FRA": "Commande"
	},
	"subOrders": {
		"ENG": "Sub Orders",
		"FRA": "Sous Commandes"
	},
	"numbrOfItems": {
		"ENG": "Number of Items",
		"FRA": "Number of Items"
	},
	"totalAmount": {
		"ENG": "Total Amount",
		"FRA": "Total Amount"
	},
	"acquiringMode": {
		"ENG": "Acquiring Mode",
		"FRA": "Acquiring Mode"
	},
	"orderNumber": {
		"ENG": "Order Number",
		"FRA": "Numero du Commande"
	},
	"carrier": {
		"ENG": "Carrier",
		"FRA": "Carrier"
	},
	"trackingNumber": {
		"ENG": "Tracking Number",
		"FRA": "Tracking Number"
	},
	"carrierToolTip": {
		"ENG": "Enter Carrier Name.",
		"FRA": "Enter Carrier Name."
	},
	"EtimeArrival": {
		"ENG": "Estimated time of Arrival",
		"FRA": "Estimated time of Arrival"
	},
	"EtimeArrivalToolTip": {
		"ENG": "Enter Estimated time of arrival.",
		"FRA": "Enter Estimated time of arrival."
	},
	"trackingNumberTooltip": {
		"ENG": "Enter tracking number.",
		"FRA": "Enter number."
	},
	"dateTimeLabel": {
		"ENG": "Date and Time when the order will be ready for pickup",
		"FRA": "Date and Time when the order will be ready for pickup"
	},
	"dateTimePlaceholder": {
		"ENG": "date and time",
		"FRA": "Enter a date and time"
	},
	"dateTimeToolTip": {
		"ENG": "Enter a date and time",
		"FRA": "Enter a date and time"
	},
	"currency": {
		"ENG": "Currency",
		"FRA": "Currency"
	},
	"totalShipping": {
		"ENG": "Total Shipping",
		"FRA": "Total Shipping"
	},
	"totalTax": {
		"ENG": "Total Tax",
		"FRA": "Total Tax"
	},
	"orderInfo": {
		"ENG": "Order info",
		"FRA": "Order info"
	},
	"back2List": {
		"ENG": "Back to List",
		"FRA": "Back to List"
	},
	"statusHistory": {
		"ENG": "Status History",
		"FRA": "Status History"
	},
	"userInfo": {
		"ENG": "User Info",
		"FRA": "User Info"
	},
	"nameFull": {
		"ENG": "Full Name",
		"FRA": "Full Name"
	},
	"shippingAddress": {
		"ENG": "Shipping Address",
		"FRA": "Shipping Address"
	},
	"phoneNumber": {
		"ENG": "Phone Number",
		"FRA": "phone Number"
	},
	"billingInfo": {
		"ENG": "Billing Info",
		"FRA": "Billing Info"
	},
	"billingCreditCard": {
		"ENG": "Billing Credit Card",
		"FRA": "Billing Credit Card"
	},
	"nameOnCard": {
		"ENG": "Name On Card",
		"FRA": "NameOn Card"
	},
	"typeCard": {
		"ENG": "Type",
		"FRA": "Type"
	},
	"expirationCC": {
		"ENG": "Expiration",
		"FRA": "Expiration"
	},
	"last4digits": {
		"ENG": "Last 4 digits",
		"FRA": "last 4 digits"
	},
	"billingAddress": {
		"ENG": "Billing Address",
		"FRA": "Billing Address"
	},
	"serial": {
		"ENG": "Serial",
		"FRA": "Serial"
	},
	"label": {
		"ENG": "Label",
		"FRA": "Label"
	},
	"driver": {
		"ENG": "Driver",
		"FRA": "Driver"
	},
	"information": {
		"ENG": "Information",
		"FRA": "Information"
	},
	"addAddressValidation": {
		"ENG": "Add Address Validation",
		"FRA": "Add Address Validation"
	},
	"addCurrencyConverter": {
		"ENG": "Add Currency Converter",
		"FRA": "Add Currency Converter"
	},
	"addPaymentMethod": {
		"ENG": "Add Payment Method",
		"FRA": "Add Payment Method"
	},
	"addTaxCalculator": {
		"ENG": "Add Tax Calculator",
		"FRA": "Add Tax Calculator"
	},
	"addDeliveryMethod": {
		"ENG": "Add Delivery Method",
		"FRA": "Add Delivery Method"
	},
	"serviceCode": {
		"ENG": "Service Code",
		"FRA": "service Code"
	},
	"startTime": {
		"ENG": "Start Time",
		"FRA": "start Time"
	},
	"intervals": {
		"ENG": "Intervals",
		"FRA": "Intervals"
	},
	"endTime": {
		"ENG": "End Time",
		"FRA": "End Time"
	},
	"key": {
		"ENG": "key",
		"FRA": "clé"
	},
	"rates": {
		"ENG": "Rates",
		"FRA": "Rates"
	},
	"rate": {
		"ENG": "Rate",
		"FRA": "Rate"
	},
	"priority": {
		"ENG": "Priority",
		"FRA": "Priority"
	},
	"addShippingMethod": {
		"ENG": "Add Shipping Method",
		"FRA": "Add Shipping Method"
	},
	"driverName": {
		"ENG": "Driver Name",
		"FRA": "Driver Name"
	},
	"driverNameToolTip": {
		"ENG": "Pick the Driver to use",
		"FRA": "Pick the Driver to use"
	},
	"driverLabel": {
		"ENG": "Driver Label",
		"FRA": "Driver Label"
	},
	"driverLabelToolTip": {
		"ENG": "Enter the display name of the Driver",
		"FRA": "Enter the display name of the Driver"
	},
	"driverLabelPlaceHd": {
		"ENG": "My Driver Name...",
		"FRA": "My Driver Name"
	},
	"rUsureUwant2removeMethod": {
		"ENG": "Are you sure you want to remove this method ?",
		"FRA": "Are you sure you want to remove this method ?"
	},
	"viewItem": {
		"ENG": "View Item",
		"FRA": "View Item"
	},
	"disableActivate": {
		"ENG": "Disable/Activate",
		"FRA": "Disable/Activate"
	},
	"addPickupMethod": {
		"ENG": "Add Pickup Method",
		"FRA": "Add Pickup Method"
	},
	"statusChangedSuccessfully": {
		"ENG": "Status Changed Successfully.",
		"FRA": "Status Changed Successfully."
	},
	"methodDeletedSuccessfully": {
		"ENG": "Method Deleted Successfully.",
		"FRA": "Method Deleted Successfully."
	},
	"methodUpdatedSuccessfully": {
		"ENG": "Method Updated Successfully.",
		"FRA": "Method Updated Successfully."
	},
	"methodAddedSuccessfully": {
		"ENG": "Method Added Successfully.",
		"FRA": "Method Added Successfully."
	},
	"viewingOne": {
		"ENG": "Viewing One ",
		"FRA": "viewing One "
	},
	"merchantsInfo": {
		"ENG": "Merchants Info",
		"FRA": "Merchants Info"
	},
	"rUsureUwant2deleteTokens": {
		"ENG": "Are you sure you want to delete the merchant tokens?",
		"FRA": "Are you sure you want to delete the merchant tokens?"
	},
	"viewRecord": {
		"ENG": "View Record",
		"FRA": "View Record"
	},
	"billing": {
		"ENG": "Billing",
		"FRA": "Billing"
	},
	"addresses": {
		"ENG": "Addresses",
		"FRA": "Addresses"
	},
	"informationSavedSuccess": {
		"ENG": "Information saved successfully.",
		"FRA": "Information saved successfully."
	},
	"labelCreated": {
		"ENG": "Label created!",
		"FRA": "Label created!"
	},
	"fulfil": {
		"ENG": "Fulfil",
		"FRA": "Fulfil"
	},
	"companyName": {
		"ENG": "Company Name",
		"FRA": "Company Name"
	},
	"on": {
		"ENG": "on",
		"FRA": "on"
	},
	"subOrderInfo": {
		"ENG": "Sub Order Info",
		"FRA": "Sub Order Info"
	},
	"storeName": {
		"ENG": "Store Name",
		"FRA": "Store Name"
	},
	"payOnPickup": {
		"ENG": "Pay On Pickup",
		"FRA": "Pay On Pickup"
	},
	"cartItems": {
		"ENG": "Cart Items",
		"FRA": "Cart Items"
	},
	"merchantName": {
		"ENG": "Merchant Name",
		"FRA": "Merchant Name"
	},
	"quantity": {
		"ENG": "Quantity",
		"FRA": "Quantity"
	},
	"attributes": {
		"ENG": "Attributes",
		"FRA": "Attributes"
	},
	"pricePerUnit": {
		"ENG": "Price Per Unit",
		"FRA": "Price Per Unit"
	},
	"productSKU": {
		"ENG": "Product Id / SKU",
		"FRA": "Product Id / SKU"
	},
	"groupId": {
		"ENG": "Group Id",
		"FRA": "Group Id"
	},
	"totalPriceItems": {
		"ENG": "Total Price of Items",
		"FRA": "Total Price of Items"
	},
	"setDelivered": {
		"ENG": "Set as Delivered",
		"FRA": "Set as Delivered"
	},
	"setPicked": {
		"ENG": "Set as Picked",
		"FRA": "Set as Picked"
	},
	"bill": {
		"ENG": "Bill",
		"FRA": "Facture"
	},
	"postageLabel": {
		"ENG": "Label",
		"FRA": "postage Label"
	},
	"trackingCode": {
		"ENG": "Tracking Code",
		"FRA": "Tracking Code"
	},
	"yPLabel": {
		"ENG": "YP Label",
		"FRA": "YP Label"
	},
	"shippingInfo": {
		"ENG": "Shipping Info",
		"FRA": "Shipping Info"
	},
	"pickupInfo": {
		"ENG": "Pickup Info",
		"FRA": "Pickup Info"
	},
	"cost": {
		"ENG": "Cost",
		"FRA": "Prix"
	},
	"estimatedDeliveryDays": {
		"ENG": "Estimated Delivery Days",
		"FRA": "Estimated Delivery Days"
	},
	"serviceShipping": {
		"ENG": "Service",
		"FRA": "Service"
	},
	"deliveryExpectedBy": {
		"ENG": "Delivery Expected By",
		"FRA": "Delivery Expected By"
	},
	"today": {
		"ENG": "Today",
		"FRA": "Today"
	},
	"pickupOptions": {
		"ENG": "Pickup Options",
		"FRA": "Pickup Options"
	},
	"itemsTotalCost": {
		"ENG": "Items Total Cost",
		"FRA": "Items Total Cost"
	},
	"tax": {
		"ENG": "Tax",
		"FRA": "Taxe"
	},
	"authorizationId": {
		"ENG": "Authorization Id",
		"FRA": "Authorization Id"
	},
	"address": {
		"ENG": "Address",
		"FRA": "Address"
	},
	"captured": {
		"ENG": "Captured",
		"FRA": "captured"
	},
	"subOrderRejected": {
		"ENG": "Sub Order rejected",
		"FRA": "Sub Order rejected"
	},
	"merchantSource": {
		"ENG": "Source",
		"FRA": "Source"
	},
	"viewSubOrder": {
		"ENG": "View Sub Order",
		"FRA": "View Sub Order"
	},
	"rUsureProcessOrder": {
		"ENG": "Are you sure you want to process this order?",
		"FRA": "Are you sure you want to process this order?"
	},
	"processOrder": {
		"ENG": "Process",
		"FRA": "Process"
	},
	"rUsureRejectOrder": {
		"ENG": "Are you sure you want to reject this order?",
		"FRA": "Are you sure you want to reject this order?"
	},
	"rejectOrder": {
		"ENG": "Reject Order",
		"FRA": "Reject Order"
	},
	"createLabel": {
		"ENG": "Create Label",
		"FRA": "Create Label"
	},
	"merchantInfo": {
		"ENG": "Merchant Info",
		"FRA": "Merchant Info"
	}
	
};

for (var attrname in orderModuleProdTranslation) {
	translation[attrname] = orderModuleProdTranslation[attrname];
}

var orderModuleProdNav = [
	// {
	// 	'id': 'userInfoList',
	// 	'label': translation.commerceUsersInfo[LANG],
	// 	'checkPermission': {
	// 		'service': 'order',
	// 		'route': '/dashboard/userInfo/list'
	// 	},
	// 	'url': '#/userInfo',
	// 	'tplPath': ModuleProdOrdLocation + '/directives/userInfo/list.tmpl',
	// 	'icon': 'user-tie',
	// 	'contentMenu': true,
	// 	'pillar': {
	// 		'name': 'operate',
	// 		'label': translation.operate[LANG],
	// 		'position': 4
	// 	},
	// 	'mainMenu': true,
	// 	'tracker': true,
	// 	'order': 4,
	// 	'scripts': [ModuleProdOrdLocation + '/config.js', ModuleProdOrdLocation + '/controller-userInfo.js'],
	// 	'ancestor': [translation.home[LANG]]
	// },
	// {
	// 	'id': 'userInfoView',
	// 	'label': translation.viewUserInfo[LANG],
	// 	'url': '#/userInfo/view/:id',
	// 	'tplPath': ModuleProdOrdLocation + '/directives/userInfo/view.tmpl',
	// 	'tracker': true,
	// 	'pillar': {
	// 		'name': 'operate',
	// 		'label': translation.operate[LANG],
	// 		'position': 4
	// 	},
	// 	'scripts': [ModuleProdOrdLocation + '/config.js', ModuleProdOrdLocation + '/controller-userInfo.js'],
	// 	'ancestor': [translation.home[LANG], translation.commerceUsersInfo[LANG]]
	// },
	{
		'id': 'orderList',
		'label': translation.orders[LANG],
		'checkPermission': {
			'service': 'order',
			'method': 'get',
			'route': '/dashboard/allOrders'
		},
		'url': '#/order',
		'tplPath': ModuleProdOrdLocation + '/directives/list.tmpl',
		'icon': 'cart',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 4,
		'scripts': [ModuleProdOrdLocation + '/config.js', ModuleProdOrdLocation + '/controller.js'],
		'ancestor': [translation.home[LANG]]
	},
	{
		'id': 'orderView',
		'label': translation.viewOrder[LANG],
		'url': '#/order/view/:id',
		'tplPath': ModuleProdOrdLocation + '/directives/view.tmpl',
		'tracker': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'scripts': [ModuleProdOrdLocation + '/config.js', ModuleProdOrdLocation + '/controller.js'],
		'ancestor': [translation.home[LANG], translation.orders[LANG]]
	}
	// {
	// 	'id': 'suborderList',
	// 	'label': translation.subOrders[LANG],
	// 	'checkPermission': {
	// 		'service': 'order',
	// 		'route': '/dashboard/merchant/subOrder/list'
	// 	},
	// 	'url': '#/subOrders',
	// 	'tplPath': ModuleProdOrdLocation + '/directives/subOrders/list.tmpl',
	// 	'icon': 'cart',
	// 	'contentMenu': true,
	// 	'pillar': {
	// 		'name': 'operate',
	// 		'label': translation.operate[LANG],
	// 		'position': 4
	// 	},
	// 	'order': 4,
	// 	'mainMenu': true,
	// 	'tracker': true,
	// 	'scripts': [ModuleProdOrdLocation + '/config.js', ModuleProdOrdLocation + '/controller-suborder.js'],
	// 	'ancestor': [translation.home[LANG]]
	// },
	// {
	// 	'id': 'subOrderView',
	// 	'label': translation.viewSubOrder[LANG],
	// 	'url': '#/subOrders/view/:id/:merchant',
	// 	'tplPath': ModuleProdOrdLocation + '/directives/subOrders/view.tmpl',
	// 	'tracker': true,
	// 	'pillar': {
	// 		'name': 'operate',
	// 		'label': translation.operate[LANG],
	// 		'position': 4
	// 	},
	// 	'scripts': [ModuleProdOrdLocation + '/config.js', ModuleProdOrdLocation + '/controller-suborder.js'],
	// 	'ancestor': [translation.home[LANG], translation.subOrders[LANG]]
	// }
];

navigation = navigation.concat(orderModuleProdNav);

errorCodes.order = {
	"200": {
		"ENG": "Missing CDN config.",
		"FRA": "Missing CDN config."
	},
	
	"253": {
		"ENG": "Missing shipping method.",
		"FRA": "Missing shipping method."
	},
	
	"260": {
		"ENG": "Missing suborder info.",
		"FRA": "Missing suborder info."
	},
	
	"400": {
		"ENG": "Failed to connect to Database.",
		"FRA": "Failed to connect to Database."
	},
	
	"501": {
		"ENG": "Invalid order Id.",
		"FRA": "Identifiant de commande invalide."
	},
	"504": {
		"ENG": "Invalid Merchant uuid.",
		"FRA": "Identifiant marchand invalide."
	},
	"505": {
		"ENG": "Missing Merchant in service config.",
		"FRA": "Marchand manquant dans la configuration."
	},
	"506": {
		"ENG": "Merchants not found.",
		"FRA": "Merchants not found."
	},
	
	"701": {
		"ENG": "Failed to Capture Payment.",
		"FRA": "Enregistrement du paiement échoué."
	},
	"702": {
		"ENG": "Failed to create Easypost label.",
		"FRA": "Failed to create Easypost label."
	},
	"703": {
		"ENG": "Failed to create Intelcom delivery request.",
		"FRA": "Failed to create Intelcom delivery request."
	},
	"704": {
		"ENG": "Failed to update stock.",
		"FRA": "Failed to update stock."
	},
	"705": {
		"ENG": "Failed to Refund Payment.",
		"FRA": "Failed to Refund Payment."
	},
	"706": {
		"ENG": "Failed to Capture Payment because the charge has expired",
		"FRA": "Failed to Capture Payment because the charge has expired"
	},
	"707": {
		"ENG": "The Payment has already been Refunded.",
		"FRA": "The Payment has already been Refunded."
	},
	
	"750": {
		"ENG": "Invalid Id provided.",
		"FRA": "Identifiant fourni invalide."
	},
	"751": {
		"ENG": "Driver Already Exists.",
		"FRA": "Driver existe déjà."
	},
	"752": {
		"ENG": "Driver Does Not Exist.",
		"FRA": "Driver n’existe pas."
	},
	"798": {
		"ENG": "Error saving bar code image.",
		"FRA": "Error saving bar code image."
	},
	"799": {
		"ENG": "Error saving bill image.",
		"FRA": "Error saving bill image."
	}
};