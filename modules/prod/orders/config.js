"use strict";
var orderModuleProdConfig = {
	'permissions': {
		'order': {
			'list': ['order', '/dashboard/allOrders', "get"],
			'view': ['order', '/dashboard/order', "get"]
		},
		"subOrder": {
			'list': ['order', '/dashboard/merchant/subOrder/list'],
			'view': ['order', '/dashboard/merchant/subOrder/view']
		}
	},
	grid: {
		'apiEndLimit': 200
	},
	order: {
		grid: {
			recordsPerPageArray: [5, 10, 50, 100],
			'columns': [
				{'label': translation.tenant[LANG], 'field': 'tenantInfo'},
				{'label': translation.orderNumber[LANG], 'field': 'orderInfo_orderNbr'},
				{'label': translation.dateCreated[LANG], 'field': 'ts', 'filter': "prettyLocalDate"},
				{'label': "Business Name", 'field': 'userInfo_businessName'},
				// {'label': translation.status[LANG], 'field': 'orderInfo_status'},
				{'label': translation.numbrOfItems[LANG], 'field': 'cartLength'},
				{'label': translation.totalAmount[LANG], 'field': 'orderInfo_totalAmount'}
			],
			'leftActions': [],
			'topActions': [],
			'defaultSortField': '',
			'defaultLimit': 50
		}
	},
	subOrder: {
		grid: {
			recordsPerPageArray: [5, 10, 50, 100],
			'columns': [
				{'label': translation.orderNumber[LANG], 'field': 'orderInfo_orderNbr'},
				{'label': translation.merchant[LANG], 'field': 'merchantName'},
				{'label': translation.dateCreated[LANG], 'field': 'ts', 'filter': "prettyLocalDate"},
				{'label': translation.userInfo[LANG], 'field': 'userInfo_fullName'},
				{'label': translation.status[LANG], 'field': 'orderInfo_status'},
				{'label': translation.acquiringMode[LANG], 'field': 'orderInfo_acquiringMode'},
				{'label': translation.numbrOfItems[LANG], 'field': 'cartLength'},
				{'label': translation.totalAmount[LANG], 'field': 'orderInfo_totalAmount'}
			],
			'leftActions': [],
			'topActions': [],
			'defaultSortField': '',
			'defaultLimit': 50
		},
		form: {
			shipping: {
				'name': '',
				'label': '',
				'actions': {},
				'entries': [
					{
						'name': 'carrier',
						'label': translation.carrier[LANG],
						'type': 'readonly',
						'placeholder': 'FedEx...',
						'value': '',
						'tooltip': translation.carrierToolTip,
						'required': false
					},
					{
						'name': 'trackingNumber',
						'label': translation.trackingNumber[LANG],
						'type': 'text',
						'placeholder': 'tr_1245lopo...',
						'value': '',
						'tooltip': translation.trackingNumberTooltip[LANG],
						'required': false
					},
					{
						'name': 'eta',
						'label': translation.EtimeArrival[LANG],
						'type': 'text',
						'placeholder': '',
						'value': '',
						'tooltip': translation.EtimeArrivalToolTip[LANG],
						'required': false
					}
				]
			},
			pickup: {
				'name': '',
				'label': '',
				'actions': {},
				'entries': [
					{
						'name': 'time',
						'label': translation.dateTimeLabel[LANG],
						'type': 'text',
						'placeholder': translation.dateTimePlaceholder[LANG],
						'value': '',
						'tooltip': translation.dateTimeToolTip[LANG],
						'required': true
					}
				]
			}
		}
	}
};

var userInfoConfig = {
	'permissions': {
		'list': ['order', '/dashboard/userInfo/list'],
		'view': ['order', '/dashboard/userInfo/view']
	},
	grid: {
		'apiEndLimit': 400
	},
	order: {
		grid: {
			recordsPerPageArray: [20, 50, 100, 200],
			'columns': [
				{'label': translation.userId[LANG], 'field': 'userId'},
				{'label': translation.username[LANG], 'field': 'username'},
				{'label': translation.dateLastModified[LANG], 'field': 'ts', 'filter': "prettyLocalDate"}
			],
			'leftActions': [],
			'topActions': [],
			'defaultSortField': '',
			'defaultLimit': 50
		}
	}
};

soajsApp.components.filter('intelcomLabel', function () {
	return function (value) {
		if (!value) {
			return '';
		}
		if (value === '61') {
			return '60-90 minutes';
		}
		if (value === '62') {
			return '2-3 hours';
		}
		if (value === '64') {
			return 'Same Day';
		}
		return value;
	}
});