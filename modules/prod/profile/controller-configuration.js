"use strict";
var knowledgebaseConfigApp = soajsApp.components;
knowledgebaseConfigApp.controller('knowledgebaseConfigurationModuleProdCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$routeParams', '$cookies', function ($scope, $modal, ngDataApi, injectFiles, $timeout, $routeParams, $cookies) {
	$scope.feedProfileId = $routeParams.id;
	$scope.ModuleProdKbaseLocation = ModuleProdKbaseLocation;
	
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, nakbModuleProdConfig.permissions);
	
	$scope.grid = {rows: []};
	
	$scope.formConfig = {
		"entries": [
			{
				"name": "name",
				"label": translation.name[LANG],
				"type": "readonly"
			},
			{
				"name": "min-qty",
				"label": translation.minimumQuantityAllowed[LANG],
				"type": "number",
				'value': 0,
				"required": true
			},
			{
				"name": "languages",
				"label": translation.languages[LANG],
				"type": "group",
				"class": "lang",
				"entries": []
			}
		
		],
		"oneLang": [
			{
				'name': "id%count%",
				'label': translation.id[LANG],
				'type': 'text',
				'placeholder': 'ENG',
				'maxlength': 3,
				'required': true
				
			},
			{
				'name': "label%count%",
				'label': translation.label[LANG],
				'type': 'text',
				'placeholder': 'English',
				'required': true
			},
			{
				"name": 'selected%count%',
				"type": "checkbox",
				"value": [
					{
						'v': "selected",
						'l': translation.defaultLanguage[LANG]
					}
				],
				"onAction": function (id, data, form) {
					form.entries.forEach(function (oneEntry) {
						if (oneEntry.name === 'languages') {
							for (var i = 0; i < oneEntry.entries.length; i++) {
								if (oneEntry.entries[i].name !== id) {
									if (oneEntry.entries[i].value && Array.isArray(oneEntry.entries[i].value)) {
										oneEntry.entries[i].value[0].selected = false;
										delete form.formData[oneEntry.entries[i].name];
									}
								}
								else {
									oneEntry.entries[i].value[0].selected = true;
								}
							}
						}
					});
					
				}
				
			},
			{
				"type": "html",
				"name": "removeLang%count%",
				"value": "<span class='red'><span class='icon icon-cross' title=" + translation.removeLanguage[LANG] + "></span></span>",
				"onAction": function (id, data, form) {
					var number = id.replace("removeLang", "");
					
					delete form.formData['id' + number];
					delete form.formData['label' + number];
					delete form.formData['selected' + number];
					
					form.entries.forEach(function (oneEntry) {
						
						if (oneEntry.name === 'languages') {
							
							for (var i = 0; i < oneEntry.entries.length; i++) {
								if (oneEntry.entries[i].name === 'id' + number) {
									oneEntry.entries.splice(i, 1);
								}
								if (oneEntry.entries[i].name === 'label' + number) {
									oneEntry.entries.splice(i, 1);
								}
								if (oneEntry.entries[i].name === 'selected' + number) {
									oneEntry.entries.splice(i, 1);
								}
								if (oneEntry.entries[i].name === 'removeLang' + number) {
									oneEntry.entries.splice(i, 1);
								}
							}
						}
					});
				}
				
			}
		]
	};
	
	//function that lists the Configurations in a grid
	$scope.listEntries = function () {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/configuration",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				var formFields = angular.copy($scope.formConfig.entries);
				var ct = 0;
				for (var i = 0; i < response.languages.length; i++) {
					formFields[2].entries.push(
						{
							'name': "id" + ct,
							'label': translation.id[LANG],
							'type': 'text',
							'placeholder': 'ENG',
							'maxlength': 3,
							'value': response.languages[i].id,
							'required': true
							
						},
						{
							'name': "label" + ct,
							'label': translation.label[LANG],
							'type': 'text',
							'placeholder': 'English',
							'value': response.languages[i].label,
							'required': true
						},
						{
							"name": 'selected' + ct,
							"type": "checkbox",
							"value": [
								{
									'v': "selected",
									'l': translation.defaultLanguage[LANG],
									"selected": (response.languages[i].selected)
								}
							],
							"onAction": function (id, data, form) {
								form.entries.forEach(function (oneEntry) {
									if (oneEntry.name === 'languages') {
										for (var i = 0; i < oneEntry.entries.length; i++) {
											if (oneEntry.entries[i].name !== id) {
												if (oneEntry.entries[i].value && Array.isArray(oneEntry.entries[i].value)) {
													oneEntry.entries[i].value[0].selected = false;
													delete form.formData[oneEntry.entries[i].name];
												}
											}
											else {
												oneEntry.entries[i].value[0].selected = true;
											}
										}
									}
								});
								
							}
							
						},
						{
							"type": "html",
							"name": "removeLang" + ct,
							"value": "<span class='red'><span class='icon icon-cross' title=" + translation.removeLanguage[LANG] + "></span></span>",
							"onAction": function (id, data, form) {
								var number = id.replace("removeLang", "");
								
								delete form.formData['id' + number];
								delete form.formData['label' + number];
								delete form.formData['selected' + number];
								
								form.entries.forEach(function (oneEntry) {
									
									if (oneEntry.name === 'languages') {
										
										for (var i = 0; i < oneEntry.entries.length; i++) {
											if (oneEntry.entries[i].name === 'id' + number) {
												oneEntry.entries.splice(i, 1);
											}
											if (oneEntry.entries[i].name === 'label' + number) {
												oneEntry.entries.splice(i, 1);
											}
											if (oneEntry.entries[i].name === 'selected' + number) {
												oneEntry.entries.splice(i, 1);
											}
											if (oneEntry.entries[i].name === 'removeLang' + number) {
												oneEntry.entries.splice(i, 1);
											}
										}
									}
								});
							}
							
						}
					);
					ct++;
				}
				
				var config = {
					'timeout': $timeout,
					'name': 'generalConfiguration',
					'label': '',
					'entries': formFields,
					'data': response,
					'actions': [
						{
							'type': 'button',
							'label': translation.addLanguage[LANG],
							'btn': 'success',
							'action': function () {
								var clone = angular.copy($scope.formConfig.oneLang);
								clone.forEach(function (oneEntry) {
									oneEntry.name = oneEntry.name.replace("%count%", ct);
									if (oneEntry.type === 'html') {
										oneEntry.value = oneEntry.value.replace("%count%", ct);
									}
									formFields[2].entries = formFields[2].entries.concat(oneEntry);
								});
								ct++;
							}
						},
						{
							'type': 'submit',
							'label': translation.submit[LANG],
							'btn': 'primary',
							'action': function (formData) {
								var postData = {
									"minQtt": formData["min-qty"],
									"languages": []
								};
								var selected = false;
								for (var x = 0; x < ct; x++) {
									if (formData["id" + x] && !formData["label" + x] || !formData["id" + x] && formData["label" + x]) {
										return false;
									}
									else {
										if (formData["id" + x] && formData["label" + x]) {
											var lang = {
												"id": formData["id" + x].toUpperCase(),
												"label": formData["label" + x]
											};
											if (formData["selected" + x] && Array.isArray(formData["selected" + x]) && formData["selected" + x].length > 0) {
												selected = true;
												lang.selected = true;
											}
											postData.languages.push(lang);
										}
									}
								}
								if (!selected) {
									return $scope.$parent.displayAlert('danger', translation.pSyDlanguage[LANG]);
								}
								
								var opts = {
									"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/configuration",
									"method": "put",
									"params": {
										"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
									},
									"data": postData
								};
								
								
								getSendDataFromServer($scope, ngDataApi, opts, function (error) {
									if (error) {
										$scope.$parent.displayAlert('danger', error.message);
									}
									else {
										$scope.$parent.displayAlert('success', translation.gCuSuccessfully[LANG]);
									}
								});
								
							}
						}
					]
				};
				buildForm($scope, null, config);
			}
		});
	};
	
	$scope.listEntries();
	
	injectFiles.injectCss(ModuleProdKbaseLocation + "/knowledgebase.css");
}]);