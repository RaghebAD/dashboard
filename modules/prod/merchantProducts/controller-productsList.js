"use strict";
var productsApp = soajsApp.components;

productsApp.controller('productsModuleProdCtrl', ['$scope', '$modal', '$route', '$routeParams', 'ngDataApi', 'injectFiles', '$timeout', '$cookies', 'productsSrv',
	function ($scope, $modal, $route, $routeParams, ngDataApi, injectFiles, $timeout, $cookies, productsSrv) {
		$scope.ModuleProdMpLocation = ModuleProdMpLocation;
		$scope.product = angular.extend($scope);
		$scope.product.$parent.isUserLoggedIn();
		$scope.product.merchantId = $routeParams.id;
		$scope.product.code = $cookies.get('knowledgebase_merchant') || '';
		
		$scope.startLimit = 0;
		$scope.totalCount = 1000;
		$scope.endLimit = knowledgeBaseModuleProdConfig.apiEndLimit;
		$scope.increment = knowledgeBaseModuleProdConfig.apiEndLimit;
		$scope.showNext = true;
		
		if (!$scope.$parent.currentSelectedEnvironment) {
			$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
		}
		$scope.product.access = {};
		constructModulePermissions($scope.product, $scope.product.access, knowledgeBaseModuleProdConfig.permissions);
		
		$scope.product.updateVariationImages = function (data) {
			var __env = $scope.$parent.currentSelectedEnvironment;
			var outerScope = $scope;
			var opts = {
				"method": "get",
				"params": {
					'merchantId': $routeParams.id,
					'groupId': data.info.groupId,
					'language': LANG,
					'__env': __env
				}
			};
			outerScope.title = '';
			data.productData.title.forEach(function (oneTitle) {
				if (oneTitle.language === LANG) {
					outerScope.title = oneTitle.value
				}
			});
			if ($scope.product.access.owner.products.getProductVariations) {
				opts.routeName = "/knowledgebase/owner/products/variations";
				opts.params.code = $scope.product.code;
			}
			else {
				opts.routeName = "/knowledgebase/tenant/products/variations";
			}
			productsSrv.getEntriesFromApi(outerScope, opts, function (error, response) {
				if (error) {
					outerScope.$parent.displayAlert('danger', error.message);
				}
				else {
					outerScope.attributes = response.attributes;
					outerScope.products = response.products;
					outerScope.images = response.images;
					outerScope.profile = response.profile;
					if (response.variationThumb) {
						outerScope.variationThumb = response.variationThumb;
					}
					else {
						outerScope.variationThumb = null;
					}
					
					$modal.open({
						templateUrl: "updateVariationImages.tmpl",
						size: 'lg',
						backdrop: true,
						keyboard: true,
						controller: function ($scope, $modalInstance) {
							$scope.title = translation.updateImages[LANG];
							$scope.data = data;
							fixBackDrop();
							$scope.ModuleProdMpLocation = ModuleProdMpLocation;
							$scope.outerScope = outerScope;
							$scope.message = {};
							$scope.formData = {};
							if (outerScope.variationThumb) {
								$scope.formData.thumbnails = {};
								for (var att in outerScope.variationThumb) {
									if (outerScope.variationThumb[att].showThumbnail === false) {
										$scope.formData.thumbnails[att] = {hideThumb: true};
									}
								}
							}
							$scope.formData.products = {};
							$scope.formData.images = [];
							$scope.formData.addedImg = [];
							$scope.formData.products[data._id] = true;
							if (outerScope.images[data._id]) {
								var imgs = angular.copy(outerScope.images[data._id]);
								imgs.forEach(function (oneImg) {
									oneImg.id = data._id;
									$scope.formData.images.push(oneImg);
								});
							}
							
							$scope.onSubmit = function () {
								var ids = [],
									serials = [],
									images = [];
								
								Object.keys($scope.formData.products).forEach(function (oneId) {
									if ($scope.formData.products[oneId]) {
										ids.push(oneId);
										if (response.serials[oneId]) {
											serials.push(response.serials[oneId]);
										}
									}
								});
								$scope.formData.images.forEach(function (oneImg) {
									if (oneImg.id) {
										delete oneImg.id;
									}
								});
								images = $scope.formData.images.concat($scope.formData.addedImg);
								overlayLoading.show();
								var formFiles = [];
								for (var i in $scope.formData.imageFiles) {
									if (typeof $scope.formData.imageFiles[i] === 'object') {
										formFiles.push($scope.formData.imageFiles[i]);
									}
								}
								if (formFiles.length !== 0) {
									productsSrv.uploadFile(outerScope, formFiles, "/knowledgebase/products/addImage", function (error, response) {
										if (error) {
											var errorString;
											if (Array.isArray(error)) {
												errorString = error.join(", ");
											}
											else {
												errorString = error.message;
											}
											overlayLoading.hide();
											$scope.message.danger = true;
											$scope.message.text = errorString;
										}
										else {
											var regex = /^(https?):/;
											var img = [];
											for (var i = 0; i < response.length; i++) {
												img.push({
													"url": response[i].original.replace(regex, ''),
													"resource": response[i].resource,
													"type": response[i].type
												});
											}
											save(ids, serials, images, img);
										}
									});
								}
								else {
									save(ids, serials, images);
								}
							};
							
							function save(ids, serials, images, fileImage) {
								var imagesResponse = [];
								var imagesData = {
									'productData': {
										'media': {
											'img': images
										}
									}
								};
								productsSrv.imageUpload(outerScope, imagesData, imagesResponse, function (error) {
									if (error) {
										overlayLoading.hide();
										$scope.message.danger = true;
										$scope.message.text = error.message;
									}
									if (fileImage) {
										imagesResponse = imagesResponse.concat(fileImage);
									}
									var postData = {
										ids: ids,
										serials: serials,
										images: imagesResponse
									};
									if ($scope.formData.thumbnails) {
										postData.thumbnails = $scope.formData.thumbnails;
									}
									getSendDataFromServer(outerScope, ngDataApi, {
										"method": "send",
										"routeName": "/knowledgebase/products/applyVariations",
										"params": {
											"__env": __env,
											'groupId': data.info.groupId
										},
										"data": postData
									}, function (error) {
										overlayLoading.hide();
										if (error) {
											$scope.message.danger = true;
											$scope.message.text = error.message;
										}
										else {
											outerScope.$parent.displayAlert('success', translation.imagesUpdatedSuccessfully[LANG]);
											$modalInstance.close();
											outerScope.product.listProducts();
										}
									});
								});
							}
							
							$scope.closeModal = function () {
								$modalInstance.close();
							};
							
							$scope.renderProducts = function () {
								if ($scope.formData.attributes) {
									var attributes = Object.keys($scope.formData.attributes);
									attributes.forEach(function (oneAtt) {
										var values = Object.keys($scope.formData.attributes[oneAtt]);
										values.forEach(function (oneValue) {
											if (!$scope.formData.attributes[oneAtt][oneValue]) {
												delete $scope.formData.attributes[oneAtt][oneValue];
												if (jQuery.isEmptyObject($scope.formData.attributes[oneAtt])) {
													delete $scope.formData.attributes[oneAtt]
												}
											}
										});
										$scope.render();
									});
								}
							};
							
							$scope.render = function () {
								if ($scope.formData.attributes && !jQuery.isEmptyObject($scope.formData.attributes)) {
									var attributes = Object.keys($scope.formData.attributes);
									if (attributes.length > 0) {
										outerScope.products.forEach(function (oneProd) {
											var selected = false;
											for (var i = 0; i < attributes.length; i++) {
												var att = attributes[i];
												if (Object.hasOwnProperty.call(oneProd.attributes, att)
													&& Object.hasOwnProperty.call($scope.formData.attributes[att], oneProd.attributes[att])
													&& ($scope.formData.attributes[att][oneProd.attributes[att]] === true)) {
													selected = true;
												}
												else {
													selected = false;
													break;
												}
											}
											$scope.formData.products[oneProd.value] = selected;
											$scope.renderImages();
										});
									}
								}
								else {
									outerScope.products.forEach(function (oneProd) {
										$scope.formData.products[oneProd.value] = false;
										$scope.renderImages()
									});
								}
							};
							
							$scope.renderImages = function () {
								Object.keys($scope.formData.products).forEach(function (id) {
									if (!$scope.formData.products[id]) {
										for (var i = 0; i < $scope.formData.images.length; i++) {
											if ($scope.formData.images[i].id === id) {
												$scope.formData.images.splice(i, 1);
												i--;
											}
										}
										for (var y = 0; y < $scope.formData.addedImg.length; y++) {
											if ($scope.formData.addedImg[y].id === id) {
												$scope.formData.addedImg.splice(y, 1);
												y--;
											}
										}
									}
								});
								Object.keys($scope.formData.products).forEach(function (id) {
									if ($scope.formData.products[id]) {
										var imgs = angular.copy(outerScope.images[id]);
										for (var x = 0; x < imgs.length; x++) {
											var match = false;
											imgs[x].id = id;
											for (var y = 0; y < $scope.formData.images.length; y++) {
												if (imgs[x].url === $scope.formData.images[y].url) {
													match = true;
													break;
												}
											}
											if (!match) {
												for (var z = 0; z < $scope.formData.addedImg.length; z++) {
													if (imgs[x].url === $scope.formData.addedImg[z].url) {
														match = true;
														break;
													}
												}
											}
											if (!match) {
												$scope.formData.images.push(imgs[x]);
											}
										}
									}
								});
							};
						}
					})
				}
			});
			
		};
		
		$scope.product.listProducts = function (firsCall) {
			var opts;
			
			if ($scope.product.access.owner.products.list) {
				opts = {
					"routeName": "/knowledgebase/owner/products",
					"method": "get",
					"params": {
						"merchantId": $scope.product.merchantId,
						"code": $scope.product.code,
						"start": $scope.startLimit,
						"limit": $scope.endLimit,
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			else {
				opts = {
					"routeName": "/knowledgebase/tenant/products",
					"method": "get",
					"params": {
						"merchantId": $scope.product.merchantId,
						"start": $scope.startLimit,
						"limit": $scope.endLimit,
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			productsSrv.getEntriesFromApi($scope.product, opts, function (error, response) {
				if (error) {
					$scope.product.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.totalCount = response.count;
					if (firsCall) {
						if ($scope.totalCount <= $scope.endLimit) {
							$scope.showNext = false;
						}
						else {
							$scope.showNext = true;
						}
					}
					else {
						var nextLimit = $scope.startLimit + $scope.increment;
						if ($scope.totalCount <= nextLimit) {
							$scope.showNext = false;
						}
						
					}
					productsSrv.printProducts($scope.product, response.records);
					overlayLoading.hide();
				}
			});
		};
		
		$scope.product.viewProduct = function (oneDataRecord) {
			var config;
			if ($scope.access.owner.products.get) {
				config = {
					"method": "get",
					"routeName": "/knowledgebase/owner/products/" + oneDataRecord._id,
					"params": {
						'merchantId': $scope.merchantId,
						'code': $scope.code,
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			else {
				config = {
					"method": "get",
					"routeName": "/knowledgebase/tenant/products/" + oneDataRecord._id,
					"params": {
						'merchantId': $scope.merchantId,
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			getSendDataFromServer($scope, ngDataApi, config, function (error, response) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					var profileRecord = response.profile;
					var defaultLanguage = '';
					for (var i = 0; i < profileRecord.languages.length; i++) {
						if (profileRecord.languages[i].selected) {
							defaultLanguage = profileRecord.languages[i].id;
						}
					}
					var lang = [];
					if (response.product.productData.title) {
						
					}
					if (response.product.productData.desc_short) {
						
					}
					if (response.product.productData.desc_full) {
						for (var z = 0; z < response.product.productData.desc_full.length; z++) {
							if (lang.indexOf(response.product.productData.desc_full[z].language) === -1) {
								lang.push(response.product.productData.desc_full[z].language);
							}
						}
					}
					
					// preview product renewal rule in pricing rules : show rule index - label
					if(response.product.merchantMeta && response.product.merchantMeta.pricing.rules){
						var temporaryRulesIds = {};
						response.product.merchantMeta.pricing.rules.forEach(function(rule, index){
							temporaryRulesIds[rule.id] = "Rule "+(index+1)+" - "+(rule.label?rule.label.en:"");
						});
						response.product.merchantMeta.pricing.rules.forEach(function(rule, index){
							if(rule.renewalRule){
								rule.renewalRuleDisplay = temporaryRulesIds[rule.renewalRule];
							}
							
							// rule type : capital + backward compatible
							if(rule.ruleType === 'adjustment'){
								rule.ruleTypeDisplay = 'Adjustment';
							}else{ // regular or undefined
								rule.ruleTypeDisplay = 'Regular';
							}
						});
					}
					
					$modal.open({
						templateUrl: ModuleProdMpLocation + "/directives/modals/previewProduct.tmpl",
						size: 'lg',
						backdrop: true,
						keyboard: true,
						controller: function ($scope, $modalInstance) {
							$scope.lang = lang;
							$scope.taxonomies = profileRecord.taxonomies;
							$scope.selectedLang = defaultLanguage;
							$scope.languages = profileRecord.languages;
							$scope.title = translation.viewingOneProduct[LANG];
							$scope.data = response.product;
							fixBackDrop();
							setTimeout(function () {
								highlightMyCode()
							}, 500);
							$scope.ok = function () {
								$modalInstance.dismiss('ok');
							};
						}
					});
				}
			});
		};
		
		$scope.product.deleteProduct = function (data) {
			var config = {
				"method": "del",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			if ($scope.product.access.owner.merchants.delete) {
				config.routeName = "/knowledgebase/owner/products/" + data._id;
				config.params.code = $scope.product.code;
			}
			else {
				config.routeName = "/knowledgebase/tenant/products/" + data._id;
			}
			
			getSendDataFromServer($scope.product, ngDataApi, config, function (error) {
				if (error) {
					$scope.product.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.product.$parent.displayAlert('success', translation.productDeletedSuccessfully[LANG]);
					$scope.product.listProducts();
				}
			});
			
		};
		
		$scope.product.deleteProducts = function () {
			var config = {
				"method": "del",
				"routeParam": true,
				'routeName': "/knowledgebase/tenant/products/%id%",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				'msg': {
					'error': translation.oneOrMoreOfTheSelectedProductWasNotDeleted[LANG],
					'success': translation.selectedProductHaveBeenDeleted[LANG]
				}
			};
			if ($scope.product.access.owner.products.delete) {
				config.routeName = "/knowledgebase/owner/products/%id%";
				config.params.code = $scope.product.code;
			}
			else {
			}
			
			multiRecordUpdate(ngDataApi, $scope.product, config, function () {
				$scope.product.listProducts();
			});
		};
		
		$scope.product.changeStatus = function (data) {
			if (data.status === 'draft') {
				return $scope.product.$parent.displayAlert('warning', translation.youCantChangeStatusOfDraftProducts[LANG]);
			}
			var status = (data.status === 'active') ? 'inactive' : 'active';
			var opts;
			if ($scope.product.access.owner.merchants.changeStatus) {
				opts = {
					"method": "put",
					"routeName": "/knowledgebase/owner/products/" + data._id + "/status",
					"params": {
						'status': status,
						'code': $scope.product.code,
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			else {
				opts = {
					"method": "put",
					"routeName": "/knowledgebase/tenant/products/" + data._id + "/status",
					"params": {
						'status': status,
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			
			getSendDataFromServer($scope.product, ngDataApi, opts, function (error) {
				if (error) {
					$scope.product.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.product.$parent.displayAlert('success', translation.productStatusChangedSuccessfully[LANG]);
					$scope.product.listProducts();
				}
			});
		};
		
		$scope.product.activate = function () {
			var config;
			config = {
				"method": "put",
				"routeParam": true,
				'routeName': "/knowledgebase/owner/products/%id%/status",
				"params": {
					'status': 'active',
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				'msg': {
					'error': translation.oneOrMoreOfTheSelectedProductStatusWasNotChanged[LANG],
					'success': translation.selectedProductStatusHaveBeenUpdated[LANG]
				}
			};
			if ($scope.product.access.owner.merchants.changeStatus) {
				config.params.code = $scope.product.code;
				config.routeName = "/knowledgebase/owner/products/%id%/status";
			}
			else {
				config.routeName = "/knowledgebase/tenant/products/%id%/status";
			}
			for (var i = $scope.product.grid.rows.length - 1; i >= 0; i--) {
				if ($scope.product.grid.rows[i].selected && $scope.product.grid.rows[i].status === 'draft') {
					$scope.product.grid.rows[i].selected = false;
				}
			}
			overlayLoading.show();
			multiRecordUpdate(ngDataApi, $scope.product, config, function () {
				overlayLoading.hide();
				$scope.product.listProducts();
			});
		};
		
		$scope.product.disable = function () {
			var config;
			if ($scope.product.access.owner.merchants.changeStatus) {
				config = {
					"method": "put",
					"routeParam": true,
					'routeName': "/knowledgebase/owner/products/%id%/status",
					"params": {
						'code': $scope.product.code,
						'status': 'inactive',
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					},
					'msg': {
						'error': translation.oneOrMoreOfTheSelectedProductStatusWasNotChanged[LANG],
						'success': translation.selectedProductStatusHaveBeenUpdated[LANG]
					}
				};
			}
			else {
				config = {
					"method": "put",
					'routeName': "/knowledgebase/tenant/products/%id%/status",
					"routeParam": true,
					"params": {
						'status': 'inactive',
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					},
					'msg': {
						'error': translation.oneOrMoreOfTheSelectedProductStatusWasNotChanged[LANG],
						'success': translation.selectedProductStatusHaveBeenUpdated[LANG]
					}
				};
			}
			
			for (var i = $scope.product.grid.rows.length - 1; i >= 0; i--) {
				if ($scope.product.grid.rows[i].selected && $scope.product.grid.rows[i].status === 'draft') {
					$scope.product.grid.rows[i].selected = false;
				}
			}
			overlayLoading.show();
			multiRecordUpdate(ngDataApi, $scope.product, config, function () {
				overlayLoading.hide();
				$scope.product.listProducts();
			});
		};
		
		$scope.product.updateStock = function (data) {
			if (data.status === 'draft') {
				return $scope.$parent.displayAlert('warning', translation.youCantUpdateStockForDraftProducts[LANG]);
			}
			var merchantPOS = angular.copy(data.merchantMeta.pos);
			
			var opts;
			if ($scope.access.owner.merchants.get) {
				opts = {
					"method": "get",
					"routeName": "/knowledgebase/owner/merchants/" + $scope.product.merchantId,
					"params": {
						"code": $scope.product.code,
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			else {
				opts = {
					"method": "get",
					"routeName": "/knowledgebase/tenant/merchants/" + $scope.product.merchantId,
					"params": {
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			getSendDataFromServer($scope, ngDataApi, opts, function (error, merchantRecordData) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					openStockForm(merchantRecordData);
				}
			});
			
			function openStockForm(merchantRecordData) {
				merchantPOS.forEach(function (onePOS) {
					for (var i = 0; i < merchantRecordData.pos.length; i++) {
						if (onePOS.id === merchantRecordData.pos[i].id) {
							onePOS.name = merchantRecordData.pos[i].name;
						}
					}
				});
				var currentScope = $scope;
				$modal.open({
					templateUrl: "stock.html",
					size: 'lg',
					backdrop: true,
					keyboard: true,
					controller: function ($scope, $modalInstance) {
						$scope.title = translation.updateProductStock[LANG];
						$scope.data = merchantPOS;
						fixBackDrop();
						$scope.applyToAll = function (t, v) {
							for (var i = 0; i < $scope.data.length; i++) {
								$scope.data[i][t] = v;
							}
						};
						
						$scope.applySaleToAll = function (t, v) {
							for (var i = 0; i < $scope.data.length; i++) {
								if (!$scope.data[i].sale) {
									$scope.data[i].sale = {};
								}
								$scope.data[i].sale[t] = v;
							}
						};
						
						$scope.updateStock = function () {
							$scope.data.forEach(function (pos) {
								if (pos.sale) {
									if (!pos.sale.percentageOff) {
										delete pos.sale;
									}
								}
							});
							var opts;
							if (currentScope.access.owner.products.updateStock) {
								opts = {
									"method": "put",
									"routeName": "/knowledgebase/owner/products/updateStock",
									"params": {
										'id': data._id,
										"code": currentScope.product.code,
										"__env": currentScope.$parent.currentSelectedEnvironment.toUpperCase()
									},
									"data": {
										"pos": $scope.data
									}
								};
							}
							else {
								opts = {
									"method": "put",
									"routeName": "/knowledgebase/tenant/products/updateStock",
									"params": {
										'id': data._id,
										"__env": currentScope.$parent.currentSelectedEnvironment.toUpperCase()
									},
									"data": {
										"pos": $scope.data
									}
								};
							}
							getSendDataFromServer(currentScope, ngDataApi, opts, function (error) {
								if (error) {
									$scope.errMsg = error.message;
								}
								else {
									currentScope.$parent.displayAlert('success', translation.productStockUpdatedSuccessfully[LANG]);
									currentScope.product.listProducts();
									$modalInstance.dismiss("ok");
								}
							});
						};
						
						$scope.cancel = function () {
							$modalInstance.close();
						};
						
						$scope.beforeRenderStartDate = function ($view, $dates, $leftDate, $upDate, $rightDate, $index) {
							if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.endDate) {
								var activeDate = moment($scope.data[$index].sale.endDate);
								for (var i = 0; i < $dates.length; i++) {
									if ($dates[i].localDateValue() >= activeDate.valueOf()) {
										$dates[i].selectable = false;
									}
								}
							}
							else if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.startDate) {
								$scope.$broadcast('RenderEndDate');
							}
							var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
							for (var i = 0; i < $dates.length; i++) {
								if ($dates[i].localDateValue() < minDate.valueOf()) {
									$dates[i].selectable = false;
								}
							}
						};
						
						$scope.beforeRenderEndDate = function ($view, $dates, $leftDate, $upDate, $rightDate, $index) {
							if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.startDate) {
								var activeDate = moment($scope.data[$index].sale.startDate).subtract(1, $view).add(1, 'minute');
								for (var i = 0; i < $dates.length; i++) {
									if ($dates[i].localDateValue() <= activeDate.valueOf()) {
										$dates[i].selectable = false;
									}
								}
							}
							else if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.endDate) {
								$scope.$broadcast('RenderStartDate');
							}
							var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
							for (var i = 0; i < $dates.length; i++) {
								if ($dates[i].localDateValue() <= minDate.valueOf()) {
									$dates[i].selectable = false;
								}
							}
						};
						
					}
				});
			}
		};
		
		$scope.product.updateTreats = function (data) {
			if (data.info.status === 'draft') {
				return $scope.$parent.displayAlert('warning', translation.youCantUpdateTreatsForDraftProducts[LANG]);
			}
			var treatForm = angular.copy(knowledgeBaseModuleProdConfig.updateTreats.form.entries);
			var config;
			if ($scope.access.owner.products.get) {
				config = {
					"method": "get",
					"routeName": "/knowledgebase/owner/products/" + data._id,
					"params": {
						'merchantId': $scope.merchantId,
						'code': $scope.code,
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			else {
				config = {
					"method": "get",
					"routeName": "/knowledgebase/tenant/products/" + data._id,
					"params": {
						'merchantId': $scope.merchantId,
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			getSendDataFromServer($scope, ngDataApi, config, function (error, response) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					var profileRecord = response.profile;
					var defaultLanguage = '';
					for (var i = 0; i < profileRecord.languages.length; i++) {
						if (profileRecord.languages[i].selected) {
							defaultLanguage = profileRecord.languages[i].id;
						}
					}
					
					if (profileRecord.treats) {
						var out = [];
						for (var i = 0; i < profileRecord.treats.length; i++) {
							var treatId = profileRecord.treats[i];
							var treatLabel = profileRecord.taxonomies[treatId].label[defaultLanguage].value;
							out.push({
								'v': treatId,
								'l': treatLabel
							});
						}
						if (response.product.productData.treats) {
							for (var x = 0; x < response.product.productData.treats.length; x++) {
								for (var y = 0; y < out.length; y++) {
									if (out[y].v === response.product.productData.treats[x].value) {
										out[y].selected = true;
									}
								}
							}
						}
						treatForm[0].value = out;
					}
				}
				var options = {
					timeout: $timeout,
					form: {"entries": treatForm},
					'name': 'updateTreats',
					'label': translation.updateTreats[LANG],
					'data': data.productData.treats,
					msgs: {
						'header': translation.updateTreatsMsg[LANG]
					},
					'actions': [
						{
							'type': 'submit',
							'label': translation.applyToAllVariations[LANG],
							'btn': 'primary',
							'action': function (formData) {
								var postData = {
									"serial": response.product.serial,
									"treats": []
								};
								if (formData.treats.length > 0) {
									for (var x = 0; x < formData.treats.length; x++) {
										postData.treats.push({"value": formData.treats[x]})
									}
								}
								var opts;
								if ($scope.access.owner.products.updateTreats) {
									opts = {
										"method": "send",
										"routeName": "/knowledgebase/owner/products/updateTreats",
										"params": {
											'groupId': data.info.groupId,
											"code": $scope.product.code,
											"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
										},
										"data": postData
									};
								}
								else {
									opts = {
										"method": "send",
										"routeName": "/knowledgebase/tenant/products/updateTreats",
										"params": {
											'groupId': data.info.groupId,
											"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
										},
										"data": postData
									};
								}
								getSendDataFromServer($scope, ngDataApi, opts, function (error) {
									if (error) {
										$scope.form.displayAlert('danger', error.message);
									}
									else {
										$scope.$parent.displayAlert('success', translation.productVariationTreatsUpdatedSuccessfully[LANG]);
										$scope.modalInstance.close();
										$scope.product.listProducts();
									}
								});
								
							}
						},
						{
							'type': 'reset',
							'label': translation.cancel[LANG],
							'btn': 'danger',
							'action': function () {
								$scope.modalInstance.dismiss('cancel');
								$scope.form.formData = {};
							}
						}
					]
				};
				buildFormWithModal($scope, $modal, options);
			});
		};
		
		$scope.product.editProduct = function (data) {
			var path = "#/merchantTenants/merchants/browse/products/" + $scope.product.merchantId + "/edit/" + data._id;
			$cookies.put("soajs_current_route", path.replace("#", ""));
			window.open(path, '_self');
		};
		
		$scope.product.addProduct = function () {
			var path = "#/merchantTenants/merchants/browse/products/" + $scope.product.merchantId + "";
			$cookies.put("soajs_current_route", path.replace("#", ""));
			window.open(path, '_self');
		};
		
		$scope.product.getPrev = function () {
			$scope.startLimit = $scope.startLimit - $scope.increment;
			if (0 <= $scope.startLimit) {
				$scope.product.listProducts(false);
				$scope.showNext = true;
			}
			else {
				$scope.startLimit = 0;
			}
		};
		
		$scope.product.getNext = function () {
			var startLimit = $scope.startLimit + $scope.increment;
			if (startLimit < $scope.totalCount) {
				$scope.startLimit = startLimit;
				$scope.product.listProducts(false);
			}
			else {
				$scope.showNext = false;
			}
		};
		
		$scope.product.migrateProducts = function () {
			productsSrv.migrateProducts($scope);
		};
		
		overlayLoading.show();
		
		$timeout(function () {
			if ($scope.product.access.tenant.products.list || $scope.product.access.owner.products.list) {
				$scope.product.listProducts(true);
			}
			else {
				overlayLoading.hide();
			}
		}, 20);
		
		injectFiles.injectCss(ModuleProdMpLocation + "/merchants.css");
	}]);
