"use strict";
var merchantsApp = soajsApp.components;

merchantsApp.controller('merchantConfigModuleProdCtrl', ['$scope', '$modal', '$routeParams', 'ngDataApi', 'injectFiles', '$timeout', 'merchantsModuleProdSrv', '$cookies', '$compile',
	function ($scope, $modal, $routeParams, ngDataApi, injectFiles, $timeout, merchantsModuleProdSrv, $cookies, $compile) {
		$scope.$parent.isUserLoggedIn();
		$scope.ModuleProdMpLocation = ModuleProdMpLocation;
		if (!$scope.$parent.currentSelectedEnvironment) {
			$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
		}
		$scope.access = {};
		constructModulePermissions($scope, $scope.access, knowledgeBaseModuleProdConfig.permissions);
		
		$scope.merchantId = $routeParams.id;
		$scope.code = $cookies.get('knowledgebase_merchant') || '';
		
		$scope.reloadMerchantRecord = function (cb) {
			var opts;
			if ($scope.access.owner.merchants.get) {
				opts = {
					"routeName": "/knowledgebase/owner/merchants/" + $scope.merchantId,
					"method": "get",
					"params": {
						// "id": $scope.merchantId,
						"code": $scope.code,
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			else {
				opts = {
					"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId,
					"method": "get",
					"params": {
						// "id": $scope.merchantId,
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
			}
			
			merchantsModuleProdSrv.getEntriesFromApi($scope, opts, function (error, response) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					if (response.shipping.extraConfig) {
						if (!Object.hasOwnProperty.call(response.shipping.extraConfig, 'requestSignature')) {
							response.shipping.extraConfig.requestSignature = false;
						}
					}
					else {
						response.shipping.extraConfig = {
							requestSignature: false
						};
					}
					$scope.myMerchant = response;
					if (cb && typeof(cb) === 'function') {
						return cb();
					}
				}
			});
		};
		
		$scope.resetForms = function (divId) {
			var el = angular.element(document.getElementById(divId));
			el.html("<ngform></ngform>");
			$compile(el.contents())($scope);
		};
		
		$scope.populateFeedForm = function () {
			var config = {
				'timeout': $timeout,
				'name': 'feedConfiguration',
				'label': '',
				'entries': angular.copy(knowledgeBaseModuleProdConfig.merchants.configure.feed),
				'data': {},
				'actions': [
					{
						'type': 'submit',
						'label': translation.submit[LANG],
						'btn': 'primary',
						'action': function (formData) {
							merchantsModuleProdSrv.updateMerchantConfigurtion($scope, ngDataApi, formData, 'feed');
						}
					}
				]
			};
			
			config.entries.forEach(function (oneEntry) {
				if (oneEntry.name === 'type') {
					oneEntry.onAction = function (id, data, form) {
						merchantsModuleProdSrv.renderFeedConfigurationForm(form, data);
					}
				}
			});
			buildForm($scope, null, config);
			$scope.resetForms('feed');
			$timeout(function () {
				merchantsModuleProdSrv.renderFeedConfigurationForm($scope.form, $scope.myMerchant);
			}, 100);
		};
		
		$scope.populateBillingForm = function () {
			var config = {
				'timeout': $timeout,
				'name': 'billing',
				'label': '',
				'entries': angular.copy(knowledgeBaseModuleProdConfig.merchants.configure.billing),
				'data': {},
				'actions': [
					{
						'type': 'submit',
						'label': translation.submit[LANG],
						'btn': 'primary',
						'action': function (formData) {
							merchantsModuleProdSrv.updateMerchantConfigurtion($scope, ngDataApi, formData, 'billing');
						}
					}
				]
			};
			config.entries.forEach(function (oneGroup) {
				if (oneGroup.name === 'cc') {
					oneGroup.entries.forEach(function (oneEntry) {
						if (oneEntry.name === 'year') {
							var currentYear = new Date().getFullYear();
							var range = 10;
							for (var i = 0; i < range; i++) {
								var year = currentYear + i;
								oneEntry.value.push({"v": year, "l": year});
							}
						}
					});
				}
			});
			buildForm($scope, null, config);
			$scope.resetForms('billing');
			$timeout(function () {
				merchantsModuleProdSrv.renderBillingConfigurationForm($scope.form, $scope.myMerchant);
			}, 100);
		};
		
		$scope.feedForm = function () {
			$scope.reloadMerchantRecord(function () {
				$scope.populateFeedForm();
			});
		};
		
		$scope.billingForm = function () {
			$scope.reloadMerchantRecord(function () {
				$scope.populateBillingForm();
			});
		};
		
		$scope.setDeliverySig = function (val) {
			
			merchantsModuleProdSrv.getEntriesFromApi($scope, {
				"routeName": "/knowledgebase/owner/merchants/setDeliverySignature",
				"method": "send",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				"data": {
					"id": $scope.merchantId,
					"code": $scope.code,
					"requestSignature": val
				}
			}, function (error) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.$parent.displayAlert('success', translation.configUpdatedSuccessfully[LANG]);
					$scope.reloadMerchantRecord();
				}
			});
		};
		
		$scope.setActiveTab = function (activeTab) {
			sessionStorage.setItem("activeTab", activeTab);
		};
		
		$scope.getActiveTab = function () {
			return sessionStorage.getItem("activeTab");
		};
		
		$scope.isActiveTab = function (tabName, index) {
			var activeTab = $scope.getActiveTab();
			return (activeTab === tabName || (activeTab === null && index === 0));
		};
		
		if ($scope.access.owner.merchants.get || $scope.access.tenant.merchants.get) {
			$scope.reloadMerchantRecord(function () {
				
				merchantsModuleProdSrv.getEntriesFromApi($scope, {
					"routeName": "/order/owner/shippingMethods/list",
					"method": "get",
					"params": {
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				}, function (error, response) {
					if (error) {
						$scope.$parent.displayAlert('danger', error.message);
					}
					else {
						$scope.shippingMethods = response;
					}
				});
			});
		}
		
		injectFiles.injectCss(ModuleProdMpLocation + "/merchants.css");
	}]);

merchantsApp.controller('promoCodesModuleProdCtrl', ['$scope', '$modal', '$routeParams', 'ngDataApi', 'injectFiles', '$timeout', 'merchantsModuleProdSrv', '$cookies', '$compile',
	function ($scope, $modal, $routeParams, ngDataApi, injectFiles, $timeout, merchantsModuleProdSrv, $cookies, $compile) {
		
		$scope.ModuleProdMpLocation = ModuleProdMpLocation;
		$scope.access = {};
		constructModulePermissions($scope, $scope.access, knowledgeBaseModuleProdConfig.permissions);
		var __env = $scope.$parent.currentSelectedEnvironment;
		$scope.languages = [];
		
		$scope.merchantId = $routeParams.id;
		$scope.code = $cookies.get('knowledgebase_merchant') || '';
		
		var gridOptions = {
			'grid': {
				"recordsPerPageArray": [10, 50, 100],
				'columns': [
					{'label': translation.codeValue[LANG], 'field': 'codeValue'},
					{'label': translation.promotionType[LANG], 'field': 'promotionType'},
					{'label': translation.startDate[LANG], 'field': 'startDate'},
					{'label': translation.endDate[LANG], 'field': 'endDate'}
				],
				'defaultLimit': 50
			},
			'defaultSortField': 'codeValue',
			'defaultSortASC': true,
			'left': [
				{
					'icon': 'search',
					'label': translation.viewItem[LANG],
					'handler': 'viewEntry'
				},
				{
					'label': translation.edit[LANG],
					'icon': 'pencil2',
					'handler': 'editPromo'
				},
				{
					'label': translation.delete[LANG],
					'icon': 'cross',
					'msg': translation.areYouSureYouWantToRemoveThisPromo[LANG],
					'handler': 'deleteEntry'
				}
			]
		};
		
		$scope.listCodes = function () {
			merchantsModuleProdSrv.getEntriesFromApi($scope, {
				"routeName": "/knowledgebase/merchant/promoCodes",
				"method": "get",
				"params": {
					"merchantId": $scope.merchantId,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			}, function (error, response) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					response.forEach(function (promo) {
						promo.startDate = promo.timebox.startDate;
						promo.endDate = promo.timebox.endDate;
					});
					gridOptions.data = response;
					buildGrid($scope, gridOptions);
				}
			});
		};
		
		function listProfiles(cb) {
			var params = {
				"__env": __env
			};
			
			getSendDataFromServer($scope, ngDataApi, {
				"routeName": "/kbprofile/owner/feed/profiles/list",
				"method": "get",
				"params": params
			}, function (error, resp) {
				if (error) {
					cb(error);
				}
				else {
					for (var x = 0; x < resp.length; x++) {
						if (resp[x].active) {
							return cb(null, resp[x]);
						}
					}
					cb();
				}
			});
		}
		
		if ($scope.access.tenant.promoCodes.list) {
			listProfiles(function (err, profile) {
				if (profile) {
					$scope.languages = profile.languages;
				}
			});
			$scope.listCodes();
		}
		
		$scope.addPromo = function () {
			var __env = $scope.$parent.currentSelectedEnvironment;
			var outerScope = $scope;
			$modal.open({
				templateUrl: ModuleProdMpLocation + "/directives/modals/addPromoCode.tmpl",
				size: 'lg',
				backdrop: true,
				keyboard: true,
				controller: function ($scope, $modalInstance) {
					$scope.title = translation.addPromoCode[LANG];
					fixBackDrop();
					$scope.ModuleProdMpLocation = ModuleProdMpLocation;
					$scope.outerScope = outerScope;
					$scope.message = {};
					$scope.timebox = {};
					$scope.beforeRenderStartDate = function ($view, $dates, $leftDate, $upDate, $rightDate) {
						if ($scope.timebox.endDate) {
							var activeDate = moment($scope.timebox.endDate);
							for (var i = 0; i < $dates.length; i++) {
								if ($dates[i].localDateValue() >= activeDate.valueOf()) {
									$dates[i].selectable = false;
								}
							}
						}
						else if ($scope.timebox.startDate) {
							$scope.$broadcast('RenderEndDate');
						}
						var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
						for (var i = 0; i < $dates.length; i++) {
							if ($dates[i].localDateValue() < minDate.valueOf()) {
								$dates[i].selectable = false;
							}
						}
					};
					
					$scope.beforeRenderEndDate = function ($view, $dates, $leftDate, $upDate, $rightDate) {
						if ($scope.timebox.startDate) {
							var activeDate = moment($scope.timebox.startDate).subtract(1, $view).add(1, 'minute');
							for (var i = 0; i < $dates.length; i++) {
								if ($dates[i].localDateValue() <= activeDate.valueOf()) {
									$dates[i].selectable = false;
								}
							}
						}
						else if ($scope.timebox.endDate) {
							$scope.$broadcast('RenderStartDate');
						}
						var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
						for (var i = 0; i < $dates.length; i++) {
							if ($dates[i].localDateValue() <= minDate.valueOf()) {
								$dates[i].selectable = false;
							}
						}
					};
					
					$scope.toTimeZone = function (time, zone) {
						var format = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]';
						return moment(moment(time), format).tz(zone).format(format);
					};
					
					$scope.onSubmit = function () {
						if ($scope.timebox) {
							if ($scope.timebox.startDate && !$scope.timebox.endDate) {
								$scope.message.danger = true;
								return $scope.message.text = translation.pleaseSelectEndDate[LANG];
							}
							if (!$scope.timebox.startDate && $scope.timebox.endDate) {
								$scope.message.danger = true;
								return $scope.message.text = translation.pleaseSelectStartDate[LANG];
							}
							if (!$scope.timebox.startDate && !$scope.timebox.endDate) {
								$scope.message.danger = true;
								return $scope.message.text = translation.pleaseSelectStartDateAndEndDate[LANG];
							}
						}
						
						var postData = {
							timebox: $scope.timebox,
							codeValue: $scope.codeValue,
							promotionType: $scope.promotionType,
							usageTimesLimit: $scope.usageTimesLimit,
							description: $scope.description,
							details: {}
						};
						
						overlayLoading.show();
						
						getSendDataFromServer(outerScope, ngDataApi, {
							"method": "send",
							"routeName": "/knowledgebase/merchant/promoCodes",
							"params": {
								"merchantId": $routeParams.id,
								"__env": __env
							},
							"data": postData
						}, function (error, response) {
							overlayLoading.hide();
							if (error) {
								$scope.message.danger = true;
								$scope.message.text = error.message;
							}
							else {
								outerScope.$parent.displayAlert('success', translation.promoAddedSuccessfully[LANG]);
								$modalInstance.close();
								outerScope.listCodes();
								outerScope.editPromo(response);
							}
						});
					};
					
					$scope.closeModal = function () {
						$modalInstance.close();
					};
				}
			});
		};
		
		$scope.viewEntry = function (data) {
			$modal.open({
				templateUrl: "infoBoxPromoCode.html",
				size: 'dialog',
				backdrop: true,
				keyboard: true,
				controller: function ($scope, $modalInstance) {
					$scope.title = translation.viewingOnePromoCode[LANG];
					$scope.data = data;
					fixBackDrop();
					$scope.ok = function () {
						$modalInstance.dismiss('ok');
					};
				}
			});
		};
		
		$scope.editPromo = function (data) {
			var __env = $scope.$parent.currentSelectedEnvironment;
			var outerScope = $scope;
			$modal.open({
				templateUrl: ModuleProdMpLocation + "/directives/modals/editPromoCode.tmpl",
				size: 'lg',
				backdrop: true,
				keyboard: true,
				controller: function ($scope, $modalInstance) {
					$scope.title = translation.editPromoCode[LANG];
					fixBackDrop();
					$scope.ModuleProdMpLocation = ModuleProdMpLocation;
					$scope.outerScope = outerScope;
					$scope.message = {};
					$scope.data = data;
					$scope.languages = outerScope.languages;
					$scope.timebox = data.timebox;
					$scope.usageTimesLimit = data.usageTimesLimit;
					$scope.descriptionObj = {};
					
					if (data.description) {
						data.description.forEach(function (obj) {
							$scope.descriptionObj[obj.language] = obj.value;
						});
					}
					
					$scope.beforeRenderStartDate = function ($view, $dates, $leftDate, $upDate, $rightDate) {
						if ($scope.timebox.endDate) {
							var activeDate = moment($scope.timebox.endDate);
							for (var i = 0; i < $dates.length; i++) {
								if ($dates[i].localDateValue() >= activeDate.valueOf()) {
									$dates[i].selectable = false;
								}
							}
						}
						else if ($scope.timebox.startDate) {
							$scope.$broadcast('RenderEndDate');
						}
						var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
						for (var i = 0; i < $dates.length; i++) {
							if ($dates[i].localDateValue() < minDate.valueOf()) {
								$dates[i].selectable = false;
							}
						}
					};
					
					$scope.beforeRenderEndDate = function ($view, $dates, $leftDate, $upDate, $rightDate) {
						if ($scope.timebox.startDate) {
							var activeDate = moment($scope.timebox.startDate).subtract(1, $view).add(1, 'minute');
							for (var i = 0; i < $dates.length; i++) {
								if ($dates[i].localDateValue() <= activeDate.valueOf()) {
									$dates[i].selectable = false;
								}
							}
						}
						else if ($scope.timebox.endDate) {
							$scope.$broadcast('RenderStartDate');
						}
						var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
						for (var i = 0; i < $dates.length; i++) {
							if ($dates[i].localDateValue() <= minDate.valueOf()) {
								$dates[i].selectable = false;
							}
						}
					};
					
					$scope.toTimeZone = function (time, zone) {
						var format = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]';
						return moment(moment(time), format).tz(zone).format(format);
					};
					
					$scope.onSubmit = function () {
						if ($scope.timebox) {
							if ($scope.timebox.startDate && !$scope.timebox.endDate) {
								$scope.message.danger = true;
								return $scope.message.text = translation.pleaseSelectEndDate[LANG];
							}
							if (!$scope.timebox.startDate && $scope.timebox.endDate) {
								$scope.message.danger = true;
								return $scope.message.text = translation.pleaseSelectStartDate[LANG];
							}
							if (!$scope.timebox.startDate && !$scope.timebox.endDate) {
								$scope.message.danger = true;
								return $scope.message.text = translation.pleaseSelectStartDateAndEndDate[LANG];
							}
						}
						var desc = [];
						for (var lang in $scope.descriptionObj) {
							if ($scope.descriptionObj[lang]) {
								desc.push({"value": $scope.descriptionObj[lang], "language": lang});
							}
						}
						var postData = {
							timebox: $scope.timebox,
							usageTimesLimit: $scope.usageTimesLimit,
							description: desc,
							details: {}
						};
						
						if ($scope.data.details) {
							postData.details = $scope.data.details;
						}
						
						overlayLoading.show();
						
						getSendDataFromServer(outerScope, ngDataApi, {
							"method": "put",
							"routeName": "/knowledgebase/merchant/promoCodes/" + data._id.toString(),
							"params": {
								"merchantId": $routeParams.id,
								"__env": __env
							},
							"data": postData
						}, function (error) {
							overlayLoading.hide();
							if (error) {
								$scope.message.danger = true;
								$scope.message.text = error.message;
							}
							else {
								outerScope.$parent.displayAlert('success', translation.promoUpdatedSuccessfully[LANG]);
								$modalInstance.close();
								outerScope.listCodes();
							}
						});
					};
					
					$scope.closeModal = function () {
						$modalInstance.close();
					};
				}
			})
		};
		
		$scope.deleteEntry = function (data) {
			var __env = $scope.$parent.currentSelectedEnvironment;
			getSendDataFromServer($scope, ngDataApi, {
				"routeName": "/knowledgebase/merchant/promoCodes/" + data._id.toString(),
				"method": "del",
				"params": {
					"merchantId": $routeParams.id,
					"__env": __env
				}
			}, function (error) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.$parent.displayAlert('success', translation.promoCodeDeletedSuccessfully[LANG]);
					$scope.listCodes();
				}
			});
		};
		
	}]);