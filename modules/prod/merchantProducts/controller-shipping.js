"use strict";
var merchantsApp = soajsApp.components;

merchantsApp.controller('merchantShippingMethodsModuleProdCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$routeParams', '$cookies', function ($scope, $modal, ngDataApi, injectFiles, $timeout, $routeParams, $cookies) {
	$scope.ModuleProdMpLocation = ModuleProdMpLocation;
	$scope.merchantId = $routeParams.id;
	$scope.code = $cookies.get('knowledgebase_merchant') || '';
	
	$scope.$parent.isUserLoggedIn();
	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, knowledgeBaseModuleProdConfig.permissions);
	
	$scope.grid = {rows: []};
	$scope.formConfig = {
		"entries": [
			{
				'name': 'driver',
				'label': translation.driverName[LANG],
				'type': 'select',
				'value': [],
				'tooltip': translation.driverNameTooltip[LANG],
				'required': true
			},
			{
				'name': 'label',
				'label': translation.driverLabel[LANG],
				'type': 'text',
				'value': '',
				'tooltip': translation.driverLabelTooltip[LANG],
				'placeholder': translation.driverLabelPlaceholder[LANG],
				'required': true
			},
			{
				'name': 'active',
				'label': 'Active',
				'type': 'radio',
				'value': [{'v': true, 'l': translation.yes[LANG], 'selected': true}, {
					'v': false,
					'l': translation.no[LANG]
				}],
				'required': true
			},
			{
				"name": "config",
				"label": translation.configuration[LANG],
				"type": "group",
				"entries": []
			}
		]
	};
	
	//function that lists the Delivery Methods in a grid
	$scope.listEntries = function () {
		$scope.noAdd = false;
		
		var opts;
		if ($scope.access.owner.merchants.get) {
			opts = {
				"routeName": "/knowledgebase/owner/merchants/" + $scope.merchantId,
				"method": "get",
				"params": {
					"code": $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		else {
			opts = {
				"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId,
				"method": "get",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		
		getSendDataFromServer($scope, ngDataApi, opts, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.myMerchant = response;
				var data = [];
				if (response.shipping && response.shipping.methods) {
					data = response.shipping.methods;
				}
				var options = {
					'grid': {
						recordsPerPageArray: [5, 10, 50, 100],
						'columns': [
							{'label': translation.driver[LANG], 'field': 'label'},
							{'label': translation.active[LANG], 'field': 'active'},
							{'label': translation.merchantSpecific[LANG], 'field': 'custom'}
						],
						'defaultLimit': 50
					},
					'defaultSortField': 'active',
					'defaultSortASC': true,
					'data': data,
					'left': [
						{
							'icon': 'search',
							'label': translation.viewItem[LANG],
							'handler': 'viewEntry'
						},
						{
							'label': translation.edit[LANG],
							'icon': 'pencil2',
							'handler': 'editEntry'
						},
						{
							'label': translation.disableActivate[LANG],
							'icon': 'spinner9',
							'handler': 'changeEntryStatus'
						},
						{
							'label': translation.delete[LANG],
							'icon': 'cross',
							'msg': translation.areYouSureYouWantToRemoveThisMethod[LANG],
							'handler': 'deleteEntry'
						}
					]
				};
				buildGrid($scope, options);
				
				$scope.checkAdd();
			}
		});
	};
	
	$scope.checkAdd = function () {
		//check if merchant shipping methods is the same as the drivers, then disable add
		if ($scope.myMerchant.shipping && $scope.myMerchant.shipping.methods) {
			$scope.noAdd = Object.keys($scope.drivers).length <= $scope.myMerchant.shipping.methods.length;
		}
	};
	
	//function that prints one data record to the console
	$scope.viewEntry = function (oneDataRecord) {
		$modal.open({
			templateUrl: "infoBoxDriver.html",
			size: 'dialog',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewingOneShippingMethod[LANG];
				$scope.data = oneDataRecord;
				fixBackDrop();
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};
	
	$scope.addEntry = function () {
		var jsonEntries = [];
		var formFields = angular.copy($scope.formConfig.entries);
		resetFormFields();
		
		////push into formFields the config.from returned per driver
		formFields[0].onAction = function (id, data, form) {
			form.entries[3].entries = [];
			if (data.indexOf("_owner") === -1) {
				var clones = angular.copy($scope.drivers[data]);
				clones.entries.forEach(function (oneEntry) {
					if (oneEntry.type === 'json') {
						jsonEntries.push(oneEntry.name);
					}
					form.entries[3].entries.push(oneEntry);
				});
			}
			else {
				var ownerDriver = angular.copy($scope.ownerDriver);
				ownerDriver.forEach(function (oneDriver) {
					if (oneDriver.driver === data.replace("_owner", "")) {
						form.entries[1].value = oneDriver.label;
						form.formData.label = oneDriver.label;
					}
				});
			}
		};
		
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'addShippingMethod',
			'label': translation.addShippingMethod[LANG],
			'actions': [
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var postData = {
							custom: true,
							config: {}
						};
						
						var predefined = [];
						$scope.formConfig.entries.forEach(function (oneField) {
							predefined.push(oneField.name);
						});
						
						for (var i in formData) {
							if (jsonEntries.indexOf(i) !== -1) {
								try {
									formData[i] = JSON.parse(formData[i]);
								}
								catch (e) {
									$scope.form.displayAlert('danger', 'Error: Invalid Json object');
									return;
								}
							}
							if (predefined.indexOf(i) !== -1) {
								postData[i] = formData[i];
							}
							else {
								postData.config[i] = formData[i];
							}
						}
						
						if (postData.driver.indexOf("_owner") !== -1) {
							postData.driver = postData.driver.replace("_owner", "");
							postData.custom = false;
							var ownerDriver = angular.copy($scope.ownerDriver);
							ownerDriver.forEach(function (oneDriver) {
								if (oneDriver.driver === postData.driver) {
									postData.config = oneDriver.config;
								}
							});
						}
						if (!$scope.myMerchant.shipping) {
							$scope.myMerchant.shipping = {
								methods: []
							};
						}
						if (!$scope.myMerchant.shipping.methods) {
							$scope.myMerchant.shipping.methods = [];
						}
						if (postData.driver && postData.label && Object.keys(postData.config).length > 0) {
							$scope.myMerchant.shipping.methods.push(postData);
							var opts = {
								"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId + "/configure",
								"method": "send",
								"params": {
									"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
								},
								"data": {
									"merchantConfig": {
										"shipping": $scope.myMerchant.shipping
									}
								}
							};
							if ($scope.access.owner.merchants.configure) {
								opts.routeName = "/knowledgebase/owner/merchants/" + $scope.merchantId + "/configure";
								opts.params.code = $scope.code;
							}
							else {
							}
							
							getSendDataFromServer($scope, ngDataApi, opts, function (error) {
								if (error) {
									$scope.form.displayAlert('danger', error.message);
								}
								else {
									$scope.$parent.displayAlert('success', translation.merchantShippingMethodAddedSuccessfully[LANG]);
									$scope.modalInstance.close();
									$scope.form.formData = {};
									$scope.listEntries();
									$scope.checkAdd();
								}
							});
						}
						else {
							$scope.form.displayAlert('danger', translation.someInputsAreStillMissing[LANG]);
						}
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);
		
		//custom function to reset form fields
		function resetFormFields() {
			var myArray = formFields[0];
			myArray.value = [];
			
			var driverNames = Object.keys($scope.drivers);
			driverNames.forEach(function (oneDriverName) {
				
				if ($scope.grid.filteredRows.length === 0) {
					myArray.value.push({
						"v": oneDriverName,
						"l": oneDriverName
					});
				}
				else {
					for (var i = 0; i < $scope.grid.filteredRows.length; i++) {
						
						//skip existing drivers
						if ($scope.grid.filteredRows[i].driver === oneDriverName) {
							continue;
						}
						
						myArray.value.push({
							"v": oneDriverName,
							"l": oneDriverName
						});
					}
				}
			});
			
			var ownerDriver = angular.copy($scope.ownerDriver);
			ownerDriver.forEach(function (oneDriver) {
				myArray.value.push({
					"v": oneDriver.driver + "_owner",
					"l": oneDriver.label + " (" + translation.useMarketplaceDefault[LANG] + ")"
				});
			});
		}
	};
	
	$scope.editEntry = function (data1) {
		var count = 0;
		var oneClone = {
			'name': 'oneGroup%count%',
			'label': 'Custom Rate',
			'type': 'group',
			'required': false,
			"class": "customRate",
			"entries": [
				{
					'name': 'service%count%',
					'label': 'Service Name',
					'type': 'select',
					'value': [
						{l: 'RegularParcel', v: 'RegularParcel'},
						{l: 'Xpresspost', v: 'Xpresspost'}
					],
					'required': true
				},
				{
					"type": "html",
					"name": "removeRate%count%",
					"value": "<span class='red'><span class='icon icon-cross' title='Remove Rate'></span></span>",
					"onAction": function (id, data, form) {
						var number = id.replace("removeRate", "");
						
						delete form.formData['flateRate' + number];
						delete form.formData['service' + number];
						delete form.formData['priceRangeMax' + number];
						delete form.formData['priceRangeMin' + number];
						
						delete form.formData['cost' + number];
						delete form.formData['currency' + number];
						delete form.formData['provinces' + number];
						delete form.formData['removeRate' + number];
						
						for (var j = form.entries.length - 1; j >= 0; j--) {
							var oneEntry = form.entries[j];
							
							if (oneEntry.type === 'group') {
								if (oneEntry.name === 'oneGroup' + number) {
									form.entries.splice(j, 1);
								}
							}
						}
						
					}
				},
				{
					'name': 'flatRate%count%',
					'label': 'Flat Rate',
					'type': 'radio',
					'value': [
						{
							l: translation.true[LANG],
							v: true
						},
						{
							l: translation.false[LANG],
							v: false
						}
					],
					'required': true
				},
				{
					'name': 'cost%count%',
					'label': 'Custom Price',
					'type': 'text',
					'required': false
				},
				{
					'name': 'currency%count%',
					'label': 'Currency',
					'type': 'text',
					'required': false
				},
				{
					'name': 'priceRangeMin%count%',
					'label': 'Min Price',
					'type': 'number',
					'required': false
				},
				{
					'name': 'priceRangeMax%count%',
					'label': 'Max Price',
					'type': 'number',
					'required': false
				},
				{
					'name': 'provinces%count%',
					'label': 'Provinces',
					'type': 'text',
					'required': false
				}
			]
		};
		
		var jsonEntries = [];
		var formFields = angular.copy($scope.formConfig.entries);
		var message = {
			'name': 'message',
			'label': 'Configure Custom Services Rates',
			'type': 'html',
			'required': false
		};
		formFields.push(message);
		
		var data = angular.copy(data1);
		resetFormFields(data);
		
		formFields[3].entries = [];
		var clones = angular.copy($scope.drivers[data.driver].entries);
		clones.forEach(function (oneEntry) {
			if (oneEntry.type === 'json') {
				jsonEntries.push(oneEntry.name);
			}
			for (var i in data.config) {
				if (i === oneEntry.name) {
					if (Array.isArray(oneEntry.value)) {
						oneEntry.value.forEach(function (sV) {
							if (Array.isArray(data.config[i])) {
								if (data.config[i].indexOf(sV.v) !== -1) {
									sV.selected = true;
									data[i] = data.config[i];
								}
							}
							else if (data.config[i].toString() === sV.v.toString()) {
								sV.selected = true;
								data[i] = data.config[i];
							}
						});
					}
					else {
						if (oneEntry.type === 'json') {
							data.config[i] = JSON.stringify(data.config[i], null, "\t");
						}
						else if (!data.custom) {
							if (oneEntry.type !== 'jsoneditor') {
								oneEntry.type = 'readonly';
							}
						}
						oneEntry.value = data.config[i];
						data[i] = data.config[i];
					}
				}
			}
			formFields[3].entries.push(oneEntry);
		});
		
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'editShippingMethod',
			'label': translation.editShippingMethod[LANG],
			'data': data,
			'actions': [
				{
					'type': 'button',
					'label': 'Add Custom Rate',
					'btn': 'success',
					'action': function () {
						var cloneAgain = angular.copy(oneClone);
						cloneAgain.name = cloneAgain.name.replace("%count%", count);
						$scope.form.entries.push(cloneAgain);
						cloneAgain.entries.forEach(function (oneEntry) {
							oneEntry.name = oneEntry.name.replace("%count%", count);
							if (oneEntry.type === 'html') {
								oneEntry.value = oneEntry.value.replace("%count%", count);
							}
						});
						count++;
					}
				},
				{
					'type': 'submit',
					'label': translation.saveChanges[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var postData = {
							custom: data.custom,
							config: {}
						};
						
						var predefined = [];
						$scope.formConfig.entries.forEach(function (oneField) {
							predefined.push(oneField.name);
						});
						if (count) {
							var services = [];
							for (var j = 0; j < count; j++) {
								if (formData['service' + j]) {
									var objRate = {
										service: formData['service' + j],
										flatRate: formData['flatRate' + j]
									};
									if (formData['priceRangeMin' + j] || formData['priceRangeMax' + j]) {
										objRate.priceRange = {};
										if (formData['priceRangeMin' + j]) {
											objRate.priceRange.min = formData['priceRangeMin' + j];
										}
										if (formData['priceRangeMax' + j]) {
											objRate.priceRange.max = formData['priceRangeMax' + j];
										}
									}
									if (formData['flatRate' + j] === true) {
										objRate.rate = {
											cost: formData['cost' + j],
											currency: formData['currency' + j]
										};
									}
									if (formData['provinces' + j]) {
										var provinces = formData['provinces' + j].replace(/\s+/g, "");
										objRate.provinces = provinces.split(',');
									}
									services.push(objRate);
								}
								delete formData['service' + j];
								delete formData['flatRate' + j];
								delete formData['currency' + j];
								delete formData['cost' + j];
								delete formData['priceRangeMin' + j];
								delete formData['priceRangeMax' + j];
								delete formData['provinces' + j];
								delete formData['removeRate' + j];
							}
						}
						
						for (var i in formData) {
							if (jsonEntries.indexOf(i) !== -1) {
								if (formData[i]) {
									try {
										formData[i] = JSON.parse(formData[i]);
									}
									catch (e) {
										$scope.form.displayAlert('danger', 'Error: Invalid Json object ');
										return;
									}
								}
							}
							if (predefined.indexOf(i) !== -1) {
								postData[i] = formData[i];
							}
							else {
								postData.config[i] = formData[i];
							}
						}
						if (count) {
							postData.config.services = services;
						}
						for (var i = 0; i < $scope.myMerchant.shipping.methods.length; i++) {
							if ($scope.myMerchant.shipping.methods[i].driver === postData.driver) {
								$scope.myMerchant.shipping.methods[i] = postData;
								break;
							}
						}
						
						if (postData.driver && postData.label && Object.keys(postData.config).length > 0) {
							var opts;
							opts = {
								"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId + "/configure",
								"method": "send",
								"params": {
									"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
								},
								"data": {
									"merchantConfig": {
										"shipping": $scope.myMerchant.shipping
									}
								}
							};
							if ($scope.access.owner.merchants.configure) {
								opts.routeName = "/knowledgebase/owner/merchants/" + $scope.merchantId + "/configure";
								opts.params.code = $scope.code;
							}
							else {
							}
							
							getSendDataFromServer($scope, ngDataApi, opts, function (error) {
								if (error) {
									$scope.form.displayAlert('danger', error.message);
								}
								else {
									$scope.$parent.displayAlert('success', translation.merchantShippingMethodUpdatedSuccessfully[LANG]);
									$scope.modalInstance.close();
									$scope.form.formData = {};
									$scope.listEntries();
								}
							});
						}
						else {
							$scope.form.displayAlert('danger', translation.someInputsAreStillMissing[LANG]);
						}
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		
		buildFormWithModal($scope, $modal, options);
		
		//custom function to reset form fields
		function resetFormFields(data) {
			var myArray = formFields[0];
			myArray.value = data.driver;
			myArray.type = "readonly";
			
			if (!data.custom) {
				formFields[1].type = 'readonly';
			}
			
			if (data.config.services) {
				count = data.config.services.length;
				for (var i = 0; i < count; i++) {
					var subClone = angular.copy(oneClone);
					subClone.name = subClone.name.replace("%count%", i);
					subClone.entries.forEach(function (oneEntry) {
						if (oneEntry.type === 'html') {
							oneEntry.value = oneEntry.value.replace("%count%", i);
						}
						else if ((oneEntry.type === 'radio') || (oneEntry.type === 'select')) {
							if (Array.isArray(oneEntry.value)) {
								oneEntry.value.forEach(function (sV) {
									if (data.config.services[i][oneEntry.name.replace("%count%", "")].toString() === sV.v.toString()) {
										sV.selected = true;
									}
								});
							}
						}
						else {
							if (oneEntry.name.replace("%count%", "") === 'currency') {
								if (Object.hasOwnProperty.call(data.config.services[i], 'rate')) {
									if (data.config.services[i].rate.currency) {
										oneEntry.value = data.config.services[i].rate.currency;
									}
								}
							}
							else if (oneEntry.name.replace("%count%", "") === 'cost') {
								if (Object.hasOwnProperty.call(data.config.services[i], 'rate')) {
									oneEntry.value = data.config.services[i].rate.cost;
								}
							}
							else if (oneEntry.name.replace("%count%", "") === 'priceRangeMin') {
								if (data.config.services[i].priceRange) {
									oneEntry.value = data.config.services[i].priceRange.min;
								}
							}
							else if (oneEntry.name.replace("%count%", "") === 'priceRangeMax') {
								if (data.config.services[i].priceRange) {
									oneEntry.value = data.config.services[i].priceRange.max;
								}
							}
							else if (oneEntry.name.replace("%count%", "") === 'provinces') {
								if (data.config.services[i].provinces) {
									oneEntry.value = data.config.services[i].provinces.join(',');
								}
							}
							else {
								if (data.config.services[i][oneEntry.name.replace("%count%", "")]) {
									oneEntry.value = data.config.services[i][oneEntry.name.replace("%count%", "")];
								}
							}
						}
						oneEntry.name = oneEntry.name.replace("%count%", i);
						
					});
					formFields.push(subClone);
				}
			}
		}
	};
	
	$scope.deleteEntry = function (data) {
		for (var i = $scope.myMerchant.shipping.methods.length - 1; i >= 0; i--) {
			if ($scope.myMerchant.shipping.methods[i].driver === data.driver) {
				$scope.myMerchant.shipping.methods.splice(i, 1);
			}
		}
		
		var opts = {
			"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId + "/configure",
			"method": "send",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			},
			"data": {
				"merchantConfig": {
					"shipping": $scope.myMerchant.shipping
				}
			}
		};
		if ($scope.access.owner.merchants.configure) {
			opts.routeName = "/knowledgebase/owner/merchants/" + $scope.merchantId + "/configure";
			opts.params.code = $scope.code;
		}
		else {
		}
		
		getSendDataFromServer($scope, ngDataApi, opts, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.listEntries();
				$scope.$parent.displayAlert('success', translation.merchantShippingMethodDeletedSuccessfully[LANG]);
				$scope.checkAdd();
			}
		});
	};
	
	$scope.changeEntryStatus = function (data) {
		for (var i = $scope.myMerchant.shipping.methods.length - 1; i >= 0; i--) {
			if ($scope.myMerchant.shipping.methods[i].driver === data.driver) {
				$scope.myMerchant.shipping.methods[i].active = !$scope.myMerchant.shipping.methods[i].active;
			}
		}
		
		var opts = {
			"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId + "/configure",
			"method": "send",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			},
			"data": {
				"merchantConfig": {
					"shipping": $scope.myMerchant.shipping
				}
			}
		};
		if ($scope.access.owner.merchants.configure) {
			opts.routeName = "/knowledgebase/owner/merchants/" + $scope.merchantId + "/configure";
			opts.params.code = $scope.code;
		}
		else {
		}
		
		getSendDataFromServer($scope, ngDataApi, opts, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.merchantShippingStatusChangedSuccessfully[LANG]);
			}
		});
		
	};
	
	function listMethods() {
		var opts = {
			"routeName": "/order/owner/shippingMethods/list",
			"method": "get",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		getSendDataFromServer($scope, ngDataApi, opts, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.drivers = response.drivers;
				$scope.ownerDriver = response.list;
				$scope.listEntries();
			}
		});
	}
	
	listMethods();
	
}
]);