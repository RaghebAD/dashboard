"use strict";
var bundlesApp = soajsApp.components;

bundlesApp.controller('promoCodesModuleProdCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$cookies', 'promoCodeModuleProdSrv',
	function ($scope, $modal, ngDataApi, injectFiles, $timeout, $cookies, promoCodeModuleProdSrv) {
		
		$scope.$parent.isUserLoggedIn();
		$scope.ModuleProdMpLocation = ModuleProdMpLocation;
		if (!$scope.$parent.currentSelectedEnvironment) {
			$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
		}
		$scope.access = {};
		constructModulePermissions($scope, $scope.access, promoModuleProdConfig.permissions);
		
		$scope.editRule = function (data) {
			$scope.openModal(data, true);
		};
		
		$scope.addPromo = function () {
			var newData = {
				type: "",
				// config: {},
				discount: {
					type: "",
					value: 0
				},
				category: {
					include: []
				},
				serial: [],
				status: "inactive",
				startDate: ""
				// endDate: ""
			};
			
			$scope.openModal(newData);
		};
		
		$scope.openModal = function (newData, isEdit) {
			var __env = $scope.$parent.currentSelectedEnvironment;
			var outerScope = $scope;
			$modal.open({
				templateUrl: ModuleProdMpLocation + "/directives/modals/promoCode.tmpl",
				size: 'lg',
				backdrop: true,
				keyboard: true,
				controller: function ($scope, $modalInstance) {
					$scope.title = "Add Promo";
					if (isEdit) {
						$scope.isEdit = true;
						$scope.title = "Edit Promo";
					}
					
					$scope.tempo = {};
					$scope.tempo.availableProducts = [];
					$scope.profileCategories = [];
					
					$scope.setAllAvailableProducts = function () {
						var opts = {
							"method": "get",
							"routeName": "/knowledgebase/product/availableProducts",
							"params": {
								"__env": __env
							},
							"data": {}
						};
						
						promoCodeModuleProdSrv.getEntriesFromApi(outerScope, opts, function (error, data) {
							if (error) {
								$scope.$parent.displayAlert('danger', error.message);
								extraUtils.refreshThumbnails($scope);
							}
							else {
								
								var products = data.records;
								var availableProducts = [];
								for (var i = 0; i < products.length; i++) {
									
									var isSelected = false;
									if (isEdit && $scope.promoCode.serial.indexOf(products[i].serial) !== -1) {
										isSelected = true;
									}
									
									var temp = {
										serial: products[i].serial,
										title: products[i].productData.title["en"],
										isSelected: isSelected
									};
									if (products[i].coreData.udac) {
										temp.udac = products[i].coreData.udac;
									}
									temp.fullName = temp.serial + " " + temp.title;
									if (temp.fullName.length > 55) {
										temp.fullName = temp.fullName.substring(0, 50) + "..";
									}
									availableProducts.push(temp);
								}
								
								$scope.tempo.availableProducts = availableProducts;
							}
						});
					};
					$scope.setAllAvailableProducts();
					
					$scope.category = '';
					fixBackDrop();
					$scope.ModuleProdMpLocation = ModuleProdMpLocation;
					$scope.showSelectedCategories = false;
					$scope.profileSelectedCategories = {};
					$scope.categoriesList = [];
					$scope.promoCode = newData;
					
					$scope.myCategories = [];
					$scope.outerScope = outerScope;
					$scope.message = {};
					
					$scope.beforeRenderStartDate = function ($view, $dates, $leftDate, $upDate, $rightDate) {
						if ($scope.promoCode.endDate) {
							var activeDate = moment($scope.promoCode.endDate);
							for (var i = 0; i < $dates.length; i++) {
								if ($dates[i].localDateValue() >= activeDate.valueOf()) {
									$dates[i].selectable = false;
								}
							}
						}
						else if ($scope.promoCode.startDate) {
							$scope.$broadcast('RenderEndDate');
						}
						var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
						for (var i = 0; i < $dates.length; i++) {
							if ($dates[i].localDateValue() < minDate.valueOf()) {
								$dates[i].selectable = false;
							}
						}
					};
					
					$scope.beforeRenderEndDate = function ($view, $dates, $leftDate, $upDate, $rightDate) {
						if ($scope.promoCode.startDate) {
							var activeDate = moment($scope.promoCode.startDate).subtract(1, $view).add(1, 'minute');
							for (var i = 0; i < $dates.length; i++) {
								if ($dates[i].localDateValue() <= activeDate.valueOf()) {
									$dates[i].selectable = false;
								}
							}
						}
						else if ($scope.promoCode.endDate) {
							$scope.$broadcast('RenderStartDate');
						}
						var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
						for (var i = 0; i < $dates.length; i++) {
							if ($dates[i].localDateValue() <= minDate.valueOf()) {
								$dates[i].selectable = false;
							}
						}
					};
					
					$scope.toTimeZone = function (time, zone) {
						var format = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]';
						return moment(moment(time), format).tz(zone).format(format);
					};
					
					var id_feed;
					
					/**
					 *  gets the children of a parent category when clicked
					 *  if category null gets the parent categories
					 * @param id
					 * @param category
					 * @param cb
					 */
					function readFeedProfile(id, category, cb) {
						var params = {
							"source": "catalog",
							"__env": __env
						};
						if (category && category !== "") {
							params["category"] = category;
						}
						
						getSendDataFromServer(outerScope, ngDataApi, {
							"routeName": "/kbprofile/owner/feed/profiles/" + id,
							"method": "get",
							"params": params
						}, function (error, resp) {
							if (error) {
								//outerScope.$parent.displayAlert('danger', error.message');
								cb();
							}
							else {
								cb(resp);
							}
						});
					}
					
					/**
					 * gets the category listing of the given profile
					 * @param profileRecord
					 * @param currentList
					 * @param parentCategory
					 */
					function buildCategoriesFromProfile(profileRecord, currentList, parentCategory) {
						var categories = [];
						var defaultLanguage = '';
						if (profileRecord) {
							for (var i = 0; i < profileRecord.languages.length; i++) {
								if (profileRecord.languages[i].selected) {
									defaultLanguage = profileRecord.languages[i].id;
								}
							}
							
							var parentIndex;
							for (var i = 0; i < currentList.length; i++) {
								if (currentList[i].v === parentCategory) {
									parentIndex = i;
								}
							}
							
							for (var cat in profileRecord.categories) {
								processCategoryLevel(cat, profileRecord.categories, currentList, parentIndex);
							}
							
							if (currentList && currentList.length > 0) {
								profileRecord.categories = currentList;
								profileRecord.categories.splice.apply(profileRecord.categories, [parentIndex + 1, 0].concat(categories));
							}
							else {
								profileRecord.categories = categories;
							}
							
						}
						
						function processCategoryLevel(cat, categoriesArray, currentList, parentIndex) {
							var id = cat;
							var label = (profileRecord.taxonomies[cat]) ? profileRecord.taxonomies[cat].label[defaultLanguage].value : cat;
							if (categoriesArray[cat]) {
								if (categoriesArray[cat]['deprecated-towards']) {
									//get the new value
									id = categoriesArray[cat]['deprecated-towards'];
									label += " (" + translation.deprecated[LANG] + ")," + translation.becomes[LANG] + ":" + profileRecord.taxonomies[id].label[defaultLanguage].value;
								}
								
								if (categoriesArray[cat].parents && categoriesArray[cat].parents.length > 0) {
									//label = getLabels(categoriesArray[cat].parents, defaultLanguage) + " / " + label;
									label = currentList[parentIndex].l + " / " + label;
								}
								
								//Getting level based on label string
								var labelArray = label.split(" / ");
								
								var t = {
									"v": id,
									"l": label,
									"level": labelArray.length - 1
								};
								
								categories.push(t);
							}
						}
						
					}
					
					function buildTree() {
						function getActiveProfile(cb) {
							var params = {
								"__env": __env
							};
							
							getSendDataFromServer(outerScope, ngDataApi, {
								"routeName": "/kbprofile/owner/feed/active/profiles",
								"method": "get",
								"params": params
							}, function (error, resp) {
								if (error) {
									cb();
								}
								else {
									id_feed = resp._id.toString();
									cb();
								}
							});
						}
						
						getActiveProfile(function () {
							
							readFeedProfile(id_feed, $scope.category, function (profile) {
								$scope.loading = true;
								if (profile) {
									buildCategoriesFromProfile(profile, $scope.categoriesList, $scope.category);
									$scope.categoriesList = angular.fromJson(angular.copy(profile.categories));
									if (newData.category && newData.category.include) {
										$scope.profileCategories = newData.category.include;
									}
									
									$scope.selectedFeed = profile._id;
								}
								else {
									return;
								}
								
								if ($scope.selectAll) {
									$scope.profileCategories = [];
									$scope.selectAllCategories = true;
									$timeout(function () {
										$scope.loading = false;
									}, 500);
								}
								else {
									loopAndRebuild($scope.categoriesList, 0, function () {
										//$scope.showSelectedCategories = true;
									});
								}
							});
						});
						
					}
					
					function getCategoriesArr(obj) {
						var arr = [];
						for (var c in obj) {
							if (obj[c] && obj[c] === true) {
								arr.push(c);
							}
						}
						return arr;
					}
					
					function loopAndRebuild(categoriesList, i, cb) {
						if (!categoriesList[i]) {
							// todo: add assurance
						}
						if ($scope.profileCategories) {
							
							if ($scope.profileCategories.indexOf(categoriesList[i].v) !== -1) { //category is selected
								$scope.profileSelectedCategories[categoriesList[i].v] = true;
								$scope.assembleCategories(categoriesList[i].v, function () {
									categoriesList = angular.copy($scope.categoriesList);
									i++;
									if (i === categoriesList.length) {
										$timeout(function () {
											$scope.loading = false;
										}, 500);
										return cb();
									}
									else {
										loopAndRebuild(categoriesList, i, cb);
									}
								});
							}
							else {
								i++;
								if (i === categoriesList.length) {
									$timeout(function () {
										$scope.loading = false;
									}, 500);
									return cb();
								}
								else {
									loopAndRebuild(categoriesList, i, cb);
								}
							}
						}
						
					}
					
					function renderFeed(cb) {
						var grabFeed = function ($scope, callback) {
							if ($scope.selectedFeed !== "") {
								readFeedProfile(id_feed, $scope.category, function (profile) {
									buildCategoriesFromProfile(profile, $scope.categoriesList, $scope.category);
									$scope.categoriesList = angular.fromJson(angular.copy(profile.categories));
									return callback();
								});
							}
							else {
								return callback();
							}
						};
						// update categ tree
						if (!cb) {
							cb = function () {
							}
						}
						grabFeed($scope, cb);
					}
					
					$scope.selectAllTrigger = function () {
						if (outerScope.selectAll) {
							outerScope.selectAll = false;
						}
						else {
							for (var cat in $scope.profileSelectedCategories) {
								$scope.profileSelectedCategories[cat] = false;
								$scope.assembleCategories(cat);
							}
							outerScope.selectAll = true;
							$scope.myCategories = [];
						}
					};
					
					$scope.assembleCategories = function (category, callback) {
						//category was previously selected and now unselected, delete listed children
						if (!callback) {
							//tracking source of function call (checkbox click or recursive function
							$scope.loading = true;
						}
						
						if ($scope.profileSelectedCategories[category] === false) {
							for (var cat in $scope.categoriesList) {
								if ($scope.categoriesList[cat].v === category) {
									var parentLabel = $scope.categoriesList[cat].l;
									break;
								}
							}
							var temp, index;
							for (var i = 0; i < $scope.categoriesList.length; i++) {
								temp = $scope.categoriesList[i].l;
								index = 0;
								while (index !== -1) {
									index = temp.lastIndexOf(" /");
									temp = temp.slice(0, index);
									if (temp === parentLabel) {
										if ($scope.profileSelectedCategories[$scope.categoriesList[i].v] === true) {
											$scope.profileSelectedCategories[$scope.categoriesList[i].v] = false;
										}
										$scope.categoriesList.splice(i, 1);
										i--;
										break;
									}
								}
							}
							
							if (callback) {
								return callback();
							}
							else {
								$timeout(function () {
									$scope.loading = false;
								}, 500);
							}
						}
						else {
							//get children of checked category here////
							$scope.category = category;
							
							renderFeed(function () {
								$scope.myCategories = getCategoriesArr($scope.profileSelectedCategories);
								
								if (callback) {
									return callback();
								}
								else {
									$timeout(function () {
										$scope.loading = false;
									}, 500);
								}
							});
						}
					};
					
					$scope.onSubmit = function () {
						outerScope.$parent.displayAlert('success', 'Submit');
						
						if (!$scope.promoCode.startDate) {
							$scope.message.danger = true;
							return $scope.message.text = translation.pleaseSelectStartDate[LANG];
						}
						
						$scope.promoCode.serial = [];
						$scope.tempo.availableProducts.forEach(function (each) {
							if (each.isSelected) {
								$scope.promoCode.serial.push(each.serial);
							}
						});
						
						var postData = {
							category: {},
							description: $scope.promoCode.description,
							codeValue : $scope.promoCode.codeValue,
							config : $scope.promoCode.config,
							discount: $scope.promoCode.discount,
							serial: $scope.promoCode.serial,
							startDate: $scope.promoCode.startDate,
							endDate: $scope.promoCode.endDate,
							status: $scope.promoCode.status,
							type: $scope.promoCode.type
						};
						
						if (!$scope.selectAllCategories && $scope.profileSelectedCategories) {
							postData.category.include = getCategoriesArr($scope.profileSelectedCategories);
						}
						overlayLoading.show();
						
						var options = {
							"method": "send",
							"routeName": "/knowledgebase/owner/promoCodes",
							"params": {
								"__env": __env
							},
							"data": {
								promoCode: postData
							}
						};
						if (newData._id) {
							options.method = "put";
							options.routeName = "/knowledgebase/owner/promoCodes/" + newData._id;
							delete options.data.promoCode._id;
						}
						getSendDataFromServer(outerScope, ngDataApi, options, function (error) {
							overlayLoading.hide();
							if (error) {
								$scope.message.danger = true;
								$scope.message.text = error.message;
							}
							else {
								outerScope.$parent.displayAlert('success', translation.saleAddedSuccessfully[LANG]);
								$modalInstance.close();
								outerScope.listAll();
							}
						});
					};
					
					$scope.changeType = function () {
						$scope.promoCode.discount.type = $scope.promoCode.type
					};
					
					$scope.closeModal = function () {
						$modalInstance.close();
					};
					
					//// on init
					buildTree();
				}
			});
		};
		
		$scope.changeStatus = function (data) {
			var status = (data.status === 'active') ? 'inactive' : 'active';
			var opts;
			opts = {
				"method": "put",
				"routeName": "/knowledgebase/owner/promoCodes/" + data._id + "/status",
				"params": {
					'status': status,
					'code': $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			
			overlayLoading.show();
			getSendDataFromServer($scope, ngDataApi, opts, function (error) {
				overlayLoading.hide();
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.$parent.displayAlert('success', 'Status Changed Successfully.');
					$scope.listAll();
				}
			});
		};
		
		$scope.listAll = function () {
			var opts = {
				"routeName": "/knowledgebase/owner/promoCodes",
				"method": "get",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			
			promoCodeModuleProdSrv.getEntriesFromApi($scope, opts, function (error, response) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					promoCodeModuleProdSrv.printGrid($scope, response.records);
				}
			});
			
		};
		
		$scope.deletePromoCode = function (data) {
			var config = {
				"method": "del",
				"routeName": "/knowledgebase/owner/promoCodes/" + data._id,
				"params": {
					'code': $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			
			getSendDataFromServer($scope, ngDataApi, config, function (error) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.$parent.displayAlert('success', 'Promo Code Deleted Successfully.');
					$scope.listAll();
				}
			});
			
		};
		
		// on init, call list
		$scope.listAll();
		
		injectFiles.injectCss(ModuleProdMpLocation + "/merchants.css");
	}]);
