"use strict";
var paymentMethodsConfig = {
	permissions: {
		'list': ['order', '/owner/paymentMethods/list'],
		'add': ['order', '/owner/paymentMethods/add'],
		'edit': ['order', '/owner/paymentMethods/edit'],
		'changeStatus': ['order', '/owner/paymentMethods/changeStatus'],
		'delete': ['order', '/owner/paymentMethods/delete']
	},
	"form": {
		"entries": [
			{
				'name': 'driver',
				'label': translation.driverName[LANG],
				'type': 'select',
				'value': [],
				'tooltip': translation.driverNameToolTip[LANG],
				'required': true
			},
			{
				'name': 'label',
				'label': translation.driverLabel[LANG],
				'type': 'text',
				'value': '',
				'tooltip': translation.driverLabelToolTip[LANG],
				'placeholder': translation.driverLabelPlaceHd[LANG],
				'required': true
			},
			{
				'name': 'active',
				'label': translation.active[LANG],
				'type': 'radio',
				'value': [{'v': true, 'l': 'Yes', 'selected': true}, {'v': false, 'l': 'No'}],
				'required': true
			},
			{
				"name": "config",
				"label": translation.configuration[LANG],
				"type": "group",
				"entries": []
			}
		]
	}
};