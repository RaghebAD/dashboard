"use strict";
var ModuleProdDrvLocation = uiModuleProd + '/drivers';

var ModuleProdDrTranslation = {
	"shippingMethods": {
		"ENG": "Shipping Methods",
		"FRA": "Méthodes de Livraison"
	},
	"shippingMethod": {
		"ENG": "Shipping Method",
		"FRA": "Méthode de Livraison"
	},
	"taxCalculator": {
		"ENG": "Tax Calculator",
		"FRA": "Tax Calculator"
	},
	"addNewTaxRate": {
		"ENG": "Add New Tax Rate",
		"FRA": "Add New Tax Rate"
	},
	"addressValidation": {
		"ENG": "Address Validation",
		"FRA": "Validation de l’adresse"
	},
	"currencyConverter": {
		"ENG": "Currency Converter",
		"FRA": "Currency Converter"
	},
	"deliveryMethods": {
		"ENG": "Delivery Methods",
		"FRA": "Delivery Methods"
	},
	"deliveryMethod": {
		"ENG": "Delivery Method",
		"FRA": "Delivery Method"
	},
	"addDeliveryInterval": {
		"ENG": "Add New Delivery Interval",
		"FRA": "Add New Delivery Interval"
	},
	"paymentMethods": {
		"ENG": "Payment Methods",
		"FRA": "Méthodes de Paiement"
	},
	"paymentMethod": {
		"ENG": "Payment Method",
		"FRA": "Méthode de Paiement"
	},
	"pickupMethods": {
		"ENG": "Pickup Methods",
		"FRA": "Pickup Methods"
	},
	"pickupMethod": {
		"ENG": "Pickup Method",
		"FRA": "Pickup Method"
	}
};

for (var attrname in ModuleProdDrTranslation) {
	translation[attrname] = ModuleProdDrTranslation[attrname];
}

var driversModuleProdNav = [
	{
		//main information
		'id': 'currencyConverter',
		'label': translation.currencyConverter[LANG],
		'url': '#/currencyConverter',
		'scripts': [ModuleProdDrvLocation + '/currencyConverter/service.js', ModuleProdDrvLocation + '/currencyConverter/controller.js', ModuleProdDrvLocation + '/currencyConverter/config.js'],
		'tplPath': ModuleProdDrvLocation + '/currencyConverter/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/currencyConverter/list'
		},
		//menu & tracker information
		'icon': 'calculator',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	}
];

navigation = navigation.concat(driversModuleProdNav);

var driversNav_BU = [
	{
		//main information
		'id': 'addressValidation',
		'label': translation.addressValidation[LANG],
		'url': '#/addressValidation',
		'scripts': [ModuleProdDrvLocation + '/addressValidation/service.js', ModuleProdDrvLocation + '/addressValidation/controller.js', ModuleProdDrvLocation + '/addressValidation/config.js'],
		'tplPath': ModuleProdDrvLocation + '/addressValidation/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/addressValidation/list'
		},
		//menu & tracker information
		'icon': 'address-book',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'currencyConverter',
		'label': translation.currencyConverter[LANG],
		'url': '#/currencyConverter',
		'scripts': [ModuleProdDrvLocation + '/currencyConverter/service.js', ModuleProdDrvLocation + '/currencyConverter/controller.js', ModuleProdDrvLocation + '/currencyConverter/config.js'],
		'tplPath': ModuleProdDrvLocation + '/currencyConverter/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/currencyConverter/list'
		},
		//menu & tracker information
		'icon': 'calculator',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'paymentMethods',
		'label': translation.paymentMethods[LANG],
		'url': '#/paymentMethods',
		'scripts': [ModuleProdDrvLocation + '/paymentMethods/service.js', ModuleProdDrvLocation + '/paymentMethods/controller.js', ModuleProdDrvLocation + '/paymentMethods/config.js'],
		'tplPath': ModuleProdDrvLocation + '/paymentMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/paymentMethods/list'
		},
		//menu & tracker information
		'icon': 'credit-card',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'shippingMethods',
		'label': translation.shippingMethods[LANG],
		'url': '#/shippingMethods',
		'scripts': [ModuleProdDrvLocation + '/shippingMethods/config.js', ModuleProdDrvLocation + '/shippingMethods/service.js', ModuleProdDrvLocation + '/shippingMethods/controller.js'],
		'tplPath': ModuleProdDrvLocation + '/shippingMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/shippingMethods/list'
		},
		//menu & tracker information
		'icon': 'airplane',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'deliveryMethods',
		'label': translation.deliveryMethods[LANG],
		'url': '#/deliveryMethods',
		'scripts': [ModuleProdDrvLocation + '/deliveryMethods/service.js', ModuleProdDrvLocation + '/deliveryMethods/controller.js', ModuleProdDrvLocation + '/deliveryMethods/config.js'],
		'tplPath': ModuleProdDrvLocation + '/deliveryMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/deliveryMethods/list'
		},
		//menu & tracker information
		'icon': 'truck',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'taxValidation',
		'label': translation.taxCalculator[LANG],
		'url': '#/taxValidation',
		'scripts': [ModuleProdDrvLocation + '/taxValidation/service.js', ModuleProdDrvLocation + '/taxValidation/controller.js', ModuleProdDrvLocation + '/taxValidation/config.js'],
		'tplPath': ModuleProdDrvLocation + '/taxValidation/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/taxValidation/list'
		},
		//menu & tracker information
		'icon': 'coin-dollar',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'pickupMethods',
		'label': translation.pickupMethods[LANG],
		'url': '#/pickupMethods',
		'scripts': [ModuleProdDrvLocation + '/pickupMethods/config.js', ModuleProdDrvLocation + '/pickupMethods/service.js', ModuleProdDrvLocation + '/pickupMethods/controller.js'],
		'tplPath': ModuleProdDrvLocation + '/pickupMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/pickupMethods/list'
		},
		//menu & tracker information
		'icon': 'home2',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	}
];