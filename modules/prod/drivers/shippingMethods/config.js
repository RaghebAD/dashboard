"use strict";
var shippingMethodsConfig = {
	permissions: {
		'list': ['order', '/owner/shippingMethods/list'],
		'add': ['order', '/owner/shippingMethods/add'],
		'edit': ['order', '/owner/shippingMethods/edit'],
		'changeStatus': ['order', '/owner/shippingMethods/changeStatus'],
		'delete': ['order', '/owner/shippingMethods/delete']
	},
	"form": {
		"entries": [
			{
				'name': 'driver',
				'label': translation.driverName[LANG],
				'type': 'select',
				'value': [],
				'tooltip': translation.driverNameToolTip[LANG],
				'required': true
			},
			{
				'name': 'label',
				'label': translation.driverLabel[LANG],
				'type': 'text',
				'value': '',
				'tooltip': translation.driverLabelToolTip[LANG],
				'placeholder': translation.driverLabelPlaceHd[LANG],
				'required': true
			},
			{
				'name': 'active',
				'label': translation.active[LANG],
				'type': 'radio',
				'value': [
					{
						'v': true,
						'l': translation.yes[LANG],
						'selected': true
					},
					{
						'v': false,
						'l': translation.no[LANG]
					}
				],
				'required': true
			},
			{
				"name": "config",
				"label": translation.configuration[LANG],
				"type": "group",
				"entries": []
			}
		]
	}
};