"use strict";
var deliveryMethodsApp = soajsApp.components;
deliveryMethodsApp.controller('deliveryMethodsModuleDevCtrl', ['$scope', '$modal', 'ngDataApi', '$cookies', 'injectFiles', '$timeout', 'deliveryMethodsSrv', function ($scope, $modal, ngDataApi, $cookies, injectFiles, $timeout, deliveryMethodsSrv) {
	$scope.$parent.isUserLoggedIn();

	//define the permissions
	var permissions = deliveryMethodsConfig.permissions;

	$scope.access = {};
	//call the method and compare the permissions with the ACL
	//allowed permissions are then stored in scope.access
	constructModulePermissions($scope, $scope.access, permissions);

	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}

	$scope.drivers = {};

	//function that lists the Delivery Methods in a grid
	$scope.listEntries = function () {
		$scope.noAdd = false;

		var opts = {
			"routeName": "/order/owner/deliveryMethods/list",
			"method": "get",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		deliveryMethodsSrv.getEntriesFromAPI($scope, opts, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.drivers = response.drivers;
				if (Object.keys(response.drivers).length === response.list.length) {
					$scope.noAdd = true;
				}

				deliveryMethodsSrv.printGrid($scope, response.list);
			}
		});
	};

	//function that prints one data record to the console
	$scope.viewEntry = function (oneDataRecord) {
		$modal.open({
			templateUrl: "infoBox.html",
			size: 'dialog',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewingOne[LANG] + translation.deliveryMethod[LANG];
				$scope.data = oneDataRecord;
				fixBackDrop();
				setTimeout(function () {
					highlightMyCode();
				}, 500);
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};

	function removeDelivery(id, data, form) {
		var number = id.replace("removeDelivery", "");
		for (var e = form.entries.length - 1; e >= 0; e--) {
			if (form.entries[e].type === 'group') {
				for (var i = form.entries[e].entries.length - 1; i >= 0; i--) {

					if (form.entries[e].entries[i].name.indexOf(number, form.entries[e].entries[i].name.length - 1) !== -1) {
						delete form.formData[form.entries[e].entries[i].name];
						form.entries[e].entries.splice(i, 1);
					}
				}
				if (form.entries[e].entries.length === 0) {
					form.entries.splice(e, 1);
				}
			}
		}
	}

	$scope.addEntry = function () {
		var formFields = angular.copy(deliveryMethodsConfig.form.entries);
		resetFormFields();
		var oneClone;
		var count = 0;

		//push into formFields the config.from returned per driver
		formFields[0].onAction = function (id, data, form) {
			form.entries[3].entries = [];
			if (!oneClone) {
				oneClone = angular.copy($scope.drivers[data].entries);
			}
			$scope.drivers[data].entries.forEach(function (oneEntry) {
				if (oneEntry.type === 'group') {
					oneEntry.entries.forEach(function (oneSubEntry) {
						oneSubEntry.name = angular.copy(oneSubEntry.name.replace("%count%", count));
						if (oneSubEntry.type === 'html') {
							oneSubEntry.value = oneSubEntry.value.replace("%count%", count);
							oneSubEntry["onAction"] = removeDelivery;
						}
					});
					form.entries = form.entries.concat(oneEntry);
				}
				else {
					oneEntry.name = angular.copy(oneEntry.name.replace("%count%", count));
					if (oneEntry.type === 'html') {
						oneEntry.value = oneEntry.value.replace("%count%", count);
						oneEntry["onAction"] = removeDelivery;
					}
					form.entries[3].entries.push(oneEntry);
				}
			});
			count++;
		};

		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'addDeliveryMethods',
			'label': translation.addDeliveryMethod[LANG],
			'actions': [
				{
					'type': 'button',
					'label': translation.addDeliveryInterval[LANG],
					'btn': 'success',
					'action': function () {
						var cloneAgain = angular.copy(oneClone);
						cloneAgain.forEach(function (oneEntry) {
							if (oneEntry.type === 'group') {
								oneEntry.entries.forEach(function (oneSubEntry) {
									oneSubEntry.name = angular.copy(oneSubEntry.name.replace("%count%", count));
									if (oneSubEntry.type === 'html') {
										oneSubEntry.value = oneSubEntry.value.replace("%count%", count);
										oneSubEntry["onAction"] = removeDelivery;
									}
								});
								$scope.form.entries = $scope.form.entries.concat(oneEntry);
							}
							else {
								oneEntry.name = angular.copy(oneEntry.name.replace("%count%", count));
								if (oneEntry.type === 'html') {
									oneEntry.value = oneEntry.value.replace("%count%", count);
									oneEntry["onAction"] = removeDelivery;
								}
								if (oneEntry.name.indexOf("%count%") !== -1) {
									$scope.form.entries[3].entries.push(oneEntry);
								}
							}
						});
						count++;
					}
				},
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formUI) {
						var formData = angular.copy(formUI);

						var postData = {
							config: {}
						};

						var predefined = [];
						deliveryMethodsConfig.form.entries.forEach(function (oneField) {
							predefined.push(oneField.name);
						});

						for (var i in formData) {
							if (predefined.indexOf(i) !== -1) {
								postData[i] = formData[i];
								delete formData[i];
							}
						}

						var status;
						if ($scope.drivers[postData.driver].customMapping) {
							var customMapping = new Function("formData", "postData", "count", $scope.drivers[postData.driver].customMapping);
							status = customMapping(formData, postData, count);
						}
						if (status === false || typeof(status) === "string") {
							if (typeof (status) === 'string') {
								$scope.form.displayAlert('danger', status);
							}
							return false;
						}

						getSendDataFromServer($scope, ngDataApi, {
							"method": "send",
							"routeName": "/order/owner/deliveryMethods/add",
							"params": {
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						}, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.code, true, 'order', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.methodAddedSuccessfully[LANG]);
								$scope.modalInstance.close();
								$scope.form.formData = {};
								$scope.listEntries();
							}
						});
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);

		//custom function to reset form fields
		function resetFormFields() {
			var myArray = formFields[0];
			myArray.value = [];

			var driverNames = Object.keys($scope.drivers);
			driverNames.forEach(function (oneDriverName) {

				if ($scope.grid.filteredRows.length === 0) {
					myArray.value.push({
						"v": oneDriverName,
						"l": oneDriverName
					});
				}
				else {
					for (var i = 0; i < $scope.grid.filteredRows.length; i++) {

						//skip existing drivers
						if ($scope.grid.filteredRows[i].driver === oneDriverName) {
							continue;
						}

						myArray.value.push({
							"v": oneDriverName,
							"l": oneDriverName
						});
					}
				}
			});
		}
	};

	$scope.editEntry = function (data) {
		var formFields = angular.copy(deliveryMethodsConfig.form.entries);
		var oneClone = angular.copy($scope.drivers[data.driver].entries);
		var count = Object.keys(data.config.intervals).length;
		resetFormFields(data);

		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'editDeliveryMethods',
			'label': translation.edit[LANG] + ' ' + translation.deliveryMethod[LANG],
			'data': data,
			'actions': [
				{
					'type': 'button',
					'label': translation.addDeliveryInterval[LANG],
					'btn': 'success',
					'action': function () {
						var cloneAgain = angular.copy(oneClone);
						cloneAgain.forEach(function (oneEntry) {
							if (oneEntry.type === 'group') {
								oneEntry.entries.forEach(function (oneSubEntry) {
									oneSubEntry.name = angular.copy(oneSubEntry.name.replace("%count%", count));
									if (oneSubEntry.type === 'html') {
										oneSubEntry.value = oneSubEntry.value.replace("%count%", count);
										oneSubEntry["onAction"] = removeDelivery;
									}
								});
								$scope.form.entries = $scope.form.entries.concat(oneEntry);
							}
							else {
								oneEntry.name = angular.copy(oneEntry.name.replace("%count%", count));
								if (oneEntry.type === 'html') {
									oneEntry.value = oneEntry.value.replace("%count%", count);
									oneEntry["onAction"] = removeDelivery;
								}
								if (oneEntry.name.indexOf("%count%") !== -1) {
									$scope.form.entries[3].entries.push(oneEntry);
								}
							}
						});
						count++;
					}
				},
				{
					'type': 'submit',
					'label': translation.save[LANG],
					'btn': 'primary',
					'action': function (formUI) {
						var formData = angular.copy(formUI);

						var postData = {
							config: {}
						};

						var predefined = [];
						deliveryMethodsConfig.form.entries.forEach(function (oneField) {
							predefined.push(oneField.name);
						});

						for (var i in formData) {
							if (predefined.indexOf(i) !== -1) {
								postData[i] = formData[i];
								delete formData[i];
							}
						}

						var status;
						if ($scope.drivers[postData.driver].customMapping) {
							var customMapping = new Function("formData", "postData", "count", $scope.drivers[postData.driver].customMapping);
							status = customMapping(formData, postData, count);
						}

						if (status === false || typeof(status) === "string") {
							if (typeof (status) === 'string') {
								$scope.form.displayAlert('danger', status);
							}
							return false;
						}

						getSendDataFromServer($scope, ngDataApi, {
							"method": "send",
							"routeName": "/order/owner/deliveryMethods/edit",
							"params": {
								'id': data._id,
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						}, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.code, true, 'order', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.methodUpdatedSuccessfully[LANG]);
								$scope.modalInstance.close();
								$scope.form.formData = {};
								$scope.listEntries();
							}
						});
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);

		function resetFormFields(data) {
			var myArray = formFields[0];
			myArray.value = data.driver;
			myArray.type = "readonly";

			formFields[3].entries = [];
			if (!oneClone) {
				oneClone = angular.copy($scope.drivers[data.driver].entries);
			}

			$scope.drivers[data.driver].entries.forEach(function (oneEntry) {
				if ($scope.drivers[data.driver].reverseMapping) {
					var reverseMapping = new Function("oneEntry", "data", "formFields", "count", "removeDelivery", "pushCharacter", $scope.drivers[data.driver].reverseMapping);
					reverseMapping(oneEntry, data, formFields, count, removeDelivery, pushCharacter);
				}
			});
		}
	};

	function pushCharacter(str, idx, rem, s) {
		if (str.length < 4) {
			str = "0" + str;
		}
		return (str.slice(0, idx) + s + str.slice(idx + Math.abs(rem)));
	}

	$scope.deleteEntry = function (data) {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/order/owner/deliveryMethods/delete",
			"params": {
				'id': data._id,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.methodDeletedSuccessfully[LANG]);
				$scope.listEntries();
			}
		});

	};

	$scope.changeEntryStatus = function (data) {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/order/owner/deliveryMethods/changeStatus",
			"params": {
				'id': data._id,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.statusChangedSuccessfully[LANG]);
				$scope.listEntries();
			}
		});

	};

	//if scope.access.list is allowed, call listEntries
	if ($scope.access.list) {
		$scope.listEntries();
	}
	injectFiles.injectCss(ModuleDevDrvLocation + "/deliveryMethods.css");
}]);