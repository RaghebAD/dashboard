"use strict";
var currencyConverterApp = soajsApp.components;
currencyConverterApp.controller('currencyConverterModuleDevCtrl', ['$scope', '$modal', 'ngDataApi', '$cookies', 'injectFiles', '$timeout', 'currencyConverterSrv', function ($scope, $modal, ngDataApi, $cookies, injectFiles, $timeout, currencyConverterSrv) {
	$scope.$parent.isUserLoggedIn();

	//define the permissions
	var permissions = currencyConverterConfig.permissions;

	$scope.access = {};
	//call the method and compare the permissions with the ACL
	//allowed permissions are then stored in scope.access
	constructModulePermissions($scope, $scope.access, permissions);

	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}
	$scope.drivers = {};

	//function that lists the Delivery Methods in a grid
	$scope.listEntries = function () {
		$scope.noAdd = false;

		var opts = {
			"routeName": "/order/owner/currencyConverter/list",
			"method": "get",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		currencyConverterSrv.getEntriesFromAPI($scope, opts, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.drivers = response.drivers;
				if (Object.keys(response.drivers).length === response.list.length) {
					$scope.noAdd = true;
				}

				currencyConverterSrv.printGrid($scope, response.list);
			}
		});
	};

	//function that prints one data record to the console
	$scope.viewEntry = function (oneDataRecord) {
		$modal.open({
			templateUrl: "infoBox.html",
			size: 'dialog',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewingOne[LANG] + translation.currencyConverter[LANG];
				$scope.data = oneDataRecord;
				fixBackDrop();
				setTimeout(function () {
					highlightMyCode()
				}, 500);
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};

	$scope.addEntry = function () {
		var formFields = angular.copy(currencyConverterConfig.form.entries);
		resetFormFields();

		//push into formFields the config.from returned per driver
		formFields[0].onAction = function (id, data, form) {
			form.entries[3].entries = [];
			$scope.drivers[data].entries.forEach(function (oneEntry) {
				form.entries[3].entries.push(oneEntry);
			});
		};

		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'addCurrencyConverter',
			'label': translation.addCurrencyConverter[LANG],
			'actions': [
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var postData = {
							config: {}
						};

						var predefined = [];
						currencyConverterConfig.form.entries.forEach(function (oneField) {
							predefined.push(oneField.name);
						});

						for (var i in formData) {
							if (predefined.indexOf(i) !== -1) {
								postData[i] = formData[i];
							}
							else {
								postData.config[i] = formData[i];
							}
						}

						getSendDataFromServer($scope, ngDataApi, {
							"method": "send",
							"routeName": "/order/owner/currencyConverter/add",
							"params": {
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						}, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.code, true, 'order', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.methodAddedSuccessfully[LANG]);
								$scope.modalInstance.close();
								$scope.form.formData = {};
								$scope.listEntries();
							}
						});
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);

		//custom function to reset form fields
		function resetFormFields() {
			var myArray = formFields[0];
			myArray.value = [];

			var driverNames = Object.keys($scope.drivers);
			driverNames.forEach(function (oneDriverName) {

				if ($scope.grid.filteredRows.length === 0) {
					myArray.value.push({
						"v": oneDriverName,
						"l": oneDriverName
					});
				}
				else {
					for (var i = 0; i < $scope.grid.filteredRows.length; i++) {

						//skip existing drivers
						if ($scope.grid.filteredRows[i].driver === oneDriverName) {
							continue;
						}

						myArray.value.push({
							"v": oneDriverName,
							"l": oneDriverName
						});
					}
				}
			});
		}
	};

	$scope.editEntry = function (data) {
		var formFields = angular.copy(currencyConverterConfig.form.entries);
		resetFormFields(data);

		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'editcurrencyConverter',
			'label': translation.edit[LANG] + ' ' + translation.currencyConverter[LANG],
			'data': data,
			'actions': [
				{
					'type': 'submit',
					'label': translation.save[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var postData = {
							config: {}
						};

						var predefined = [];
						currencyConverterConfig.form.entries.forEach(function (oneField) {
							predefined.push(oneField.name);
						});

						for (var i in formData) {
							if (predefined.indexOf(i) !== -1) {
								postData[i] = formData[i];
							}
							else {
								postData.config[i] = formData[i];
							}
						}

						getSendDataFromServer($scope, ngDataApi, {
							"method": "send",
							"routeName": "/order/owner/currencyConverter/edit",
							"params": {
								'id': data._id,
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						}, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.code, true, 'order', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.methodUpdatedSuccessfully[LANG]);
								$scope.modalInstance.close();
								$scope.form.formData = {};
								$scope.listEntries();
							}
						});
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options, function () {
			////push into formFields the config.from returned per driver
			$scope.form.entries[3].entries = [];
			$scope.drivers[data.driver].entries.forEach(function (oneEntry) {
				for (var i in data.config) {
					if (i === oneEntry.name) {
						if (Array.isArray(oneEntry.value)) {
							oneEntry.value.forEach(function (sV) {
								if (Array.isArray(data.config[i])) {
									if (data.config[i].indexOf(sV.v) !== -1) {
										sV.selected = true;
										$scope.form.formData[i] = data.config[i];
									}
								}
								else if (data.config[i].toString() === sV.v.toString()) {
									sV.selected = true;
									$scope.form.formData[i] = data.config[i];
								}
							})
						}
						else {
							oneEntry.value = data.config[i];
							$scope.form.formData[i] = data.config[i];
						}
					}
				}
				$scope.form.entries[3].entries.push(oneEntry);
			});
		});

		//custom function to reset form fields
		function resetFormFields(data) {
			var myArray = formFields[0];
			myArray.value = data.driver;
			myArray.type = "readonly";
		}
	};

	$scope.deleteEntry = function (data) {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/order/owner/currencyConverter/delete",
			"params": {
				'id': data._id,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.methodDeletedSuccessfully[LANG]);
				$scope.listEntries();
			}
		});

	};

	$scope.changeEntryStatus = function (data) {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/order/owner/currencyConverter/changeStatus",
			"params": {
				'id': data._id,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.statusChangedSuccessfully[LANG]);
				$scope.listEntries();
			}
		});

	};

	//if scope.access.list is allowed, call listEntries
	if ($scope.access.list) {
		$scope.listEntries();
	}

}]);