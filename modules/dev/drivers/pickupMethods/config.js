"use strict";
var pickupMethodsConfig = {
	permissions: {
		'list': ['order', '/owner/pickupMethods/list'],
		'add': ['order', '/owner/pickupMethods/add'],
		'edit': ['order', '/owner/pickupMethods/edit'],
		'changeStatus': ['order', '/owner/pickupMethods/changeStatus'],
		'delete': ['order', '/owner/pickupMethods/delete']
	},
	"form": {
		"entries": [
			{
				'name': 'driver',
				'label': translation.driverName[LANG],
				'type': 'select',
				'value': [],
				'tooltip': translation.driverNameToolTip[LANG],
				'required': true
			},
			{
				'name': 'label',
				'label': translation.driverLabel[LANG],
				'type': 'text',
				'value': '',
				'tooltip': translation.driverLabelToolTip[LANG],
				'placeholder': translation.driverLabelPlaceHd[LANG],
				'required': true
			},
			{
				'name': 'active',
				'label': translation.active[LANG],
				'type': 'radio',
				'value': [
					{'v': true, 'l': translation.yes[LANG], 'selected': true},
					{'v': false, 'l': translation.no[LANG]}
				],
				'required': true
			},
			{
				"name": "config",
				"label": translation.configuration[LANG],
				"type": "group",
				"entries": []
			}
		]
	}
};