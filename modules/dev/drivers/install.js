"use strict";
var ModuleDevDrvLocation = uiModuleDev + '/drivers';

var ModuleDevDrTranslation = {
	"shippingMethods": {
		"ENG": "Shipping Methods",
		"FRA": "Méthodes de Livraison"
	},
	"shippingMethod": {
		"ENG": "Shipping Method",
		"FRA": "Méthode de Livraison"
	},
	"taxCalculator": {
		"ENG": "Tax Calculator",
		"FRA": "Tax Calculator"
	},
	"addNewTaxRate": {
		"ENG": "Add New Tax Rate",
		"FRA": "Add New Tax Rate"
	},
	"addressValidation": {
		"ENG": "Address Validation",
		"FRA": "Validation de l’adresse"
	},
	"currencyConverter": {
		"ENG": "Currency Converter",
		"FRA": "Currency Converter"
	},
	"deliveryMethods": {
		"ENG": "Delivery Methods",
		"FRA": "Delivery Methods"
	},
	"deliveryMethod": {
		"ENG": "Delivery Method",
		"FRA": "Delivery Method"
	},
	"addDeliveryInterval": {
		"ENG": "Add New Delivery Interval",
		"FRA": "Add New Delivery Interval"
	},
	"paymentMethods": {
		"ENG": "Payment Methods",
		"FRA": "Méthodes de Paiement"
	},
	"paymentMethod": {
		"ENG": "Payment Method",
		"FRA": "Méthode de Paiement"
	},
	"pickupMethods": {
		"ENG": "Pickup Methods",
		"FRA": "Pickup Methods"
	},
	"pickupMethod": {
		"ENG": "Pickup Method",
		"FRA": "Pickup Method"
	}
};

for (var attrname in ModuleDevDrTranslation) {
	translation[attrname] = ModuleDevDrTranslation[attrname];
}

var driversModuleDevNav = [
	{
		//main information
		'id': 'currencyConverter',
		'label': translation.currencyConverter[LANG],
		'url': '#/currencyConverter',
		'scripts': [ModuleDevDrvLocation + '/currencyConverter/service.js', ModuleDevDrvLocation + '/currencyConverter/controller.js', ModuleDevDrvLocation + '/currencyConverter/config.js'],
		'tplPath': ModuleDevDrvLocation + '/currencyConverter/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/currencyConverter/list'
		},
		//menu & tracker information
		'icon': 'calculator',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	}
];

navigation = navigation.concat(driversModuleDevNav);

var driversNav_BU = [
	{
		//main information
		'id': 'addressValidation',
		'label': translation.addressValidation[LANG],
		'url': '#/addressValidation',
		'scripts': [ModuleDevDrvLocation + '/addressValidation/service.js', ModuleDevDrvLocation + '/addressValidation/controller.js', ModuleDevDrvLocation + '/addressValidation/config.js'],
		'tplPath': ModuleDevDrvLocation + '/addressValidation/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/addressValidation/list'
		},
		//menu & tracker information
		'icon': 'address-book',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'currencyConverter',
		'label': translation.currencyConverter[LANG],
		'url': '#/currencyConverter',
		'scripts': [ModuleDevDrvLocation + '/currencyConverter/service.js', ModuleDevDrvLocation + '/currencyConverter/controller.js', ModuleDevDrvLocation + '/currencyConverter/config.js'],
		'tplPath': ModuleDevDrvLocation + '/currencyConverter/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/currencyConverter/list'
		},
		//menu & tracker information
		'icon': 'calculator',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'paymentMethods',
		'label': translation.paymentMethods[LANG],
		'url': '#/paymentMethods',
		'scripts': [ModuleDevDrvLocation + '/paymentMethods/service.js', ModuleDevDrvLocation + '/paymentMethods/controller.js', ModuleDevDrvLocation + '/paymentMethods/config.js'],
		'tplPath': ModuleDevDrvLocation + '/paymentMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/paymentMethods/list'
		},
		//menu & tracker information
		'icon': 'credit-card',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'shippingMethods',
		'label': translation.shippingMethods[LANG],
		'url': '#/shippingMethods',
		'scripts': [ModuleDevDrvLocation + '/shippingMethods/config.js', ModuleDevDrvLocation + '/shippingMethods/service.js', ModuleDevDrvLocation + '/shippingMethods/controller.js'],
		'tplPath': ModuleDevDrvLocation + '/shippingMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/shippingMethods/list'
		},
		//menu & tracker information
		'icon': 'airplane',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'deliveryMethods',
		'label': translation.deliveryMethods[LANG],
		'url': '#/deliveryMethods',
		'scripts': [ModuleDevDrvLocation + '/deliveryMethods/service.js', ModuleDevDrvLocation + '/deliveryMethods/controller.js', ModuleDevDrvLocation + '/deliveryMethods/config.js'],
		'tplPath': ModuleDevDrvLocation + '/deliveryMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/deliveryMethods/list'
		},
		//menu & tracker information
		'icon': 'truck',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'taxValidation',
		'label': translation.taxCalculator[LANG],
		'url': '#/taxValidation',
		'scripts': [ModuleDevDrvLocation + '/taxValidation/service.js', ModuleDevDrvLocation + '/taxValidation/controller.js', ModuleDevDrvLocation + '/taxValidation/config.js'],
		'tplPath': ModuleDevDrvLocation + '/taxValidation/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/taxValidation/list'
		},
		//menu & tracker information
		'icon': 'coin-dollar',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'pickupMethods',
		'label': translation.pickupMethods[LANG],
		'url': '#/pickupMethods',
		'scripts': [ModuleDevDrvLocation + '/pickupMethods/config.js', ModuleDevDrvLocation + '/pickupMethods/service.js', ModuleDevDrvLocation + '/pickupMethods/controller.js'],
		'tplPath': ModuleDevDrvLocation + '/pickupMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/pickupMethods/list'
		},
		//menu & tracker information
		'icon': 'home2',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	}
];