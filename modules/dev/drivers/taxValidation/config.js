"use strict";
var taxValidationModuleDevConfig = {
	permissions: {
		'list': ['order', '/owner/taxValidation/list'],
		'add': ['order', '/owner/taxValidation/add'],
		'edit': ['order', '/owner/taxValidation/edit'],
		'changeStatus': ['order', '/owner/taxValidation/changeStatus'],
		'delete': ['order', '/owner/taxValidation/delete']
	},
	"form": {
		"entries": [
			{
				'name': 'driver',
				'label': translation.driverName[LANG],
				'type': 'select',
				'value': [],
				'tooltip': translation.driverNameToolTip[LANG],
				'required': true
			},
			{
				'name': 'label',
				'label': translation.driverLabel[LANG],
				'type': 'text',
				'value': '',
				'tooltip': translation.driverLabelToolTip[LANG],
				'placeholder': translation.driverLabelPlaceHd[LANG],
				'required': true
			},
			{
				'name': 'active',
				'label': translation.active[LANG],
				'type': 'radio',
				'value': [
					{
						'v': true,
						'l': translation.yes[LANG],
						'selected': true
					}, 
					{
						'v': false,
						'l': translation.no[LANG]
					}
				],
				'required': true
			},
			{
				"name": "config",
				"label": translation.configuration[LANG],
				"type": "group",
				"class": "rates",
				"entries": []
			}
		]
	}
};