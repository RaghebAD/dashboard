"use strict";
var orderApp = soajsApp.components;

orderApp.controller('userInfoListDevCtrl', ['$scope', '$timeout', '$modal', '$cookies', '$routeParams', 'ngDataApi', 'injectFiles', function ($scope, $timeout, $modal, $cookies, $routeParams, ngDataApi, injectFiles) {
	$scope.$parent.isUserLoggedIn();
	$scope.ModuleDevOrdLocation = ModuleDevOrdLocation;
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, userInfoConfig.permissions);
	$scope.orderRows = [];
	$scope.startLimit = 0;
	$scope.totalCount = 10000;
	$scope.endLimit = userInfoConfig.grid.apiEndLimit;
	$scope.increment = userInfoConfig.grid.apiEndLimit;
	$scope.showNext = true;
	
	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}
	
	$scope.getAll = function (firsCall) {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/order/dashboard/userInfo/list",
			"params": {
				"start": $scope.startLimit,
				"limit": $scope.endLimit,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.totalCount = response.count;
				$scope.orderRows = response.records;
				
				if (firsCall) {
					if ($scope.totalCount <= $scope.endLimit) {
						$scope.showNext = false;
					}
					else {
						$scope.showNext = true;
					}
				}
				else {
					var nextLimit = $scope.startLimit + $scope.increment;
					if ($scope.totalCount <= nextLimit) {
						$scope.showNext = false;
					}
				}
				//response.records.forEach(function (user) {});
				
				var options = {
					grid: userInfoConfig.order.grid,
					data: response.records,
					defaultSortField: 'ts',
					defaultSortASC: true,
					left: [],
					top: []
				};
				options.left.push({
					'icon': 'search',
					'label': translation.viewItem[LANG],
					'handler': 'previewEntry'
				});
				if ($scope.access.view) {
					options.left.push({
						'label': translation.edit[LANG],
						'icon': 'pencil2',
						'handler': 'viewOneRecord'
					});
				}
				buildGrid($scope, options);
				
			}
		});
	};
	
	$scope.getAll(true);
	
	$scope.previewEntry = function (oneDataRecord) {
		var myScope = $scope;
		$modal.open({
			templateUrl: "infoBox.html",
			size: 'lg',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewRecord[LANG];
				$scope.record = oneDataRecord;
				$scope.ModuleDevOrdLocation = myScope.ModuleDevOrdLocation;
				fixBackDrop();
				setTimeout(function () {
					highlightMyCode()
				}, 500);
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};
	
	$scope.viewOneRecord = function (order) {
		var id = order._id;
		var path = '#/userInfo/view/' + id;
		$cookies.put("soajs_current_route", path.replace("#", ""));
		window.open(path, '_blank'); // in new tab
	};
	
	$scope.getPrev = function () {
		$scope.startLimit = $scope.startLimit - $scope.increment;
		if (0 <= $scope.startLimit) {
			$scope.getAll();
			$scope.showNext = true;
		}
		else {
			$scope.startLimit = 0;
		}
	};
	
	$scope.getNext = function () {
		var startLimit = $scope.startLimit + $scope.increment;
		if (startLimit < $scope.totalCount) {
			$scope.startLimit = startLimit;
			$scope.getAll();
		}
		else {
			$scope.showNext = false;
		}
		
	};
	injectFiles.injectCss(ModuleDevOrdLocation + "/userInfo.css");
}]);

orderApp.controller('userInfoViewDevCtrl', ['$scope', '$timeout', '$modal', '$routeParams', 'ngDataApi', 'injectFiles', function ($scope, $timeout, $modal, $routeParams, ngDataApi, injectFiles) {
	$scope.$parent.isUserLoggedIn();
	$scope.ModuleDevOrdLocation = ModuleDevOrdLocation;
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, userInfoConfig.permissions);
	$scope.record = false;
	
	$scope.getOrder = function () {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/order/dashboard/userInfo/view",
			"params": {
				"id": $routeParams.id,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.record = response;
			}
		});
	};
	
	$scope.getOrder();
	
	$scope.deleteMerchant = function (merchant) {
		overlayLoading.show();
		getSendDataFromServer($scope, ngDataApi, {
			"method": "send",
			"routeName": "/order/dashboard/userInfo/update",
			"params": {
				"id": $routeParams.id,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			},
			"data": {
				"operation": "clearMerchant",
				"key": merchant
			}
		}, function (error, response) {
			overlayLoading.hide();
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.record = response;
			}
		});
	};
	injectFiles.injectCss(ModuleDevOrdLocation + "/userInfo.css");
	
}]);