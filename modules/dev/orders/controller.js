"use strict";
var orderApp = soajsApp.components;

orderApp.controller('orderListDevCtrl', ['$scope', '$timeout', '$modal', '$cookies', '$routeParams', 'ngDataApi', function ($scope, $timeout, $modal, $cookies, $routeParams, ngDataApi) {
	$scope.$parent.isUserLoggedIn();
	$scope.ModuleDevOrdLocation = ModuleDevOrdLocation;
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, orderModuleDevConfig.permissions);
	$scope.orderRows = [];
	$scope.startLimit = 0;
	$scope.totalCount = 10000;
	$scope.endLimit = orderModuleDevConfig.grid.apiEndLimit;
	$scope.increment = orderModuleDevConfig.grid.apiEndLimit;
	$scope.showNext = true;

	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}

	$scope.getOrders = function (firsCall) {
		overlayLoading.show();
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/order/dashboard/allOrders",
			"params": {
				"start": $scope.startLimit,
				"limit": $scope.endLimit,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, response) {
			overlayLoading.hide();
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.totalCount = response.count;
				$scope.orderRows = response.records;

				if (firsCall) {
					if ($scope.totalCount <= $scope.endLimit) {
						$scope.showNext = false;
					}
					else {
						$scope.showNext = true;
					}
				}
				else {
					var nextLimit = $scope.startLimit + $scope.increment;
					if ($scope.totalCount <= nextLimit) {
						$scope.showNext = false;
					}
				}
				response.records.forEach(function (order) {
					order.macId = order.orderInfo.macId;
					order.orderInfo_orderNbr = order.orderInfo.orderNbr;
					// order.userInfo_firstName = order.userInfo.firstName;
					order.userInfo_businessName = order.userInfo.businessName;
					// order.orderInfo_status = order.orderInfo.status;
					order.orderInfo_totalAmount = order.payment.paymentAmount;
					
					order.cartLength = 0;
					if (order.products.items) {
						order.products.items.forEach(function (item) {
							order.cartLength = order.cartLength + item.quantity;
						});
					}
					order.tenantInfo = order.tenant.code;
				});

				var options = {
					grid: orderModuleDevConfig.order.grid,
					data: response.records,
					defaultSortField: 'ts',
					defaultSortASC: true,
					left: [],
					top: []
				};

				if ($scope.access.order.view) {
					options.left.push({
						'label': translation.viewRecord[LANG],
						'icon': 'file-text',
						'handler': 'viewOneOrder'
					});
				}
				buildGrid($scope, options);

			}
		});
	};

	$scope.getOrders(true);

	$scope.viewOneOrder = function (order) {
		var id = order._id;
		var path = '#/order/view/' + id;
		$cookies.put("soajs_current_route", path.replace("#", ""));
		window.open(path, '_blank'); // in new tab
	};

	$scope.getPrev = function () {
		$scope.startLimit = $scope.startLimit - $scope.increment;
		if (0 <= $scope.startLimit) {
			$scope.getOrders();
			$scope.showNext = true;
		}
		else {
			$scope.startLimit = 0;
		}
	};

	$scope.getNext = function () {
		var startLimit = $scope.startLimit + $scope.increment;
		if (startLimit < $scope.totalCount) {
			$scope.startLimit = startLimit;
			$scope.getOrders();
		}
		else {
			$scope.showNext = false;
		}

	};

}]);

orderApp.controller('orderViewDevCtrl', ['$scope', '$timeout', '$modal', '$routeParams', 'ngDataApi', 'injectFiles', function ($scope, $timeout, $modal, $routeParams, ngDataApi, injectFiles) {
	$scope.$parent.isUserLoggedIn();
	$scope.ModuleDevOrdLocation = ModuleDevOrdLocation;
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, orderModuleDevConfig.permissions);
	$scope.order = false;

	$scope.getOrder = function () {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/order/dashboard/order",
			"params": {
				"orderId": $routeParams.id,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.order = {};
				response.products.items.forEach(function (item) {
					item.titleObj = {
						default: "ENG",
						"ENG": "",
						"FRA": ""
					};
					if (item.title) {
						item.titleObj.ENG = item.title.en;
						item.titleObj.FRA = item.title.fr;
					}
				});
				$scope.order = response;
			}
		});
	};

	$scope.getOrder();

	injectFiles.injectCss(ModuleDevOrdLocation + '/order.css');

}]);