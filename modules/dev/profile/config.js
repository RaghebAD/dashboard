"use strict";
var nakbModuleDevConfig = {
	'permissions': {
		'feedProfiles': {
			'list': ['kbprofile', '/owner/feed/profiles/list', 'get'],
			'edit': ['kbprofile', '/owner/feed/profiles/:id', 'put']
		}
	},
	"feedProfiles": {
		grid: {
			recordsPerPageArray: [5, 10, 50, 100],
			'columns': [
				{'label': translation.name[LANG], 'field': 'name'},
				{'label': translation.active[LANG], 'field': 'active'},
				{'label': translation.minimumQuantity[LANG], 'field': 'min-qty'},
				{'label': translation.defaultLanguage[LANG], 'field': 'lang'},
				{'label': translation.numberOfCategories[LANG], 'field': 'cats'}
			],
			'leftActions': [],
			'topActions': [],
			'defaultSortField': '',
			'defaultLimit': 50
		}
	}
};