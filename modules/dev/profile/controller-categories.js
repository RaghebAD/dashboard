"use strict";
var knowledgebaseCategoriesApp = soajsApp.components;

knowledgebaseCategoriesApp.controller('knowledgebaseCategoriesModuleDevCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$routeParams', '$cookies', function ($scope, $modal, ngDataApi, injectFiles, $timeout, $routeParams, $cookies) {
	$scope.feedProfileId = $routeParams.id;
	$scope.ModuleDevKbaseLocation = ModuleDevKbaseLocation;
	
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, nakbModuleDevConfig.permissions);
	
	$scope.grid = {rows: []};
	
	$scope.formConfig = {
		"entries": [
			{
				"name": "translation",
				"label": translation.translation[LANG],
				"type": "group",
				"entries": []
			},
			{
				"name": "parents",
				"label": translation.parents[LANG],
				"type": "select",
				"required": true,
				"value": [
					{
						"v": "noParentSelected",
						"l": "-- No Parent --"
					}
				]
			},
			{
				"name": "classification",
				"label": translation.classification[LANG],
				"type": "multi-select",
				"value": []
			},
			{
				"name": "department",
				"label": translation.department[LANG],
				"type": "multi-select",
				"value": []
			},
			{
				"name": "filters",
				"label": translation.filters[LANG],
				"type": "group",
				"entries": []
			}
		]
	};
	
	$scope.listEntries = function () {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/categories",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				var categories = [];
				$scope.languages = response.languages;
				$scope.categories = response.categories;
				$scope.taxonomies = response.taxonomies;
				var category = Object.keys(response.categories);
				
				$scope.defaultLang = response.languages[0].id;
				response.languages.forEach(function (oneLang) {
					if (oneLang.selected) {
						$scope.defaultLang = oneLang.id;
					}
				});
				
				for (var x = 0; x < category.length; x++) {
					var p = response.categories[category[x]].parents;
					categories.push({
						"category": category[x],
						"categoryL": (response.taxonomies[category[x]]) ? response.taxonomies[category[x]].label[$scope.defaultLang].value : category[x],
						//"categoryL": response.taxonomies[category[x]].label[$scope.defaultLang].value,
						"translation": response.taxonomies[category[x]],
						"parents": p,
						"parentsL": (p && p.length > 0 && response.taxonomies[p[0]]) ? response.taxonomies[p[0]].label[$scope.defaultLang].value : '',
						"children": response.categories[category[x]].children,
						"attributes": (response.categories[category[x]].attributes) ? response.categories[category[x]].attributes : {}
					})
				}
				var options = {
					'grid': {
						recordsPerPageArray: [25, 50, 100, 200],
						'columns': [
							{'label': translation.categories[LANG], 'field': 'categoryL'},
							{'label': translation.parent[LANG], 'field': 'parentsL'}
						],
						'defaultLimit': 25
					},
					'defaultSortField': 'category',
					'data': categories,
					'left': [
						{
							'icon': 'search',
							'label': translation.viewItem[LANG],
							'handler': 'viewEntry'
						},
						{
							'label': translation.edit[LANG],
							'icon': 'pencil2',
							'handler': 'editEntry'
						},
						{
							'label': translation.delete[LANG],
							'icon': 'cross',
							'msg': translation.rUsRcategory[LANG],
							'handler': 'deleteEntry'
						}
					]
				};
				buildGrid($scope, options);
			}
		});
	};
	
	$scope.viewEntry = function (oneDataRecord) {
		var taxonomies = $scope.taxonomies;
		var selectedLang = $scope.defaultLang;
		$modal.open({
			templateUrl: "infoBoxCat.html",
			size: 'dialog',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewingOneCategory[LANG];
				$scope.taxonomies = taxonomies;
				$scope.selectedLang = selectedLang;
				$scope.data = oneDataRecord;
				if (oneDataRecord.attributes.filters) {
					$scope.filters = Object.keys(oneDataRecord.attributes.filters);
				}
				fixBackDrop();
				setTimeout(function () {
					highlightMyCode()
				}, 500);
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};
	
	$scope.addEntry = function () {
		var formFields = angular.copy($scope.formConfig.entries);
		for (var i = 0; i < $scope.languages.length; i++) {
			formFields[0].entries.push(
				{
					'name': $scope.languages[i].id,
					'label': ($scope.languages[i].selected) ? $scope.languages[i].label + " (" + translation.default[LANG] + ")" : $scope.languages[i].label,
					'type': 'text',
					'required': ($scope.languages[i].selected)
				}
			);
			if ($scope.languages[i].selected) {
				$scope.selected = $scope.languages[i].id;
			}
		}
		// preselect no parent
		formFields[1].value[0].selected = true;
		
		var cat = Object.keys($scope.categories);
		for (var x = 0; x < cat.length; x++) {
			formFields[1].value.push({'v': cat[x], 'l': $scope.taxonomies[cat[x]].label[$scope.selected].value});
		}
		var taxonomies = Object.keys($scope.taxonomies);
		var filter = [];
		for (var y = 0; y < taxonomies.length; y++) {
			if ($scope.taxonomies[taxonomies[y]].type === "classification") {
				formFields[2].value.push({
					'v': taxonomies[y],
					'l': $scope.taxonomies[taxonomies[y]].label[$scope.selected].value
				});
			}
			if ($scope.taxonomies[taxonomies[y]].type === "department") {
				formFields[3].value.push({
					'v': taxonomies[y],
					'l': $scope.taxonomies[taxonomies[y]].label[$scope.selected].value
				});
			}
			if ($scope.taxonomies[taxonomies[y]].type === "filter") {
				formFields[4].entries.push(
					{
						"name": taxonomies[y],
						"type": "checkbox",
						"value": [
							{'v': taxonomies[y], 'l': $scope.taxonomies[taxonomies[y]].label[$scope.selected].value}
						],
						"onAction": function (id, data, form) {
							for (var i = 0; i < form.entries.length; i++) {
								if (form.entries[i].name === 'filters') {
									for (var y = 0; y < form.entries[i].entries.length; y++) {
										if (form.entries[i].entries[y].name === id) {
											var ele3 = angular.element(document.getElementsByClassName("tr-" + id + "-wrapper"));
											if (form.entries[i].entries[y + 1] && form.entries[i].entries[y + 1].name === id + "Required") {
												form.entries[i].entries.splice(y + 1, 1);
												ele3.addClass('fullW');
											}
											else {
												form.entries[i].entries.splice(
													y + 1, 0,
													{
														'name': data[0] + "Required",
														'label': translation.required[LANG],
														'type': 'radio',
														'value': [{'v': true}, {'v': false}],
														'required': true
													}
												);
												ele3.removeClass('fullW');
											}
											break;
										}
									}
								}
							}
							
						}
						
					}
				);
				filter.push(taxonomies[y]);
			}
		}
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'addCategory',
			'label': translation.addCategory[LANG],
			'actions': [
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var postData = {
							label: {},
							parents: [],
							children: []
						};
						
						if (formData[$scope.selected]) {
							var categoryId = formData[$scope.selected].toLowerCase();
							postData.category = categoryId.toLowerCase().trim().replace(/[^A-Z0-9]+/ig, "_");
						}
						else {
							return false;
						}
						if (Array.isArray(formData.parents) && formData.parents.length === 0) {
							postData.parents = [];
						}
						else if (formData.parents !== "noParentSelected") {
							postData.parents.push(formData.parents);
						}
						
						postData.attributes = {};
						postData.attributes.classification = formData.classification;
						postData.attributes.department = formData.department;
						postData.attributes.filters = {};
						
						for (var y = 0; y < filter.length; y++) {
							if (formData[filter[y]] && formData[filter[y]].length > 0) {
								if (Object.hasOwnProperty.call(formData, filter[y] + "Required")) {
									postData.attributes.filters[filter[y]] = formData[filter[y] + "Required"];
								}
								else {
									return $scope.form.displayAlert('danger', translation.pleaseSelectIfFilterIsRequired[LANG].replace("%filter%", filter[y]));
								}
								
							}
						}
						
						for (var i = 0; i < $scope.languages.length; i++) {
							var lang = $scope.languages[i].id;
							if (formData[lang]) {
								postData.label[lang] = {};
								postData.label[lang].value = formData[lang]
							}
						}
						
						var opts = {
							"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/category",
							"method": "put",
							"params": {
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						};
						
						getSendDataFromServer($scope, ngDataApi, opts, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.catAddSuccess[LANG]);
								$scope.listEntries();
								$scope.modalInstance.close();
								$scope.form.formData = {};
							}
						});
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options, function () {
			setTimeout(function () {
				for (var i = 0; i < $scope.form.entries.length; i++) {
					if ($scope.form.entries[i].name === 'filters') {
						for (var y = 0; y < $scope.form.entries[i].entries.length; y++) {
							var ele3 = angular.element(document.getElementsByClassName("tr-" + $scope.form.entries[i].entries[y].name + "-wrapper"));
							ele3.addClass('fullW');
						}
					}
				}
			}, 500);
		});
		
	};
	
	$scope.editEntry = function (data) {
		data.classification = data.attributes.classification;
		data.department = data.attributes.department;
		
		if (data.attributes && data.attributes.filters) {
			var filters = Object.keys(data.attributes.filters);
			for (var f in filters) {
				data[filters[f]] = [filters[f]];
				data[filters[f] + "Required"] = data.attributes.filters[filters[f]];
			}
		}
		if (data.parents.length === 0) {
			data.parents[0] = 'noParentSelected';
		}
		var formFields = angular.copy($scope.formConfig.entries);
		
		for (var i = 0; i < $scope.languages.length; i++) {
			formFields[0].entries.push(
				{
					'name': $scope.languages[i].id,
					'label': ($scope.languages[i].selected) ? $scope.languages[i].label + " (" + translation.default[LANG] + ")" : $scope.languages[i].label,
					'type': 'text',
					"value": (data.translation.label[$scope.languages[i].id]) ? data.translation.label[$scope.languages[i].id].value : "",
					'required': ($scope.languages[i].selected)
				}
			);
			if ($scope.languages[i].selected) {
				$scope.selected = $scope.languages[i].id;
			}
		}
		var cat = Object.keys($scope.categories);
		for (var x = 0; x < cat.length; x++) {
			var found = false;
			for (var i = 0; i < data.children.length; i++) {
				if ($scope.categories[data.children[i]].children.indexOf(cat[x]) !== -1) {
					found = true;
				}
			}
			if (data.category !== cat[x] && data.children.indexOf(cat[x]) === -1 && !found) {
				formFields[1].value.push({
					'v': cat[x],
					'l': ($scope.taxonomies[cat[x]]) ? $scope.taxonomies[cat[x]].label[$scope.selected].value : cat[x]
				});
			}
		}
		var taxonomies = Object.keys($scope.taxonomies);
		var filter = [];
		for (var y = 0; y < taxonomies.length; y++) {
			if ($scope.taxonomies[taxonomies[y]].type === "classification") {
				formFields[2].value.push({
					'v': taxonomies[y],
					'l': $scope.taxonomies[taxonomies[y]].label[$scope.selected].value
				});
			}
			if ($scope.taxonomies[taxonomies[y]].type === "department") {
				formFields[3].value.push({
					'v': taxonomies[y],
					'l': $scope.taxonomies[taxonomies[y]].label[$scope.selected].value
				});
			}
			if ($scope.taxonomies[taxonomies[y]].type === "filter") {
				formFields[4].entries.push(
					{
						"name": taxonomies[y],
						"type": "checkbox",
						"value": [
							{'v': taxonomies[y], 'l': $scope.taxonomies[taxonomies[y]].label[$scope.selected].value}
						],
						"onAction": function (id, data, form) {
							for (var i = 0; i < form.entries.length; i++) {
								if (form.entries[i].name === 'filters') {
									for (var y = 0; y < form.entries[i].entries.length; y++) {
										if (form.entries[i].entries[y].name === id) {
											var ele3 = angular.element(document.getElementsByClassName("tr-" + id + "-wrapper"));
											if (form.entries[i].entries[y + 1] && form.entries[i].entries[y + 1].name === id + "Required") {
												form.entries[i].entries.splice(y + 1, 1);
												ele3.addClass('fullW');
											}
											else {
												form.entries[i].entries.splice(
													y + 1, 0,
													{
														'name': data[0] + "Required",
														'label': translation.required[LANG],
														'type': 'radio',
														'value': [{'v': true}, {'v': false}],
														'required': true
													}
												);
												ele3.removeClass('fullW');
											}
											break;
										}
									}
								}
							}
							
						}
						
					}
				);
				filter.push(taxonomies[y]);
			}
		}
		var selectedF = [];
		for (var x = 0; x < filter.length; x++) {
			for (var i = 0; i < formFields.length; i++) {
				if (formFields[i].name === 'filters') {
					for (var y = 0; y < formFields[i].entries.length; y++) {
						if (formFields[i].entries[y].name === filter[x] && data.attributes && data.attributes.filters && ( typeof data.attributes.filters[filter[x]] === 'boolean')) {
							selectedF.push(filter[x]);
							selectedF.push(filter[x] + "Required");
							formFields[i].entries.splice(
								y + 1, 0,
								{
									'name': filter[x] + "Required",
									'label': translation.required[LANG],
									'type': 'radio',
									'value': [{'v': true}, {'v': false}],
									'required': true
								}
							);
							break;
						}
					}
				}
			}
		}
		
		
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'editCategory',
			'label': translation.editCategory[LANG],
			'data': data,
			'actions': [
				{
					'type': 'submit',
					'label': translation.saveChanges[LANG],
					'btn': 'primary',
					'action': function (formData) {
						data.category = data.category.replace(/[^A-Z0-9]+/ig, "_").toLowerCase().trim();
						var postData = {
							label: {},
							parents: [],
							children: data.children,
							category: data.category,
							oldParent: data.parents
						};
						if (!formData[$scope.selected]) {
							return false;
						}
						
						if (Array.isArray(formData.parents) && formData.parents.length === 0) {
							postData.parents = [];
						}
						else {
							if (Array.isArray(formData.parents)) {
								if (formData.parents[0] !== "noParentSelected") {
									postData.parents.push(formData.parents[0]);
								}
							}
							else {
								if (formData.parents !== "noParentSelected") {
									postData.parents.push(formData.parents);
								}
							}
						}
						postData.attributes = {};
						postData.attributes.classification = formData.classification;
						postData.attributes.department = formData.department;
						postData.attributes.filters = {};
						for (var y = 0; y < filter.length; y++) {
							if (formData[filter[y]] && formData[filter[y]].length > 0) {
								if (Object.hasOwnProperty.call(formData, filter[y] + "Required")) {
									postData.attributes.filters[filter[y]] = formData[filter[y] + "Required"];
								}
								else {
									return $scope.form.displayAlert('danger', translation.pleaseSelectIfFilterIsRequired[LANG].replace("%filter%", filter[y]));
								}
								
							}
						}
						
						for (var i = 0; i < $scope.languages.length; i++) {
							var lang = $scope.languages[i].id;
							if (formData[lang]) {
								postData.label[lang] = {};
								postData.label[lang].value = formData[lang]
							}
						}
						var opts = {
							"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/category",
							"method": "put",
							"params": {
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase().trim()
							},
							"data": postData
						};
						getSendDataFromServer($scope, ngDataApi, opts, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.catUpdateSuccess[LANG]);
								$scope.listEntries();
								$scope.modalInstance.close();
								$scope.form.formData = {};
							}
						});
						
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options, function () {
			setTimeout(function () {
				for (var i = 0; i < $scope.form.entries.length; i++) {
					if ($scope.form.entries[i].name === 'filters') {
						for (var y = 0; y < $scope.form.entries[i].entries.length; y++) {
							var ele3 = angular.element(document.getElementsByClassName("tr-" + $scope.form.entries[i].entries[y].name + "-wrapper"));
							if (selectedF.indexOf($scope.form.entries[i].entries[y].name) === -1) {
								ele3.addClass('fullW');
							}
						}
					}
				}
			}, 500);
		});
	};
	
	$scope.deleteEntry = function (data) {
		var opts = {
			"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/category",
			"method": "del",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase(),
				"category": data.category.replace(/[^A-Z0-9]+/ig, "_").toLowerCase().trim()
			}
		};
		
		getSendDataFromServer($scope, ngDataApi, opts, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.catDeleteSuccess[LANG]);
				$scope.listEntries();
			}
		});
	};
	
	$scope.listEntries();
	
	injectFiles.injectCss(ModuleDevKbaseLocation + "/knowledgebase.css");
}]);