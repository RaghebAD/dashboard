"use strict";
var knowledgebaseFiltersApp = soajsApp.components;
knowledgebaseFiltersApp.controller('knowledgebaseFiltersModuleDevCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$routeParams', '$cookies', function ($scope, $modal, ngDataApi, injectFiles, $timeout, $routeParams, $cookies) {
	$scope.feedProfileId = $routeParams.id;
	$scope.ModuleDevKbaseLocation = ModuleDevKbaseLocation;
	
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, nakbModuleDevConfig.permissions);
	
	$scope.grid = {rows: []};
	$scope.formConfig = {
		"entries": [
			{
				"name": "translation",
				"label": translation.translation[LANG],
				"type": "group",
				"entries": []
			},
			{
				'name': 'imfv',
				'label': translation.imfv[LANG],
				'type': 'jsoneditor',
				'options': {
					'mode': 'code',
					'availableModes': [
						{'v': 'code', 'l': 'Code View'},
						{'v': 'tree', 'l': 'Tree View'},
						{'v': 'form', 'l': 'Form View'}
					]
				},
				'height': '350px',
				'required': true
			}
		
		]
	};
	
	var __env = $scope.$parent.currentSelectedEnvironment.toUpperCase();
	
	//function that lists the filters in a grid
	$scope.listEntries = function () {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/kbprofile/owner/feed/profiles/"+$scope.feedProfileId+"/filters",
			"params": {
				"__env": __env
			}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				var filters = [];
				$scope.languages = response.languages;
				var defaultLang = response.languages[0].id;
				response.languages.forEach(function (oneLang) {
					if (oneLang.selected) {
						defaultLang = oneLang.id;
					}
				});
				
				var filter = Object.keys(response.filters);
				for (var x = 0; x < filter.length; x++) {
					filters.push({
						"token": filter[x],
						"filter": (response.taxonomies[filter[x]]) ? response.taxonomies[filter[x]].label[defaultLang].value : filter[x],
						"IMFV": response.filters[filter[x]],
						"translation": response.taxonomies[filter[x]]
					})
				}
				var options = {
					'grid': {
						recordsPerPageArray: [5, 10, 50, 100],
						'columns': [
							{'label': translation.filters[LANG], 'field': 'filter'}
						],
						'defaultLimit': 50
					},
					'defaultSortField': 'filter',
					'data': filters,
					'left': [
						{
							'icon': 'search',
							'label': translation.viewItem[LANG],
							'handler': 'viewEntry'
						},
						{
							'label': translation.edit[LANG],
							'icon': 'pencil2',
							'handler': 'editEntry'
						},
						{
							'label': translation.delete[LANG],
							'icon': 'cross',
							'msg': translation.rUsRfilter[LANG],
							'handler': 'deleteEntry'
						}
					]
				};
				buildGrid($scope, options);
			}
		});
	};
	
	//function that prints one data record to the console
	$scope.viewEntry = function (oneDataRecord) {
		$modal.open({
			templateUrl: "infoBox.html",
			size: 'dialog',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewingOneFilter[LANG];
				$scope.data = oneDataRecord;
				$scope.IMFV = JSON.stringify(oneDataRecord.IMFV, null, 2);
				fixBackDrop();
				setTimeout(function () {
					highlightMyCode()
				}, 500);
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};
	
	$scope.addEntry = function () {
		var formFields = angular.copy($scope.formConfig.entries);
		if (formFields[1].name === "imfv") {
			formFields[1].value = {
				"validation": {
					"type": "array",
					"minItems": 1,
					"items": {
						"type": "object",
						"properties": {
							"value": {
								"type": "string",
								"enum": []
							}
						}
					}
				}
			};
		}
		for (var i = 0; i < $scope.languages.length; i++) {
			formFields[0].entries.push(
				{
					'name': $scope.languages[i].id,
					'label': ($scope.languages[i].selected) ? $scope.languages[i].label + " (" + translation.default[LANG] + ")" : $scope.languages[i].label,
					'type': 'text',
					'required': ($scope.languages[i].selected)
				}
			);
			if ($scope.languages[i].selected) {
				var selected = $scope.languages[i].id;
			}
		}
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'addFilter',
			'label': translation.addFilter[LANG],
			'actions': [
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var postData = {
							customImfv: formData.imfv,
							label: {}
						};
						
						if (formData[selected]) {
							var filterId = formData[selected].toLowerCase();
							postData.filter = filterId.replace(/[^A-Z0-9]+/ig, "_").trim().toLowerCase();
						}
						else {
							return false;
						}
						
						for (var i = 0; i < $scope.languages.length; i++) {
							var lang = $scope.languages[i].id;
							if (formData[lang]) {
								postData.label[lang] = {};
								postData.label[lang].value = formData[lang]
							}
						}
						var opts = {
							"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/filter",
							"method": "put",
							"params": {
								"__env": __env
							},
							"data": postData
						};
						
						getSendDataFromServer($scope, ngDataApi, opts, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.filterAddSuccess[LANG]);
								$scope.listEntries();
								$scope.modalInstance.close();
								$scope.form.formData = {};
							}
						});
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);
	};
	
	$scope.editEntry = function (data) {
		var formFields = angular.copy($scope.formConfig.entries);
		formFields[1].value = data.IMFV;
		for (var i = 0; i < $scope.languages.length; i++) {
			formFields[0].entries.push(
				{
					'name': $scope.languages[i].id,
					'label': ($scope.languages[i].selected) ? $scope.languages[i].label + " (" + translation.default[LANG] + ")" : $scope.languages[i].label,
					'type': 'text',
					"value": (data.translation.label[$scope.languages[i].id]) ? data.translation.label[$scope.languages[i].id].value : "",
					'required': ($scope.languages[i].selected)
				}
			);
			if ($scope.languages[i].selected) {
				var selected = $scope.languages[i].id;
			}
		}
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'editFilter',
			'label': translation.editFilter[LANG],
			'data': data,
			'actions': [
				{
					'type': 'submit',
					'label': translation.saveChanges[LANG],
					'btn': 'primary',
					'action': function (formData) {
						data.filter = data.token.replace(/[^A-Z0-9]+/ig, "_").trim().toLowerCase();
						var postData = {
							filter: data.filter,
							customImfv: formData.imfv,
							label: {}
						};
						
						if (!formData[selected]) {
							return false;
						}
						
						for (var i = 0; i < $scope.languages.length; i++) {
							var lang = $scope.languages[i].id;
							if (formData[lang]) {
								postData.label[lang] = {};
								postData.label[lang].value = formData[lang]
							}
						}
						var opts = {
							"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/filter",
							"method": "put",
							"params": {
								"__env": __env
							},
							"data": postData
						};
						getSendDataFromServer($scope, ngDataApi, opts, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.filterUpdateSuccess[LANG]);
								$scope.listEntries();
								$scope.modalInstance.close();
								$scope.form.formData = {};
							}
						});
						
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);
	};
	
	$scope.deleteEntry = function (data) {
		var opts = {
			"routeName": "/kbprofile/owner/feed/profiles/"+$scope.feedProfileId+"/filter",
			"method": "del",
			"params": {
				"__env": __env,
				"filter": data.token.replace(/[^A-Z0-9]+/ig, "_").trim().toLowerCase()
			}
		};
		
		getSendDataFromServer($scope, ngDataApi, opts, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.filterDeleteSuccess[LANG]);
				$scope.listEntries();
			}
		});
	};
	
	$scope.listEntries();
	
}]);