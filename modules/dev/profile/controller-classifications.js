"use strict";
var knowledgebaseClassApp = soajsApp.components;
knowledgebaseClassApp.controller('knowledgebaseClassificationsModuleDevCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$routeParams', '$cookies', function ($scope, $modal, ngDataApi, injectFiles, $timeout, $routeParams, $cookies) {
	$scope.feedProfileId = $routeParams.id;
	$scope.ModuleDevKbaseLocation = ModuleDevKbaseLocation;
	
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, nakbModuleDevConfig.permissions);
	
	$scope.grid = {rows: []};
	
	$scope.formConfig = {
		"entries": [
			{
				"name": "translation",
				"label": translation.translation[LANG],
				"type": "group",
				"entries": []
			}
		]
	};
	
	//function that lists the classifications in a grid
	$scope.listEntries = function () {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/kbprofile/owner/feed/profiles/"+$scope.feedProfileId+"/attributes",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				var classifications = [];
				$scope.languages = response.languages;
				
				var defaultLang = response.languages[0].id;
				response.languages.forEach(function (oneLang) {
					if (oneLang.selected) {
						defaultLang = oneLang.id;
					}
				});
				
				var taxonomies = Object.keys(response.taxonomies);
				for (var y = 0; y < taxonomies.length; y++) {
					if (response.taxonomies[taxonomies[y]].type === "classification") {
						classifications.push({
							"token": taxonomies[y],
							"classification": response.taxonomies[taxonomies[y]].label[defaultLang].value,
							"translation": response.taxonomies[taxonomies[y]]
						});
					}
				}
				var options = {
					'grid': {
						recordsPerPageArray: [5, 10, 50, 100],
						'columns': [
							{'label': translation.classifications[LANG], 'field': 'classification'}
						],
						'defaultLimit': 50
					},
					'defaultSortField': 'classification',
					'data': classifications,
					'left': [
						{
							'icon': 'search',
							'label': translation.viewItem[LANG],
							'handler': 'viewEntry'
						},
						{
							'label': translation.edit[LANG],
							'icon': 'pencil2',
							'handler': 'editEntry'
						},
						{
							'label': translation.delete[LANG],
							'icon': 'cross',
							'msg': translation.rUsRclassification[LANG],
							'handler': 'deleteEntry'
						}
					]
				};
				buildGrid($scope, options);
			}
		});
	};
	
	//function that prints one data record to the console
	$scope.viewEntry = function (oneDataRecord) {
		$modal.open({
			templateUrl: "infoBox.html",
			size: 'dialog',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewingOneClassification[LANG];
				$scope.data = oneDataRecord;
				fixBackDrop();
				setTimeout(function () {
					highlightMyCode()
				}, 500);
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};
	
	$scope.addEntry = function () {
		var formFields = angular.copy($scope.formConfig.entries);
		for (var i = 0; i < $scope.languages.length; i++) {
			formFields[0].entries.push(
				{
					'name': $scope.languages[i].id,
					'label': ($scope.languages[i].selected) ? $scope.languages[i].label + " (" + translation.default[LANG] + ")" : $scope.languages[i].label,
					'type': 'text',
					'required': ($scope.languages[i].selected)
				}
			);
			if ($scope.languages[i].selected) {
				var selected = $scope.languages[i].id;
			}
		}
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'addClassification',
			'label': translation.addClassification[LANG],
			'actions': [
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var postData = {
							label: {}
						};
						
						if (formData[selected]) {
							var classificationId = formData[selected].toLowerCase();
							postData.classification = classificationId.replace(/[^A-Z0-9]+/ig, "_").trim();
						}
						else {
							return false;
						}
						
						for (var i = 0; i < $scope.languages.length; i++) {
							var lang = $scope.languages[i].id;
							if (formData[lang]) {
								postData.label[lang] = {};
								postData.label[lang].value = formData[lang]
							}
						}
						var opts = {
							"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/classification",
							"method": "put",
							"params": {
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						};
						
						
						getSendDataFromServer($scope, ngDataApi, opts, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.classAddSuccess[LANG]);
								$scope.listEntries();
								$scope.modalInstance.close();
								$scope.form.formData = {};
							}
						});
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);
		
	};
	
	$scope.editEntry = function (data) {
		var formFields = angular.copy($scope.formConfig.entries);
		for (var i = 0; i < $scope.languages.length; i++) {
			formFields[0].entries.push(
				{
					'name': $scope.languages[i].id,
					'label': ($scope.languages[i].selected) ? $scope.languages[i].label + " (" + translation.default[LANG] + ")" : $scope.languages[i].label,
					'type': 'text',
					"value": (data.translation.label[$scope.languages[i].id]) ? data.translation.label[$scope.languages[i].id].value : "",
					'required': ($scope.languages[i].selected)
				}
			);
			if ($scope.languages[i].selected) {
				var selected = $scope.languages[i].id;
			}
		}
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'editClassification',
			'label': translation.editClassification[LANG],
			'data': data,
			'actions': [
				{
					'type': 'submit',
					'label': translation.saveChanges[LANG],
					'btn': 'primary',
					'action': function (formData) {
						data.classification = data.token.replace(/[^A-Z0-9]+/ig, "_").toLowerCase().trim();
						var postData = {
							classification: data.classification,
							label: {}
						};
						
						if (!formData[selected]) {
							return false;
						}
						
						for (var i = 0; i < $scope.languages.length; i++) {
							var lang = $scope.languages[i].id;
							if (formData[lang]) {
								postData.label[lang] = {};
								postData.label[lang].value = formData[lang]
							}
						}
						var opts = {
							"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/classification",
							"method": "put",
							"params": {
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						};
						getSendDataFromServer($scope, ngDataApi, opts, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.classUpdateSuccess[LANG]);
								$scope.listEntries();
								$scope.modalInstance.close();
								$scope.form.formData = {};
							}
						});
						
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);
	};
	
	$scope.deleteEntry = function (data) {
		var opts = {
			"routeName": "/kbprofile/owner/feed/profiles/"+$scope.feedProfileId+"/classification",
			"method": "del",
			"params": {
				"classification": data.token.replace(/[^A-Z0-9]+/ig, "_").toLowerCase().trim(),
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		
		getSendDataFromServer($scope, ngDataApi, opts, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.classDeleteSuccess[LANG]);
				$scope.listEntries();
			}
		});
	};
	
	$scope.listEntries();
	
	
}]);