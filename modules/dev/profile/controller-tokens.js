"use strict";
var knowledgebaseTokensApp = soajsApp.components;
knowledgebaseTokensApp.controller('knowledgebaseTokensModuleDevCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$routeParams', '$cookies', function ($scope, $modal, ngDataApi, injectFiles, $timeout, $routeParams, $cookies) {
	$scope.feedProfileId = $routeParams.id;
	$scope.ModuleDevKbaseLocation = ModuleDevKbaseLocation;
	
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, nakbModuleDevConfig.permissions);

	$scope.grid = {rows: []};
	$scope.formConfig = {
		"entries": [
			{
				"name": "translation",
				"label": translation.translation[LANG],
				"type": "group",
				"entries": []
			}
		]
	};

	//function that lists the tokens in a grid
	$scope.listEntries = function () {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/kbprofile/owner/feed/profiles/"+$scope.feedProfileId+"/tokens",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.languages = response.languages;
				var defaultLang = response.languages[0].id;
				response.languages.forEach(function(oneLang){
					if(oneLang.selected){
						defaultLang = oneLang.id;
					}
				});

				var tokens = [];
				var token = Object.keys(response.static_taxonomies);
				for (var y = 0; y < token.length; y++) {
					tokens.push({
						"token": token[y],
						"display": response.static_taxonomies[token[y]].label[defaultLang].value,
						"translation": response.static_taxonomies[token[y]]
					});
				}
				var options = {
					'grid': {
						recordsPerPageArray: [5, 10, 50, 100],
						'columns': [
							{'label': translation.tokens[LANG], 'field': 'token'},
							{'label': translation.display[LANG], 'field': 'display'}
						],
						'defaultLimit': 50
					},
					'defaultSortField': 'token',
					'data': tokens,
					'left': [
						{
							'icon': 'search',
							'label': translation.viewItem[LANG],
							'handler': 'viewEntry'
						},
						{
							'label': translation.edit[LANG],
							'icon': 'pencil2',
							'handler': 'editEntry'
						},
						{
							'label': translation.delete[LANG],
							'icon': 'cross',
							'msg': translation.rUsRtoken[LANG],
							'handler': 'deleteEntry'
						}
					]
				};
				buildGrid($scope, options);
			}
		});
	};

	//function that prints one data record to the console
	$scope.viewEntry = function (oneDataRecord) {
		$modal.open({
			templateUrl: "infoBox.html",
			size: 'dialog',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewingOneToken[LANG];
				$scope.data = oneDataRecord;
				fixBackDrop();
				setTimeout(function () {
					highlightMyCode()
				}, 500);
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};

	$scope.addEntry = function () {
		var formFields = angular.copy($scope.formConfig.entries);
		for (var i = 0; i < $scope.languages.length; i++) {
			formFields[0].entries.push(
				{
					'name': $scope.languages[i].id,
					'label': ($scope.languages[i].selected) ? $scope.languages[i].label + " (" + translation.default[LANG]+")" : $scope.languages[i].label,
					'type': 'text',
					'required': ($scope.languages[i].selected)
				}
			);
			if ($scope.languages[i].selected) {
				var selected = $scope.languages[i].id;
			}
		}
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'addToken',
			'label': translation.addToken[LANG],
			'actions': [
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var postData = {
							label: {}
						};

						if (formData[selected]) {
							var tokenId = formData[selected].toLowerCase();
							postData.token = tokenId.trim().toLowerCase();
							//postData.token = tokenId.replace(/[^A-Z0-9]+/ig, "_").trim().toLowerCase();
						}
						else {
							return false;
						}

						for (var i = 0; i < $scope.languages.length; i++) {
							var lang = $scope.languages[i].id;
							if (formData[lang]) {
								postData.label[lang] = {};
								postData.label[lang].value = formData[lang]
							}
						}
						var opts = {
							"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/token",
							"method": "put",
							"params": {
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						};


						getSendDataFromServer($scope, ngDataApi, opts, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.tokenAddSuccess[LANG]);
								$scope.listEntries();
								$scope.modalInstance.close();
								$scope.form.formData = {};
							}
						});
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);

	};

	$scope.editEntry = function (data) {
		var formFields = angular.copy($scope.formConfig.entries);
		for (var i = 0; i < $scope.languages.length; i++) {
			formFields[0].entries.push(
				{
					'name': $scope.languages[i].id,
					'label': ($scope.languages[i].selected) ? $scope.languages[i].label + " (" + translation.default[LANG]+")" : $scope.languages[i].label,
					'type': 'text',
					"value": (data.translation.label[$scope.languages[i].id]) ? data.translation.label[$scope.languages[i].id].value : "",
					'required': ($scope.languages[i].selected)
				}
			);
			if ($scope.languages[i].selected) {
				var selected = $scope.languages[i].id;
			}
		}
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'editToken',
			'label': translation.editToken[LANG],
			'data': data,
			'actions': [
				{
					'type': 'submit',
					'label': translation.saveChanges[LANG],
					'btn': 'primary',
					'action': function (formData) {
						data.token = data.token.replace(/[^A-Z0-9]+/ig, "_").trim().toLowerCase();
						var postData = {
							token: data.token,
							label: {}
						};

						if (!formData[selected]) {
							return false;
						}

						for (var i = 0; i < $scope.languages.length; i++) {
							var lang = $scope.languages[i].id;
							if (formData[lang]) {
								postData.label[lang] = {};
								postData.label[lang].value = formData[lang]
							}
						}
						var opts = {
							"routeName": "/kbprofile/owner/feed/profiles/" + $scope.feedProfileId + "/token",
							"method": "put",
							"params": {
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						};
						getSendDataFromServer($scope, ngDataApi, opts, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.tokenUpdateSuccess[LANG]);
								$scope.listEntries();
								$scope.modalInstance.close();
								$scope.form.formData = {};
							}
						});

					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);
	};

	$scope.deleteEntry = function (data) {
		var opts = {
			"routeName": "/kbprofile/owner/feed/profiles/"+$scope.feedProfileId+"/token",
			"method": "del",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase(),
				"token": data.token.replace(/[^A-Z0-9]+/ig, "_").trim().toLowerCase()
			}
		};

		getSendDataFromServer($scope, ngDataApi, opts, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.tokenDeleteSuccess[LANG]);
				$scope.listEntries();
			}
		});
	};
	
	$scope.listEntries();

}]);