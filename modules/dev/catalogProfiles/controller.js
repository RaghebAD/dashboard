"use strict";
var catalogProfilesApp = soajsApp.components;

catalogProfilesApp.controller('catalogProfilesModuleDevCtrl', ['$scope', '$rootScope', '$modal', 'ngDataApi', '$cookies', 'injectFiles', '$timeout', 'catalogProfilesSrv', function ($scope, $rootScope, $modal, ngDataApi, $cookies, injectFiles, $timeout, catalogProfilesSrv) {
	$scope.$parent.isUserLoggedIn();
	$rootScope.ModuleDevCatalog = ModuleDevCatalog;
	var permissions = catalogModuleDevConfig.permissions;
	
	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}
	$scope.merchants = [];
	$scope.access = {};
	$scope.sources = [];
	$scope.feedProfile;
	
	constructModulePermissions($scope, $scope.access, permissions);
	
	var getMerchants = function () {
		
		catalogProfilesSrv.getEntriesFromAPI($scope, {
			"routeName": "/catalog/owner/catalog/merchants",
			"method": "get",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, resp) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.merchants = resp;
			}
		});
	};
	
	var getFeedProfile = function (id, category, cb) {
		var params = {
			"source": "catalog",
			"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
		};
		if (category && category !== "") {
			params["category"] = category;
		}
		
		catalogProfilesSrv.getEntriesFromAPI($scope, {
			"routeName": "/kbprofile/owner/feed/profiles/" + id,
			"method": "get",
			"params": params
		}, function (error, resp) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
				cb();
			}
			else {
				cb(resp);
			}
		});
	};
	
	function getFeeds() {
		catalogProfilesSrv.getEntriesFromAPI($scope, {
			"routeName": "/kbprofile/owner/feed/profiles/list",
			"method": "get",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.feedProfile = response;
			}
		});
	}
	
	function getTenants() {
		catalogProfilesSrv.getEntriesFromAPI($scope, {
			"routeName": "/dashboard/tenant/list",
			"method": "get",
			"params": {"type": "client"}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.sources = response;
				getMerchants();
			}
		});
	}
	
	$scope.listEntries = function () {
		var opts = {
			"routeName": "/catalog/owner/catalog/profiles",
			"method": "get",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		catalogProfilesSrv.getEntriesFromAPI($scope, opts, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				catalogProfilesSrv.printGrid($scope, response);
			}
		});
	};
	
	$scope.viewEntry = function (oneDataRecord) {
		getFeedProfile(oneDataRecord.feed_profile, oneDataRecord.config.categories.selected, function (profile) {
			$modal.open({
				templateUrl: "infoBox.html",
				size: 'dialog',
				backdrop: true,
				keyboard: true,
				controller: function ($scope, $modalInstance) {
					for (var x = 0; x < profile.languages.length; x++) {
						if (profile.languages[x].selected) {
							$scope.selectedLang = profile.languages[x].id;
							break;
						}
					}
					var data = angular.copy(oneDataRecord);
					$scope.profile = profile;
					fixBackDrop();
					delete data.config.categories.generated;
					delete data.sources;
					delete data.feed_profile;
					delete data.feed_profile_name;
					$scope.title = "Viewing One Catalog Profile";
					$scope.data = data;
					setTimeout(function () {
						highlightMyCode();
					}, 500);
					$scope.ok = function () {
						$modalInstance.dismiss('ok');
					};
				}
			});
		});
	};
	
	var toggle_source = function (outerScope, index, hierarchy, $scope) {
		if (hierarchy === 'source') {
			var condition;
			if (outerScope.sourcesModel[index].selected === true) {
				condition = true;
				$scope.postData.config.sources[outerScope.sourcesModel[index].code] = [];
			}
			else {
				condition = false;
				if ($scope.postData.config.sources[outerScope.sourcesModel[index].code]) {
					delete $scope.postData.config.sources[outerScope.sourcesModel[index].code];
				}
			}
			for (var n = 0; n < outerScope.sourcesModel[index].merchants.length; n++) {
				outerScope.sourcesModel[index].merchants[n].selected = condition;
			}
			
			for (var t = 0; t < outerScope.sourcesModel[index].merchants.length; t++) {
				if (outerScope.sourcesModel[index].merchants[t].selected === true) {
					$scope.postData.config.sources[outerScope.sourcesModel[index].code].push({
						id: outerScope.sourcesModel[index].merchants[t].id,
						name: outerScope.sourcesModel[index].merchants[t].name
					});
				}
			}
		}
		else {
			var count = 0;
			$scope.postData.config.sources[outerScope.sourcesModel[index].code] = [];
			
			for (var x = 0; x < outerScope.sourcesModel[index].merchants.length; x++) {
				if (outerScope.sourcesModel[index].merchants[x].selected === true) {
					outerScope.sourcesModel[index].selected = true;
					$scope.postData.config.sources[outerScope.sourcesModel[index].code].push({
						id: outerScope.sourcesModel[index].merchants[x].id,
						name: outerScope.sourcesModel[index].merchants[x].name
					});
				}
				else {
					count++;
				}
			}
			if (count === outerScope.sourcesModel[index].merchants.length) {
				outerScope.sourcesModel[index].selected = false;
				delete $scope.postData.config.sources[outerScope.sourcesModel[index].code];
			}
		}
		
		var sourcesLength = Object.keys($scope.postData.config.sources).length;
		$scope.catalogRequired.sources = (sourcesLength === 0);
	};
	
	var initCatalog = function ($scope) {
		$scope.postData = {
			name: null,
			config: {
				sources: {}
			}
		};
		$scope.catalogRequired = {
			name: true,
			feed: true,
			categories: true
		};
		$scope.message = {
			danger: false,
			text: ''
		};
	};
	
	var generateSources = function ($scope) {
		if ($scope.merchants) {
			delete $scope.merchants.soajsauth;
		}
		$scope.sourcesModel = [];
		for (var i = 0; i < $scope.sources.length; i++) {
			for (var j in $scope.merchants) {
				if ($scope.merchants.hasOwnProperty(j)) {
					if ($scope.sources[i].code === j) {
						$scope.sourcesModel.push({
							code: $scope.sources[i].code,
							name: $scope.sources[i].name,
							selected: false,
							merchants: $scope.merchants[j].slice()
						});
					}
				}
			}
		}
		for (var k = 0; k < $scope.sourcesModel.length; k++) {
			for (var m = 0; m < $scope.sourcesModel[k].merchants.length; m++) {
				$scope.sourcesModel[k].merchants[m].selected = false;
			}
		}
	};
	
	var grabFeed = function ($scope, callback) {
		if ($scope.selectedFeed !== "") {
			getFeedProfile($scope.selectedFeed, $scope.category, function (profile) {
				//$scope.feedPerProfile = angular.copy(profile);
				catalogProfilesSrv.buildCategoriesFromProfile(profile, $scope.categoriesList, $scope.category);
				$scope.categoriesList = angular.fromJson(angular.copy(profile.categories));
				
				$scope.postData.config.feed_profile = profile._id;//$scope.feedPerProfile._id;
				return callback();
			});
		}
		else {
			return callback();
		}
	};
	
	function getCategoriesArr(obj) {
		var arr = [];
		for (var c in obj) {
			if (obj[c] && obj[c] === true) {
				arr.push(c);
			}
		}
		return arr;
	}
	
	$scope.addCatalogProfile = function () {
		generateSources($scope);
		var outerScope = $scope;
		$modal.open({
			templateUrl: "addcat.tmpl",
			size: 'lg',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				fixBackDrop();
				$scope.title = "Add Catalog Profiles";
				$scope.catalogDisabled = {
					name: false
				};
				initCatalog($scope);
				
				$scope.catalogRequired.sources = true;
				$scope.outerScope = outerScope;
				$scope.toggleSources = function (index, hierarchy) {
					toggle_source(outerScope, index, hierarchy, $scope);
				};
				$scope.categoriesList = [];
				$scope.profileCategories = [];
				$scope.profileSelectedCategories = {};
				
				outerScope.selectAll = false;
				
				$scope.getFeed = function (cb) {
					if (!cb) {
						cb = function () {
						}
					}
					grabFeed($scope, cb);
				};
				
				$scope.assembleCategories = function (category) {
					//category was previously selected and now unselected, delete listed children
					if ($scope.profileSelectedCategories[category] === false) {
						for (var cat in $scope.categoriesList) {
							if ($scope.categoriesList[cat].v === category) {
								var parentLabel = $scope.categoriesList[cat].l;
								break;
							}
						}
						var temp, index;
						for (var i = 0; i < $scope.categoriesList.length; i++) {
							temp = $scope.categoriesList[i].l;
							index = 0;
							while (index !== -1) {
								index = temp.lastIndexOf(" /");
								temp = temp.slice(0, index);
								if (temp === parentLabel) {
									if ($scope.profileSelectedCategories[$scope.categoriesList[i].v] === true) {
										delete $scope.profileSelectedCategories[$scope.categoriesList[i].v];
									}
									$scope.categoriesList.splice(i, 1);
									i--;
									break;
								}
							}
						}
						$scope.postData.config.categories = getCategoriesArr($scope.profileSelectedCategories);
					}
					else {
						//get children of checked category here////
						$scope.loading = true;
						$scope.category = category;
						$scope.getFeed(function () {
							$scope.postData.config.categories = getCategoriesArr($scope.profileSelectedCategories); //included in onSubmit
							if (!$scope.postData.config.categories || ($scope.postData.config.categories.length === 0)) {
								$scope.message.danger = true;
								$scope.message.text = "Please select at least one category";
							} else {
								$scope.message.danger = false;
								$scope.message.text = "";
							}
							
							$timeout(function () {
								$scope.loading = false;
							}, 500);
						});
					}
				};
				
				$scope.selectAllTrigger = function () {
					if (outerScope.selectAll) {
						outerScope.selectAll = false;
					}
					else {
						for (var cat in $scope.profileSelectedCategories) {
							$scope.profileSelectedCategories[cat] = false;
							//delete $scope.profileSelectedCategories[cat];
							$scope.assembleCategories(cat);
						}
						outerScope.selectAll = true;
						$scope.postData.config.categories = [];
					}
				};
				
				
				$scope.onSubmit = function () {
					//todo: check that the name does not contain any space characters in it.
					$scope.postData.name.trim();
					if ($scope.postData.name.indexOf(' ') > 0) {
						$scope.message.danger = true;
						$scope.message.text = "Catalog name can't contain spaces";
						return;
					}
					
					$scope.postData.config.selectAll = outerScope.selectAll;
					$scope.postData.config.categories = getCategoriesArr($scope.profileSelectedCategories);
					$scope.postData.config.name = $scope.postData.name;
					
					getSendDataFromServer(outerScope, ngDataApi, {
						"method": "send",
						"routeName": "/catalog/owner/catalog/profiles",
						"params": {
							"__env": outerScope.$parent.currentSelectedEnvironment.toUpperCase()
						},
						"data": {
							config: $scope.postData.config
						}
					}, function (error) {
						if (error) {
							$scope.message.danger = true;
							$scope.message.text = error.message;
						}
						else {
							outerScope.$parent.displayAlert('success', 'Catalog Profile Added Successfully.');
							$modalInstance.close();
							$scope.postData = {
								name: null,
								config: {
									sources: {}
								}
							};
							outerScope.listEntries();
						}
					});
				};
				
				$scope.closeModal = function () {
					$modalInstance.close();
					$scope.postData = {
						name: null,
						config: {
							sources: {}
						}
					};
					outerScope.listEntries();
				};
			}
		});
	};
	
	$scope.editCatalogProfile = function (data) {
		generateSources($scope);
		var outerScope = $scope;
		$modal.open({
			templateUrl: "addcat.tmpl",
			size: 'lg',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = "Edit Catalog Profiles";
				$scope.catalogDisabled = {
					name: true
				};
				fixBackDrop();
				$scope.showSelectedCategories = false;
				$scope.profileSelectedCategories = {};
				$scope.categoriesList = [];
				initCatalog($scope);
				$scope.catalogRequired.sources = false;
				$scope.postData.name = data.name;
				$scope.outerScope = outerScope;
				
				
				for (var j in data.config.sources) {
					for (var k = 0; k < $scope.outerScope.sourcesModel.length; k++) {
						if ($scope.outerScope.sourcesModel[k].code === j) {
							$scope.outerScope.sourcesModel[k].selected = true;
							for (var i = 0; i < $scope.outerScope.sourcesModel[k].merchants.length; i++) {
								for (var m = 0; m < data.config.sources[j].length; m++) {
									if (data.config.sources[j][m].id === $scope.outerScope.sourcesModel[k].merchants[i].id) {
										$scope.outerScope.sourcesModel[k].merchants[i].selected = true;
									}
								}
							}
						}
					}
				}
				
				$scope.postData.config.sources = angular.copy(data.config.sources);
				getFeedProfile(data.config.feed_profile, $scope.category, function (profile) {
					$scope.loading = true;
					//$scope.feedPerProfile = angular.copy(profile);
					if (profile) {
						catalogProfilesSrv.buildCategoriesFromProfile(profile, $scope.categoriesList, $scope.category);
						$scope.categoriesList = angular.fromJson(angular.copy(profile.categories));
						
						$scope.postData.config.feed_profile = profile._id;//$scope.feedPerProfile._id;
						
						$scope.profileCategories = data.config.categories.selected;
						$scope.selectedFeed = profile._id;//$scope.feedPerProfile._id;
					}
					else {
						return;
					}
					
					if (data.config.selectAll) {
						$scope.profileCategories = [];
						$scope.selectAllCategories = true;
						$timeout(function () {
							$scope.loading = false;
						}, 500);
					} else {
						loopAndRebuild($scope.categoriesList, 0, function () {
							$scope.postData.config.categories = getCategoriesArr($scope.profileSelectedCategories);
							$scope.showSelectedCategories = true;
						});
					}
				});
				
				function loopAndRebuild(categoriesList, i, cb) {
					if (!categoriesList[i]) {
						// todo: add assurance
					}
					if ($scope.profileCategories.indexOf(categoriesList[i].v) !== -1) { //category is selected
						$scope.profileSelectedCategories[categoriesList[i].v] = true;
						$scope.assembleCategories(categoriesList[i].v, function () {
							categoriesList = angular.copy($scope.categoriesList);
							i++;
							if (i === categoriesList.length) {
								$timeout(function () {
									$scope.loading = false;
								}, 500);
								return cb();
							}
							else {
								loopAndRebuild(categoriesList, i, cb);
							}
						});
					}
					else {
						i++;
						if (i === categoriesList.length) {
							$timeout(function () {
								$scope.loading = false;
							}, 500);
							return cb();
						}
						else {
							loopAndRebuild(categoriesList, i, cb);
						}
					}
				}
				
				$scope.getFeed = function (cb) {
					if (!cb) {
						cb = function () {
						};
					}
					grabFeed($scope, cb);
				};
				
				$scope.selectAllTrigger = function () {
					if (outerScope.selectAll) {
						outerScope.selectAll = false;
					} else {
						for (var cat in $scope.profileSelectedCategories) {
							$scope.profileSelectedCategories[cat] = false;
							$scope.assembleCategories(cat);
						}
						outerScope.selectAll = true;
						$scope.postData.config.categories = [];
					}
				};
				
				$scope.assembleCategories = function (category, callback) {
					//category was previously selected and now unselected, delete listed children
					//tracking source of function call (checkbox click or recursive function
					if (!callback) {
						$scope.loading = true;
					}
					
					if ($scope.profileSelectedCategories[category] === false) {
						for (var cat in $scope.categoriesList) {
							if ($scope.categoriesList[cat].v === category) {
								var parentLabel = $scope.categoriesList[cat].l;
								break;
							}
						}
						var temp, index;
						for (var i = 0; i < $scope.categoriesList.length; i++) {
							temp = $scope.categoriesList[i].l;
							index = 0;
							while (index !== -1) {
								index = temp.lastIndexOf(" /");
								temp = temp.slice(0, index);
								if (temp === parentLabel) {
									if ($scope.profileSelectedCategories[$scope.categoriesList[i].v] === true) {
										$scope.profileSelectedCategories[$scope.categoriesList[i].v] = false;
									}
									$scope.categoriesList.splice(i, 1);
									i--;
									break;
								}
							}
						}
						
						if (callback) {
							return callback();
						}
						else {
							$timeout(function () {
								$scope.loading = false;
							}, 500);
						}
					}
					else {
						//get children of checked category here////
						$scope.category = category;
						$scope.getFeed(function () {
							$scope.postData.config.categories = getCategoriesArr($scope.profileSelectedCategories);
							if (!$scope.postData.config.categories || ($scope.postData.config.categories.length === 0)) {
								$scope.message.danger = true;
								$scope.message.text = "Please select at least one category";
							}
							else {
								$scope.message.danger = false;
								$scope.message.text = "";
							}
							/*$timeout(function () {
							 $scope.loading = false;
							 }, 500);*/
							
							if (callback) {
								return callback();
							}
							else {
								$timeout(function () {
									$scope.loading = false;
								}, 500);
							}
						});
					}
				};
				
				$scope.toggleSources = function (index, hierarchy) {
					toggle_source(outerScope, index, hierarchy, $scope);
				};
				
				$scope.onSubmit = function () {
					if ($scope.selectAllCategories) {
						$scope.postData.config.selectAll = $scope.selectAllCategories;
					}
					else {
						$scope.postData.config.selectAll = false;
					}
					
					$scope.postData.config.categories = getCategoriesArr($scope.profileSelectedCategories);
					overlayLoading.show();
					getSendDataFromServer(outerScope, ngDataApi, {
						"method": "put",
						"routeName": "/catalog/owner/catalog/profiles/" + data._id,
						"params": {
							"__env": outerScope.$parent.currentSelectedEnvironment.toUpperCase()
						},
						"data": {
							config: $scope.postData.config
						}
					}, function (error) {
						overlayLoading.hide();
						if (error) {
							$scope.message.danger = true;
							$scope.message.text = error.message;
						}
						else {
							outerScope.$parent.displayAlert('success', 'Catalog Profile Edited Successfully.');
							$modalInstance.close();
							$scope.postData = {
								name: null,
								config: {
									sources: {}
								}
							};
							outerScope.listEntries();
						}
					});
				};
				
				$scope.closeModal = function () {
					$modalInstance.close();
					$scope.postData = {
						name: null,
						config: {
							sources: {}
						}
					};
					outerScope.listEntries();
				};
			}
		});
	};
	
	$scope.deleteCatalogProfile = function (data) {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "del",
			"routeName": "/catalog/owner/catalog/profiles/" + data._id,
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', ' Catalog Profile Deleted Successfully.');
				$scope.listEntries();
			}
		});
		
	};
	
	$scope.deleteCatalogProfiles = function () {
		var config;
		config = {
			"method": "del",
			"routeParam": true,
			'routeName': "/catalog/owner/catalog/profiles/%id%",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			},
			'msg': {
				'error': 'one or more of the selected Catalog Profile(s) was not deleted.',
				'success': 'Selected Catalog Profile(s) have been deleted.'
			}
		};
		
		multiRecordUpdate(ngDataApi, $scope, config, function () {
			$scope.listEntries();
		});
	};
	
	$scope.buildCatalog = function (data) {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/catalog/owner/catalog/build",
			"params": {
				'id': data._id,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', 'Catalog will be built, it may take some time to be ready.');
				$scope.listEntries();
			}
		});
		
	};
	
	if ($scope.access.list) {
		getFeeds();
		$timeout(function () {
			getTenants();
		}, 20);
		$timeout(function () {
			$scope.listEntries();
		}, 40);
	}
	injectFiles.injectCss(ModuleDevCatalog + "/catalogProfiles.css");
	
}]);
