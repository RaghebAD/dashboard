"use strict";
var ModuleDevCatalog = uiModuleDev + '/catalogProfiles';

var ctModuleDevTranslation = {
	"catalogProfiles": {
		"ENG": "Catalog Profiles",
		"FRA": "Catalog Profiles"
	}
};

for (var attrname in ctModuleDevTranslation) {
	translation[attrname] = ctModuleDevTranslation[attrname];
}

var catalogProfilesModuleDevNav = [
	{
		'id': 'catalogProfiles',
		'label': translation.catalogProfiles[LANG],
		'url': '#/catalogProfiles',
		'scripts': [
			ModuleDevCatalog + '/config.js', ModuleDevCatalog + '/service.js',
			ModuleDevCatalog + '/controller.js'
		],
		'tplPath': ModuleDevCatalog + '/directives/list.tmpl',
		'checkPermission': {
			'service': 'catalog',
			'method': 'get',
			'route': '/owner/catalog/profiles'
		},
		'icon': 'profile',
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'contentMenu': true,
		'mainMenu': true,
		'tracker': true,
		'order': 2,
		'ancestor': [translation.home[LANG]]
	}
];

navigation = navigation.concat(catalogProfilesModuleDevNav);

errorCodes.catalog = {
	"400": {
		"ENG": "Failed to connect to Database",
		"FRA": "Échec de la connexion à la base de données"
	}
};