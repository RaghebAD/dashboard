"use strict";
var productsApp = soajsApp.components;

productsApp.controller('addProductModuleDevCtrl', ['$scope', '$route', '$routeParams', '$cookies', '$compile', '$localStorage', 'ngDataApi', 'injectFiles', 'productsSrv', 'cbInputHelper', 'popUpConfiguration', '$modal', '$timeout',
	function ($scope, $route, $routeParams, $cookies, $compile, $localStorage, ngDataApi, injectFiles, productsSrv, cbInputHelper, popUpConfiguration, $modal, $timeout) {
		$scope.showError = false;
		$scope.$parent.isUserLoggedIn();
		$scope.access = {};
		$scope.ModuleDevMpLocation = ModuleDevMpLocation;
		$scope.disabledSelector = [];
		$scope.merchantId = $routeParams.id;
		$scope.code = $cookies.get('knowledgebase_merchant') || '';
		$scope.maxlength = knowledgeBaseModuleDevConfig.maxLength.product;
		$scope.saveDraftActive = true;
		
		// new stuff start
		$scope.formData = extraUtils.confirmStandardProduct($scope.formData);
		
		$scope.tempoImages = angular.copy($scope.formData.productData.media.images);
		
		// new stuff end
		
		extraUtils.addAndEditCommonFunctions($scope, ngDataApi, cbInputHelper, popUpConfiguration, productsSrv);
		
		extraUtils.importSegmentationData($scope, ngDataApi, function () {
			extraUtils.compareAndConstructSegmentationData($scope);
		});
		
		if (!$scope.$parent.currentSelectedEnvironment) {
			$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
		}
		constructModulePermissions($scope, $scope.access, knowledgeBaseModuleDevConfig.permissions);
		
		if ($scope.access.owner.merchants.get) {
			$scope.isOwner = true;
		}
		else {
			$scope.isOwner = false;
		}
		
		function getMyMerchant() {
			var opts = {
				"method": "get",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			if ($scope.access.owner.merchants.get) {
				opts.params.code = $scope.code;
				opts.routeName = "/knowledgebase/owner/merchants/" + $scope.merchantId;
			}
			else {
				opts.routeName = "/knowledgebase/tenant/merchants/" + $scope.merchantId;
			}
			getSendDataFromServer($scope, ngDataApi, opts, function (error, merchantRecordData) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.data = merchantRecordData.pos;
				}
			});
		}
		
		$scope.add = {
			search: false,
			disable: {
				brand: false,
				title: false,
				upc: false
			}
		};
		$scope.fetchProduct = {
			'upc': '',
			'brand': '',
			'title': ''
		};
		
		$scope.variationsValues = {};
		
		$scope.addProduct = function (search) {
			$scope.add = {
				search: (search !== undefined && typeof(search) === 'boolean') ? search : false,
				disable: {
					brand: false,
					title: false,
					upc: false
				}
			};
			$scope.edit = true;
			$scope.fetchProduct = {
				'upc': '',
				'brand': '',
				'title': ''
			};
		};
		
		$scope.checkDisable = function () {
			$scope.add = {
				search: false,
				disable: {
					brand: false,
					title: false,
					upc: false
				}
			};
			
			if ($scope.fetchProduct.upc && $scope.fetchProduct.upc.length >= 1) {
				$scope.add.disable.brand = true;
				$scope.add.disable.title = true;
			}
			
			if (($scope.fetchProduct.brand && $scope.fetchProduct.brand.length >= 1) || ($scope.fetchProduct.title && $scope.fetchProduct.title.length >= 1)) {
				$scope.add.disable.upc = true;
			}
		};
		
		$scope.disabled = true;
		$scope.searchProduct = function () {
			var searchParams = {};
			if ($scope.fetchProduct.title || $scope.fetchProduct.brand) {
				searchParams = {
					brand: $scope.fetchProduct.brand,
					title: $scope.fetchProduct.title,
					'merchantId': $scope.merchantId,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			} else if ($scope.fetchProduct.upc) {
				searchParams = {
					upc: $scope.fetchProduct.upc,
					merchantId: $scope.merchantId,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			}
			
			if (Object.keys(searchParams).length > 0) {
				var opts = {
					"routeName": "/knowledgebase/products/search",
					"method": "get",
					"params": searchParams
				};
				$scope.edit = false;
				$scope.processing = true;
				$scope.progress = 0;
				productsSrv.getEntriesFromApi($scope, opts, function (error, response) {
					productsSrv.buildCategoriesFromProfile($scope, $localStorage, response.profile, function (error, categories) {
						$scope.processing = false;
						response.profile.categories = categories;
						$scope.response = response;
						$scope.treats = (response.profile.treats) ? true : false;
						if (error) {
							$scope.$parent.displayAlert('danger', error.message);
						}
						else if (response.product.length === 1) {
							$scope.disabled = false;
							$scope.add.search = true;
							
							$scope.response.product[0].coreData.groupDefault = false;
							$scope.response.product[0].coreData.udac = '';
							$scope.response.product[0].coreData.groupId = "";
							
							productsSrv.dataDbtoFormUiUi($scope, $scope.response.product[0], $scope.formData, false);
							$scope.renderCategoriesAttributes();
							
							var variations = Object.keys($scope.response.product[0].attributes.variations);
							for (var i = 0; i < variations.length; i++) {
								$scope.changeVariationValue(variations[i], 0);
							}
							
							extraUtils.refreshThumbnails($scope);
							extraUtils.importSegmentationData($scope, ngDataApi, function () {
								extraUtils.compareAndConstructSegmentationData($scope);
							});
							$scope.productTypeOnChange();
						}
						else if (response.product.length > 1) {
							$scope.disabled = false;
							$scope.searchResults = [];
							for (var i = 0; i < response.product.length; i++) {
								var gtin = '';
								if (response.product[i].coreData && response.product[i].coreData.upi) {
									if (response.product[i].coreData.upi['gtin']) {
										gtin = response.product[i].coreData.upi['gtin'];
									}
								}
								
								var udac = '';
								if (response.product[i].coreData && response.product[i].coreData.udac) {
									udac = response.product[i].coreData.udac;
								}
								
								$scope.searchResults.push({
									'v': i,
									'l': response.product[i].productData.title['en'],
									'gtin': gtin,
									'udac': udac
								});
							}
						}
						else {
							$scope.disabled = false;
							$scope.searchResults = [];
							$scope.$parent.displayAlert('warning', translation.noMatchFoundCheckYourSpellingOrClickCreateNewProduct[LANG]);
						}
						
						if (!$scope.$$phase) {
							$scope.$apply();
						}
						
					});
				});
			}
			else {
				$scope.$parent.displayAlert('danger', translation.searchForProductByEnteringItsNameBrandOrItsUPC[LANG]);
			}
		};
		
		$scope.fillProduct = function (index) {
			$scope.add.search = true;
			$scope.chosenProductIndex = index;
			
			$scope.response.product[index].coreData.groupDefault = false;
			$scope.response.product[index].coreData.udac = "";
			$scope.response.product[index].coreData.groupId = "";
			
			productsSrv.dataDbtoFormUiUi($scope, $scope.response.product[index], $scope.formData, false);
			$scope.renderCategoriesAttributes();
			
			var variations = Object.keys($scope.response.product[index].attributes.variations);
			
			for (var i = 0; i < variations.length; i++) {
				$scope.changeVariationValue(variations[i], 0);
			}
			
			extraUtils.refreshThumbnails($scope);
			extraUtils.importSegmentationData($scope, ngDataApi, function () {
				extraUtils.compareAndConstructSegmentationData($scope);
			});
			$scope.productTypeOnChange();
		};
		
		$scope.changeVariationValue = function (data, index) {
			if (data) {
				if (typeof data === "object") {
					angular.forEach($scope.response.profile.filters, function (value, key) {
						if (data.productData.attributes) {
							var keys = Object.keys(data.productData.attributes);
							for (var i = 0; i < keys.length; i++) {
								if (keys[i] === key) {
									if (value.validation.items.properties) {
										//variationsValues
										$scope.variationsValues[keys[i]] = value.validation.items.properties.value.enum;
									}
								}
							}
						}
					});
				}
				else if (typeof data === "string") {
					$scope.disabledSelector[index] = data;
					angular.forEach($scope.response.profile.filters, function (value, key) {
						if ($scope.response.profile.filters) {
							if (key === data) {
								if (value.validation.items && value.validation.items.properties) {
									//variationsValues
									$scope.variationsValues[key] = value.validation.items.properties.value.enum;
								}
							}
						}
					});
				}
			}
			if ($scope.formData.attributes.variations) {
				for (var x = 0; x < $scope.formData.attributes.variations.length; x++) {
					var myFilterArrayValue;
					angular.forEach($scope.response.profile.filters, function (value, key) {
						if (key === $scope.formData.attributes.variations[x].type) {
							if (value.validation.items.properties) {
								myFilterArrayValue = value.validation.items.properties.value.enum;
							}
						}
					});
					if (myFilterArrayValue && $scope.formData.attributes.variations[x].value) {
						for (var y = 0; y < myFilterArrayValue.length; y++) {
							
							if (myFilterArrayValue[y] === $scope.formData.attributes.variations[x].value) {
								break;
							}
							if (y === myFilterArrayValue.length - 1) {
								$scope.formData.attributes.variations.splice(x, 1);
							}
						}
					}
					
				}
				if ($scope.formData.attributes.variations.length === 0) {
					$scope.formData.attributes.variations.push({ 'type': '', 'value': '' });
				}
			}
		};
		
		$scope.striptags = function (html) {
			if (html) {
				if (/<[a-z][\s\S]*>/i.test(html)) {
					return $(html).text();
				}
				else return html;
			}
		};
		
		$scope.save = function (validForm, error) {
			if (!validForm) {
				$scope.showError = true;
			}
			else {
				$scope.showError = false;
			}
			var langs = Object.keys($scope.tempo.browsedImages);
			var invalidBrowsedImages = false;
			langs.forEach(function (lang) {
				if ($scope.tempo.browsedImages[lang].applicable && !$scope.tempo.browsedImages[lang].valid) {
					invalidBrowsedImages = true;
				}
			});
			
			if (invalidBrowsedImages) {
				$scope.$parent.displayAlert('danger', 'Invalid images browsed');
				return;
			}
			
			overlayLoading.show();
			// TO DO : add error message if not all the pos prices are filled.
			var myFiles = productsSrv.extractFilesFromPostedData($scope);
			
			if (myFiles.length !== 0) {
				productsSrv.uploadFile($scope, myFiles, "/knowledgebase/products/addImage", function (error, response) {
					if (error) {
						var errorString;
						if (Array.isArray(error)) {
							errorString = error.join(", ");
						}
						else {
							errorString = error.message;
						}
						overlayLoading.hide();
						$scope.$parent.displayAlert('danger', errorString);
					}
					else {
						var img = [];
						for (var i = 0; i < response.length; i++) {
							img.push({
								"url": response[i].original,
								"resource": response[i].resource,
								"type": response[i].type,
								"language": response[i].language
							});
						}
						for (var i = 0; i < img.length; i++) {
							var current = img[i];
							var obj = {
								url: current.url,
								type: current.type,
								resource: current.resource
							};
							$scope.formData.productData.media.images[current.language].push(obj);
						}
						
						var language = ["en", "fr"];
						
						for (var i = 0; i < language.length; i++) {
							document.getElementById("browseimage_" + language[i]).value = "";
						}
						
						$scope.saveResources($scope, productsSrv, function () {
							$scope.saveMyProduct(productsSrv, "add", false);
						});
						
					}
				});
			}
			else {
				$scope.saveResources($scope, productsSrv, function () {
					$scope.saveMyProduct(productsSrv, "add", false);
				});
			}
		};
		
		$scope.saveDraft = function () {
			// check title
			if (!$scope.formData.productData.title['en']) {
				overlayLoading.hide();
				return $scope.$parent.displayAlert('danger', 'Product Title is mandatory to Save as Draft');
			}
			var myFiles = productsSrv.extractFilesFromPostedData($scope);
			overlayLoading.show();
			if (myFiles.length !== 0) {
				productsSrv.uploadFile($scope, myFiles, "/knowledgebase/products/addImage", function (error, response) {
					if (error) {
						var errorString;
						if (Array.isArray(error)) {
							errorString = error.join(", ");
						}
						else {
							errorString = error.message;
						}
						overlayLoading.hide();
						$scope.$parent.displayAlert('danger', errorString);
					}
					else {
						var img = [];
						for (var i = 0; i < response.length; i++) {
							img.push({
								"url": response[i].original,
								"resource": response[i].resource,
								"type": response[i].type,
								"language": response[i].language
							});
						}
						for (var i = 0; i < img.length; i++) {
							var current = img[i];
							var obj = {
								url: current.url,
								type: current.type,
								resource: current.resource
							};
							$scope.formData.productData.media.images[current.language].push(obj);
						}
						
						var language = ["en", "fr"];
						
						for (var i = 0; i < language.length; i++) {
							document.getElementById("browseimage_" + language[i]).value = "";
						}
						
						$scope.saveResources($scope, productsSrv, function () {
							$scope.saveMyProduct(productsSrv, "add", true);
						});
					}
				});
			}
			else {
				$scope.saveResources($scope, productsSrv, function () {
					$scope.saveMyProduct(productsSrv, "add", true);
				});
			}
		};
		
		getMyMerchant();
		
		if ($scope.access.tenant.products.add || $scope.access.owner.products.add) {
			$scope.addProduct();
		}
		
		// on init
		injectFiles.injectCss(ModuleDevMpLocation + "/merchants.css");
	}]);

productsApp.controller('editProductModuleDevCtrl', ['$scope', '$route', '$routeParams', '$cookies', '$localStorage', 'ngDataApi', 'injectFiles', 'productsSrv', 'cbInputHelper', 'popUpConfiguration', function ($scope, $route, $routeParams, $cookies, $localStorage, ngDataApi, injectFiles, productsSrv, cbInputHelper, popUpConfiguration) {
	$scope.showError = false;
	$scope.$parent.isUserLoggedIn();
	$scope.access = {};
	$scope.ModuleDevMpLocation = ModuleDevMpLocation;
	$scope.disabledSelector = [];
	$scope.merchantId = $routeParams.merchantId;
	$scope.id = $routeParams.id;
	$scope.code = $cookies.get('knowledgebase_merchant') || '';
	$scope.maxlength = knowledgeBaseModuleDevConfig.maxLength.product;
	$scope.variationsValues = {};
	
	// config filled in get product
	extraUtils.addAndEditCommonFunctions($scope, ngDataApi, cbInputHelper, popUpConfiguration, productsSrv);
	
	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}
	constructModulePermissions($scope, $scope.access, knowledgeBaseModuleDevConfig.permissions);
	
	if ($scope.access.owner.products.get) {
		$scope.isOwner = true;
	}
	else {
		$scope.isOwner = false;
	}
	
	$scope.getProduct = function () {
		var config = {
			"method": "get",
			"routeName": "/knowledgebase/tenant/products/" + $scope.id,
			"params": {
				'merchantId': $scope.merchantId,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		if ($scope.access.owner.products.get) {
			config.routeName = "/knowledgebase/owner/products/" + $scope.id;
			config.params.code = $scope.code;
		}
		
		$scope.processing = true;
		$scope.progress = 0;
		getSendDataFromServer($scope, ngDataApi, config, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
				return;
			}
			
			$scope.saveDraftActive = false;
			if (response.product.status === 'draft') {
				$scope.saveDraftActive = true;
			}
			$scope.formData = {};
			$scope.edit = true;
			
			$scope.formData = productsSrv.dataDbtoFormUiUi($scope, response.product, $scope.formData, true);
			$scope.formData = extraUtils.confirmStandardProduct($scope.formData);
			
			extraUtils.refreshThumbnails($scope);
			
			extraUtils.importSegmentationData($scope, ngDataApi, function () {
				extraUtils.compareAndConstructSegmentationData($scope);
			});
			
			$scope.productTypeOnChange();
			
			$scope.refreshAvailableConditions();
			
			productsSrv.buildCategoriesFromProfile($scope, $localStorage, response.profile, function (error, categories) {
				response.profile.categories = categories;
				$scope.processing = false;
				$scope.response = response;
				
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.edit = true;
					$scope.treats = (response.profile.treats) ? true : false;
					
					$scope.formData.created = response.product.created;
					$scope.formData.modified = response.product.modified;
					$scope.renderCategoriesAttributes();
					
					var variations = Object.keys(response.product.attributes.variations);
					
					for (var i = 0; i < variations.length; i++) {
						$scope.changeVariationValue(variations[i], 0);
					}
					
					var merchantPOS = angular.copy(response.product.merchantMeta.pos);
					var opts;
					opts = {
						"method": "get",
						"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId,
						"params": {
							"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
						}
					};
					if ($scope.access.owner.merchants.get) {
						opts.routeName = "/knowledgebase/owner/merchants/" + $scope.merchantId;
						opts.params.code = $scope.code;
					}
					
					getSendDataFromServer($scope, ngDataApi, opts, function (error, merchantRecordData) {
						if (error) {
							$scope.$parent.displayAlert('danger', error.message);
						}
						else {
							if (merchantPOS) {
								merchantPOS.forEach(function (onePOS) {
									for (var i = 0; i < merchantRecordData.pos.length; i++) {
										if (onePOS.id === merchantRecordData.pos[i].id) {
											onePOS.name = merchantRecordData.pos[i].name;
										}
									}
								});
							}
						}
					});
					$scope.data = merchantPOS;
				}
			});
		});
	};
	
	$scope.changeVariationValue = function (data, index) {
		if (data) {
			$scope.disabledSelector[index] = data;
			angular.forEach($scope.response.profile.filters, function (value, key) {
				if (data == key) {
					if (value.validation.items.properties) {
						//variationsValues
						$scope.variationsValues[data] = value.validation.items.properties.value.enum;
					}
				}
			});
		}
		else {
			angular.forEach($scope.response.profile.filters, function (value, key) {
				if ($scope.response.product.productData.attributes) {
					var keys = Object.keys($scope.response.product.productData.attributes);
					for (var i = 0; i < keys.length; i++) {
						if (keys[i] === key) {
							if (value.validation.items && value.validation.items.properties) {
								// variationsValues
								$scope.variationsValues[keys[i]] = value.validation.items.properties.value.enum;
							}
						}
					}
				}
			});
		}
		
		if ($scope.formData.attributes.variations) {
			for (var x = 0; x < $scope.formData.attributes.variations.length; x++) {
				var myFilterArrayValue;
				angular.forEach($scope.response.profile.filters, function (value, key) {
					if (key === $scope.formData.attributes.variations[x].type) {
						if (value.validation.items.properties) {
							myFilterArrayValue = value.validation.items.properties.value.enum;
						}
					}
				});
				if ($scope.formData.attributes.variations[x].value) {
					if (myFilterArrayValue) {
						for (var y = 0; y < myFilterArrayValue.length; y++) {
							if (myFilterArrayValue[y] === $scope.formData.attributes.variations[x].value) {
								break;
							}
							if (y === myFilterArrayValue.length - 1) {
								$scope.formData.attributes.variations.splice(x, 1);
							}
						}
					}
				}
				
			}
			if ($scope.formData.attributes.variations.length === 0) {
				$scope.formData.attributes.variations.push({ 'type': '', 'value': '' });
			}
		}
	};
	
	$scope.striptags = function (html) {
		if (html) {
			if (html.indexOf('<') === 0) {
				return $(html).text();
			}
			else {
				return html;
			}
		}
	};
	
	$scope.save = function (validForm, error) {
		if (!validForm) {
			$scope.showError = true;
		}
		else {
			$scope.showError = false;
		}
		var langs = Object.keys($scope.tempo.browsedImages);
		var invalidBrowsedImages = false;
		langs.forEach(function (lang) {
			if ($scope.tempo.browsedImages[lang].applicable && !$scope.tempo.browsedImages[lang].valid) {
				invalidBrowsedImages = true;
			}
		});
		
		if (invalidBrowsedImages) {
			$scope.$parent.displayAlert('danger', 'Invalid images browsed');
			return;
		}
		
		overlayLoading.show();
		var newPos = [];
		// temporary,
		if ($scope.data) {
			$scope.data.forEach(function (pos) {
				if (pos.id) {
					newPos.push(pos);
				}
			});
			$scope.data = newPos;
		}
		
		var myFiles = productsSrv.extractFilesFromPostedData($scope);
		if (myFiles.length !== 0) {
			productsSrv.uploadFile($scope, myFiles, "/knowledgebase/products/addImage", function (error, response) {
				if (error) {
					var errorString;
					if (Array.isArray(error)) {
						errorString = error.join(", ");
					}
					else {
						errorString = error.message;
					}
					overlayLoading.hide();
					return $scope.$parent.displayAlert('danger', errorString);
				}
				else {
					var img = [];
					for (var i = 0; i < response.length; i++) {
						img.push({
							"url": response[i].original,
							"resource": response[i].resource,
							"type": response[i].type,
							"language": response[i].language
						});
					}
					for (var i = 0; i < img.length; i++) {
						var current = img[i];
						var obj = {
							url: current.url,
							resource: current.resource,
							type: current.type
						};
						$scope.formData.productData.media.images[current.language].push(obj);
					}
					
					var language = ["en", "fr"];
					
					for (var i = 0; i < language.length; i++) {
						document.getElementById("browseimage_" + language[i]).value = "";
					}
					
					$scope.saveResources($scope, productsSrv, function () {
						$scope.saveMyProduct(productsSrv, "edit", false);
					});
					
				}
			});
		}
		else {
			$scope.saveResources($scope, productsSrv, function () {
				$scope.saveMyProduct(productsSrv, "edit", false);
			});
		}
		
	};
	
	$scope.saveDraft = function () {
		overlayLoading.show();
		var myFiles = productsSrv.extractFilesFromPostedData($scope);
		
		if (!$scope.formData.productData.title['en']) {
			overlayLoading.hide();
			return $scope.$parent.displayAlert('danger', 'Product Title is mandatory to Save as Draft');
		}
		
		if (myFiles.length !== 0) {
			productsSrv.uploadFile($scope, myFiles, "/knowledgebase/products/addImage", function (error, response) {
				if (error) {
					var errorString;
					if (Array.isArray(error)) {
						errorString = error.join(", ");
					}
					else {
						errorString = error.message;
					}
					overlayLoading.hide();
					return $scope.$parent.displayAlert('danger', errorString);
				}
				else {
					var img = [];
					for (var i = 0; i < response.length; i++) {
						img.push({
							"url": response[i].original,
							"resource": response[i].resource,
							"type": response[i].type,
							"language": response[i].language
						});
					}
					for (var i = 0; i < img.length; i++) {
						var current = img[i];
						var obj = {
							url: current.url,
							type: current.type,
							resource: current.resource
						};
						$scope.formData.productData.media.images[current.language].push(obj);
					}
					
					var language = ["en", "fr"];
					
					for (var i = 0; i < language.length; i++) {
						document.getElementById("browseimage_" + language[i]).value = "";
					}
					
					$scope.formData.media = {};
					$scope.saveResources($scope, productsSrv, function () {
						$scope.saveMyProduct(productsSrv, "edit", true);
					});
					
				}
			});
		}
		else {
			$scope.saveResources($scope, productsSrv, function () {
				$scope.saveMyProduct(productsSrv, "edit", true);
			});
		}
		
	};
	
	if ($scope.access.tenant.products.edit || $scope.access.owner.products.edit) {
		$scope.getProduct();
	}
	
	injectFiles.injectCss(ModuleDevMpLocation + "/merchants.css");
}]);
