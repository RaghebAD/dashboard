"use strict";

function getLabelValue(item) {
	var labelAtt;
	if (item.length > 1) {
		//30x30
		var values = [];
		item.forEach(function (oneValue) {
			values.push(oneValue.value);
		});
		labelAtt = values.join("x");
	}
	else {
		if (!item[0].unit) {
			//blue
			labelAtt = item[0].value;
		}
		else {
			//8L
			labelAtt = item[0].value + '' + item[0].unit;
		}
	}
	//item.labelAtt = labelAtt;
	return labelAtt;
}

function cleanArray(array, operator, param1, param2, param3, param4) {
	// loop through the array and delete the params provided if they are null (based on the operator param)
	// and ==> if all of them are empty, or ==> if one of them is empty
	if (!array) {
		return;
	}
	
	for (var i = array.length - 1; i >= 0; i--) {
		var current = array[i];
		if (operator === 'and') {
			if (!current[param1] && !current[param2] && !current[param3] && !current[param4]) {
				array.splice(i, 1);
			}
		}
		else { // default OR
			if (!current[param1] || !current[param2] || !current[param3] || !current[param4]) {
				array.splice(i, 1);
			}
		}
	}
}

function cleanData(obj) {
	// clean empty properties
	var languages = ["en", "fr"]; // -=-=-=-=-
	
	// product's array
	var productData, media, images, imagesArr, videos, resources, resourcesArr, videosArr, crossSellPath, upsellPath;
	var attributes, variation;
	var rules, prerequisiteProduct, incompatibility, products;
	var upi;
	
	if (productData = obj.productData) {
		if (media = productData.media) {
			if (images = media.images) {
				for (var i = 0; i < languages.length; i++) {
					var currentLanguage = languages[i];
					cleanArray(imagesArr = images[currentLanguage], 'and', 'url', 'altTag');
				}
			}
			if (videos = media.videos) {
				for (var i = 0; i < languages.length; i++) {
					var currentLanguage = languages[i];
					cleanArray(videosArr = videos[currentLanguage], 'and', 'url');
				}
			}
			if (resources = media.resources) {
				for (var i = 0; i < languages.length; i++) {
					var currentLanguage = languages[i];
					cleanArray(resourcesArr = resources[currentLanguage], 'and', 'url');
				}
			}
		}
		
		cleanArray(crossSellPath = productData.crossSellPath, 'and', 'udac', 'serial');
		cleanArray(upsellPath = productData.upsellPath, 'and', 'udac', 'serial');
	}
	
	if (attributes = obj.attributes) {
		cleanArray(attributes.category, 'and', 'value');
		cleanArray(variation = attributes.variations, 'and', 'type', 'value');
		
		if (attributes.variations) {
			delete attributes.variations[''];
		}
	}
	
	if (rules = obj.rules) {
		cleanArray(prerequisiteProduct = rules.prerequisiteProduct, 'and', 'serial', 'udac');
		
		if (incompatibility = rules.incompatibility) {
			cleanArray(products = incompatibility.products, 'and', 'serial', 'udac');
		}
	}
	
	// clean upi data
	if (upi = obj.coreData.upi) {
		if (!upi.upc || upi.upc === '') {
			delete upi.upc;
		}
	}
	
	var cleanData = {
		status: obj.status,
		coreData: obj.coreData,
		segmentation: obj.segmentation,
		rules: obj.rules,
		productConfiguration: obj.productConfiguration,
		attributes: obj.attributes,
		productData: obj.productData,
		shippingData: obj.shippingData,
		merchantMeta: obj.merchantMeta,
		fulfillmentRules: obj.fulfillmentRules,
		legal: obj.legal
	};
	return cleanData;
}

function langArray2Object(array) {
	var out = {};
	if (!array) {
		return out;
	}
	for (var i = 0; i < array.length; i++) {
		var currentObject = array[i];
		out[currentObject.language] = currentObject.value;
	}
	return out;
}

function langObject2Array(obj) {
	var output = [];
	if (!obj) {
		return output;
	}
	var elements = Object.keys(obj);
	for (var i = 0; i < elements.length; i++) {
		output.push({
			language: elements[i],
			value: obj[elements[i]]
		});
	}
	return output;
}

function booleanObject2array(obj) {
	var output = [];
	if (!obj) {
		return output;
	}
	var elements = Object.keys(obj);
	for (var i = 0; i < elements.length; i++) {
		if (obj[elements[i]] === true) {
			output.push(elements[i]);
		}
	}
	return output;
}

function array2booleanObject(array) {
	var output = {};
	if (!array) {
		return output;
	}
	array.forEach(function (one) {
		output[one] = true;
	});
	return output;
}

function string2Array(str) {
	if (!str || typeof str !== 'string') {
		return [];
	}
	
	var b = str.split(",");
	var bb = [];
	for (var i = 0; i < b.length; i++) {
		bb.push(
			{
				"value": b[i]
			}
		)
	}
	
	return bb;
}

function array2str(array) {
	if (!array) {
		return '';
	}
	
	var obj = [];
	
	array.forEach(function (one) {
		obj.push(one.value);
	});
	
	return obj.join(',');
}

/**
 * fetch objects from subobjects
 *
 * @param input
 */
function fetchMarketsFromSubObjects(input) {
	var output = [];
	if (!input) {
		return [];
	}
	var inputAsArray = Object.keys(input);
	inputAsArray.forEach(function (each) { // each = array of province
		output = output.concat(input[each]);
	});
	
	return output;
};

function convertIncludedAndExcludedFormToDb(tempo, include, isMarket) {
	var incOrExc;
	
	if (include) {
		incOrExc = 'included';
	} else {
		incOrExc = 'excluded';
	}
	
	var output = {
		included: [],
		excluded: []
	};
	
	for (var i = 0; i < tempo.length; i++) {
		if (tempo[i].isSelected) {
			
			var obj = {
				value: tempo[i].code,
				label: tempo[i].value
			};
			
			// save parent for markets
			if (isMarket) {
				obj.parent = tempo[i].parent;
			}
			
			output[incOrExc].push(obj);
			
			
		}
	}
	return output;
}

function convertIncludedAndExcludedDbToForm(object) {
	var output = [];
	
	var included = object.included;
	var excluded = object.excluded;
	
	for (var i = 0; included && i < included.length; i++) {
		var tempo = {
			code: included[i].value,
			value: included[i].label,
			isSelected: true
		};
		
		// for markets
		if (included[i].parent) {
			tempo.parent = included[i].parent;
		}
		
		output.push(tempo);
	}
	
	for (var i = 0; excluded && i < excluded.length; i++) {
		var tempo = {
			code: excluded[i].value,
			value: excluded[i].label,
			isSelected: false
		};
		
		// for markets
		if (excluded[i].parent) {
			tempo.parent = excluded[i].parent;
		}
		
		output.push(tempo);
	}
	
	return output;
}

function convertVariationsDbToForm(dbData) {
	var tempoVariation = [];
	
	if (dbData.attributes.variations) {
		var variations = dbData.attributes.variations;
		for (var att in dbData.attributes.variations) {
			
			if (variations[att].length > 1) {
				//30x30
				var values = [];
				variations[att].forEach(function (oneValue) {
					values.push(oneValue.value);
				});
				tempoVariation.push(
					{
						type: att,
						value: values.join("x")
					}
				);
			}
			else {
				if (!variations[att][0].unit) {
					//blue
					tempoVariation.push(
						{
							type: att,
							value: variations[att][0].value
						}
					);
				}
				else {
					//8L
					tempoVariation.push(
						{
							type: att,
							value: variations[att][0].value + '' + variations[att][0].unit
						}
					);
				}
			}
		}
	}
	return tempoVariation;
}

function convertVariationsFormToDb(tempoVariations) {
	var variations = {};
	
	for (var i = 0; i < tempoVariations.length; i++) {
		
		variations[tempoVariations[i].type] = [
			{
				'value': tempoVariations[i].value
			}
		];
		
	}
	
	return variations;
}

/**
 * generate group id based on the udac and the current timestamp, if there is no groupid
 *
 * @param {object} coreData
 * @returns {string} groupId
 */
function generateGroupId(coreData) {
	if (coreData.groupId && coreData.groupId !== '') {
		return coreData.groupId;
	} else {
		if (coreData.udac && coreData.udac !== '') {
			return coreData.udac + '_' + Date.now();
		} else {
			return ''; // if the user didnt enter the udac yet (on draft for ex)
		}
	}
}

var productsService = soajsApp.components;
productsService.service('productsSrv', ['$timeout', 'ngDataApi', '$cookies', 'Upload', '$modal', '$localStorage',
	function ($timeout, ngDataApi, $cookies, Upload, $modal, $localStorage) {
		
		function callAPI(currentScope, config, callback) {
			getSendDataFromServer(currentScope, ngDataApi, config, callback);
		}
		
		function html_entity_decode(string) {
			var txt = document.createElement("textarea");
			txt.innerHTML = string;
			string = txt.value;
			return string;
		}
		
		function pad(n, l) {
			var limit = (l) ? l : 14;
			while (n.length < limit) {
				n = '0' + n;
			}
			return n;
		}
		
		function generateGTIN(upi) {
			var gtin;
			if (upi.isbn) {
				gtin = pad(upi.isbn, 14);
			}
			else if (upi.upc) {
				gtin = pad(upi.upc, 14);
			}
			else if (upi.ean) {
				gtin = pad(upi.ean, 14);
			}
			else if (upi.jan) {
				gtin = pad(upi.jan, 14);
			}
			return gtin;
		}
		
		function validateEAN(ean) {
			var lastD;
			if (ean.length === 8) {
				lastD = parseInt(ean[ean.length - 1], 10);
				var sum1 = ean[1] + ean[3] + ean[5];
				var sum2 = 3 * (ean[0] + ean[2] + ean[4] + ean[6]);
				
				var checksumValue = sum1 + sum2;
				var checksumDigit = 10 - (checksumValue % 10);
				return (checksumDigit === lastD);
			} else {
				lastD = parseInt(ean[ean.length - 1], 10);
				var chars = [];
				var sum = 0;
				for (var i = 0; i < ean.length - 1; i++) {
					if (i % 2 === 0) {
						chars.push(ean[i] * 1);
					} else {
						chars.push(ean[i] * 3);
					}
					sum += chars[i];
				}
				var h10 = Math.ceil(sum / 10) * 10;
				return (h10 - sum === lastD);
			}
		}
		
		function validateUPC(upc) {
			if (upc.length < 12) {
				upc = pad(upc, 12);
			}
			var s1 = 0, s2 = 0, s3 = 0;
			var lastD = parseInt(upc[upc.length - 1], 10);
			upc = upc.substr(0, upc.length - 1);
			for (var i = 0; i < upc.length; i++) {
				if (i % 2 === 0) {
					s1 += parseInt(upc[i], 10);
				} else {
					s2 += parseInt(upc[i], 10);
				}
			}
			s3 = (s1 * 3) + s2;
			var h10 = Math.ceil(s3 / 10) * 10;
			return ( h10 - s3 === lastD);
		}
		
		function validateISBN(isbn) {
			var lastD;
			var chars = [];
			var remainder;
			var sum = 0;
			if (isbn.length === 10) {
				lastD = isbn[isbn.length - 1];
				lastD = (lastD === 'x' || lastD === 'X') ? 'X' : parseInt(lastD, 10);
				
				var j = 10;
				for (var i = 0; i < (isbn.length - 1); i++) {
					chars.push(j * isbn[i]);
					j--;
					sum += chars[i];
				}
				remainder = sum % 11;
				if (lastD === 'X' && (11 - remainder) === 10) {
					return true;
				}
				//return ((11 - remainder) % 11 === lastD) ? true : false;  //we can also use this.
				return (sum % 11 === lastD);
			} else {
				lastD = parseInt(isbn[isbn.length - 1], 10);
				for (var k = 0; k < isbn.length - 1; k++) {
					if (k % 2 === 0) {
						chars.push(isbn[k] * 1);
					} else {
						chars.push(isbn[k] * 3);
					}
					sum += chars[k];
				}
				remainder = sum % 10;
				return (10 - remainder === lastD);
			}
		}
		
		function extractFilterValue(string) {
			string = string.trim().replace(/[\s]+/g, " ").toUpperCase();
			if ((/^[0-9\s]+X[\s0-9]+$/).test(string)) {
				var att = string.split('X');
				return [parseInt(att[0]), parseInt(att[1])];
			}
			else if ((/^[0-9]+\s[a-zA-Z]+$/).test(string)) {
				var att = string.split(' ');
				return [parseInt(att[0]), att[1]];
			}
			else if ((/^[0-9]+[a-zA-Z]+$/).test(string)) {
				var pattern = /[0-9]+/;
				var y = string.match(pattern);
				return [parseInt(y[0]), string.replace(y[0], '')];
			}
			else {
				return string;
			}
		}
		
		function uploadFile($scope, files, url, cb) {
			var err = [], myResponse = [];
			if (files && files.length > 0) {
				var max = files.length, counter = 0;
				for (var i = 0; i < files.length; i++) {
					if (files[i]) {
						var config = {
							language: files[i].language,
							url: url,
							file: files[i]
						};
						doUpload(config, function (error, response) {
							if (error) {
								err.push(error.message);
							}
							else {
								myResponse.push(response[0]);
							}
							counter++;
							if (counter === max) {
								return (err.length > 0) ? cb(err) : cb(null, myResponse);
							}
						});
					}
				}
			}
			else {
				cb(null, myResponse);
			}
			function doUpload(config, cb) {
				var opt = {
					"method": "send",
					"file": config.file,
					"routeName": config.url,
					"upload": true,
					"params": {
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
				
				getSendDataFromServer($scope, ngDataApi, opt, function (error, response) {
					if (error) {
						return cb(new Error(translation.errorOccurredWhileUploadingFile[LANG] + " " + config.file));
					}
					else {
						response[0].language = config.language;
						return cb(null, response);
					}
				});
			}
		}
		
		function imageUpload($scope, postData, images, cb) {
			if (postData.productData && postData.productData.media.images) {
				var lang = ["en", "fr"];
				var doLang = function (l, cback) {
					images[l] = [];
					if (postData.productData.media.images[l] && postData.productData.media.images[l].length !== 0) {
						if (postData.productData.media.images[l][0].url === '') {
							postData.productData.media.images[l] = [];
							return cback();
						}
						
						async.mapSeries(postData.productData.media.images[l], function (oneImg, uCb) {
							if (oneImg.type !== 'ypg_media_server' && oneImg.url !== undefined && oneImg.url !== '') {
								// TODO: remove condition when data is cleaned
								if (oneImg.type === 'main' || oneImg.type === '') {
									var opt = {
										"method": "send",
										"routeName": '/knowledgebase/products/addImageUrl',
										"params": {
											"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
										},
										"data": {
											"url": oneImg.url
										}
									};
									getSendDataFromServer($scope, ngDataApi, opt, function (error, response) {
										if (error) {
											return uCb(error.message);
										}
										else {
											var image = {
												"url": response.original,
												"type": response.type,
												"resource": response.resource
											};
											if (oneImg.altTag) {
												image.altTag = oneImg.altTag
											}
											images[l].push(image);
											return uCb(null, true)
										}
									});
								}
							}
							else {
								if (oneImg.type === 'ypg_media_server') {
									if (oneImg.altTag && oneImg.altTag === '') {
										delete oneImg.altTag;
									}
									images[l].push(oneImg);
								}
								return uCb(null, true);
							}
						}, function () {
							return cback(null, true);
						});
					}
					else {
						cback();
					}
				};
				async.each(lang, doLang, function () {
					postData.productData.media.images = images;
					return cb(null, true);
				});
			}
			else {
				return cb(null, true);
			}
		}
		
		function buildCategoriesFromProfile(currentScope, localStorage, profileRecord, cb) {
			var defaultLanguage = '';
			var getFullPath = function (category, profileRecord, path) {
				
				if (category['parents'].length === 0) { // root
					return path;
				} else { // 1 parent
					var records = Object.keys(profileRecord['categories']);
					var parent = category['parents'][0];
					for (var i = 0; i < records.length; i++) {
						if (records[i] === parent) {
							var current = profileRecord['categories'][records[i]];
							var label = profileRecord.taxonomies[records[i]].label[defaultLanguage].value;
							return getFullPath(current, profileRecord, label + " / " + path);
						}
					}
				}
			};
			
			for (var i = 0; i < profileRecord.languages.length; i++) {
				if (profileRecord.languages[i].selected) {
					defaultLanguage = profileRecord.languages[i].id;
				}
			}
			currentScope.defaultLang = defaultLanguage;
			
			function sortParent() {
				function doCategory(cat, cb) {
					catCount++;
					currentScope.progress = Math.ceil((catCount * 100) / catnames.length);
					if (!currentScope.$$phase) {
						currentScope.$apply();
					}
					if (!profileRecord.categories[cat].parents || profileRecord.categories[cat].parents.length === 0) {
						processCategoryLevel(cat, profileRecord.categories, function () {
							return cb(null, true);
						});
					}
					else {
						return cb(null, true);
					}
				}
				
				function processCategoryLevel(cat, categoriesArray, cb) {
					var label = cat;
					if (profileRecord.taxonomies[cat]) {
						if (profileRecord.taxonomies[cat].label[defaultLanguage]) {
							label = profileRecord.taxonomies[cat].label[defaultLanguage].value;
						}
						else {
							console.log(profileRecord.taxonomies[cat]);
						}
					}
					else {
						console.log('Missing ' + cat);
					}
					var id = cat;
					if (categoriesArray[cat]) {
						if (categoriesArray[cat]['deprecated-towards']) {
							//get the new value
							id = categoriesArray[cat]['deprecated-towards'];
							label += " (deprecated), becomes: " + profileRecord.taxonomies[id].label[defaultLanguage].value;
						}
						
						if (categoriesArray[cat].parents && categoriesArray[cat].parents.length > 0) {
							label = getLabels(categoriesArray[cat].parents, defaultLanguage) + " / " + label;
						}
						
						var t = {
							"v": id,
							"l": label
						};
						
						if (categoriesArray[cat].attributes) {
							if (categoriesArray[cat].attributes.department) {
								var departments = [];
								for (var i = 0; i < categoriesArray[cat].attributes.department.length; i++) {
									var depId = categoriesArray[cat].attributes.department[i];
									if (profileRecord.taxonomies && profileRecord.taxonomies[depId] && profileRecord.taxonomies[depId].label) {
										departments.push({
											"v": depId,
											"l": profileRecord.taxonomies[depId].label[defaultLanguage].value
										});
									}
								}
								t["department"] = departments;
							}
							
							if (categoriesArray[cat].attributes.classification) {
								var classification = [];
								for (var i = 0; i < categoriesArray[cat].attributes.classification.length; i++) {
									var classId = categoriesArray[cat].attributes.classification[i];
									
									if (profileRecord.taxonomies && profileRecord.taxonomies[classId] && profileRecord.taxonomies[classId].label) {
										classification.push({
											"v": classId,
											"l": profileRecord.taxonomies[classId].label[defaultLanguage].value
										});
									}
								}
								t["classification"] = classification;
							}
							if (categoriesArray[cat].attributes.filters) {
								var filters = [];
								var filter = Object.keys(categoriesArray[cat].attributes.filters);
								for (var i = 0; i < filter.length; i++) {
									if (profileRecord.taxonomies && profileRecord.taxonomies[filter[i]] && profileRecord.taxonomies[filter[i]].label) {
										filters.push({
											"v": filter[i],
											"l": profileRecord.taxonomies[filter[i]].label[defaultLanguage].value
										});
									}
								}
								t["filters"] = filters;
								t["filtersObject"] = categoriesArray[cat].attributes.filters;
							}
						}
						
						categories.push(t);
						
						if (categoriesArray[cat].children && categoriesArray[cat].children.length > 0) {
							async.eachLimit(categoriesArray[cat].children, categoriesArray[cat].children.length, doCatChild, function () {
								return cb();
							});
						}
						else {
							return cb();
						}
					}
					else {
						return cb();
					}
					
					function doCatChild(oneChild, cb) {
						processCategoryLevel(oneChild, categoriesArray, cb);
					}
				}
				
				function getLabels(parentsArray, langCode) {
					var out = [];
					for (var i = 0; i < parentsArray.length; i++) {
						var oneParentCat = parentsArray[i];
						var label = profileRecord.taxonomies[oneParentCat].label[langCode].value;
						
						if (profileRecord.categories[oneParentCat].parents && profileRecord.categories[oneParentCat].parents.length > 0) {
							label = getLabels(profileRecord.categories[oneParentCat].parents, langCode) + " / " + label;
						}
						out.push(label);
					}
					
					
					return out.join(" / ");
				}
				
				if (profileRecord.treats) {
					var out = [];
					for (var i = 0; i < profileRecord.treats.length; i++) {
						var treatId = profileRecord.treats[i];
						var treatLabel = treatId;
						if (profileRecord.taxonomies[treatId]) {
							treatLabel = profileRecord.taxonomies[treatId].label[defaultLanguage].value;
						}
						out.push({
							'v': treatId,
							'l': treatLabel
						});
					}
					profileRecord.treats = out;
				}
				if (localStorage.profile && localStorage.profile.categories && localStorage.profile.ts && localStorage.profile.env && localStorage.profile.ts === profileRecord.ts && localStorage.profile.env === currentScope.$parent.currentSelectedEnvironment.toUpperCase()) {
					return cb(null, localStorage.profile.categories);
				}
				else {
					var categories = [];
					var catnames = [];
					if (profileRecord.categories) {
						catnames = Object.keys(profileRecord.categories);
					}
					var catCount = 0;
					async.eachSeries(catnames, doCategory, function () {
						delete profileRecord.taxonomies;
						localStorage.profile = {
							"categories": categories,
							"ts": profileRecord.ts,
							"env": currentScope.$parent.currentSelectedEnvironment.toUpperCase()
						};
						return cb(null, categories);
					});
				}
			}
			
			sortParent();
		}
		
		function migrateProducts(currentScope) {
			var products = currentScope.grid.rows;
			$modal.open({
				templateUrl: "migration.html",
				size: 'lg',
				backdrop: true,
				keyboard: true,
				controller: function ($scope, $modalInstance) {
					var otherEnvs = angular.copy($localStorage.environments);
					for (var i = otherEnvs.length - 1; i >= 0; i--) {
						if (otherEnvs[i].code === currentScope.currentSelectedEnvironment.toUpperCase()) {
							otherEnvs.splice(i, 1);
						}
						else if (otherEnvs[i].code === 'DASHBOARD') {
							otherEnvs.splice(i, 1);
						}
					}
					fixBackDrop();
					$scope.title = translation.productsMigration[LANG];
					$scope.data = {
						all: false,
						envs: otherEnvs
					};
					
					$scope.data.products = [];
					var originalProducts = [];
					products.forEach(function (oneProduct) {
						var obj = {
							"_id": oneProduct._id,
							"name": oneProduct.productData.title["en"]
						};
						if (oneProduct.coreData.udac) {
							obj.name += " (" + oneProduct.coreData.udac + ")";
						}
						originalProducts.push(obj);
					});
					$scope.data.products = angular.copy(originalProducts);
					$scope.filterData = function (query) {
						if (query && query !== '' && query.length >= 3) {
							var filtered = [];
							for (var i = 0; i < $scope.data.products.length; i++) {
								var pattern = query.toLowerCase();
								if ($scope.data.products[i].name.toLowerCase().indexOf(pattern) !== -1) {
									filtered.push($scope.data.products[i]);
								}
							}
							$scope.data.products = filtered;
						}
						else {
							$scope.data.products = originalProducts;
						}
					};
					
					$scope.submit = function () {
						if (!$scope.data.all && !$scope.data.selectedProducts) {
							alert("Select at least one product");
							return;
						}
						
						if (!$scope.data.toEnv) {
							alert("Select a destination environment");
							return;
						}
						
						var postData = {
							"type": "products",
							"all": $scope.data.all,
							"toEnv": $scope.data.toEnv.toUpperCase()
						};
						if (!$scope.data.all) {
							postData.ids = Object.keys($scope.data.selectedProducts)
						}
						else {
							postData.code = currentScope.code;
							postData.merchantId = currentScope.merchantId;
						}
						if (currentScope.access.owner.products.registerMigration || currentScope.access.tenant.products.registerMigration) {
							var opts = {
								"routeName": "/knowledgebase/migrate/data/register",
								"method": "send",
								"params": {
									"__env": currentScope.currentSelectedEnvironment.toUpperCase()
								},
								"data": postData
							};
							overlayLoading.show();
							getSendDataFromServer(currentScope, ngDataApi, opts, function (error) {
								overlayLoading.hide();
								if (error) {
									$scope.message = {
										"danger": true,
										"text": error.message
									};
								}
								else {
									$scope.message = {};
									$scope.data = {};
									currentScope.$parent.displayAlert('success', 'Products Registered for Migration.');
									$modalInstance.close();
								}
							});
						}
						else {
							$modalInstance.close();
						}
					};
					
					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};
				}
			});
		}
		
		return {
			
			'getEntriesFromApi': function (currentScope, opts, callback) {
				callAPI(currentScope, opts, callback);
			},
			
			'printProducts': function (currentScope, response) {
				var brand = [], title = [], treats = [];
				if (!response) {
					response = [];
				}
				
				for (var x = 0; x < response.length; x++) {
					response[x].priceSource = response[x].coreData.priceSource;
					if (response[x].coreData.upi && response[x].coreData.upi.gtin) {
						response[x].gtin = response[x].coreData.upi.gtin;
					}
					response[x].udac = response[x].coreData.udac;
					if (response[x].coreData.groupId) {
						response[x].groupId = response[x].coreData.groupId;
					}
					if (response[x].merchantMeta) {
						response[x].price = response[x].merchantMeta.pricing.price;
						if (response[x].merchantMeta.pos) {
							if (response[x].merchantMeta.pos[0]) {
								response[x].availability = response[x].merchantMeta.pos[0].availability;
							}
						}
					}
					
					response[x].treats = '';
					response[x].variations = '';
					response[x].category = '';
					if (response[x].productData) {
						if (response[x].productData.title) {
							response[x].title = response[x].productData.title['en']; // LANG-=-=-=-=-
						}
						//get brand
						if (response[x].attributes && response[x].attributes.brand) {
							response[x].attributes.brand.forEach(function (oneBrand) {
								brand.push(oneBrand.value);
							});
							response[x].brand = brand.join(',');
						}
						if (response[x].attributes.category && response[x].attributes.category.length > 0) {
							response[x].category = response[x].attributes.category[0].value;
						}
						if (response[x].productData.treats) {
							response[x].productData.treats.forEach(function (tr) {
								treats.push(tr.value);
							});
							response[x].treats = treats.join(', ');
						}
						if (response[x].attributes.variations) {
							for (var a in response[x].attributes.variations) {
								response[x].variations = response[x].variations + a + ': ' + getLabelValue(response[x].attributes.variations[a]) + ';<br>';
							}
						}
						brand = [];
						treats = [];
					}
				}
				var options = {
					'grid': {
						recordsPerPageArray: [50, 100, 200, 400],
						'columns': [
							// { 'label': translation.gtin[LANG], 'field': 'gtin' },
							{ 'label': "UDAC", 'field': 'udac' },
							// {'label': translation.group[LANG], 'field': 'groupId'},
							{ 'label': translation.mpFields.serial[LANG], 'field': 'serial' },
							{ 'label': translation.title[LANG], 'field': 'title', 'filter': 'trimmed100' },
							// {'label': translation.attributeVariations[LANG], 'field': 'variations'},
							// {'label': translation.treats[LANG], 'field': 'treats'},
							{ 'label': "Category", 'field': 'category' },
							//{'label': translation.brand[LANG], 'field': 'brand'},
							// {'label': translation.availability[LANG], 'field': 'availability'},
							{ 'label': translation.price[LANG], 'field': 'price' },
							{ 'label': 'Price Source', 'field': 'priceSource' },
							{ 'label': translation.status[LANG], 'field': 'status' }
						],
						'defaultLimit': 200
					},
					'defaultSortField': '',
					'data': response,
					'left': [],
					'top': []
				};
				if (currentScope.access.tenant.products.list || currentScope.access.owner.products.list) {
					options.left.push({
						'icon': 'search',
						'label': translation.view[LANG],
						'handler': 'viewProduct'
					});
				}
				if (currentScope.access.tenant.products.edit || currentScope.access.owner.products.edit) {
					options.left.push({
						'label': translation.edit[LANG],
						'icon': 'pencil2',
						'handler': 'editProduct'
					});
				}
				// if (currentScope.access.tenant.products.updateStock || currentScope.access.owner.products.updateStock) {
				// 	options.left.push({
				// 		'label': translation.updateStock[LANG],
				// 		'icon': 'stats-bars',
				// 		'handler': 'updateStock'
				// 	});
				// }
				if (currentScope.access.tenant.sale.update) {
					// options.left.push({
					// 	'label': translation.updateSale[LANG],
					// 	'icon': 'coin-dollar',
					// 	'handler': 'applyVariationSale'
					// });
				}
				if (currentScope.access.tenant.products.updateTreats || currentScope.access.owner.products.updateTreats) {
					// options.left.push({
					// 	'label': translation.updateTreats[LANG],
					// 	'icon': 'clipboard',
					// 	'handler': 'updateTreats'
					// });
				}
				if (currentScope.access.tenant.products.getProductVariations || currentScope.access.owner.products.getProductVariations) {
					// options.left.push({
					// 	'label': translation['updateImages'][LANG],
					// 	'icon': 'images',
					// 	'handler': 'updateVariationImages'
					// });
				}
				if (currentScope.access.tenant.products.changeStatus || currentScope.access.owner.products.changeStatus) {
					options.left.push({
						'label': translation.disableActivate[LANG],
						'icon': 'spinner9',
						'msg': translation.areYouSureThatYouWantToChangeStatusOfThisProduct[LANG],
						'handler': 'changeStatus'
					});
					options.top.push({
						'label': translation.activateProduct[LANG],
						'msg': translation.areYouSureYouWantToActivateTheSelectedProduct[LANG],
						'handler': 'activate'
					});
					
					options.top.push({
						'label': translation.disableProduct[LANG],
						'msg': translation.areYouSureYouWantToActivateTheSelectedProduct[LANG],
						'handler': 'disable'
					});
				}
				
				if (currentScope.access.tenant.products.delete || currentScope.access.owner.products.delete) {
					options.left.push({
						'label': translation.delete[LANG],
						'icon': 'cross',
						'msg': translation.areYouSureYouWantToDeleteThisProduct[LANG],
						'handler': 'deleteProduct'
					});
					
					options.top.push({
						'label': translation.delete[LANG],
						'msg': translation.areYouSureYouWantToDeleteTheSelectedProduct[LANG],
						'handler': 'deleteProducts'
					});
				}
				
				buildGrid(currentScope, options);
			},
			
			'dataDbtoFormUiUi': function (currentScope, dbData, formData, editMode) {
				formData.status = dbData.status;
				formData.coreData = dbData.coreData;
				formData.segmentation = dbData.segmentation;
				formData.rules = dbData.rules;
				formData.productConfiguration = dbData.productConfiguration;
				formData.productConfiguration.forEach(function (one) {
					if (one.name === 'plan') {
						one.name = 'paymentPlan';
					}
				});
				formData.attributes = dbData.attributes;
				formData.productData = dbData.productData;
				formData.shippingData = dbData.shippingData;
				formData.merchantMeta = dbData.merchantMeta;
				formData.fulfillmentRules = dbData.fulfillmentRules;
				formData.legal = dbData.legal;
				formData.serial = dbData.serial;
				
				currentScope.tempo.variation = convertVariationsDbToForm(dbData);
				
				currentScope.tempo.brand = array2str(formData.attributes.brand);
				currentScope.tempo.classification = array2str(formData.attributes.classification);
				
				var segmentation = formData.segmentation;
				if (segmentation && segmentation != null) {
					if (segmentation.salesRep && segmentation.salesRep.serviceArea) {
						currentScope.tempo.salesRepMarkets = convertIncludedAndExcludedDbToForm(segmentation.salesRep.serviceArea);
						
						if (segmentation.salesRep.serviceArea.province) {
							currentScope.tempo.salesRepServiceArea = convertIncludedAndExcludedDbToForm(segmentation.salesRep.serviceArea.province);
							
							// append to display
							var provincesSavedInDb = segmentation.salesRep.serviceArea.province.included;
							provincesSavedInDb.forEach(function (province) {
								var condition = {
									provinceCode: province.value
								};
								extraUtils.setSegmentationDataSpecificApi(currentScope, ngDataApi, "/knowledgebase/product/markets", 'completeSalesRepMarkets', 'salesRepMarkets', condition, function (error) {
									currentScope.tempo.salesRepSelectedMarketForDisplay[province.value] = extraUtils.assignSelectedSegmentation(currentScope.tempo.completeSalesRepMarkets, currentScope.tempo.salesRepMarkets, currentScope.tempo.salesRepMarketIsInclusive, province.value);
								});
							});
						}
					}
					
					if (segmentation.serviceArea) {
						currentScope.tempo.markets = convertIncludedAndExcludedDbToForm(segmentation.serviceArea);
						
						if (segmentation.serviceArea.province) {
							currentScope.tempo.serviceArea = convertIncludedAndExcludedDbToForm(segmentation.serviceArea.province);
							
							// append to display
							var provincesSavedInDb = segmentation.serviceArea.province.included;
							provincesSavedInDb.forEach(function (province) {
								var condition = {
									provinceCode: province.value
								};
								extraUtils.setSegmentationDataSpecificApi(currentScope, ngDataApi, "/knowledgebase/product/markets", 'completeMarkets', 'markets', condition, function (error) {
									currentScope.tempo.selectedMarketForDisplay[province.value] = extraUtils.assignSelectedSegmentation(currentScope.tempo.completeMarkets, currentScope.tempo.markets, currentScope.tempo.marketIsInclusive, province.value);
								});
							});
						}
					}
					if (segmentation.verticals) {
						currentScope.tempo.verticals = convertIncludedAndExcludedDbToForm(segmentation.verticals);
						
						if (segmentation.verticals.excluded) {
							currentScope.tempo.verticalsIsInclusive = (segmentation.verticals.excluded.length === 0);
						}
					}
					if (segmentation.headings) {
						currentScope.tempo.headings = convertIncludedAndExcludedDbToForm(segmentation.headings);
						
						if (segmentation.headings.excluded) {
							currentScope.tempo.headingsIsInclusive = (segmentation.headings.excluded.length === 0);
						}
					}
					if (segmentation.sensitiveHeadings) {
						currentScope.tempo.sensitiveHeadings = convertIncludedAndExcludedDbToForm(segmentation.sensitiveHeadings);
						
						if (segmentation.sensitiveHeadings.excluded) {
							currentScope.tempo.sensitiveHdingsIsInclusive = (segmentation.sensitiveHeadings.excluded.length === 0);
						}
					}
				}
				
				// backward compatible
				var salesChannels;
				if (formData.segmentation.salesRep && formData.segmentation.salesRep.salesChannels) {
					salesChannels = formData.segmentation.salesRep.salesChannels;
				} else {
					salesChannels = formData.segmentation.salesChannels;
					if (!formData.segmentation.salesRep) {
						formData.segmentation.salesRep = {};
					}
					
					formData.segmentation.salesRep.salesChannels = salesChannels;
					delete formData.segmentation.salesChannels;
				}
				
				currentScope.tempo.salesChannels = array2booleanObject(salesChannels);
				currentScope.tempo.accountType = array2booleanObject(formData.segmentation.accountType);
				
				// pricing rules {onetime, recurring, registration} labels backward compatible : accepted as a string
				if (formData.merchantMeta.pricing && formData.merchantMeta.pricing.rules) {
					var rules = formData.merchantMeta.pricing.rules;
					var count = 0;
					var currentTime = (new Date().getTime()).toString();
					
					rules.forEach(function (eachRule) {
						count++;
						
						// if the rule doesn't have an id, generate one
						if (!eachRule.id) {
							eachRule.id = currentTime + count;
						}
						
						// if the rule type is not set, default it to regular
						// if the price source is set to internal, force all price sources to regular // override the first statement
						if (!eachRule.ruleType) {
							eachRule.ruleType = 'regular';
						}
						if (formData.coreData.priceSource === 'internal') {
							eachRule.ruleType = 'regular';
						}
						
						// conditions values boolean to string
						if (eachRule.conditions) {
							eachRule.conditions.forEach(function (condition) {
								if (typeof condition.value === "boolean") {
									condition.value = String(condition.value);
								}
							});
						}
						
						if (eachRule.onetime) {
							if (typeof eachRule.onetime.name === 'string') {
								eachRule.onetime.name = {
									"en": eachRule.onetime.name,
									"fr": ""
								};
							}
						}
						
						if (eachRule.registration && eachRule.registration.length > 0) {
							eachRule.registration.forEach(function (eachRegistrationRule) {
								if (typeof eachRegistrationRule.name === 'string') {
									eachRegistrationRule.name = {
										"en": eachRegistrationRule.name,
										"fr": ""
									};
								}
							});
						}
						
						if (eachRule.recurring && eachRule.recurring.length > 0) {
							eachRule.recurring.forEach(function (eachRecurringRule) {
								if (typeof eachRecurringRule.name === 'string') {
									eachRecurringRule.name = {
										"en": eachRecurringRule.name,
										"fr": ""
									};
								}
							});
						}
					});
				}
				
				// fulfillment plan convert config to tempo
				if (formData.fulfillmentRules.fulfillmentPlan && formData.fulfillmentRules.fulfillmentPlan.providers) {
					formData.fulfillmentRules.fulfillmentPlan.providers.forEach(function (provider) {
						provider.tempo = [];
						var configKeys = Object.keys(provider.config);
						configKeys.forEach(function (key) {
							provider.tempo.push({
								key,
								value: provider.config[key]
							});
						});
					});
				}
				
				if (!editMode) { // fill product
					formData.serial = '';
				}
				
				return formData;
			},
			
			'dataFormtoDb': function (currentScope, postData, formData, editMode, cb) {
				if (postData.legal.termsConditionsLink) {
					if (Object.hasOwnProperty.call(postData.legal.termsConditionsLink, 'en')) {
						if (postData.legal.termsConditionsLink.en === '') {
							delete postData.legal.termsConditionsLink.en;
						}
					}
					if (Object.hasOwnProperty.call(postData.legal.termsConditionsLink, 'fr')) {
						if (postData.legal.termsConditionsLink.fr === '') {
							delete postData.legal.termsConditionsLink.fr;
						}
					}
				}
				postData.attributes.brand = string2Array(currentScope.tempo.brand);
				postData.attributes.classification = string2Array(currentScope.tempo.classification);
				postData.attributes.variations = convertVariationsFormToDb(currentScope.tempo.variation);
				
				currentScope.tempo.markets = fetchMarketsFromSubObjects(currentScope.tempo.selectedMarketForDisplay);
				postData.segmentation.serviceArea = convertIncludedAndExcludedFormToDb(currentScope.tempo.markets, currentScope.tempo.serviceAreaIsInclusive, true);
				postData.segmentation.serviceArea.province = convertIncludedAndExcludedFormToDb(currentScope.tempo.serviceArea, currentScope.tempo.serviceAreaIsInclusive, false);
				
				currentScope.tempo.salesRepMarkets = fetchMarketsFromSubObjects(currentScope.tempo.salesRepSelectedMarketForDisplay);
				postData.segmentation.salesRep.serviceArea = convertIncludedAndExcludedFormToDb(currentScope.tempo.salesRepMarkets, currentScope.tempo.salesRepServiceAreaIsInclusive, true);
				postData.segmentation.salesRep.serviceArea.province = convertIncludedAndExcludedFormToDb(currentScope.tempo.salesRepServiceArea, currentScope.tempo.salesRepServiceAreaIsInclusive, false);
				
				postData.segmentation.verticals = convertIncludedAndExcludedFormToDb(currentScope.tempo.verticals, currentScope.tempo.verticalsIsInclusive, false);
				postData.segmentation.headings = convertIncludedAndExcludedFormToDb(currentScope.tempo.headings, currentScope.tempo.headingsIsInclusive, false);
				postData.segmentation.sensitiveHeadings = convertIncludedAndExcludedFormToDb(currentScope.tempo.sensitiveHeadings, currentScope.tempo.sensitiveHdingsIsInclusive, false);
				
				postData.segmentation.salesRep.salesChannels = booleanObject2array(currentScope.tempo.salesChannels);
				postData.segmentation.accountType = booleanObject2array(currentScope.tempo.accountType);
				
				postData.coreData.groupId = generateGroupId(postData.coreData);
				
				if (postData.merchantMeta.pricing.repCommission) {
					postData.merchantMeta.pricing.repCommission = {};
				}
				
				// fulfillment plan providers, tempo array => config object
				if (postData.fulfillmentRules.fulfillmentPlan && postData.fulfillmentRules.fulfillmentPlan.providers) {
					postData.fulfillmentRules.fulfillmentPlan.providers.forEach(function (provider) {
						provider.tempo.forEach(function (temp) {
							provider.config[temp.key] = temp.value;
						});
						delete provider.tempo;
					});
				}
				// clean pricing rules : check if set to true, clear renewal rule
				// clear fields based on ruletype
				if (postData.merchantMeta && postData.merchantMeta.pricing && postData.merchantMeta.pricing.rules) {
					postData.merchantMeta.pricing.rules.forEach(function (rule) {
						if (rule.autoRenewal) {
							delete rule.renewalRule;
						}
						
						if (rule.conditions) {
							rule.conditions.forEach(function (condition) {
								if (condition.value === "true" || condition.value === "false") {
									condition.value = condition.value === "true" ? true : false;
								}
							});
						}
						
						if (rule.ruleType === 'adjustment') {
							delete rule.onetime;
							delete rule.registration;
							delete rule.recurring;
							delete rule.term;
						} else { // undefined or regular
							delete  rule.adjustment;
						}
					});
				}
				
				postData = cleanData(postData);
				
				var images = {};
				imageUpload(currentScope, postData, images, function () {
					return cb(postData);
				});
			},
			
			'extractFilesFromPostedData': function extractFilesFromPostedData($scope) {
				var formUrlFiles = [];
				if ($scope.response.profile.languages) {
					
				}
				var lang = ["en", "fr"];
				if ($scope.formData.media && $scope.formData.media.upload) {
					if ($scope.formData.media.upload.images) {
						lang.forEach(function (l) {
							if ($scope.formData.media.upload.images[l]) {
								for (var i in $scope.formData.media.upload.images[l]) {
									if (typeof $scope.formData.media.upload.images[l][i] === 'object') {
										var obj = $scope.formData.media.upload.images[l][i];
										obj.language = l;
										formUrlFiles.push(obj);
									}
								}
							}
						});
					}
				}
				
				return formUrlFiles;
			},
			
			'extractResourcesFromPostedData': function extractResourcesFromPostedData($scope) {
				var formUrlResources = [];
				
				var lang = ["en", "fr"];
				if ($scope.formData.media && $scope.formData.media.upload) {
					if ($scope.formData.media.upload.resources) {
						lang.forEach(function (l) {
							if ($scope.formData.media.upload.resources[l]) {
								for (var i in $scope.formData.media.upload.resources[l]) {
									if (typeof $scope.formData.media.upload.resources[l][i] === 'object') {
										var obj = $scope.formData.media.upload.resources[l][i];
										obj.language = l;
										formUrlResources.push(obj);
									}
								}
							}
						});
					}
				}
				
				
				return formUrlResources;
			},
			
			'uploadFile': uploadFile,
			
			'imageUpload': imageUpload,
			
			"buildCategoriesFromProfile": buildCategoriesFromProfile,
			
			"migrateProducts": migrateProducts
		}
	}]);