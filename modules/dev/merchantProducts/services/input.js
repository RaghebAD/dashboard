"use strict";
var cbInputService = soajsApp.components;

cbInputService.service('cbInputHelper', ['ngDataApi', '$timeout', '$modal', '$window', function (ngDataApi, $timeout, $modal, $window) {

	var valuesRow = [
		{
			'name': 'valuesValue_%count%',
			'label': 'Value',
			'type': 'text',
			'required': true
		},
		{
			'name': 'valuesLabel_en_%count%',
			'label': 'Label English',
			'type': 'text',
			'required': true
		},
		{
			'name': 'valuesLabel_fr_%count%',
			'label': 'Label French',
			'type': 'text',
			'required': true
		},
		{
			"name": "removeValues_%count%",
			"type": "html",
			"value": "<span class='red'><span class='icon icon-cross' title='Remove'></span></span>",
			"onAction": function (id, data, form) {
				var number = id.replace("removeValues_", "");
				//todo: need to decrease count
				delete form.formData['label' + number];
				delete form.formData['value' + number];
				form.entries.forEach(function (oneEntry) {
					if (oneEntry.type === 'group' && oneEntry.name === 'values') {
						for (var i = oneEntry.entries.length - 1; i >= 0; i--) {
							if (oneEntry.entries[i].name === 'valuesLabel_en_' + number) {
								oneEntry.entries.splice(i, 1);
							}
							else if (oneEntry.entries[i].name === 'valuesLabel_fr_' + number) {
								oneEntry.entries.splice(i, 1);
							}
							else if (oneEntry.entries[i].name === 'valuesValue_' + number) {
								oneEntry.entries.splice(i, 1);
							}
							else if (oneEntry.entries[i].name === 'removeValues_' + number) {
								oneEntry.entries.splice(i, 1);
							}
						}
					}
				});
			}
		}
	];

	function getIndexOfEntry(entries, entry) {
		// find between entries.name the entry u r looking for and return its index
		for (var i = 0; i < entries.length; i++) {
			if (entries[i].name === entry) {
				return i;
			}
		}
		console.error("error not found");
		return -1;
	}

	function getFulfillmentRulesContent(valuesCount) {

		var fulfillmentRulesContent = [
			{
				"name": "name",
				"label": "Name",
				"type": "select",
				"value": [
					{ "v": "businessAddress", "l": "businessAddress" },
					{ "v": "businessLine", "l": "Business Line" },
					{ "v": "businessName", "l": "businessName" },
					{ "v": "businessPhone", "l": "businessPhone" },
					{ "v": "methodsOfPayment", "l": "methodsOfPayment" },
					{ "v": "mobileTagLineEN", "l": "mobileTagLineEN" },
					{ "v": "mobileTagLineFR", "l": "mobileTagLineFR" },
					{ "v": "operatingHours", "l": "operatingHours" },
					{ "v": "placementLogoEN", "l": "placementLogoEN" },
					{ "v": "placementLogoFR", "l": "placementLogoFR" },
					{ "v": "placementPromoTextEN", "l": "placementPromoTextEN" },
					{ "v": "placementPromoTextFR", "l": "placementPromoTextFR" },
					{ "v": "primaryHeading", "l": "primaryHeading" },
					{ "v": "productAndServices", "l": "productAndServices" },
					{ "v": "spokenLanguage", "l": "spokenLanguage" },
					{ "v": "calendar", "l": "Calendar" },
					{ "v": "location", "l": "Location" },
					{ "v": "custom", "l": "Custom ..." }
				],
				"required": true,
				'onAction': function (id, data, form) {
					form.entries.forEach(function (entry) {
						if (entry.name === 'customName') {
							entry.hidden = (data !== 'custom');
						}
					});
				}
			},
			{
				'name': 'customName',
				'label': "Custom Name",
				'type': 'text',
				"hidden": true,
				'required': false,
				'fieldMsg': "Enter a customName"
			},
			{
				'name': 'order',
				'label': "Order",
				'type': 'number',
				'required': true,
				'fieldMsg': "Enter an order"
			},
			{
				'name': 'label_en',
				'label': "English Input Label",
				'type': 'text',
				'placeholder': "",
				'value': '',
				'tooltip': 'Enter the input label as it should display in the UI',
				'required': true,
				'fieldMsg': "Enter a label in English"
			},
			{
				'name': 'label_fr',
				'label': "French Input Label",
				'type': 'text',
				'placeholder': "",
				'value': '',
				'tooltip': 'Enter the input label as it should display in the UI',
				'required': true,
				'fieldMsg': "Enter a label in French"
			},
			{
				"name": "required",
				"label": translation.mpFields.required[LANG],
				"type": "radio",
				"value": [
					{ "v": "false", "l": "False" },
					{ "v": "true", "l": "True" }
				],
				"required": false,
				'fieldMsg': "Is this field required for fulfilment?"
			},
			{
				"name": "type",
				"label": "Field Type",
				"type": "select",
				"value": [
					{ "v": "address", "l": "Address" },
					{ "v": "businessSelector", "l": "Business Selector" },
					{ "v": "calendar", "l": "Calendar (Date & Time)" },
					{ "v": "checkbox", "l": "Checkbox: List Multiple Value" },
					{ "v": "date", "l": "Date" },
					{ "v": "editor", "l": translation.advancedEditor[LANG] },
					{ "v": "email", "l": translation.emailAddress[LANG] },
					{ "v": "heading", "l": "Heading" },
					{ "v": "market", "l": "Market" },
					{ "v": "multiselect", "l": "Multi-Select Drop Down" },
					{ "v": "number", "l": translation.number[LANG] },
					// { "v": "password", "l": translation.password[LANG] },
					{ "v": "operatingHours", "l": "Operating Hours" },
					{ "v": "phone", "l": translation.phoneNumber[LANG] },
					{ "v": "productAndServices", "l": "Products And Services" },
					{ "v": "radio", "l": "Radio: List One Value" },
					{ "v": "select", "l": "Select Drop Down" },
					{ "v": "text", "l": 'Text Input' },
					{ "v": "textarea", "l": translation.textBox[LANG] },
					{ "v": "url", "l": "URL" }
				],
				"required": true,
				"fieldMsg": "Pick the type of the input, to be rendered",
				'onAction': function (id, data) {
					var arr1 = ['radio', 'checkbox', 'select', 'multiselect'];
					if (arr1.indexOf(data) !== -1) {
						jQuery("#addContentInput #values-wrapper").slideDown();
						jQuery("#addContentInput #addValues-wrapper").slideDown()
					}
					else {
						jQuery("#addContentInput #values-wrapper").slideUp();
						jQuery("#addContentInput #addValues-wrapper").slideUp();
					}
				}
			},
			{
				'name': 'values',
				'label': 'Default Value',
				'required': false,
				"type": "group",
				'collapsed': false,
				"class": "valuesList",
				"entries": []
			},
			{
				"name": "addValues",
				"type": "html",
				"value": '<span class=""><input type="button" class="btn btn-sm btn-success" value="Add New Value"></span>',
				"onAction": function (id, data, form) {
					var oneClone = angular.copy(valuesRow);
					form.entries.forEach(function (entry) {
						if (entry.name === 'values' && entry.type === 'group') {
							for (var i = 0; i < oneClone.length; i++) {
								oneClone[i].name = oneClone[i].name.replace("%count%", valuesCount);
							}
							entry.entries = entry.entries.concat(oneClone);
						}
					});
					valuesCount++;
				}
			}
		];

		return fulfillmentRulesContent;
	}

	function getIndexOf(array, objectName) {
		for (var i = 0; i < array.length; i++) {
			if (array[i].name === objectName) {
				return i;
			}
		}
		return -1;
	}

	function addInput(currentScope) {
		var newOrder = 1;
		if (currentScope.formData.fulfillmentRules && currentScope.formData.fulfillmentRules.content) {
			newOrder = currentScope.formData.fulfillmentRules.content.length + 1;
		}
		var valuesCount = 0;
		var entries = getFulfillmentRulesContent(valuesCount);

		var config = {
			'name': '',
			'label': '',
			'actions': {},
			"entries": angular.copy(entries)
		};
		config.entries.forEach(function (one) {
			if (one.name === 'order') {
				one.value = newOrder;
			}
		});
		var options = {
			timeout: $timeout,
			form: config,
			"msgs": {
				header: 'This Type of inputs is entered by the User while filling the data record fields.'
			},
			'name': 'addContentInput',
			'label': "Add New Input",
			'actions': [
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formData) {
						if (!formData.label_en) {
							$window.alert(translation.enterLabelForInputProceed[LANG]);
						}
						else {
							var machineName;
							if (formData.name === 'custom') {
								machineName = formData.customName;
								formData.name = machineName;
							}
							else {
								machineName = formData.name;
							}

							var indexOfMachine = getIndexOf(currentScope.formData.fulfillmentRules.content, machineName);

							if (indexOfMachine !== -1) {
								$window.alert(translation.youAlreadyHaveInputNamed[LANG] + " " + formData.name);
							}
							else {
								formData.name = machineName;
								formData = reformatOutput(formData);
								currentScope.formData.fulfillmentRules.content.push(formData);
								currentScope.modalInstance.dismiss('cancel');
							}
						}
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						currentScope.modalInstance.dismiss('cancel');
						currentScope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal(currentScope, $modal, options);
	}

	function removeInput(currentScope, fieldName) {
		var indexOfMachine = getIndexOf(currentScope.formData.fulfillmentRules.content, fieldName);
		if (indexOfMachine !== -1) {
			currentScope.formData.fulfillmentRules.content.splice(indexOfMachine, 1);
		}
	}

	function editInput(currentScope, inputType, fieldInfo, indexOfMachine) {
		var formInfo;
		var fieldName = fieldInfo.name;

		currentScope.formData.fulfillmentRules.content.forEach(function (formField) {
			if (formField.name === fieldName) {
				formInfo = formField;
			}
		});

		var labelEn = '';
		var labelFr = '';
		if (formInfo.label) {
			if (formInfo.label.en) {
				labelEn = formInfo.label.en;
			}
			if (formInfo.label.fr) {
				labelFr = formInfo.label.fr
			}
		}

		var values = formInfo.values;
		var valuesCount = values ? values.length : 0;
		var entries = getFulfillmentRulesContent(valuesCount);

		for (var j = 0; j < valuesCount; j++) {
			var savedValue = values[j];
			var oneClone = angular.copy(valuesRow);

			for (var i = 0; i < oneClone.length; i++) {
				oneClone[i].name = oneClone[i].name.replace("%count%", j);

				if (oneClone[i].name.includes('valuesValue')) {
					oneClone[i].value = savedValue.v;
				}
				if (oneClone[i].name.includes('valuesLabel_en')) {
					oneClone[i].value = savedValue.label.en;
				}
				if (oneClone[i].name.includes('valuesLabel_fr')) {
					oneClone[i].value = savedValue.label.fr;
				}
			}

			var valuesIndex = getIndexOfEntry(entries, 'values');
			var tempVal = entries[valuesIndex].entries;
			entries[valuesIndex].entries = tempVal.concat(oneClone);
		}

		var custom = true;

		var standardNames = entries[getIndexOfEntry(entries, 'name')].value;
		standardNames.forEach(function (name) {
			if (name.v === formInfo.name) {
				custom = false;
			}
		});

		var data = {
			"name": fieldName,
			"label_en": labelEn,
			"label_fr": labelFr,
			"type": formInfo.type,
			"placeholder": formInfo.placeholder,
			"tooltip": formInfo.tooltip,
			"required": '' + formInfo.required // values -=-=-=
		};
		
		if (formInfo.order) {
			data.order = formInfo.order;
		}

		if (custom) {
			data.name = 'custom';
			data.customName = formInfo.name;
			entries[getIndexOfEntry(entries, 'customName')].hidden = false;
		} else {
			entries[getIndexOfEntry(entries, 'customName')].hidden = true;
		}

		var config = {
			'name': '',
			'label': '',
			'actions': {},
			"entries": angular.copy(entries)
		};

		var options = {
			timeout: $timeout,
			form: config,
			"msgs": {
				header: "This Type of inputs is entered by the User while filling the data."
			},
			'data': data,
			'name': 'addContentInput',
			'label': "Edit User Input",
			"postBuild": function () {
				var arr1 = ['radio', 'checkbox', 'select', 'multiselect'];
				var type = data['type'];
				if (arr1.indexOf(type) !== -1) {
					jQuery("#addContentInput #values-wrapper").slideDown();
					jQuery("#addContentInput #addValues-wrapper").slideDown()
				}
				else {
					jQuery("#addContentInput #values-wrapper").slideUp();
					jQuery("#addContentInput #addValues-wrapper").slideUp();
				}
			},
			'actions': [
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formData) {
						if (!formData.label_en) {
							$window.alert(translation.enterLabelForInputProceed[LANG]);
						}
						else {
							var machineName;

							if (formData.name === 'custom') {
								formData.name = formData.customName;
							}
							machineName = formData.name;

							var indexIfExists = getIndexOf(currentScope.formData.fulfillmentRules.content, machineName);

							if (indexIfExists !== indexOfMachine && indexIfExists !== -1) {
								$window.alert(translation.youAlreadyHaveInputNamed[LANG] + " " + formData.name);
							}
							else {
								formData = reformatOutput(formData);

								currentScope.formData.fulfillmentRules.content[indexOfMachine] = formData;
								// sort
								currentScope.formData.fulfillmentRules.content.sort(function (a, b) {
									return parseFloat(a.order) - parseFloat(b.order);
								});
								currentScope.modalInstance.dismiss('cancel');
							}
						}
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						currentScope.modalInstance.dismiss('cancel');
						currentScope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal(currentScope, $modal, options);
	}

	function reformatOutput(formData) {
		// input object from pop up form
		// output object to submit

		formData.label = {
			en: formData.label_en,
			fr: formData.label_fr
		};

		var values = [];
		var valuesLength = 0;
		var inputAsArray = Object.keys(formData);
		for (var i = 0; i < inputAsArray.length; i++) {
			if (inputAsArray[i].includes('valuesValue_')) {
				valuesLength++;
			}
		}

		for (var i = 0; i < valuesLength; i++) {
			var tempo = {
				"label": {
					"en": formData['valuesLabel_en_' + i],
					"fr": formData['valuesLabel_fr_' + i]
				},
				"v": formData['valuesValue_' + i]
			};
			values.push(tempo);
		}

		var output = {
			name: formData.name,
			label: formData.label,
			required: formData.required ? true : false,
			order: formData.order,
			type: formData.type,
			placeholder: formData.placeholder,
			tooltip: formData.tooltip,
			values: values
		};

		return output;
	}

	return {
		'addInput': addInput,
		'editInput': editInput,
		'removeInput': removeInput
	}

}]);
