"use strict";
var ModuleDevMpLocation = uiModuleDev + '/merchantProducts';

var merchantsModuleDevTranslation = {
	// bundles attributes
	bundle: {
		"bundleName": {
			"ENG": "Bundle Name",
			"FRA": "Bundle Name"
		}
	},
	"discountEngine": {
		"ENG": "Discount Engine",
		"FRA": "Discount Engine"
	},
	
	// merchant & products / add products / form.tmpl
	"mpFields": {
		// root
		"status": {
			"ENG": "Status",
			"FRA": "Status"
		},
		"legalContent": {
			"ENG": "Legal Content",
			"FRA": "Legal Content"
		},
		// COREDATA starts
		"coreData": {
			"ENG": "Core Data",
			"FRA": "Core Data"
		},
		"serial": {
			"ENG": "Serial",
			"FRA": "Serial"
		},
		"productType": {
			"ENG": "Product Type",
			"FRA": "Product Type"
		},
		"groupId": {
			"ENG": "Group Id",
			"FRA": "Group Id"
		},
		"groupDefault": {
			"ENG": "Group Default",
			"FRA": "Group Default"
		},
		"upi": {
			"ENG": "UPI",
			"FRA": "UPI"
		},
		"upc": {
			"ENG": "UPC",
			"FRA": "UPC"
		},
		"gtin": {
			"ENG": "GTIN",
			"FRA": "GTIN"
		},
		"condition": {
			"ENG": "Condition",
			"FRA": "Condition"
		},
		"udac": {
			"ENG": "UDAC",
			"FRA": "UDAC"
		},
		"isAddOn": {
			"ENG": "Is Add On",
			"FRA": "Is Add On"
		},
		"addOns": {
			"ENG": "Add Ons",
			"FRA": "Add Ons"
		},
		"allowAddOn": {
			"ENG": "Allow Add On",
			"FRA": "Allow Add On"
		},
		"inventory": {
			"ENG": "Inventory",
			"FRA": "Inventory"
		},
		"multipleQuantity": {
			"ENG": "Multiple Quantity",
			"FRA": "Multiple Quantity"
		},
		"newProduct": {
			"ENG": "New Product",
			"FRA": "New Product"
		},
		"allowReservation": {
			"ENG": "Allow Reservation",
			"FRA": "Allow Reservation"
		},
		"allowWaitingList": {
			"ENG": "Allow Waiting List",
			"FRA": "Allow Waiting List"
		},
		"renewal": {
			"ENG": "Renewal",
			"FRA": "Renewal"
		},
		"autoRenewal": {
			"ENG": "Auto Renewal",
			"FRA": "Auto Renewal"
		},
		"grandfathered": {
			"ENG": "Grand Fathered",
			"FRA": "Grand Fathered"
		},
		"cancellationPolicy": {
			"ENG": "Cancellation Policy",
			"FRA": "Cancellation Policy"
		},
		"termsNotification": {
			"ENG": "Terms Notification",
			"FRA": "Terms Notification"
		},
		"fees": {
			"ENG": "Fees",
			"FRA": "Fees"
		},
		// COREDATA ends
		
		// SEGMENTATION starts
		"segmentation": {
			"ENG": "Segmentation",
			"FRA": "Segmentation"
		},
		"salesChannels": {
			"ENG": "Sales Channels",
			"FRA": "Sales Channels"
		},
		"accountType": {
			"ENG": "Account Type",
			"FRA": "Account Type"
		},
		"include": {
			"ENG": "Include",
			"FRA": "Include"
		},
		"exclude": {
			"ENG": "Exclude",
			"FRA": "Exclude"
		},
		"serviceArea": {
			"ENG": "Service Area",
			"FRA": "Service Area"
		},
		"verticals": {
			"ENG": "Verticals",
			"FRA": "Verticals"
		},
		"headings": {
			"ENG": "Headings",
			"FRA": "Headings"
		},
		"sensitiveHeadings": {
			"ENG": "Sensitive Headings",
			"FRA": "Sensitive Headings"
		},
		// SEGMENTATION ends
		
		// Rules starts
		"rules": {
			"ENG": "Rules",
			"FRA": "Rules"
		},
		"prerequisiteProduct": {
			"ENG": "Prerequisite Product",
			"FRA": "Prerequisite Product"
		},
		// serial
		// udac
		"incompatibility": {
			"ENG": "Incompatibility",
			"FRA": "Incompatibility"
		},
		"marketHeading": {
			"ENG": "Market Heading",
			"FRA": "Market Heading"
		},
		"businessAddress": {
			"ENG": "Business Address",
			"FRA": "Business Address"
		},
		"uniqueMarketHeadingMid": {
			"ENG": "Unique Market & Heading per Store Location",
			"FRA": "Unique Market & Heading per Store Location"
		},
		"uniqueMid": {
			"ENG": "Unique product per Store Location",
			"FRA": "Unique product per Store Location"
		},
		// PRODUCTCONFIGURATION ends
		
		// PRODUCTCONFIGURATION starts
		"productConfiguration": {
			"ENG": "Product Configuration",
			"FRA": "Product Configuration"
		},
		
		"isRequired": {
			"ENG": "Required",
			"FRA": "Required"
		},
		"priceCheck": {
			"ENG": "Affects Price",
			"FRA": "Affects Price"
		},
		//businessAdress
		"market": {
			"ENG": "Market",
			"FRA": "Market"
		},
		"heading": {
			"ENG": "Heading",
			"FRA": "Heading"
		},
		"startingDate": {
			"ENG": "Starting Date",
			"FRA": "Starting Date"
		},
		"contractLength": {
			"ENG": "Contract Length",
			"FRA": "Contract Length"
		},
		// language
		"languageOption": {
			"ENG": "Language Option",
			"FRA": "Language Option"
		},
		"domainName": {
			"ENG": "Domain Name",
			"FRA": "Domain Name"
		},
		// PRODUCTCONFIGURATION ends
		
		// ATTRIBUTES starts
		"attributes": {
			"ENG": "Attributes",
			"FRA": "Attributes"
		},
		"category": {
			"ENG": "Category",
			"FRA": "Category"
		},
		"brand": {
			"ENG": "Brand",
			"FRA": "Brand"
		},
		"classification": {
			"ENG": "Classification",
			"FRA": "Classification"
		},
		"variations": {
			"ENG": "Variations",
			"FRA": "Variations"
		},
		"color": {
			"ENG": "Color",
			"FRA": "Color"
		},
		"size": {
			"ENG": "Size",
			"FRA": "Size"
		},
		"language": {
			"ENG": "Language",
			"FRA": "Language"
		},
		"review": {
			"ENG": "Review",
			"FRA": "Review"
		},
		"average": {
			"ENG": "Average",
			"FRA": "Average"
		},
		"count": {
			"ENG": "Count",
			"FRA": "Count"
		},
		// ATTRIBUTES ends
		
		// PRODUCTDATA starts
		"productData": {
			"ENG": "Product Data",
			"FRA": "Product Data"
		},
		"crossSellPath": {
			"ENG": "Cross Sell Path",
			"FRA": "Cross Sell Path"
		},
		"upsellPath": {
			"ENG": "Upsell Path",
			"FRA": "Upsell Path"
		},
		"resources": {
			"ENG": "Resources",
			"FRA": "Resources"
		},
		
		"removeResource": {
			"ENG": "Remove Resource",
			"FRA": "Remove Resource"
		},
		
		// PRODUCTDATA ends
		
		// SHIPPINGDATA starts
		"shippingData": {
			"ENG": "Shipping Data",
			"FRA": "Shipping Data"
		},
		// MERCHANTMETA starts
		"merchantMeta": {
			"ENG": "Merchant Meta",
			"FRA": "Merchant Meta"
		},
		"pricing": {
			"ENG": "Pricing",
			"FRA": "Pricing"
		},
		"productValue": {
			"ENG": "Product Value",
			"FRA": "Product Value"
		},
		"minimumCommitment": {
			"ENG": "Minimum Commitment",
			"FRA": "Minimum Commitment"
		},
		"mandatoryNotice": {
			"ENG": "Mandatory Notice",
			"FRA": "Mandatory Notice"
		},
		"penaltyFees": {
			"ENG": "Penalty Fees",
			"FRA": "Penalty Fees"
		},
		"cancellation": {
			"ENG": "Cancellation",
			"FRA": "Cancellation"
		},
		"pause": {
			"ENG": "Pause",
			"FRA": "Pause"
		},
		"downgrade": {
			"ENG": "Downgrade",
			"FRA": "Downgrade"
		},
		"label": {
			"ENG": "Label",
			"FRA": "Label"
		},
		"englishLabel": {
			"ENG": "English Label",
			"FRA": "English Label"
		},
		"frenchLabel": {
			"ENG": "French Label",
			"FRA": "French Label"
		},
		"maxTimeFrame": {
			"ENG": "Max Time Frame",
			"FRA": "Max Time Frame"
		},
		"price": {
			"ENG": "Price",
			"FRA": "Price"
		},
		"commissionValue": {
			"ENG": "Commission Value",
			"FRA": "Commission Value"
		},
		// value
		// category
		"maxDiscountLevel": {
			"ENG": "Max Discount Level",
			"FRA": "Max Discount Level"
		},
		"onetime": {
			"ENG": "One time Fee",
			"FRA": "One time Fee"
		},
		"name": {
			"ENG": "Name",
			"FRA": "Name"
		},
		"value": {
			"ENG": "Value",
			"FRA": "Value"
		},
		"registration": {
			"ENG": "Registration Fee",
			"FRA": "Registration Fee"
		},
		"quantity": {
			"ENG": "Quantity",
			"FRA": "Quantity"
		},
		"recurring": {
			"ENG": "Recurring Fee",
			"FRA": "Recurring Fee"
		},
		"unit": {
			"ENG": "Unit",
			"FRA": "Unit"
		},
		"frequency": {
			"ENG": "Frequency",
			"FRA": "Frequency"
		},
		"term": {
			"ENG": "Contract Length",
			"FRA": "Contract Length"
		},
		// MERCHANTMETA ends
		
		// FULFILLMENTRULES starts
		"fulfillmentRules": {
			"ENG": "Fulfillment Rules",
			"FRA": "Fulfillment Rules"
		},
		"sla": {
			"ENG": "SLA in Days",
			"FRA": "SLA in Days"
		},
		"minDelay": {
			"ENG": "Minimum Delay",
			"FRA": "Minimum Delay"
		},
		"maxDelay": {
			"ENG": "Maximum Delay",
			"FRA": "Maximum Delay"
		},
		"renewalSla": {
			"ENG": "Renewal SLA in Days",
			"FRA": "Renewal SLA in Days"
		},
		"supplier": {
			"ENG": "Supplier",
			"FRA": "Supplier"
		},
		"content": {
			"ENG": "Content",
			"FRA": "Content"
		},
		"fulfillmentPlan": {
			"ENG": "Fulfillment Plan",
			"FRA": "Fulfillment Plan"
		},
		"required": {
			"ENG": "Required",
			"FRA": "Required"
		},
		// FULFILLMENTRULES ends
		
		// bundles extras
		"bundleData": {
			"ENG": "Bundle Data",
			"FRA": "Bundle Data"
		},
	},
	
	"hideThumbnail": {
		"ENG": "Hide Thumbnail",
		"FRA": "Hide Thumbnail"
	},
	"chooseOneMid": {
		"ENG": "--- Choose One ---",
		"FRA": "--- Choose One ---"
	},
	"shoe_size": {
		"ENG": "Shoe size",
		"FRA": "Shoe size"
	},
	"disabled": {
		"ENG": "Disabled",
		"FRA": "Desactivé"
	},
	"merchantsProducts": {
		"ENG": "Merchants & Products",
		"FRA": "Merchants & Products"
	},
	"pendingMerchants": {
		"ENG": "Pending Merchants",
		"FRA": "Pending Merchants"
	},
	"domain": {
		"ENG": "Domain",
		"FRA": "Domain"
	},
	"preferredLanguage": {
		"ENG": "Preferred Language",
		"FRA": "Preferred Language"
	},
	"logo": {
		"ENG": "Logo",
		"FRA": "Logo"
	},
	"uploadLogo": {
		"ENG": "Upload Logo",
		"FRA": "Upload Logo"
	},
	"altTagForLogo": {
		"ENG": "Alt Tag for Logo",
		"FRA": "Alt Tag for Logo"
	},
	"altTag": {
		"ENG": "Alt Tag",
		"FRA": "Alt Tag"
	},
	"storeImage": {
		"ENG": "Store Image",
		"FRA": "Store Image"
	},
	"uploadStoreImage": {
		"ENG": "Upload Store Image",
		"FRA": "Upload Store Image"
	},
	"altTagForStoreImage": {
		"ENG": "Alt Tag for Store Image",
		"FRA": "Alt Tag for Store Image"
	},
	"homepage": {
		"ENG": "Homepage",
		"FRA": "Homepage"
	},
	"tags": {
		"ENG": "Tags",
		"FRA": "Tags"
	},
	"categories": {
		"ENG": "Categories",
		"FRA": "Categories"
	},
	"optionalRating": {
		"ENG": "Rating (optional)",
		"FRA": "Rating (optional)"
	},
	"rating": {
		"ENG": "Rating",
		"FRA": "Rating"
	},
	"average": {
		"ENG": "Average",
		"FRA": "Average"
	},
	"count": {
		"ENG": "Count",
		"FRA": "Count"
	},
	"information": {
		"ENG": "Information",
		"FRA": "Information"
	},
	"phone": {
		"ENG": "Phone",
		"FRA": "Phone"
	},
	"address": {
		"ENG": "Address",
		"FRA": "Address"
	},
	"customerServiceContact": {
		"ENG": "Customer Service Contact",
		"FRA": "Contact Service Client"
	},
	"returnPolicy": {
		"ENG": "Return Policy",
		"FRA": "Politique de retour"
	},
	"termsAndConditions": {
		"ENG": "Terms and Conditions",
		"FRA": "Termes et Conditions"
	},
	"feedingSource": {
		"ENG": "Feeding Source",
		"FRA": "Feeding Source"
	},
	"driver": {
		"ENG": "Driver",
		"FRA": "Driver"
	},
	"driverName": {
		"ENG": "Driver Name",
		"FRA": "Driver Name"
	},
	"driverNameTooltip": {
		"ENG": "Pick the Driver to use",
		"FRA": "Pick the Driver to use"
	},
	"driverLabel": {
		"ENG": "Driver Label",
		"FRA": "Driver Label"
	},
	"driverLabelTooltip": {
		"ENG": "Enter the display name of the Driver",
		"FRA": "Enter the display name of the Driver"
	},
	"driverLabelPlaceholder": {
		"ENG": "My Driver Name...",
		"FRA": "My Driver Name..."
	},
	"url": {
		"ENG": "URL",
		"FRA": "URL"
	},
	"token": {
		"ENG": "Token",
		"FRA": "Token"
	},
	"additionalInformation": {
		"ENG": "Additional Information",
		"FRA": "Additional Information"
	},
	"creditCard": {
		"ENG": "Credit Card",
		"FRA": "Credit Card"
	},
	"nameOnCard": {
		"ENG": "Name on Card",
		"FRA": "Name on Card"
	},
	"cardNumber": {
		"ENG": "Card Number",
		"FRA": "Card Number"
	},
	"expirationMonth": {
		"ENG": "Expiration Month",
		"FRA": "Expiration Month"
	},
	"expirationYear": {
		"ENG": "Expiration Year",
		"FRA": "Expiration Year"
	},
	"cvvCode": {
		"ENG": "Cvv Code",
		"FRA": "Cvv Code"
	},
	"billingAddress": {
		"ENG": "Billing Address",
		"FRA": "Billing Address"
	},
	"addressLine1": {
		"ENG": "Address Line 1",
		"FRA": "Address Line 1"
	},
	"addressLine2": {
		"ENG": "Address Line 2",
		"FRA": "Address Line 2"
	},
	"addressLine3": {
		"ENG": "Address Line 3",
		"FRA": "Address Line 3"
	},
	"country": {
		"ENG": "Country",
		"FRA": "Country"
	},
	"city": {
		"ENG": "City",
		"FRA": "City"
	},
	"state": {
		"ENG": "State",
		"FRA": "State"
	},
	"zip": {
		"ENG": "Zip",
		"FRA": "Zip"
	},
	"customFeedConfiguration": {
		"ENG": "Custom Feed Configuration",
		"FRA": "Custom Feed Configuration"
	},
	"date": {
		"ENG": "Date",
		"FRA": "Date"
	},
	"removeException": {
		"ENG": "Remove Exception",
		"FRA": "Remove Exception"
	},
	"otherEmails": {
		"ENG": "Other Emails",
		"FRA": "Other Emails"
	},
	"additionalEmail": {
		"ENG": "Additional Email",
		"FRA": "Additional Email"
	},
	"removeEmail": {
		"ENG": "Remove Email",
		"FRA": "Remove Email"
	},
	"id": {
		"ENG": "ID",
		"FRA": "ID"
	},
	"currency": {
		"ENG": "Currency",
		"FRA": "Currency"
	},
	"type": {
		"ENG": "Type",
		"FRA": "Type"
	},
	"pos": {
		"ENG": "POS",
		"FRA": "POS"
	},
	"warehouse": {
		"ENG": "Warehouse",
		"FRA": "Warehouse"
	},
	"longitude": {
		"ENG": "Longitude",
		"FRA": "Longitude"
	},
	"latitude": {
		"ENG": "Latitude",
		"FRA": "Latitude"
	},
	"preparationTime": {
		"ENG": "Preparation time",
		"FRA": "Preparation time"
	},
	"daysOfOperation": {
		"ENG": "Days of Operation",
		"FRA": "Days of Operation"
	},
	"from": {
		"ENG": "From",
		"FRA": "From"
	},
	"till": {
		"ENG": "Till",
		"FRA": "Till"
	},
	"monday": {
		"ENG": "Monday",
		"FRA": "Monday"
	},
	"tuesday": {
		"ENG": "Tuesday",
		"FRA": "Tuesday"
	},
	"wednesday": {
		"ENG": "Wednesday",
		"FRA": "Wednesday"
	},
	"thursday": {
		"ENG": "Thursday",
		"FRA": "Thursday"
	},
	"friday": {
		"ENG": "Friday",
		"FRA": "Friday"
	},
	"saturday": {
		"ENG": "Saturday",
		"FRA": "Saturday"
	},
	"sunday": {
		"ENG": "Sunday",
		"FRA": "Sunday"
	},
	"exceptionalDates": {
		"ENG": "Exceptional Dates",
		"FRA": "Exceptional Dates"
	},
	"ypMerchantId": {
		"ENG": "YP Merchant Id",
		"FRA": "YP Merchant Id"
	},
	"ypMidFieldMsg": {
		"ENG": "If you do not find your YP Merchant Id, Contact us at support@ypg.com",
		"FRA": "If you do not find your YP Merchant Id, Contact us at support@ypg.com"
	},
	"availability": {
		"ENG": "Availability",
		"FRA": "Availability"
	},
	"price": {
		"ENG": "Price",
		"FRA": "Price"
	},
	"myDomain": {
		"ENG": "My Domain",
		"FRA": "My Domain"
	},
	"logoAltTagTooltip": {
		"ENG": "Enter optional logo Alt tag",
		"FRA": "Enter optional logo Alt tag"
	},
	"imageAltTagTooltip": {
		"ENG": "Enter optional Store image Alt tag",
		"FRA": "Enter optional Store image Alt tag"
	},
	"optional": {
		"ENG": "optional",
		"FRA": "optional"
	},
	"tagsTooltip": {
		"ENG": "Enter merchant optional tags",
		"FRA": "Enter merchant optional tags"
	},
	"categoriesTooltip": {
		"ENG": "Enter merchant categories",
		"FRA": "Enter merchant categories"
	},
	"descriptionPlaceHolder": {
		"ENG": "Description...",
		"FRA": "Description..."
	},
	"descriptionFieldMsg": {
		"ENG": "Description is required. Maximum 800 Characters Allowed",
		"FRA": "Description is required. Maximum 800 Characters Allowed"
	},
	"customerServiceFieldMsg": {
		"ENG": "Customer Service Contact is required. Maximum 800 Characters Allowed",
		"FRA": "Customer Service Contact is required. Maximum 800 Characters Allowed"
	},
	"returnPolicyFieldMsg": {
		"ENG": "Return Policy is required. Maximum 800 Characters Allowed",
		"FRA": "Return Policy is required. Maximum 800 Characters Allowed"
	},
	"termsAndConditionsFieldMsg": {
		"ENG": "Terms and Conditions is optional. Maximum 800 Characters Allowed",
		"FRA": "Terms and Conditions is optional. Maximum 800 Characters Allowed"
	},
	"managementInterfaceOnly": {
		"ENG": "Management Interface Only",
		"FRA": "Management Interface Only"
	},
	"iHaveAnApiThatSendDataToYou": {
		"ENG": "I have an API that send data to you",
		"FRA": "I have an API that send data to you"
	},
	"youCreatedAdriverForMe": {
		"ENG": "You created a driver for me",
		"FRA": "You created a driver for me"
	},
	"feedingSourceTooltip": {
		"ENG": "Select the type of validation to use",
		"FRA": "Select the type of validation to use"
	},
	"driverTooltip": {
		"ENG": "Enter the name of the driver to use for pull2push product feeding",
		"FRA": "Enter the name of the driver to use for pull2push product feeding"
	},
	"driverPlaceholder": {
		"ENG": "myDriver",
		"FRA": "myDriver"
	},
	"urlTooltip": {
		"ENG": "Enter your Remote validation URL",
		"FRA": "Enter your Remote validation URL"
	},
	"tokenTooltip": {
		"ENG": "Enter API token",
		"FRA": "Enter API token"
	},
	"tokenPlaceholder": {
		"ENG": "myApiToken",
		"FRA": "myApiToken"
	},
	"additionalInformationTooltip": {
		"ENG": "Provide optional additional information used while contacting the API",
		"FRA": "Provide optional additional information used while contacting the API"
	},
	"countryTooltip": {
		"ENG": "Select your country.",
		"FRA": "Select your country."
	},
	"configTooltip": {
		"ENG": "Override the default Rigid feed Profile configuration",
		"FRA": "Override the default Rigid feed Profile configuration"
	},
	"addAnother": {
		"ENG": "Add Another",
		"FRA": "Add Another"
	},
	"optionalCustomId": {
		"ENG": "Optional Custom ID",
		"FRA": "Optional Custom ID"
	},
	"maximum44CharactersAllowed": {
		"ENG": "Maximum 44 characters allowed",
		"FRA": "Maximum 44 characters allowed"
	},
	"100": {
		"ENG": "Please Select A Status.",
		"FRA": "Please Select A Status."
	},
	"101": {
		"ENG": "Please Select If Its Downloadable.",
		"FRA": "Please Select If Its Downloadable."
	},
	"102": {
		"ENG": "Please Insert At Least One UPI.",
		"FRA": "Please Insert At Least One UPI."
	},
	"103": {
		"ENG": "Please Insert A Valid UPC.",
		"FRA": "Please Insert A Valid UPC."
	},
	"104": {
		"ENG": "Please Insert A Valid EAN.",
		"FRA": "Please Insert A Valid EAN."
	},
	"105": {
		"ENG": "Please Insert A Valid JAN.",
		"FRA": "Please Insert A Valid JAN."
	},
	"106": {
		"ENG": "Please Insert A Valid ISBN.",
		"FRA": "Please Insert A Valid ISBN."
	},
	"107": {
		"ENG": "Please Insert A Short Description.",
		"FRA": "Please Insert A Short Description."
	},
	"108": {
		"ENG": "Please Insert A Full Description.",
		"FRA": "Please Insert A Full Description."
	},
	"109": {
		"ENG": "Please Insert At Least One Image",
		"FRA": "Please Insert At Least One Image"
	},
	"200": {
		"ENG": "Please Insert The Average In Review.",
		"FRA": "Please Insert The Average In Review."
	},
	"201": {
		"ENG": "Please Insert The Count In Review.",
		"FRA": "Please Insert The Count In Review."
	},
	"202": {
		"ENG": "Please Insert both Fields In Variations.",
		"FRA": "Please Insert both Fields In Variations."
	},
	"203": {
		"ENG": "Please insert a title in %lang%",
		"FRA": "Please insert a title in %lang%"
	},
	"204": {
		"ENG": "Please insert a short description in %lang%",
		"FRA": "Please insert a short description in %lang%"
	},
	"addMerchant": {
		"ENG": "Add Merchant",
		"FRA": "Add Merchant"
	},
	"editMerchant": {
		"ENG": "Edit Merchant",
		"FRA": "Edit Merchant"
	},
	"saveChanges": {
		"ENG": "Save Changes",
		"FRA": "Save Changes"
	},
	"merchantOverrideFeedProfile": {
		"ENG": "Merchant Override Feed Profile",
		"FRA": "Merchant Override Feed Profile"
	},
	"viewingOneMerchant": {
		"ENG": "Viewing One Merchant",
		"FRA": "Viewing One Merchant"
	},
	"merchantSpecific": {
		"ENG": "Merchant Specific",
		"FRA": "Merchant Specific"
	},
	"viewItem": {
		"ENG": "View Item",
		"FRA": "View Item"
	},
	"addPaymentMethod": {
		"ENG": "Add Payment Method",
		"FRA": "Add Payment Method"
	},
	"editPaymentMethod": {
		"ENG": "Edit Payment Method",
		"FRA": "Edit Payment Method"
	},
	"useMarketplaceDefault": {
		"ENG": "Use Marketplace Default",
		"FRA": "Use Marketplace Default"
	},
	"merchantPaymentStatusChangedSuccessfully": {
		"ENG": "Merchant Payment Status Changed Successfully.",
		"FRA": "Merchant Payment Status Changed Successfully."
	},
	"someInputsAreStillMissing": {
		"ENG": "Some Inputs are still missing",
		"FRA": "Some Inputs are still missing"
	},
	"merchantPaymentDeletedSuccessfully": {
		"ENG": "Merchant Payment Deleted Successfully.",
		"FRA": "Merchant Payment Deleted Successfully."
	},
	"viewingOnePaymentMethod": {
		"ENG": "Viewing One Payment Method",
		"FRA": "Viewing One Payment Method"
	},
	"areYouSureYouWantToRemoveThisMethod": {
		"ENG": "Are you sure you want to remove this method ?",
		"FRA": "Are you sure you want to remove this method ?"
	},
	"viewingOnePickupMethod": {
		"ENG": "Viewing One pickup Method",
		"FRA": "Viewing One pickup Method"
	},
	"addPickupMethod": {
		"ENG": "Add pickup Method",
		"FRA": "Add pickup Method"
	},
	"merchantPickupStatusChangedSuccessfully": {
		"ENG": "Merchant pickup Status Changed Successfully.",
		"FRA": "Merchant pickup Status Changed Successfully."
	},
	"merchantPickupDeletedSuccessfully": {
		"ENG": "Merchant pickup Deleted Successfully.",
		"FRA": "Merchant pickup Deleted Successfully."
	},
	"addPOS": {
		"ENG": "Add POS",
		"FRA": "Add POS"
	},
	"getYpMidOptions": {
		"ENG": "Get Yp Mid Options",
		"FRA": "Get Yp Mid Options"
	},
	"addExceptionalDate": {
		"ENG": "Add Exceptional date",
		"FRA": "Add Exceptional date"
	},
	"addOtherEmail": {
		"ENG": "Add Other Email",
		"FRA": "Add Other Email"
	},
	"fromCantBeEqualOrGreaterThanTillInDaysOfOperation": {
		"ENG": "From cant be equal or greater than Till in Days of Operation",
		"FRA": "From cant be equal or greater than Till in Days of Operation"
	},
	"fromCantBeEqualOrGreaterThanTillInExceptionalDate": {
		"ENG": "From cant be equal or greater than Till in Exceptional Date",
		"FRA": "From cant be equal or greater than Till in Exceptional Date"
	},
	"posAddedSuccessfully": {
		"ENG": "POS Added Successfully.",
		"FRA": "POS Added Successfully."
	},
	"editPos": {
		"ENG": "Edit POS",
		"FRA": "Edit POS"
	},
	"viewingOnePOS": {
		"ENG": "Viewing One POS",
		"FRA": "Viewing One POS"
	},
	"posStatusChangedSuccessfully": {
		"ENG": "POS Status Changed Successfully.",
		"FRA": "POS Status Changed Successfully."
	},
	"posDeletedSuccessfully": {
		"ENG": "POS Deleted Successfully.",
		"FRA": "POS Deleted Successfully."
	},
	"posUpdatedSuccessfully": {
		"ENG": "POS Updated Successfully.",
		"FRA": "POS Updated Successfully."
	},
	"disableActivate": {
		"ENG": "Disable/Activate",
		"FRA": "Disable/Activate"
	},
	"viewingOneShippingMethod": {
		"ENG": "Viewing One Shipping Method",
		"FRA": "Viewing One Shipping Method"
	},
	"addShippingMethod": {
		"ENG": "Add Shipping Method",
		"FRA": "Add Shipping Method"
	},
	"editShippingMethod": {
		"ENG": "Edit Shipping Method",
		"FRA": "Edit Shipping Method"
	},
	"deleteShippingMethod": {
		"ENG": "Delete Shipping Method",
		"FRA": "Delete Shipping Method"
	},
	"merchantShippingMethodAddedSuccessfully": {
		"ENG": "Merchant Shipping Method Added Successfully.",
		"FRA": "Merchant Shipping Method Added Successfully."
	},
	"merchantShippingMethodUpdatedSuccessfully": {
		"ENG": "Merchant Shipping Method Updated Successfully.",
		"FRA": "Merchant Shipping Method Updated Successfully."
	},
	"merchantShippingMethodDeletedSuccessfully": {
		"ENG": "Merchant Shipping Method Deleted Successfully.",
		"FRA": "Merchant Shipping Method Deleted Successfully."
	},
	"merchantShippingStatusChangedSuccessfully": {
		"ENG": "Merchant Shipping Status Changed Successfully.",
		"FRA": "Merchant Shipping Status Changed Successfully."
	},
	"addSale": {
		"ENG": "Add Sale",
		"FRA": "Add Sale"
	},
	"updateTreats": {
		"ENG": "Update Treats",
		"FRA": "Update Treats"
	},
	"updateTreatsMsg": {
		"ENG": "Select The Treats That You Want To Apply For This Product And All Variations",
		"FRA": "Select The Treats That You Want To Apply For This Product And All Variations"
	},
	"applyToAllVariations": {
		"ENG": "Apply To All Variations",
		"FRA": "Apply To All Variations"
	},
	"deprecated": {
		"ENG": "deprecated",
		"FRA": "deprecated"
	},
	"becomes": {
		"ENG": "becomes",
		"FRA": "becomes"
	},
	"pleaseSelectEndDate": {
		"ENG": "Please Select End Date",
		"FRA": "Please Select End Date"
	},
	"pleaseSelectStartDate": {
		"ENG": "Please Select Start Date",
		"FRA": "Please Select Start Date"
	},
	"pleaseSelectStartDateAndEndDate": {
		"ENG": "Please Select a Start Date And an End Date",
		"FRA": "Please Select a Start Date And an End Date"
	},
	"saleAddedSuccessfully": {
		"ENG": "Rule added Successfully.",
		"FRA": "Rule added Successfully."
	},
	"imagesUpdatedSuccessfully": {
		"ENG": "Images Updated Successfully.",
		"FRA": "Images Updated Successfully."
	},
	"viewingOneProduct": {
		"ENG": "Viewing One Product",
		"FRA": "Viewing One Product"
	},
	"productDeletedSuccessfully": {
		"ENG": "Product Deleted Successfully.",
		"FRA": "Product Deleted Successfully."
	},
	"oneOrMoreOfTheSelectedProductWasNotDeleted": {
		"ENG": "one or more of the selected Product(s) was not deleted.",
		"FRA": "one or more of the selected Product(s) was not deleted."
	},
	"selectedProductHaveBeenDeleted": {
		"ENG": "Selected Product(s) have been deleted.",
		"FRA": "Selected Product(s) have been deleted."
	},
	"youCantChangeStatusOfDraftProducts": {
		"ENG": "You cant Change status of draft Products.",
		"FRA": "You cant Change status of draft Products."
	},
	"productStatusChangedSuccessfully": {
		"ENG": "Product Status Changed Successfully.",
		"FRA": "Product Status Changed Successfully."
	},
	"oneOrMoreOfTheSelectedProductStatusWasNotChanged": {
		"ENG": "one or more of the selected Product(s) status was not changed.",
		"FRA": "one or more of the selected Product(s) status was not changed."
	},
	"selectedProductStatusHaveBeenUpdated": {
		"ENG": "Selected Product(s) status have been updated.",
		"FRA": "Selected Product(s) status have been updated."
	},
	"youCantUpdateStockForDraftProducts": {
		"ENG": "You cant update Stock for draft Products.",
		"FRA": "You cant update Stock for draft Products."
	},
	"updateProductStock": {
		"ENG": "Update Product Stock",
		"FRA": "Update Product Stock"
	},
	"productStockUpdatedSuccessfully": {
		"ENG": "Product Stock Updated Successfully.",
		"FRA": "Product Stock Updated Successfully."
	},
	"youCantUpdateTreatsForDraftProducts": {
		"ENG": "You cant update Treats for draft Products.",
		"FRA": "You cant update Treats for draft Products."
	},
	"productVariationTreatsUpdatedSuccessfully": {
		"ENG": "Product Variation Treats Updated Successfully.",
		"FRA": "Product Variation Treats Updated Successfully."
	},
	"noMatchFoundCheckYourSpellingOrClickCreateNewProduct": {
		"ENG": "No match found, check your spelling or click create new product.",
		"FRA": "No match found, check your spelling or click create new product."
	},
	"searchForProductByEnteringItsNameBrandOrItsUPC": {
		"ENG": "Search for product by entering its name & brand or its UPC!",
		"FRA": "Search for product by entering its name & brand or its UPC!"
	},
	"productAddedSuccessfully": {
		"ENG": "Product added Successfully.",
		"FRA": "Product added Successfully."
	},
	"draftAddedSuccessfully": {
		"ENG": "Draft added Successfully.",
		"FRA": "Draft added Successfully."
	},
	"youDoNotHaveAccessToPerformScopeOperation": {
		"ENG": "You do not have access to perform $scope operation!",
		"FRA": "You do not have access to perform $scope operation!"
	},
	"productUpdatedSuccessfully": {
		"ENG": "Product Updated Successfully.",
		"FRA": "Product Updated Successfully."
	},
	"draftUpdatedSuccessfully": {
		"ENG": "Draft saved Successfully.",
		"FRA": "Draft saved Successfully."
	},
	"updateSale": {
		"ENG": "Update Sale",
		"FRA": "Update Sale"
	},
	"updateImages": {
		"ENG": "Update Images",
		"FRA": "Update Images"
	},
	"errorOccurredWhileUploadingFile": {
		"ENG": "Error Occurred while uploading file:",
		"FRA": "Error Occurred while uploading file:"
	},
	"pleaseEnterAvalidCardNumberToProceed": {
		"ENG": "Please Enter A Valid Card Number To Proceed",
		"FRA": "Please Enter A Valid Card Number To Proceed"
	},
	"pleaseEnterAvalidCvv": {
		"ENG": "Please enter a valid cvv!",
		"FRA": "Please enter a valid cvv!"
	},
	"productsMigration": {
		"ENG": "Products Migration",
		"FRA": "Products Migration"
	},
	"group": {
		"ENG": "Group",
		"FRA": "Group"
	},
	"groupId": {
		"ENG": "Group ID",
		"FRA": "Group ID"
	},
	"title": {
		"ENG": "Title",
		"FRA": "Title"
	},
	"attributeVariations": {
		"ENG": "Attribute Variations",
		"FRA": "Attributes Variations"
	},
	"treats": {
		"ENG": "Treats",
		"FRA": "Treats"
	},
	"brand": {
		"ENG": "Brand",
		"FRA": "Brand"
	},
	"view": {
		"ENG": "View",
		"FRA": "View"
	},
	"updateStock": {
		"ENG": "Update Stock",
		"FRA": "Update Stock"
	},
	"activateProduct": {
		"ENG": "Activate Product",
		"FRA": "Activate Product"
	},
	"disableProduct": {
		"ENG": "Disable Product",
		"FRA": "Disable Product"
	},
	"areYouSureThatYouWantToChangeStatusOfThisProduct": {
		"ENG": "Are you sure that you want to change status of this product?",
		"FRA": "Are you sure that you want to change status of this product?"
	},
	"areYouSureThatYouWantToChangeStatusOfThisBundle": {
		"ENG": "Are you sure that you want to change status of this bundle?",
		"FRA": "Are you sure that you want to change status of this bundle?"
	},
	"areYouSureYouWantToDeleteThisProduct": {
		"ENG": "Are you sure you want to delete this product?",
		"FRA": "Are you sure you want to delete this product?"
	},
	"areYouSureYouWantToDeleteThisBundle": {
		"ENG": "Are you sure you want to delete this bundle?",
		"FRA": "Are you sure you want to delete this bundle?"
	},
	"areYouSureYouWantToDeleteTheSelectedProduct": {
		"ENG": "Are you sure you want to delete the selected product(s)?",
		"FRA": "Are you sure you want to delete the selected product(s)?"
	},
	"tenant": {
		"ENG": "Tenant",
		"FRA": "Tenant"
	},
	"merchantId": {
		"ENG": "Merchant ID",
		"FRA": "Merchant ID"
	},
	"source": {
		"ENG": "Source",
		"FRA": "Source"
	},
	"manageProducts": {
		"ENG": "Manage Products",
		"FRA": "Manage Products"
	},
	"configure": {
		"ENG": "Configure",
		"FRA": "Configure"
	},
	"overrideProfile": {
		"ENG": "Override Profile",
		"FRA": "Override Profile"
	},
	"disableMerchants": {
		"ENG": "Disable Merchants",
		"FRA": "Disable Merchants"
	},
	"enableMerchants": {
		"ENG": "Enable Merchants",
		"FRA": "Enable Merchants"
	},
	"disableMerchantsMsg": {
		"ENG": "Are you sure that you want to change the status the selected merchant(s)?",
		"FRA": "Are you sure that you want to change the status the selected merchant(s)?"
	},
	"enableMerchantsMsg": {
		"ENG": "Are you sure you want to change the status the selected merchant(s)?",
		"FRA": "Are you sure you want to change the status the selected merchant(s)?"
	},
	"deleteMerchants": {
		"ENG": "Delete Merchants",
		"FRA": "Delete Merchants"
	},
	"areYouSureYouWantToDeleteThisMerchant": {
		"ENG": "Are you sure you want to delete this Merchant?",
		"FRA": "Are you sure you want to delete this Merchant?"
	},
	"areYouSureYouWantToDeleteTheSelectedMerchant": {
		"ENG": "Are you sure you want to delete the selected merchant(s)?",
		"FRA": "Are you sure you want to delete the selected merchant(s)?"
	},
	"areYouSureThatYouWantToChangeTheStatusOfThisMerchant": {
		"ENG": "Are you sure that you want to change the status of this merchant?",
		"FRA": "Are you sure that you want to change the status of this merchant?"
	},
	"ypMid": {
		"ENG": "Yp Mid",
		"FRA": "Yp Mid"
	},
	"areYouSureThatYouWantToChangeTheStatusOfThisPOS": {
		"ENG": "Are you sure that you want to change the status of this POS?",
		"FRA": "Are you sure that you want to change the status of this POS?"
	},
	"merchantConfigurationUpdated": {
		"ENG": "Merchant Configuration updated.",
		"FRA": "Merchant Configuration updated."
	},
	"merchantsMigration": {
		"ENG": "Merchants Migration",
		"FRA": "Merchants Migration"
	},
	"merchantRegisteredForMigration": {
		"ENG": "Merchant Registered for Migration.",
		"FRA": "Merchant Registered for Migration."
	},
	"fromEnvironment": {
		"ENG": "from Environment",
		"FRA": "from Environment"
	},
	"toEnvironment": {
		"ENG": "to Environment",
		"FRA": "to Environment"
	},
	"deleteEntries": {
		"ENG": "Delete Entries",
		"FRA": "Delete Entries"
	},
	"areYouSureYouWantToDeleteTheSelectedEntry": {
		"ENG": "Are you sure you want to delete the selected item(s)?",
		"FRA": "Are you sure you want to delete the selected item(s)?"
	},
	"oneOrMoreOfTheSelectedEntriesWasNotRemoved": {
		"ENG": "One or more of the selected Item(s) was not removed.",
		"FRA": "One or more of the selected Item(s) was not removed."
	},
	"selectedEntriesHaveBeenRemoved": {
		"ENG": "Selected Item(s) have been removed.",
		"FRA": "Selected Item(s) have been removed."
	},
	"areYouSureYouWantToActivateTheSelectedProduct": {
		"ENG": "Are you sure you want to Activate the selected product(s)?",
		"FRA": "Are you sure you want to Activate the selected product(s)?"
	},
	"createNewTenant": {
		"ENG": "-- Create New Tenant --",
		"FRA": "-- Create New Tenant --"
	},
	"newTenantName": {
		"ENG": "New Tenant Name",
		"FRA": "New Tenant Name"
	},
	"approve": {
		"ENG": "Approve",
		"FRA": "Approve"
	},
	"reject": {
		"ENG": "Reject",
		"FRA": "Reject"
	},
	"areYouSureYouWantToRejectThisMerchant": {
		"ENG": "Are you sure you want to Reject this Merchant ?",
		"FRA": "Are you sure you want to Reject this Merchant ?"
	},
	"addTenant": {
		"ENG": "Add Tenant",
		"FRA": "Add Tenant"
	},
	"merchantIsApprovedSuccessfully": {
		"ENG": "Merchant is approved Successfully.",
		"FRA": "Merchant is approved Successfully."
	},
	"merchantIsRejectedSuccessfully": {
		"ENG": "Merchant is rejected Successfully.",
		"FRA": "Merchant is rejected Successfully."
	},
	"image": {
		"ENG": "Image",
		"FRA": "Image"
	},
	"images": {
		"ENG": "Images",
		"FRA": "Images"
	},
	"searchForProduct": {
		"ENG": "Search For Product",
		"FRA": "Search For Product"
	},
	"or": {
		"ENG": "OR",
		"FRA": "OR"
	},
	"search": {
		"ENG": "Search",
		"FRA": "Search"
	},
	"processing": {
		"ENG": "Processing",
		"FRA": "Processing"
	},
	"productInformation": {
		"ENG": "Product Information",
		"FRA": "Product Information"
	},
	"createNewProduct": {
		"ENG": "Create New Product",
		"FRA": "Create New Product"
	},
	"gtin": {
		"ENG": "GTIN",
		"FRA": "GTIN"
	},
	"upc": {
		"ENG": "UPC",
		"FRA": "UPC"
	},
	"ean": {
		"ENG": "EAN",
		"FRA": "EAN"
	},
	"jan": {
		"ENG": "JAN",
		"FRA": "JAN"
	},
	"isbn": {
		"ENG": "ISBN",
		"FRA": "ISBN"
	},
	"saleStartDate": {
		"ENG": "Sale Start Date",
		"FRA": "Sale Start Date"
	},
	"saleEndDate": {
		"ENG": "Sale End Date",
		"FRA": "Sale End Date"
	},
	"percentageOff": {
		"ENG": "Percentage Off",
		"FRA": "Percentage Off"
	},
	"selectAll": {
		"ENG": "Select All",
		"FRA": "Select All"
	},
	"updateSaleProductAllVariations": {
		"ENG": "Update Sale For This Product And All Variations",
		"FRA": "Update Sale For This Product And All Variations"
	},
	"updateImagesOf": {
		"ENG": "Update Images Of %title%",
		"FRA": "Update Images Of %title%"
	},
	"sku": {
		"ENG": "SKU",
		"FRA": "SKU"
	},
	"condition": {
		"ENG": "Condition",
		"FRA": "Condition"
	},
	"upi": {
		"ENG": "UPI",
		"FRA": "UPI"
	},
	"downloadable": {
		"ENG": "Downloadable",
		"FRA": "Downloadable"
	},
	"modified": {
		"ENG": "Modified",
		"FRA": "Modified"
	},
	"classification": {
		"ENG": "Classification",
		"FRA": "Classification"
	},
	"category": {
		"ENG": "Category",
		"FRA": "Category"
	},
	"department": {
		"ENG": "Department",
		"FRA": "Department"
	},
	"review": {
		"ENG": "Review",
		"FRA": "Review"
	},
	"reviews": {
		"ENG": "Reviews",
		"FRA": "Reviews"
	},
	"translation": {
		"ENG": "Translation",
		"FRA": "Translation"
	},
	"shortDescription": {
		"ENG": "Short Description",
		"FRA": "Short Description"
	},
	"fullDescription": {
		"ENG": "Full Description",
		"FRA": "Full Description"
	},
	"media": {
		"ENG": "Media",
		"FRA": "Media"
	},
	"videos": {
		"ENG": "Videos",
		"FRA": "Videos"
	},
	"video": {
		"ENG": "Video",
		"FRA": "Video"
	},
	"removeVideo": {
		"ENG": "Remove Video",
		"FRA": "Remove Video"
	},
	"dimensionsAndWeight": {
		"ENG": "Dimensions & Weight",
		"FRA": "Dimensions & Weight"
	},
	"dimensions": {
		"ENG": "Dimensions",
		"FRA": "Dimensions"
	},
	"weight": {
		"ENG": "Weight",
		"FRA": "Weight"
	},
	"availabilityAndPricing": {
		"ENG": "Availability & Pricing",
		"FRA": "Availability & Pricing"
	},
	"quantity": {
		"ENG": "Quantity",
		"FRA": "Quantity"
	},
	"purchaseLimit": {
		"ENG": "Purchase Limit",
		"FRA": "Purchase Limit"
	},
	"applyToAll": {
		"ENG": "Apply to All",
		"FRA": "Apply to All"
	},
	"environment": {
		"ENG": "Environment",
		"FRA": "Environment"
	},
	"products": {
		"ENG": "Product(s)",
		"FRA": "Product(s)"
	},
	"migrateAll": {
		"ENG": "Migrate All",
		"FRA": "Migrate All"
	},
	"backToMerchantsManagement": {
		"ENG": "Back to Merchants Management",
		"FRA": "Back to Merchants Management"
	},
	"viewDetailsForMerchant": {
		"ENG": "View details for merchant",
		"FRA": "View details for merchant"
	},
	"addNewProduct": {
		"ENG": "Add New Product",
		"FRA": "Add New Product"
	},
	"migrateProducts": {
		"ENG": "Migrate Products",
		"FRA": "Migrate Products"
	},
	"custom": {
		"ENG": "Custom",
		"FRA": "Custom"
	},
	"posManagement": {
		"ENG": "POS Management",
		"FRA": "POS Management"
	},
	"addPosWarehouse": {
		"ENG": "Add POS/Warehouse",
		"FRA": "Add POS/Warehouse"
	},
	"shipping": {
		"ENG": "Shipping",
		"FRA": "Shipping"
	},
	"pickup": {
		"ENG": "Pickup",
		"FRA": "Pickup"
	},
	"payment": {
		"ENG": "Payment",
		"FRA": "Payment"
	},
	"billing": {
		"ENG": "Billing",
		"FRA": "Billing"
	},
	"feedConfiguration": {
		"ENG": "Feed Configuration",
		"FRA": "Feed Configuration"
	},
	"manageMerchantsAndProducts": {
		"ENG": "Manage Merchants & Products",
		"FRA": "Manage Merchants & Products"
	},
	"MerchantOrS": {
		"ENG": "Merchant(s)",
		"FRA": "Merchant(s)"
	},
	"merchants": {
		"ENG": "Merchants",
		"FRA": "Merchants"
	},
	"backToTenantsList": {
		"ENG": "Back to Tenants List",
		"FRA": "Back to Tenants List"
	},
	"migrateMerchants": {
		"ENG": "Migrate Merchants",
		"FRA": "Migrate Merchants"
	},
	"dataMigration": {
		"ENG": "Data Migration",
		"FRA": "Data Migration"
	},
	"variations": {
		"ENG": "Variations",
		"FRA": "Variations"
	},
	"size": {
		"ENG": "Size",
		"FRA": "Size"
	},
	"etc": {
		"ENG": "etc",
		"FRA": "etc"
	},
	"value": {
		"ENG": "Value",
		"FRA": "Value"
	},
	"removeVariation": {
		"ENG": "Remove Variation",
		"FRA": "Remove Variation"
	},
	"addVariation": {
		"ENG": "Add Variation",
		"FRA": "Add Variation"
	},
	"maximum": {
		"ENG": "Maximum",
		"FRA": "Maximum"
	},
	"charactersAllowed": {
		"ENG": "Characters Allowed",
		"FRA": "Characters Allowed"
	},
	"charactersLeft": {
		"ENG": "Characters left",
		"FRA": "Characters left"
	},
	"removeImage": {
		"ENG": "Remove Image",
		"FRA": "Remove Image"
	},
	"pricing": {
		"ENG": "Pricing",
		"FRA": "Pricing"
	},
	"posPricing": {
		"ENG": "POS Pricing",
		"FRA": "POS Pricing"
	},
	"unit": {
		"ENG": "Unit",
		"FRA": "Unit"
	},
	"length": {
		"ENG": "Length",
		"FRA": "Length"
	},
	"depth": {
		"ENG": "Depth",
		"FRA": "Depth"
	},
	"width": {
		"ENG": "Width",
		"FRA": "Width"
	},
	"height": {
		"ENG": "Height",
		"FRA": "Height"
	},
	"someFieldsAreStillMissing_verifyAllYourPOSes": {
		"ENG": "Some fields are still missing. Make sure you entered a quantity for all your POSes.",
		"FRA": "Some fields are still missing. Make sure you entered a quantity for all your POSes."
	},
	"someFieldsAreStillMissing": {
		"ENG": "Some fields are still missing.",
		"FRA": "Some fields are still missing."
	},
	"saveAsDraft": {
		"ENG": "Save as Draft",
		"FRA": "Save as Draft"
	},
	"color": {
		"ENG": "color",
		"FRA": "color"
	},
	"enterTranslations": {
		"ENG": "Enter Translations",
		"FRA": "Enter Translations"
	},
	"onConfig": {
		"ENG": "On",
		"FRA": "On"
	},
	"offConfig": {
		"ENG": "Off",
		"FRA": "Off"
	},
	"moreConfig": {
		"ENG": "Additional Configuration",
		"FRA": "Additional Configuration"
	},
	"endDate": {
		"ENG": "End Date",
		"FRA": "End Date"
	},
	"startDate": {
		"ENG": "Start Date",
		"FRA": "Start Date"
	},
	"promoCodeAddedSuccessfully": {
		"ENG": "promoCodeAddedSuccessfully",
		"FRA": "promoCodeAddedSuccessfully"
	},
	"promoCodes": {
		"ENG": "Promo Codes",
		"FRA": "Promo Codes"
	},
	"addPromoCode": {
		"ENG": "Add Promo Code",
		"FRA": "Add Promo Code"
	},
	"editPromoCode": {
		"ENG": "Edit Promo Code",
		"FRA": "Edit Promo Code"
	},
	"promotionType": {
		"ENG": "Promotion Type",
		"FRA": "PromotionType"
	},
	"promotionTypeTooltip": {
		"ENG": "Choose a Promotion Type",
		"FRA": "Choose a PromotionType"
	},
	"codeValue": {
		"ENG": "Code Value",
		"FRA": "Code Value"
	},
	"codeValueTooltip": {
		"ENG": "Enter your Code Value",
		"FRA": "Enter your Code Value"
	},
	"areYouSureYouWantToRemoveThisPromo": {
		"ENG": "Are You Sure You Want To Remove This Promo Code?",
		"FRA": "Are You Sure You Want To Remove This Promo Code?"
	},
	"promoAddedSuccessfully": {
		"ENG": "Promo Code Added Successfully",
		"FRA": "Promo Code Added Successfully"
	},
	"promoCodeDeletedSuccessfully": {
		"ENG": "Promo Code Deleted Successfully",
		"FRA": "Promo Code Deleted Successfully"
	},
	"promoUpdatedSuccessfully": {
		"ENG": "Promo Code Updated Successfully",
		"FRA": "Promo Code Updated Successfully"
	},
	"viewingOnePromoCode": {
		"ENG": "Viewing One Promo Code",
		"FRA": "Viewing One Promo Code"
	},
	"details": {
		"ENG": "Details",
		"FRA": "Details"
	},
	"amountDiscounted": {
		"ENG": "Discounted Amount",
		"FRA": "amount Discounted"
	},
	"usageTimesLimit": {
		"ENG": "Usage Times Limit",
		"FRA": "Usage Times Limit"
	},
	"promoDescription": {
		"ENG": "Description",
		"FRA": "Description"
	},
	"enterPromoDescription": {
		"ENG": "Enter Description",
		"FRA": "Enter Description"
	},
	"enterLimitOfUsageTimes": {
		"ENG": "Enter Limit Of Usage Times",
		"FRA": "Enter Limit Of Usage Times"
	},
	"configUpdatedSuccessfully": {
		"ENG": "Configuration updated successfully",
		"FRA": "Configuration updated successfully"
	},
	"forcedDeliverySignature": {
		"ENG": "Forced Delivery with Signature:",
		"FRA": "Forced Delivery with Signature:"
	},
	"manageBundles": {
		"ENG": "Manage Bundles",
		"FRA": "Manage Bundles"
	},
	"enterLabelForInputProceed": {
		"ENG": "Enter a label for the input to proceed!",
		"FRA": "Enter a label for the input to proceed!"
	},
	"youAlreadyHaveInputNamed": {
		"ENG": "You already have an input named:",
		"FRA": "You already have an input named:"
	},
	"textBox": {
		"ENG": "Text Box",
		"FRA": "Text Box"
	},
	"inputFormProperties": {
		"ENG": "Input Form Properties",
		"FRA": "Input Form Properties"
	},
	"textInput": {
		"ENG": "Text Input",
		"FRA": "Text Input"
	},
	"emailAddress": {
		"ENG": "Email Address",
		"FRA": "Email Address"
	},
	"phoneNumber": {
		"ENG": "Phone Number",
		"FRA": "Phone Number"
	},
	"number": {
		"ENG": "Number",
		"FRA": "Number"
	},
	
	"advancedEditor": {
		"ENG": "Advanced Editor",
		"FRA": "Advanced Editor"
	},
	"listOneValue": {
		"ENG": "list (one value)",
		"FRA": "list (one value)"
	},
	"listMultipleValue": {
		"ENG": "list (multiple values)",
		"FRA": "list (multiple values)"
	},
	"listDropDownMenu": {
		"ENG": "list (drop down menu)",
		"FRA": "list (drop down menu)"
	},
	"listDropDownMenuMultipleSelection": {
		"ENG": "list (drop down menu multiple selection)",
		"FRA": "list (drop down menu multiple selection)"
	},
	"placeholder": {
		"ENG": "Placeholder",
		"FRA": "Placeholder"
	},
	"cbFormStep2UserFormPlaceholderPlaceholder": {
		"ENG": "placeholder...",
		"FRA": "placeholder..."
	},
	"cbFormStep2UserFormPlaceholderTooltip": {
		"ENG": "Enter an optional placeholder value for the input",
		"FRA": "Enter an optional placeholder value for the input"
	},
	"cbFormStep2UserFormPlaceholderFieldMsg": {
		"ENG": "The value of this field shows up in text & textarea inputs when provided as a placeholder value.",
		"FRA": "The value of this field shows up in text & textarea inputs when provided as a placeholder value."
	}
};

for (var attrname in merchantsModuleDevTranslation) {
	translation[ attrname ] = merchantsModuleDevTranslation[ attrname ];
}

var merchantsModuleDevNav = [
	{
		//main information
		'id': 'merchantTenants',
		'label': translation.merchantsProducts[ LANG ],
		'url': '#/merchantTenants',
		'scripts': [
			ModuleDevMpLocation + '/config.js',
			ModuleDevMpLocation + '/services/merchants.js',
			ModuleDevMpLocation + '/controller.js'
		],
		'tplPath': ModuleDevMpLocation + '/directives/listTenants.tmpl',
		//menu & tracker information
		'icon': 'database',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 3,
		'ancestor': [ translation.home[ LANG ] ]
	},
	{
		//main information
		'id': 'merchantsClient',
		'label': translation.merchantsProducts[ LANG ],
		'url': '#/merchantTenants/merchants',
		'scripts': [
			ModuleDevMpLocation + '/config.js',
			ModuleDevMpLocation + '/services/merchants.js', ModuleDevMpLocation + '/controller.js' ],
		'tplPath': ModuleDevMpLocation + '/directives/list.tmpl',
		//menu & tracker information
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'ancestor': [ translation.home[ LANG ] ],
		'tracker': true
	},
	{
		'id': 'Products',
		'label': 'Show Products',
		'url': '#/merchantTenants/merchants/browse/:id',
		'tplPath': ModuleDevMpLocation + '/directives/browse.tmpl',
		'tracker': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'scripts': [
			ModuleDevMpLocation + '/config.js',
			ModuleDevMpLocation + '/services/merchants.js',
			ModuleDevMpLocation + '/async.js',
			ModuleDevMpLocation + '/services/products.js',
			ModuleDevMpLocation + '/controller-pos.js',
			ModuleDevMpLocation + '/controller-productsList.js' ],
		'ancestor': [ translation.home[ LANG ], translation.merchantsProducts[ LANG ] ]
	},
	{
		'id': 'Configure',
		'label': 'Configure Merchant',
		'url': '#/merchantTenants/merchants/configure/:id',
		'tplPath': ModuleDevMpLocation + '/directives/configure.tmpl',
		'tracker': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'scripts': [
			ModuleDevMpLocation + '/config.js',
			ModuleDevMpLocation + '/services/merchants.js', ModuleDevMpLocation + '/controller-pos.js',
			ModuleDevMpLocation + '/controller-configure.js', ModuleDevMpLocation + '/controller-shipping.js',
			ModuleDevMpLocation + '/controller-pickup.js', ModuleDevMpLocation + '/controller-payment.js' ],
		'ancestor': [ translation.home[ LANG ], translation.merchantsProducts[ LANG ] ]
	},
	{
		'id': 'addProduct',
		'label': 'Add Product',
		'url': '#/merchantTenants/merchants/browse/products/:id',
		'tplPath': ModuleDevMpLocation + '/directives/add.tmpl',
		'tracker': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'scripts': [
			ModuleDevMpLocation + '/config.js',
			ModuleDevMpLocation + '/services/helper.js',
			ModuleDevMpLocation + '/async.js',
			ModuleDevMpLocation + '/services/products.js',
			ModuleDevMpLocation + '/controller-productsAdd.js',
			ModuleDevMpLocation + '/services/input.js',
			ModuleDevMpLocation + '/services/popUpConfiguration.js'
		],
		'ancestor': [ translation.home[ LANG ], translation.merchantsProducts[ LANG ] ]
	},
	{
		'id': 'editProduct',
		'label': 'Edit Product',
		'url': '#/merchantTenants/merchants/browse/products/:merchantId/edit/:id',
		'tplPath': ModuleDevMpLocation + '/directives/edit.tmpl',
		'tracker': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'scripts': [
			ModuleDevMpLocation + '/config.js',
			ModuleDevMpLocation + '/services/helper.js',
			ModuleDevMpLocation + '/async.js',
			ModuleDevMpLocation + '/services/products.js',
			ModuleDevMpLocation + '/controller-productsAdd.js',
			ModuleDevMpLocation + '/services/input.js',
			ModuleDevMpLocation + '/services/popUpConfiguration.js'
		],
		'ancestor': [ translation.home[ LANG ], translation.merchantsProducts[ LANG ] ]
	},
	{
		'id': 'pendingMerchants',
		'label': translation.pendingMerchants[ LANG ],
		'url': '#/pendingMerchants',
		'scripts': [
			ModuleDevMpLocation + '/pendingMerchants/config.js',
			ModuleDevMpLocation + '/pendingMerchants/controller.js',
			ModuleDevMpLocation + '/async.js'
		],
		'tplPath': ModuleDevMpLocation + '/pendingMerchants/directives/list.tmpl',
		'checkPermission': {
			'service': 'knowledgebase',
			'method': 'get',
			'route': '/owner/merchants/pending/list'
		},
		'icon': 'database',
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		// 'contentMenu': true, // hidden
		// 'mainMenu': true,
		'tracker': true,
		'order': 50,
		'ancestor': [ translation.home[ LANG ] ]
	},
	
	{
		'id': 'discountEngine',
		'label': translation.discountEngine[ LANG ],
		'url': '#/discountEngine',
		'scripts': [
			ModuleDevMpLocation + '/discountEngine/config.js',
			ModuleDevMpLocation + '/discountEngine/controller.js'
		],
		'tplPath': ModuleDevMpLocation + '/directives/discountEngine.tmpl',
		'icon': 'books',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'checkPermission': {
			'service': 'knowledgebase',
			'method': 'get',
			'route': '/product/discounts'
		},
		'mainMenu': true,
		'tracker': true,
		'order': 3,
		'ancestor': [ translation.home[ LANG ] ]
	},
	{
		//main information
		'label': "Rules",
		'id': 'discount',
		'url': '#/discountEngine/rules',
		'scripts': [
			ModuleDevMpLocation + '/discountEngine/config.js',
			ModuleDevMpLocation + '/discountEngine/controller.js',
			ModuleDevMpLocation + '/discountEngine/services/service.js'
		],
		'tplPath': ModuleDevMpLocation + '/discountEngine/directives/list.tmpl',
		//permissions information
		
		//menu & tracker information
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'tracker': true,
		'ancestor': [ translation.home[ LANG ], translation.discountEngine[ LANG ] ]
	},
	{
		//main information
		'id': 'promos',
		'label': translation.promoCodes[ LANG ],
		'url': '#/discountEngine/promoCodes',
		'scripts': [
			ModuleDevMpLocation + '/discountEngine/promo/config.js',
			ModuleDevMpLocation + '/discountEngine/promo/controller.js',
			ModuleDevMpLocation + '/discountEngine/promo/services/service.js'
		],
		'tplPath': ModuleDevMpLocation + '/discountEngine/promo/directives/list.tmpl',
		//menu & tracker information
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'tracker': true,
		'ancestor': [ translation.home[ LANG ], translation.discountEngine[ LANG ] ]
	},
	{
		//main information
		'id': 'bundles',
		'label': translation.manageBundles[ LANG ],
		'url': '#/discountEngine/bundles',
		'scripts': [
			ModuleDevMpLocation + '/discountEngine/bundle/config.js',
			ModuleDevMpLocation + '/discountEngine/bundle/controller.js',
			ModuleDevMpLocation + '/async.js',
			ModuleDevMpLocation + '/discountEngine/bundle/services/bundles.js'
		],
		'tplPath': ModuleDevMpLocation + '/discountEngine/bundle/directives/list.tmpl',
		//menu & tracker information
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'tracker': true,
		'ancestor': [ translation.home[ LANG ], translation.discountEngine[ LANG ] ]
	},
	{
		//main information
		'id': 'add-bundles',
		'url': '#/discountEngine/bundles/add',
		'label': "Add Bundle",
		'scripts': [
			ModuleDevMpLocation + '/discountEngine/bundle/config.js',
			ModuleDevMpLocation + '/services/helper.js',
			ModuleDevMpLocation + '/discountEngine/bundle/controller-add.js',
			ModuleDevMpLocation + '/async.js',
			ModuleDevMpLocation + '/services/input.js',
			ModuleDevMpLocation + '/discountEngine/bundle/services/bundles.js',
			ModuleDevMpLocation + '/services/popUpConfiguration.js'
		],
		'tplPath': ModuleDevMpLocation + '/discountEngine/bundle/directives/add.tmpl',
		//menu & tracker information
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'tracker': true,
		'ancestor': [ translation.home[ LANG ], translation.manageBundles[ LANG ] ]
	},
	{
		//main information
		'id': 'edit-bundles',
		'label': "Edit Bundle",
		'url': '#/discountEngine/bundles/edit/:id',
		'scripts': [
			ModuleDevMpLocation + '/discountEngine/bundle/config.js',
			ModuleDevMpLocation + '/services/helper.js',
			ModuleDevMpLocation + '/discountEngine/bundle/controller-add.js',
			ModuleDevMpLocation + '/async.js',
			ModuleDevMpLocation + '/services/input.js',
			ModuleDevMpLocation + '/discountEngine/bundle/services/bundles.js',
			ModuleDevMpLocation + '/services/popUpConfiguration.js'
		],
		'tplPath': ModuleDevMpLocation + '/discountEngine/bundle/directives/add.tmpl',
		//menu & tracker information
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'tracker': true,
		'ancestor': [ translation.home[ LANG ], translation.manageBundles[ LANG ] ]
	},
	{
		'id': 'copy-bundles',
		'label': "Copy Bundle",
		'url': '#/discountEngine/bundles/copy/:id',
		'scripts': [
			ModuleDevMpLocation + '/discountEngine/bundle/config.js',
			ModuleDevMpLocation + '/services/helper.js',
			ModuleDevMpLocation + '/discountEngine/bundle/controller-add.js',
			ModuleDevMpLocation + '/async.js',
			ModuleDevMpLocation + '/services/input.js',
			ModuleDevMpLocation + '/discountEngine/bundle/services/bundles.js',
			ModuleDevMpLocation + '/services/popUpConfiguration.js'
		],
		'tplPath': ModuleDevMpLocation + '/discountEngine/bundle/directives/add.tmpl',
		'pillar': {
			'name': 'operate',
			'label': translation.operate[ LANG ],
			'position': 4
		},
		'tracker': true,
		'ancestor': [ translation.home[ LANG ], translation.manageBundles[ LANG ] ]
	}
];

navigation = navigation.concat(merchantsModuleDevNav);
