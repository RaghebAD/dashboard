"use strict";
var merchantsApp = soajsApp.components;

merchantsApp.controller('merchantsTenantsModuleDevCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$cookies', 'merchantsModuleDevSrv', function ($scope, $modal, ngDataApi, injectFiles, $timeout, $cookies, merchantsModuleDevSrv) {
	console.log('started merchantsTenantsModuleDevCtrl');
	$scope.$parent.isUserLoggedIn();
	$scope.ModuleDevMpLocation = ModuleDevMpLocation;
	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, knowledgeBaseModuleDevConfig.permissions);

	$scope.code = $cookies.get('knowledgebase_merchant') || '';
	$scope.changeCode = function (newCode) {
		if (newCode && newCode !== '') {
			$scope.code = newCode.toString();
			$cookies.put('knowledgebase_merchant', newCode);
			$scope.$parent.go('/merchantTenants/merchants');
		}
		else {
			$scope.code = '';
			$cookies.remove('knowledgebase_merchant');
			$scope.listTenants();
		}
	};

	$scope.listTenants = function () {
		overlayLoading.show();
		var opts = {
			"routeName": "/dashboard/tenant/list",
			"method": "get",
			"params": {"type": "client"}
		};
		merchantsModuleDevSrv.getEntriesFromApi($scope, opts, function (error, response) {
			overlayLoading.hide();
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.tenants = response;
			}
		});
	};

	$scope.patternUrl = new RegExp("^((ftp|http|https):)?\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$");
	if ($scope.access.owner.merchants.list) {
		if ($scope.code && $scope.code !== '') {
			$scope.$parent.go('/merchantTenants/merchants');
		}
		else {
			$scope.listTenants();
		}
	}
	else {
		$scope.$parent.go('/merchantTenants/merchants');
	}

	injectFiles.injectCss(ModuleDevMpLocation + "/merchants.css");
}]);

merchantsApp.controller('merchantsModuleDevCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$cookies', 'merchantsModuleDevSrv', function ($scope, $modal, ngDataApi, injectFiles, $timeout, $cookies, merchantsModuleDevSrv) {
	$scope.$parent.isUserLoggedIn();
	$scope.ModuleDevMpLocation = ModuleDevMpLocation;
	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, knowledgeBaseModuleDevConfig.permissions);

	$scope.code = $cookies.get('knowledgebase_merchant') || '';

	$scope.clearCode = function (newCode) {
		$scope.code = '';
		$cookies.remove('knowledgebase_merchant');
		$scope.$parent.go('/merchantTenants');
	};

	$scope.listEntries = function () {
		var opts;
		if ($scope.access.owner.merchants.list) {
			opts = {
				"routeName": "/knowledgebase/owner/merchants",
				"method": "get",
				"params": {
					"code": $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		else {
			opts = {
				"routeName": "/knowledgebase/tenant/merchants",
				"method": "get",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}

		merchantsModuleDevSrv.getEntriesFromApi($scope, opts, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.profileLanguages = [];
				$scope.languages = [];
				if (response.profile) {
					$scope.languages = response.profile.languages;
					$scope.languages.forEach(function (oneLanguage) {
						var tmpL = {
							'v': oneLanguage.id,
							'l': oneLanguage.label
						};
						$scope.profileLanguages.push(tmpL);
					});
				}
				else {
					$scope.$parent.displayAlert('danger', "No Profile Found");
				}
				merchantsModuleDevSrv.printGrid($scope, response.list);
			}
		});
	};

	$scope.viewEntry = function (oneDataRecord) {
		var opts;
		if ($scope.access.owner.merchants.list) {
			opts = {
				"routeName": "/knowledgebase/owner/merchants/" + oneDataRecord._id,
				"method": "get",
				"params": {
					"code": $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		else {
			opts = {
				"routeName": "/knowledgebase/tenant/merchants/" + oneDataRecord._id,
				"method": "get",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		var langs = $scope.languages;
		merchantsModuleDevSrv.getEntriesFromApi($scope, opts, function (error, data) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$modal.open({
					templateUrl: "infoBox.html",
					size: 'lg',
					backdrop: true,
					keyboard: true,
					controller: function ($scope, $modalInstance) {
						$scope.title = translation.viewingOneMerchant[LANG];
						$scope.data = data;
						$scope.languages = langs;
						fixBackDrop();
						setTimeout(function () {
							highlightMyCode()
						}, 500);
						$scope.ok = function () {
							$modalInstance.dismiss('ok');
						};
					}
				});
			}
		});
	};

	$scope.deleteMerchant = function (data) {
		var config;
		if ($scope.access.owner.merchants.delete) {
			config = {
				"method": "del",
				"routeName": "/knowledgebase/owner/merchants/" + data._id,
				"params": {
					'code': $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		else {
			config = {
				"method": "del",
				"routeName": "/knowledgebase/tenant/merchants/" + data._id,
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		getSendDataFromServer($scope, ngDataApi, config, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', ' Merchant Deleted Successfully.');
				$scope.listEntries();
			}
		});

	};

	$scope.deleteMerchants = function () {
		var config = {
			"method": "del",
			"routeParam": true,
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			},
			'msg': {
				'error': 'one or more of the selected Merchant(s) was not deleted.',
				'success': 'Selected Merchant(s) have been deleted.'
			}
		};
		if ($scope.access.owner.merchants.delete) {
			config.routeName = "/knowledgebase/owner/merchants/%id%";
			config.params.code = $scope.code;
		}
		else {
			config.routeName = "/knowledgebase/tenant/merchants/%id%";
		}

		multiRecordUpdate(ngDataApi, $scope, config, function () {
			$scope.listEntries();
		});
	};

	$scope.changeStatus = function (data) {
		var status = (data.status === 'active') ? 'disabled' : 'active';
		var opts;
		if ($scope.access.owner.merchants.changeStatus) {
			opts = {
				"method": "put",
				"routeName": "/knowledgebase/owner/merchants/" + data._id + "/status",
				"params": {
					'status': status,
					'code': $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		else {
			opts = {
				"method": "put",
				"routeName": "/knowledgebase/tenant/merchants/" + data._id + "/status",
				"params": {
					'status': status,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		overlayLoading.show();
		getSendDataFromServer($scope, ngDataApi, opts, function (error) {
			overlayLoading.hide();
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', ' Merchant Status Changed Successfully.');
				$scope.listEntries();
			}
		});

	};

	$scope.enableMerchants = function () {
		var config = {
			"routeParam": true,
			"params": {
				'status': 'active',
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			},
			'msg': {
				'error': 'one or more of the selected Merchant(s) status was not changed.',
				'success': 'Selected Merchant(s) status have been updated.'
			}
		};
		if ($scope.access.owner.merchants.changeStatus) {
			config.routeName = "/knowledgebase/owner/merchants/%id%/status";
			config.params.code = $scope.code;
		}
		else {
			config.routeName = "/knowledgebase/tenant/merchants/%id%/status";
		}

		overlayLoading.show();
		multiRecordUpdate(ngDataApi, $scope, config, function () {
			overlayLoading.hide();
			$scope.listEntries();
		});
	};

	$scope.disableMerchants = function () {
		var config;
		if ($scope.access.owner.merchants.changeStatus) {
			config = {
				'routeName': "/knowledgebase/owner/merchants/%id%/status",
				"method": "put",
				"routeParam": true,
				"params": {
					'code': $scope.code,
					'status': 'disabled',
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				'msg': {
					'error': 'one or more of the selected Merchant(s) status was not changed.',
					'success': 'Selected Merchant(s) status have been updated.'
				}
			};
		}
		else {
			config = {
				'routeName': "/knowledgebase/tenant/merchants/%id%/status",
				"method": "put",
				"routeParam": true,
				"params": {
					'status': 'disabled',
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				'msg': {
					'error': 'one or more of the selected Merchant(s) status was not changed.',
					'success': 'Selected Merchant(s) status have been updated.'
				}
			};
		}
		overlayLoading.show();
		multiRecordUpdate(ngDataApi, $scope, config, function () {
			overlayLoading.hide();
			$scope.listEntries();
		});
	};

	$scope.manageProducts = function (merchantRecord) {
		$scope.$parent.go("/merchantTenants/merchants/browse/" + merchantRecord._id.toString());

		// if (!merchantRecord.pos || !merchantRecord.pos.length > 0) {
		// 	$scope.$parent.displayAlert('warning', 'Please add at least one POS.');
		// }

	};

	$scope.patternUrl = new RegExp("^((ftp|http|https):)?\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$");

	$scope.addMerchant = function () {
		var config = angular.copy(knowledgeBaseModuleDevConfig.merchants.form);
		config.entries[2].value = $scope.profileLanguages;
		config.entries[2].value[0].selected = true;
		config.entries[3].value[0].selected = true;
		config.entries.forEach(function (oneEntry) {
			if (oneEntry.name === 'translations') {
				$scope.languages.forEach(function (oneLang) {
					var clone = angular.copy(knowledgeBaseModuleDevConfig.merchants.form.oneTab);
					clone.label = oneLang.label;
					for (var i = 0; i < clone.entries.length; i++) {
						clone.entries[i].name = clone.entries[i].name.replace("%lang%", oneLang.id);
					}
					oneEntry.tabs = oneEntry.tabs.concat(clone);
				});
			}

		});

		var options = {
			timeout: $timeout,
			form: config,
			'name': 'addMerchant',
			'label': translation.addMerchant[LANG],
			'actions': [
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var tags = [];
						var categories = [];
						if (formData.tags) {
							tags = formData.tags.split(',');
							for (var x = 0; x < tags.length; x++) {
								tags[x] = tags[x].trim();
							}
						}

						if (formData.categories) {
							categories = formData.categories.split(',');
							for (var i = 0; i < categories.length; i++) {
								categories[i] = categories[i].trim();
							}
						}
						if (!formData.logo && !formData.uploadLogo_0) {
							return $scope.form.displayAlert('danger', "Please insert or Upload a Logo");
						}
						if (!formData.image && !formData.uploadImage_0) {
							return $scope.form.displayAlert('danger', "Please insert or Upload a Store image");
						}
						var postData = {
							"merchantId": Math.floor((Math.random() * 10000) + 10000).toString(),
							"language": formData.language,
							"domain": formData.domain,
							"name": formData.name,
							"homepage": formData.homepage,
							"description": {},
							"tags": tags,
							"categories": categories,
							"rating": {
								"average": 0,
								"count": 0
							}
						};
						if (formData.image) {
							postData.image = {
								"url": formData.image
							}
						}
						if (formData.logo) {
							postData.logo = {
								"url": formData.logo
							}
						}
						if (postData.image && formData.imageAltTag) {
							postData.image.altTag = formData.imageAltTag;
						}
						if (postData.logo && formData.logoAltTag) {
							postData.logo.altTag = formData.logoAltTag;
						}

						postData.status = formData.status;

						if (!formData.average && formData.count || formData.average && !formData.count) {
							return;
						}
						if (formData.average && formData.count) {
							postData.rating = {
								average: formData.average,
								count: formData.count
							};
						}

						postData.information = {
							'customer_service': {},
							'return_policy': {},
							'terms_condition': {}
						};
						$scope.languages.forEach(function (oneLang) {
							postData.description[oneLang.id] = formData["description" + oneLang.id];
							postData.information.customer_service[oneLang.id] = formData["customer_service" + oneLang.id];
							postData.information.return_policy[oneLang.id] = formData["return_policy" + oneLang.id];
							postData.information.terms_condition[oneLang.id] = formData["terms_condition" + oneLang.id];
						});

						if (postData.information.terms_condition.length === 0) {
							delete postData.information.terms_condition;
						}
						if (formData.address !== '') {
							postData.information.address = formData.address
						}
						if (formData.phone !== '') {
							postData.information.phone = formData.phone
						}
						if (formData.email !== '') {
							postData.information.email = formData.email
						}
						overlayLoading.show();
						if (postData.logo && !formData.uploadLogo_0) {
							merchantsModuleDevSrv.imageUpload($scope, postData, 'logo', function (error) {
								if (error) {
									overlayLoading.hide();
									return $scope.form.displayAlert('danger', error);
								}
								if (postData.image && !formData.uploadImage_0) {
									merchantsModuleDevSrv.imageUpload($scope, postData, 'image', function (error) {
										if (error) {
											overlayLoading.hide();
											return $scope.form.displayAlert('danger', error);
										}
										save();
									});
								}
								else if (formData.uploadImage_0) {
									merchantsModuleDevSrv.uploadFile($scope, formData.uploadImage_0, "/knowledgebase/products/addImage", function (error, response) {
										if (error) {
											var errorString;
											if (Array.isArray(error)) {
												errorString = error.join(", ");
											}
											else {
												errorString = error.message;
											}
											overlayLoading.hide();
											$scope.$parent.displayAlert('danger', errorString);
										}
										else {
											postData.image = {
												"url": response[0].original,
												"resource": response[0].resource,
												"type": response[0].type
											};
										}

										if (formData.imageAltTag) {
											postData.image.altTag = formData.imageAltTag;
										}
										save();
									});
								}

							});
						}
						else if (formData.uploadLogo_0) {
							merchantsModuleDevSrv.uploadFile($scope, formData.uploadLogo_0, "/knowledgebase/products/addImage", function (error, response) {
								if (error) {
									var errorString;
									if (Array.isArray(error)) {
										errorString = error.join(", ");
									}
									else {
										errorString = error.message;
									}
									overlayLoading.hide();
									return $scope.form.displayAlert('danger', errorString);
								}
								postData.logo = {
									"url": response[0].original,
									"resource": response[0].resource,
									"type": response[0].type
								};
								if (formData.logoAltTag) {
									postData.logo.altTag = formData.logoAltTag;
								}
								if (postData.image && !formData.uploadImage_0) {
									merchantsModuleDevSrv.imageUpload($scope, postData, 'image', function (error) {
										if (error) {
											overlayLoading.hide();
											return $scope.form.displayAlert('danger', error);
										}
										save();
									});
								}
								else if (formData.uploadImage_0) {
									merchantsModuleDevSrv.uploadFile($scope, formData.uploadImage_0, "/knowledgebase/products/addImage", function (error, response) {
										if (error) {
											var errorString;
											if (Array.isArray(error)) {
												errorString = error.join(", ");
											}
											else {
												errorString = error.message;
											}
											overlayLoading.hide();
											return $scope.form.displayAlert('danger', errorString);
										}
										postData.image = {
											"url": response[0].original,
											"resource": response[0].resource,
											"type": response[0].type
										};
										if (formData.imageAltTag) {
											postData.image.altTag = formData.imageAltTag;
										}
										save();
									});
								}

							});
						}
						function save() {
							var opts;
							if ($scope.access.owner.merchants.add) {
								opts = {
									"method": "send",
									"routeName": "/knowledgebase/owner/merchants",
									"params": {
										"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
									},
									"data": {
										"merchant": postData,
										"code": $scope.code
									}
								};
							}
							else {
								opts = {
									"method": "send",
									"routeName": "/knowledgebase/tenant/merchants",
									"params": {
										"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
									},
									"data": {
										"merchant": postData
									}
								};
							}
							getSendDataFromServer($scope, ngDataApi, opts, function (error) {
								overlayLoading.hide();
								if (error) {
									$scope.form.displayAlert('danger', error.message);
								}
								else {
									$scope.$parent.displayAlert('success', ' Merchant Added Successfully.');
									$scope.modalInstance.close();
									$scope.form.formData = {};
									$scope.listEntries();
								}
							});
						}
					}

				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);

	};

	$scope.editMerchant = function (data) {

		var dataCopy = angular.copy(data);
		if (dataCopy.logo && dataCopy.logo.url) {
			data.logo = dataCopy.logo.url;
		}
		if (dataCopy.image && dataCopy.image.url) {
			data.image = dataCopy.image.url;
		}
		if (dataCopy.image.altTag) {
			data.imageAltTag = dataCopy.image.altTag;
		}
		if (dataCopy.logo.altTag) {
			data.logoAltTag = dataCopy.logo.altTag;
		}

		var config = angular.copy(knowledgeBaseModuleDevConfig.merchants.form);
		config.entries.forEach(function (oneEntry) {

			if (oneEntry.name === 'rating' && data.rating) {
				oneEntry.entries.forEach(function (oneField) {
					if (oneField.name === "average") {
						oneField.value = data.rating.average;
					}
					if (oneField.name === "count") {
						oneField.value = data.rating.count;
					}
				});
			}
			if (oneEntry.name === 'information' && data.information) {
				oneEntry.collapsed = false;
				oneEntry.entries.forEach(function (oneField) {
					if (oneField.name === "email") {
						oneField.value = data.information.email;
					}
					if (oneField.name === "phone") {
						if (data.information && data.information.phone) {
							oneField.value = data.information.phone;
							if (data.information.phone.charAt(0) === "+" && data.information.phone.charAt(1) === "1") {
								oneField.value = data.information.phone.substring(2);
							}
						}
					}
					if (oneField.name === "address") {
						oneField.value = data.information.address;
					}
				});
			}
			if (oneEntry.name === 'translations' && oneEntry.type === 'tabset') {
				$scope.languages.forEach(function (oneLang) {
					var clone = angular.copy(knowledgeBaseModuleDevConfig.merchants.form.oneTab);
					clone.label = oneLang.label;
					for (var i = 0; i < clone.entries.length; i++) {
						clone.entries[i].name = clone.entries[i].name.replace("%lang%", oneLang.id);
					}
					oneEntry.tabs = oneEntry.tabs.concat(clone);
				});
				oneEntry.tabs.forEach(function (oneTab) {
					$scope.languages.forEach(function (oneLang) {
						oneTab.entries.forEach(function (oneField) {
							if (oneField.name === "description" + oneLang.id) {
								oneField.value = data.description[oneLang.id];
							}
							if (oneField.name === "customer_service" + oneLang.id) {
								oneField.value = data.information.customer_service[oneLang.id];
							}
							if (oneField.name === "return_policy" + oneLang.id) {
								oneField.value = data.information.return_policy[oneLang.id];
							}
							if (oneField.name === "terms_condition" + oneLang.id) {
								oneField.value = data.information.terms_condition[oneLang.id];
							}
						});
					})
				});
			}
			if (oneEntry.name === 'rating' && data.rating.average) {
				oneEntry.collapsed = false;
			}

			if (oneEntry.name === 'merchantId') {
				oneEntry.type = 'readonly';
			}
		});

		config.entries[2].value = angular.copy($scope.profileLanguages);
		config.entries[2].value.forEach(function (oneValue) {
			if (oneValue.v === data.language) {
				oneValue.selected = true;
			}
		});
		var options = {
			timeout: $timeout,
			form: config,
			'name': 'editMerchants',
			'label': translation.editMerchant[LANG],
			'data': data,
			'actions': [
				{
					'type': 'submit',
					'label': translation.saveChanges[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var tags = [];
						var categories = [];
						if (formData.tags) {
							tags = formData.tags.split(',');
							for (var x = 0; x < tags.length; x++) {
								tags[x] = tags[x].trim();
							}
						}

						if (formData.categories) {
							categories = formData.categories.split(',');
							for (var i = 0; i < categories.length; i++) {
								categories[i] = categories[i].trim();
							}
						}
						if (!formData.logo && !formData.uploadLogo_0) {
							return $scope.form.displayAlert('danger', "Please insert or Upload a Logo");
						}
						if (!formData.image && !formData.uploadImage_0) {
							return $scope.form.displayAlert('danger', "Please insert or Upload a Store image");
						}
						var postData = {
							"merchantId": data.merchantId,
							"language": formData.language,
							"domain": formData.domain,
							"name": formData.name,
							"homepage": formData.homepage,
							'description': {},
							"tags": tags,
							"categories": categories,
							"rating": {
								"average": 0,
								"count": 0
							}
						};
						if (dataCopy.logo.url && formData.logo && JSON.stringify(formData.logo).trim() === JSON.stringify(dataCopy.logo.url).trim()) {
							postData.logo = {
								"url": dataCopy.logo.url,
								"resource": dataCopy.logo.resource,
								"type": dataCopy.logo.type
							};
						}
						else if (formData.logo) {
							postData.logo = {
								"url": formData.logo
							};
						}

						if (dataCopy.image.url && formData.image && JSON.stringify(formData.image).trim() === JSON.stringify(dataCopy.image.url).trim()) {
							postData.image = {
								"url": dataCopy.image.url,
								"resource": dataCopy.image.resource,
								"type": dataCopy.image.type
							};
						}
						else if (formData.image) {
							postData.image = {
								"url": formData.image
							};
						}
						if (postData.image && formData.imageAltTag) {
							postData.image.altTag = formData.imageAltTag;
						}
						if (postData.logo && formData.logoAltTag) {
							postData.logo.altTag = formData.logoAltTag;
						}
						postData.status = formData.status;
						if (!formData.average && formData.count || formData.average && !formData.count) {
							return;
						}
						if (formData.average && formData.count) {
							postData.rating = {
								average: formData.average,
								count: formData.count
							};
						}
						postData.information = {
							'customer_service': {},
							'return_policy': {},
							'terms_condition': {}
						};
						$scope.languages.forEach(function (oneLang) {
							postData.description[oneLang.id] = formData["description" + oneLang.id];
							postData.information.customer_service[oneLang.id] = formData["customer_service" + oneLang.id];
							postData.information.return_policy[oneLang.id] = formData["return_policy" + oneLang.id];
							postData.information.terms_condition[oneLang.id] = formData["terms_condition" + oneLang.id];
						});
						if (postData.information.terms_condition.length === 0) {
							delete postData.information.terms_condition;
						}
						if (formData.address !== '') {
							postData.information.address = formData.address;
						}
						if (formData.phone !== '') {
							if (formData.phone.charAt(0) === "+" && formData.phone.charAt(1) === "1") {
								formData.phone = formData.phone.substring(2);
							}
							postData.information.phone = formData.phone;
						}
						if (formData.email !== '') {
							postData.information.email = formData.email;
						}
						overlayLoading.show();
						if (postData.logo && !formData.uploadLogo_0) {
							merchantsModuleDevSrv.imageUpload($scope, postData, 'logo', function (error) {
								if (error) {
									overlayLoading.hide();
									return $scope.form.displayAlert('danger', error);
								}
								if (postData.image && !formData.uploadImage_0) {
									merchantsModuleDevSrv.imageUpload($scope, postData, 'image', function (error) {
										if (error) {
											overlayLoading.hide();
											return $scope.form.displayAlert('danger', error);
										}
										save();
									});
								}
								else if (formData.uploadImage_0) {
									merchantsModuleDevSrv.uploadFile($scope, formData.uploadImage_0, "/knowledgebase/products/addImage", function (error, response) {
										if (error) {
											var errorString;
											if (Array.isArray(error)) {
												errorString = error.join(", ");
											}
											else {
												errorString = error.message;
											}
											overlayLoading.hide();
											return $scope.form.displayAlert('danger', errorString);
										}
										postData.image = {
											"url": response[0].original,
											"resource": response[0].resource,
											"type": response[0].type
										};
										if (formData.imageAltTag) {
											postData.image.altTag = formData.imageAltTag;
										}
										save();
									});
								}

							});
						}
						else if (formData.uploadLogo_0) {
							merchantsModuleDevSrv.uploadFile($scope, formData.uploadLogo_0, "/knowledgebase/products/addImage", function (error, response) {
								if (error) {
									var errorString;
									if (Array.isArray(error)) {
										errorString = error.join(", ");
									}
									else {
										errorString = error.message;
									}
									overlayLoading.hide();
									return $scope.form.displayAlert('danger', errorString);
								}

								postData.logo = {
									"url": response[0].original,
									"resource": response[0].resource,
									"type": response[0].type
								};
								if (formData.logoAltTag) {
									postData.logo.altTag = formData.logoAltTag;
								}
								if (postData.image && !formData.uploadImage_0) {
									merchantsModuleDevSrv.imageUpload($scope, postData, 'image', function (error) {
										if (error) {
											overlayLoading.hide();
											return $scope.form.displayAlert('danger', error);
										}
										save();
									});
								}
								else if (formData.uploadImage_0) {
									merchantsModuleDevSrv.uploadFile($scope, formData.uploadImage_0, "/knowledgebase/products/addImage", function (error, response) {
										if (error) {
											var errorString;
											if (Array.isArray(error)) {
												errorString = error.join(", ");
											}
											else {
												errorString = error.message;
											}
											overlayLoading.hide();
											return $scope.form.displayAlert('danger', errorString);
										}
										postData.image = {
											"url": response[0].original,
											"resource": response[0].resource,
											"type": response[0].type
										};
										if (formData.imageAltTag) {
											postData.image.altTag = formData.imageAltTag;
										}
										save();
									});
								}

							});
						}
						function save() {
							var opts = {
								"method": "put",
								"params": {
									"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
								},
								"data": {
									"merchant": postData
								}
							};
							if ($scope.access.owner.merchants.edit) {
								opts.routeName = "/knowledgebase/owner/merchants/" + data._id;
								opts.params.code = $scope.code;
							}
							else {
								opts.routeName = "/knowledgebase/tenant/merchants/" + data._id;
							}

							getSendDataFromServer($scope, ngDataApi, opts, function (error) {
								overlayLoading.hide();
								if (error) {
									$scope.form.displayAlert('danger', error.message);
								}
								else {
									$scope.$parent.displayAlert('success', ' Merchant Updated Successfully.');
									$scope.modalInstance.close();
									$scope.form.formData = {};
									$scope.listEntries();
								}
							});
						}
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);

	};

	$scope.configureMerchant = function (merchantRecord) {
		$scope.$parent.go("/merchantTenants/merchants/configure/" + merchantRecord._id.toString());
	};

	$scope.overrideProfile = function (data) {
		var opts;
		if ($scope.access.owner.merchants.get) {
			opts = {
				"method": "get",
				"routeName": "/knowledgebase/owner/merchants/" + data._id,
				"params": {
					"code": $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		else {
			opts = {
				"method": "get",
				"routeName": "/knowledgebase/tenant/merchants/" + data._id,
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		getSendDataFromServer($scope, ngDataApi, opts, function (error, merchantRecordData) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				openEditForm(merchantRecordData);
			}
		});

		function openEditForm(merchantRecordData) {
			var config = angular.copy(knowledgeBaseModuleDevConfig.merchants.override);
			var formData = merchantRecordData.feedConfig;
			var options = {
				timeout: $timeout,
				form: config,
				'name': 'overrideProfile',
				'label': translation.merchantOverrideFeedProfile[LANG],
				'data': {
					"config": formData
				},
				'actions': [
					{
						'type': 'submit',
						'label': translation.saveChanges[LANG],
						'btn': 'primary',
						'action': function (formData) {
							var postData = {
								"config": (formData.config)
							};

							var k = ['id', 'name', 'value', 'children', 'attributes'];
							merchantsModuleDevSrv.fixIMFVPropertyNames(postData.config, k);

							var opts;
							if ($scope.access.owner.merchants.overrideProfile) {
								opts = {
									"method": "send",
									"routeName": "/knowledgebase/owner/merchants/overrideProfile",
									"params": {
										'id': data._id,
										"code": $scope.code,
										"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
									},
									"data": postData
								};
							}
							else {
								opts = {
									"method": "send",
									"routeName": "/knowledgebase/tenant/merchants/overrideProfile",
									"params": {
										'id': data._id,
										"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
									},
									"data": postData
								};
							}

							getSendDataFromServer($scope, ngDataApi, opts, function (error) {
								if (error) {
									$scope.form.displayAlert('danger', error.message);
								}
								else {
									$scope.$parent.displayAlert('success', ' Merchant Updated Successfully.');
									$scope.modalInstance.close();
									$scope.form.formData = {};
								}
							});
						}
					},
					{
						'type': 'reset',
						'label': translation.cancel[LANG],
						'btn': 'danger',
						'action': function () {
							$scope.modalInstance.dismiss('cancel');
							$scope.form.formData = {};
						}
					}
				]
			};
			buildFormWithModal($scope, $modal, options);
		}
	};

	$scope.migrateMerchants = function () {
		merchantsModuleDevSrv.migrateMerchants($scope);
	};

	$scope.listMigrationMerchants = function () {
		merchantsModuleDevSrv.listMigrationMerchants($scope);
	};

	$scope.removeMigrationMerchants = function (data) {
		merchantsModuleDevSrv.removeMigrationMerchants($scope);
	};

	if ($scope.access.tenant.merchants.list || $scope.access.owner.merchants.list) {
		$scope.listEntries();
	}

	injectFiles.injectCss(ModuleDevMpLocation + "/merchants.css");
}]);