"use strict";
var bundlesApp = soajsApp.components;

function setSegmentationDataSpecificApi(outerScope, $scope, ngDataApi, route, tempoVar, APIvar, condition, callback) {
	var opts = {
		"method": "get",
		"routeName": route,
		"params": {}
	};
	
	if (condition && condition.provinceCode !== "**ALL**") {
		opts.params = condition;
	}
	
	getSendDataFromServer(outerScope, ngDataApi, opts, function (error, response) {
		if (error) {
			return callback(error.message);
		}
		
		$scope.tempo[tempoVar] = [];
		
		if (response && response[APIvar]) {
			
			var reply = response[APIvar];
			
			for (var i = 0; i < reply.length; i++) {
				if (APIvar.toLowerCase().includes("headings")) { // headings or sensitive headings
					var temp = {
						code: reply[i].code,
						value: reply[i].code,
						isSelected: false
					};
					if (reply[i].name && reply[i].name['en']) {
						temp.value = reply[i].name['en'];
					}
				}
				else {
					var temp = {
						code: reply[i].code,
						value: reply[i]['en'],
						isSelected: false
					};
					
					// for markets
					if (condition) {
						if (condition.provinceCode === "**ALL**") {
							temp.parent = reply[i].province;
							if (temp.parent) {
								temp.parent = temp.parent.toLowerCase()
							}
						} else {
							temp.parent = condition.provinceCode;
						}
					}
					// for markets
					if (reply[i].name && reply[i].name['en']) {
						temp.value = reply[i].name['en'];
					}
				}
				$scope.tempo[tempoVar].push(temp);
			}
		}
		return callback(null);
	});
}

function assignSelectedSegmentation(allValues, array2, include, parent) {
	
	if (!array2) {
		array2 = [];
	}
	
	var array = [];
	for (var i = 0; allValues && i < allValues.length; i++) {
		var found = false;
		
		for (var j = 0; j < array2.length; j++) {
			if (allValues[i].code === array2[j].code) {
				
				var tempo = {
					code: allValues[i].code,
					value: allValues[i].value,
					isSelected: include === array2[j].isSelected // xnor
				};
				
				if (parent) {
					tempo.parent = parent;
				}
				
				array.push(tempo);
				
				found = true;
				break;
			}
		}
		
		if (!found) {
			var tempo = {
				code: allValues[i].code,
				value: allValues[i].value,
				isSelected: false
			};
			
			if (parent) {
				tempo.parent = parent;
			}
			
			array.push(tempo);
		}
		
	}
	
	return array;
}

function array2booleanObject(array) {
	var output = {};
	if (!array) {
		return output;
	}
	array.forEach(function (one) {
		output[one] = true;
	});
	return output;
}

function booleanObject2array(obj) {
	var output = [];
	if (!obj) {
		return output;
	}
	var elements = Object.keys(obj);
	for (var i = 0; i < elements.length; i++) {
		if (obj[elements[i]] === true) {
			output.push(elements[i]);
		}
	}
	return output;
}

// from db to form and vice versa

function fetchMarketsFromSubObjects(input) {
	var output = [];
	if (!input) {
		return [];
	}
	var inputAsArray = Object.keys(input);
	inputAsArray.forEach(function (each) { // each = array of province
		output = output.concat(input[each]);
	});
	
	return output;
}

function convertIncludedAndExcludedFormToDb(tempo, include, isMarket) {
	var incOrExc;
	
	if (include) {
		incOrExc = 'included';
	} else {
		incOrExc = 'excluded';
	}
	
	var output = {
		included: [],
		excluded: []
	};
	
	for (var i = 0; i < tempo.length; i++) {
		if (tempo[i].isSelected) {
			
			var obj = {
				value: tempo[i].code,
				label: tempo[i].value
			};
			
			// save parent for markets
			if (isMarket) {
				obj.parent = tempo[i].parent;
			}
			
			output[incOrExc].push(obj);
			
			
		}
	}
	return output;
}

function convertIncludedAndExcludedDbToForm(object) {
	var output = [];
	
	var included = object.included;
	var excluded = object.excluded;
	
	for (var i = 0; included && i < included.length; i++) {
		var tempo = {
			code: included[i].value,
			value: included[i].label,
			isSelected: true
		};
		
		// for markets
		if (included[i].parent) {
			tempo.parent = included[i].parent;
		}
		
		output.push(tempo);
	}
	
	for (var i = 0; excluded && i < excluded.length; i++) {
		var tempo = {
			code: excluded[i].value,
			value: excluded[i].label,
			isSelected: false
		};
		
		// for markets
		if (excluded[i].parent) {
			tempo.parent = excluded[i].parent;
		}
		
		output.push(tempo);
	}
	
	return output;
}

bundlesApp.controller('discountEngineModuleDevCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$cookies', 'discountModuleDevSrv',
	function ($scope, $modal, ngDataApi, injectFiles, $timeout, $cookies, discountModuleDevSrv) {
		
		$scope.$parent.isUserLoggedIn();
		$scope.ModuleDevMpLocation = ModuleDevMpLocation;
		if (!$scope.$parent.currentSelectedEnvironment) {
			$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
		}
		$scope.access = {};
		constructModulePermissions($scope, $scope.access, discountModuleDevConfig.permissions);
		
		$scope.editRule = function (data) {
			// if (data.description) {
			// 	if (typeof (data.description) === 'string') {
			// 		data.description = {
			// 			en: data.description
			// 		};
			// 	}
			// }
			$scope.openModal(data, true);
		};
		
		$scope.addRule = function () {
			var newData = {
				type: "",
				// config: {},
				discount: {
					type: "",
					value: 0
				},
				category: {
					include: []
				},
				serial: [],
				status: "inactive",
				startDate: ""
				// endDate: ""
			};
			
			$scope.openModal(newData);
		};
		
		$scope.openModal = function (newData, isEdit) {
			var __env = $scope.$parent.currentSelectedEnvironment;
			var outerScope = $scope;
			$modal.open({
				templateUrl: ModuleDevMpLocation + "/discountEngine/directives/modals/addDiscountRule.tmpl",
				size: 'lg',
				backdrop: true,
				keyboard: true,
				controller: function ($scope, $modalInstance) {
					$scope.title = "Add Rule";
					$scope.tempo = {
						availableProducts: [],
						salesChannels: {},
						serviceAreaIsInclusive: true,
						serviceArea: [],
						selectedMarketForDisplay: {},
						markets: []
					};
					
					if (isEdit) {
						$scope.isEdit = true;
						$scope.title = "Edit Rule";
						setSegmentationDataSpecificApi(outerScope, $scope, ngDataApi, "/knowledgebase/product/serviceArea", 'completeServiceArea', 'serviceArea', null, function (error) {
							if (!newData.criteria.serviceArea) {
								newData.criteria.serviceArea = {
									included: [],
									province: {
										included: []
									}
								};
							}
							
							$scope.tempo.markets = convertIncludedAndExcludedDbToForm(newData.criteria.serviceArea);
							
							if (newData.criteria.serviceArea.province) {
								$scope.tempo.serviceArea = convertIncludedAndExcludedDbToForm(newData.criteria.serviceArea.province);
								$scope.tempo.serviceArea = assignSelectedSegmentation($scope.tempo.completeServiceArea, $scope.tempo.serviceArea, $scope.tempo.serviceAreaIsInclusive);
								
								var provincesSavedInDb = newData.criteria.serviceArea.province.included;
								provincesSavedInDb.forEach(function (province) {
									var condition = {
										provinceCode: province.value
									};
									setSegmentationDataSpecificApi(outerScope, $scope, ngDataApi, "/knowledgebase/product/markets", 'completeMarkets', 'markets', condition, function (error) {
										$scope.tempo.selectedMarketForDisplay[province.value] = assignSelectedSegmentation($scope.tempo.completeMarkets, $scope.tempo.markets, true, province.value);
									});
								});
							}
							
						});
					} else {
						setSegmentationDataSpecificApi(outerScope, $scope, ngDataApi, "/knowledgebase/product/serviceArea", 'completeServiceArea', 'serviceArea', null, function (error) {
							$scope.tempo.serviceArea = assignSelectedSegmentation($scope.tempo.completeServiceArea, $scope.tempo.serviceArea, $scope.tempo.serviceAreaIsInclusive);
						});
					}
					
					$scope.profileCategories = [];
					
					$scope.setAllAvailableProducts = function () {
						var opts = {
							"method": "get",
							"routeName": "/knowledgebase/product/availableProducts",
							"params": {
								"productsOrBundles": "both",
								"__env": __env
							}
						};
						
						discountModuleDevSrv.getEntriesFromApi(outerScope, opts, function (error, data) {
							if (error) {
								$scope.$parent.displayAlert('danger', error.message);
							}
							else {
								var products = data.records;
								var availableProducts = [];
								for (var i = 0; i < products.length; i++) {
									
									var isSelected = false;
									if (isEdit && $scope.discount.criteria) {
										if ($scope.discount.criteria.serial && $scope.discount.criteria.serial.indexOf(products[i].serial) !== -1) {
											isSelected = true;
										}
									}
									
									var temp = {
										serial: products[i].serial,
										title: products[i].productData.title["en"],
										isSelected: isSelected
									};
									if (products[i].coreData.udac) {
										temp.udac = products[i].coreData.udac;
									}
									temp.fullName = temp.serial + " " + temp.title;
									if (temp.fullName.length > 55) {
										temp.fullName = temp.fullName.substring(0, 50) + "..";
									}
									availableProducts.push(temp);
								}
								
								$scope.tempo.availableProducts = availableProducts;
							}
						});
					};
					$scope.setAllAvailableProducts();
					
					$scope.category = '';
					fixBackDrop();
					$scope.ModuleDevMpLocation = ModuleDevMpLocation;
					$scope.showSelectedCategories = false;
					$scope.profileSelectedCategories = {};
					$scope.categoriesList = [];
					$scope.discount = newData;
					
					$scope.myCategories = [];
					$scope.outerScope = outerScope;
					$scope.message = {};
					
					if (newData && newData.criteria) {
						if (newData.criteria.salesRep) {
							$scope.tempo.salesChannels = array2booleanObject(newData.criteria.salesRep.salesChannels);
						} else {
							newData.criteria.salesRep = {};
						}
					} else {
						newData.criteria = {};
					}
					
					$scope.beforeRenderStartDate = function ($view, $dates, $leftDate, $upDate, $rightDate) {
						if ($scope.discount.endDate) {
							var activeDate = moment($scope.discount.endDate);
							for (var i = 0; i < $dates.length; i++) {
								if ($dates[i].localDateValue() >= activeDate.valueOf()) {
									$dates[i].selectable = false;
								}
							}
						}
						else if ($scope.discount.startDate) {
							$scope.$broadcast('RenderEndDate');
						}
						var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
						for (var i = 0; i < $dates.length; i++) {
							if ($dates[i].localDateValue() < minDate.valueOf()) {
								$dates[i].selectable = false;
							}
						}
					};
					
					$scope.beforeRenderEndDate = function ($view, $dates, $leftDate, $upDate, $rightDate) {
						if ($scope.discount.startDate) {
							var activeDate = moment($scope.discount.startDate).subtract(1, $view).add(1, 'minute');
							for (var i = 0; i < $dates.length; i++) {
								if ($dates[i].localDateValue() <= activeDate.valueOf()) {
									$dates[i].selectable = false;
								}
							}
						}
						else if ($scope.discount.endDate) {
							$scope.$broadcast('RenderStartDate');
						}
						var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
						for (var i = 0; i < $dates.length; i++) {
							if ($dates[i].localDateValue() <= minDate.valueOf()) {
								$dates[i].selectable = false;
							}
						}
					};
					
					$scope.toTimeZone = function (time, zone) {
						var format = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]';
						return moment(moment(time), format).tz(zone).format(format);
					};
					
					var id_feed;
					
					/**
					 *  gets the children of a parent category when clicked
					 *  if category null gets the parent categories
					 * @param id
					 * @param category
					 * @param cb
					 */
					function readFeedProfile(id, category, cb) {
						var params = {
							"source": "catalog",
							"__env": __env
						};
						if (category && category !== "") {
							params["category"] = category;
						}
						
						getSendDataFromServer(outerScope, ngDataApi, {
							"routeName": "/kbprofile/owner/feed/profiles/" + id,
							"method": "get",
							"params": params
						}, function (error, resp) {
							if (error) {
								//outerScope.$parent.displayAlert('danger', error.message');
								cb();
							}
							else {
								cb(resp);
							}
						});
					}
					
					/**
					 * gets the category listing of the given profile
					 * @param profileRecord
					 * @param currentList
					 * @param parentCategory
					 */
					function buildCategoriesFromProfile(profileRecord, currentList, parentCategory) {
						var categories = [];
						var defaultLanguage = '';
						if (profileRecord) {
							for (var i = 0; i < profileRecord.languages.length; i++) {
								if (profileRecord.languages[i].selected) {
									defaultLanguage = profileRecord.languages[i].id;
								}
							}
							
							var parentIndex;
							for (var i = 0; i < currentList.length; i++) {
								if (currentList[i].v === parentCategory) {
									parentIndex = i;
								}
							}
							
							for (var cat in profileRecord.categories) {
								processCategoryLevel(cat, profileRecord.categories, currentList, parentIndex);
							}
							
							if (currentList && currentList.length > 0) {
								profileRecord.categories = currentList;
								profileRecord.categories.splice.apply(profileRecord.categories, [parentIndex + 1, 0].concat(categories));
							}
							else {
								profileRecord.categories = categories;
							}
							
						}
						
						function processCategoryLevel(cat, categoriesArray, currentList, parentIndex) {
							var id = cat;
							var label = (profileRecord.taxonomies[cat]) ? profileRecord.taxonomies[cat].label[defaultLanguage].value : cat;
							if (categoriesArray[cat]) {
								if (categoriesArray[cat]['deprecated-towards']) {
									//get the new value
									id = categoriesArray[cat]['deprecated-towards'];
									label += " (" + translation.deprecated[LANG] + ")," + translation.becomes[LANG] + ":" + profileRecord.taxonomies[id].label[defaultLanguage].value;
								}
								
								if (categoriesArray[cat].parents && categoriesArray[cat].parents.length > 0) {
									//label = getLabels(categoriesArray[cat].parents, defaultLanguage) + " / " + label;
									label = currentList[parentIndex].l + " / " + label;
								}
								
								//Getting level based on label string
								var labelArray = label.split(" / ");
								
								var t = {
									"v": id,
									"l": label,
									"level": labelArray.length - 1
								};
								
								categories.push(t);
							}
						}
						
					}
					
					function buildTree() {
						function getActiveProfile(cb) {
							var params = {
								"__env": __env
							};
							
							getSendDataFromServer(outerScope, ngDataApi, {
								"routeName": "/kbprofile/owner/feed/active/profiles",
								"method": "get",
								"params": params
							}, function (error, resp) {
								if (error) {
									cb();
								}
								else {
									id_feed = resp._id.toString();
									cb();
								}
							});
						}
						
						getActiveProfile(function () {
							
							readFeedProfile(id_feed, $scope.category, function (profile) {
								$scope.loading = true;
								if (profile) {
									buildCategoriesFromProfile(profile, $scope.categoriesList, $scope.category);
									$scope.categoriesList = angular.fromJson(angular.copy(profile.categories));
									if (newData.criteria && newData.criteria.category && newData.criteria.category.include) {
										$scope.profileCategories = newData.criteria.category.include;
									}
									
									$scope.selectedFeed = profile._id;
								}
								else {
									return;
								}
								
								if ($scope.selectAll) {
									$scope.profileCategories = [];
									$scope.selectAllCategories = true;
									$timeout(function () {
										$scope.loading = false;
									}, 500);
								}
								else {
									loopAndRebuild($scope.categoriesList, 0, function () {
										//$scope.showSelectedCategories = true;
									});
								}
							});
						});
						
					}
					
					function getCategoriesArr(obj) {
						var arr = [];
						for (var c in obj) {
							if (obj[c] && obj[c] === true) {
								arr.push(c);
							}
						}
						return arr;
					}
					
					function loopAndRebuild(categoriesList, i, cb) {
						if (!categoriesList[i]) {
							// todo: add assurance
						}
						if ($scope.profileCategories) {
							
							if ($scope.profileCategories.indexOf(categoriesList[i].v) !== -1) { //category is selected
								$scope.profileSelectedCategories[categoriesList[i].v] = true;
								$scope.assembleCategories(categoriesList[i].v, function () {
									categoriesList = angular.copy($scope.categoriesList);
									i++;
									if (i === categoriesList.length) {
										$timeout(function () {
											$scope.loading = false;
										}, 500);
										return cb();
									}
									else {
										loopAndRebuild(categoriesList, i, cb);
									}
								});
							}
							else {
								i++;
								if (i === categoriesList.length) {
									$timeout(function () {
										$scope.loading = false;
									}, 500);
									return cb();
								}
								else {
									loopAndRebuild(categoriesList, i, cb);
								}
							}
						}
						
					}
					
					function renderFeed(cb) {
						var grabFeed = function ($scope, callback) {
							if ($scope.selectedFeed !== "") {
								readFeedProfile(id_feed, $scope.category, function (profile) {
									buildCategoriesFromProfile(profile, $scope.categoriesList, $scope.category);
									$scope.categoriesList = angular.fromJson(angular.copy(profile.categories));
									return callback();
								});
							}
							else {
								return callback();
							}
						};
						// update categ tree
						if (!cb) {
							cb = function () {
							}
						}
						grabFeed($scope, cb);
					}
					
					$scope.selectAllTrigger = function () {
						if (outerScope.selectAll) {
							outerScope.selectAll = false;
						}
						else {
							for (var cat in $scope.profileSelectedCategories) {
								$scope.profileSelectedCategories[cat] = false;
								$scope.assembleCategories(cat);
							}
							outerScope.selectAll = true;
							$scope.myCategories = [];
						}
					};
					
					$scope.assembleCategories = function (category, callback) {
						//category was previously selected and now unselected, delete listed children
						if (!callback) {
							//tracking source of function call (checkbox click or recursive function
							$scope.loading = true;
						}
						
						if ($scope.profileSelectedCategories[category] === false) {
							for (var cat in $scope.categoriesList) {
								if ($scope.categoriesList[cat].v === category) {
									var parentLabel = $scope.categoriesList[cat].l;
									break;
								}
							}
							var temp, index;
							for (var i = 0; i < $scope.categoriesList.length; i++) {
								temp = $scope.categoriesList[i].l;
								index = 0;
								while (index !== -1) {
									index = temp.lastIndexOf(" /");
									temp = temp.slice(0, index);
									if (temp === parentLabel) {
										if ($scope.profileSelectedCategories[$scope.categoriesList[i].v] === true) {
											$scope.profileSelectedCategories[$scope.categoriesList[i].v] = false;
										}
										$scope.categoriesList.splice(i, 1);
										i--;
										break;
									}
								}
							}
							
							if (callback) {
								return callback();
							}
							else {
								$timeout(function () {
									$scope.loading = false;
								}, 500);
							}
						}
						else {
							//get children of checked category here////
							$scope.category = category;
							
							renderFeed(function () {
								$scope.myCategories = getCategoriesArr($scope.profileSelectedCategories);
								
								if (callback) {
									return callback();
								}
								else {
									$timeout(function () {
										$scope.loading = false;
									}, 500);
								}
							});
						}
					};
					
					$scope.onSubmit = function () {
						outerScope.$parent.displayAlert('success', 'Submit');
						
						// if (!$scope.discount.endDate) {
						// 	$scope.message.danger = true;
						// 	return $scope.message.text = translation.pleaseSelectEndDate[LANG];
						// }
						if (!$scope.discount.startDate) {
							$scope.message.danger = true;
							return $scope.message.text = translation.pleaseSelectStartDate[LANG];
						}
						// if (!$scope.discount.startDate && !$scope.discount.endDate) {
						// 	$scope.message.danger = true;
						// 	return $scope.message.text = translation.pleaseSelectStartDateAndEndDate[LANG];
						// }
						
						$scope.discount.criteria.serial = [];
						$scope.tempo.availableProducts.forEach(function (each) {
							if (each.isSelected) {
								$scope.discount.criteria.serial.push(each.serial);
							}
						});
						
						if (!$scope.discount.salesRep) {
							$scope.discount.salesRep = {};
						}
						$scope.discount.salesRep.salesChannels = booleanObject2array($scope.tempo.salesChannels);
						
						$scope.tempo.markets = fetchMarketsFromSubObjects($scope.tempo.selectedMarketForDisplay);
						var serviceArea = convertIncludedAndExcludedFormToDb($scope.tempo.markets, $scope.tempo.serviceAreaIsInclusive, true);
						serviceArea.province = convertIncludedAndExcludedFormToDb($scope.tempo.serviceArea, $scope.tempo.serviceAreaIsInclusive, false);
						
						var postData = {
							description: $scope.discount.description,
							discount: $scope.discount.discount,
							startDate: $scope.discount.startDate,
							endDate: $scope.discount.endDate,
							status: $scope.discount.status,
							type: $scope.discount.type,
							criteria: {
								category: {},
								serial: $scope.discount.criteria.serial,
								serviceArea,
								salesRep: {
									salesChannels: $scope.discount.salesRep.salesChannels
								}
							}
						};
						if ($scope.discount.config) {
							postData.config = $scope.discount.config;
						}
						// either no service area selected and no sales channel selected, or at least one of each
						if (!postData.criteria.salesRep.salesChannels || postData.criteria.salesRep.salesChannels.length === 0) {
							if (postData.criteria.serviceArea.included.length !== 0) { // markets
								alert("You need to select at least one sales channel with your service area");
								return;
							} // else both not selected
						} else {
							if (postData.criteria.serviceArea.included.length === 0) { // markets
								alert("You need to select at least one market with your sales channel");
								return;
							}
						}
						
						if (!$scope.selectAllCategories && $scope.profileSelectedCategories) {
							postData.criteria.category.include = getCategoriesArr($scope.profileSelectedCategories);
						}
						overlayLoading.show();
						
						var options = {
							"method": "send",
							"routeName": "/knowledgebase/product/discounts",
							"params": {
								"__env": __env
							},
							"data": {
								discount: postData
							}
						};
						if (newData._id) {
							options.method = "put";
							options.routeName = "/knowledgebase/product/discounts/" + newData._id;
							delete options.data.discount._id;
						}
						getSendDataFromServer(outerScope, ngDataApi, options, function (error) {
							overlayLoading.hide();
							if (error) {
								$scope.message.danger = true;
								$scope.message.text = error.message;
							}
							else {
								outerScope.$parent.displayAlert('success', translation.saleAddedSuccessfully[LANG]);
								$modalInstance.close();
								outerScope.listAll();
							}
						});
					};
					
					$scope.changeType = function () {
						if ($scope.discount.type === "volumeDiscount" || $scope.discount.type === "loyaltyDiscount") {
							jQuery("#number-wraper").slideDown();
						} else {
							jQuery("#number-wraper").slideUp();
						}
					};
					
					$scope.closeModal = function () {
						$modalInstance.close();
					};
					
					/**
					 * SERVICE AREA AND MARKETS FUNCTIONS STARTS
					 */
					
					$scope.selectAllSegmentation = function (variable) {
						var selectAllSegmentation = document.getElementById(variable + "SelectAllCb").checked;
						
						for (var i = 0; i < $scope.tempo[variable].length; i++) {
							$scope.tempo[variable][i].isSelected = selectAllSegmentation;
						}
						
						// special for service area and markets : merchant
						if (variable === 'serviceArea') {
							if (selectAllSegmentation) {
								$scope.getMarketForProvince("**ALL**");
							}
						}
						
						if (variable === 'markets') {
							var variableForDisplay = 'selectedMarketForDisplay';
							var variableServiceArea = 'serviceArea';
							
							for (var i = 0; i < $scope.tempo[variableServiceArea].length; i++) {
								if ($scope.tempo[variableServiceArea][i].isSelected) {
									for (var j = 0; j < $scope.tempo[variableForDisplay][$scope.tempo[variableServiceArea][i].code].length; j++) {
										$scope.tempo[variableForDisplay][$scope.tempo[variableServiceArea][i].code][j].isSelected = selectAllSegmentation;
									}
								}
							}
						}
					}; // renamed to selectAllSegmentation since categories use selectAll
					
					$scope.uncheckSelectAll = function (variable) {
						document.getElementById(variable + "SelectAllCb").checked = false;
					};
					
					$scope.analyseAllMarkets = function (arrayOfMarkets) {
						var output = {
							allProvinces: [],
							completeMarkets: {}
						};
						
						if (!arrayOfMarkets) {
							return output;
						}
						
						arrayOfMarkets.forEach(function (each) {
							if (each.parent) { // ignore undefined parents
								if (!output.allProvinces.includes(each.parent)) {
									output.allProvinces.push(each.parent);
								}
								
								// add complete markets
								if (!output.completeMarkets[each.parent]) { // initiate the first one
									output.completeMarkets[each.parent] = [];
								}
								
								output.completeMarkets[each.parent].push(each);
							}
						});
						
						return output;
					};
					
					$scope.getMarketForProvince = function (provinceCode) {
						var condition = {
							provinceCode
						};
						
						var completeMarketsVariable = "completeMarkets";
						var marketsVariable = "markets";
						var selectedMarketsVariable = "selectedMarketForDisplay";
						var marketIsInclusiveVariable = "marketIsInclusive";
						
						// call api and get complete markets
						// if the province code is ALL, loop through each and assign selected segmentation
						// if not get it specifically for this province
						setSegmentationDataSpecificApi(outerScope, $scope, ngDataApi, "/knowledgebase/product/markets", completeMarketsVariable, marketsVariable, condition, function (error) {
							if (provinceCode !== "**ALL**") {
								$scope.tempo[selectedMarketsVariable][provinceCode] = assignSelectedSegmentation($scope.tempo[completeMarketsVariable], $scope.tempo[selectedMarketsVariable][provinceCode], $scope.tempo[marketIsInclusiveVariable], provinceCode);
							} else {
								var apiResultMarkets = $scope.tempo[completeMarketsVariable];
								var allProvincesAnalysis = $scope.analyseAllMarkets(apiResultMarkets);
								
								var allProvinces = allProvincesAnalysis.allProvinces;
								
								allProvinces.forEach(function (oneProvinceOfAll) {
									$scope.tempo[completeMarketsVariable] = allProvincesAnalysis.completeMarkets[oneProvinceOfAll];
									$scope.tempo[selectedMarketsVariable][oneProvinceOfAll] = assignSelectedSegmentation($scope.tempo[completeMarketsVariable], $scope.tempo[selectedMarketsVariable][oneProvinceOfAll], $scope.tempo[marketIsInclusiveVariable], oneProvinceOfAll);
								});
							}
						});
					};
					
					$scope.serviceAreaIsSelected = function (provinceKey) {
						
						var serviceAreaVariable = 'serviceArea';
						var selected = false;
						$scope.tempo[serviceAreaVariable].forEach(function (serviceArea) {
							if (serviceArea.code === provinceKey) {
								if (serviceArea.isSelected) {
									selected = true;
								}
							}
						});
						
						return selected;
					};
					
					$scope.cleanMarketOnUncheck = function (serviceArea) {
						
						var selectedMarketForDisplayVariable = 'selectedMarketForDisplay';
						var marketsVariable = 'markets';
						
						if (!serviceArea.isSelected) {
							var marketForDisplay = $scope.tempo[selectedMarketForDisplayVariable][serviceArea.code];
							marketForDisplay.forEach(function (market) {
								market.isSelected = false;
							});
						}
					};
					
					/**
					 * SERVICE AREA AND MARKETS FUNCTIONS ENDS
					 */
					
					//// on init
					buildTree();
				}
			});
		};
		
		$scope.changeStatus = function (data) {
			var status = (data.status === 'active') ? 'inactive' : 'active';
			var opts;
			opts = {
				"method": "put",
				"routeName": "/knowledgebase/product/discounts/" + data._id + "/status",
				"params": {
					'status': status,
					'code': $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			
			overlayLoading.show();
			getSendDataFromServer($scope, ngDataApi, opts, function (error) {
				overlayLoading.hide();
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.$parent.displayAlert('success', 'Status Changed Successfully.');
					$scope.listAll();
				}
			});
			
		};
		
		$scope.listAll = function () {
			var opts = {
				"routeName": "/knowledgebase/product/discounts",
				"method": "get",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			
			discountModuleDevSrv.getEntriesFromApi($scope, opts, function (error, response) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					discountModuleDevSrv.printGrid($scope, response.records);
				}
			});
			
		};
		
		$scope.deleteDiscount = function (data) {
			var config = {
				"method": "del",
				"routeName": "/knowledgebase/product/discounts/" + data._id,
				"params": {
					'code': $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			
			getSendDataFromServer($scope, ngDataApi, config, function (error) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.$parent.displayAlert('success', 'Discount Deleted Successfully.');
					$scope.listAll();
				}
			});
			
		};
		
		// on init, call list
		$scope.listAll();
		
		injectFiles.injectCss(ModuleDevMpLocation + "/merchants.css");
	}]);
