"use strict";
var merchantsApp = soajsApp.components;

merchantsApp.controller('bundlesAddModuleDevCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$cookies', "$routeParams", "bundlesModuleDevSrv", "cbInputHelper", "$localStorage", "popUpConfiguration",
	function ($scope, $modal, ngDataApi, injectFiles, $timeout, $cookies, $routeParams, bundlesModuleDevSrv, cbInputHelper, $localStorage, popUpConfiguration) {
		$scope.$parent.isUserLoggedIn();
		$scope.ModuleDevMpLocation = ModuleDevMpLocation;
		if (!$scope.$parent.currentSelectedEnvironment) {
			$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
		}
		$scope.isBundle = true;
		$scope.access = {};
		constructModulePermissions($scope, $scope.access, kbBundlesModuleDevConfig.permissions);
		$scope.response = {};
		
		$scope.removePostSales = function (index) {
			$scope.formData.merchantMeta.pricing.postSale.splice(index, 1);
		};
		
		$scope.addPostSales = function () {
			if (!$scope.formData.merchantMeta.pricing.postSale) {
				$scope.formData.merchantMeta.pricing.postSale = [];
			}
			var obj = {
				"name": "",
				"value": 0,
				"label": {
					"en": "",
					"fr": ""
				}
			};
			$scope.formData.merchantMeta.pricing.postSale.push(obj);
			
		};
		
		function confirmStandardBundle(formData) {
			var languages = ["en", "fr"];
			var imagesBody = {};
			var videoBody = {};
			var resourcesBody = {};
			var shortDescriptionsBody = {};
			var longDescriptionsBody = {};
			
			for (var i = 0; i < languages.length; i++) {
				imagesBody[languages[i]] = [
					{url: '', altTag: ''}
				];
				videoBody[languages[i]] = [
					{url: ''}
				];
				resourcesBody[languages[i]] = [
					{url: ''}
				];
				shortDescriptionsBody[languages[i]] = '';
				longDescriptionsBody[languages[i]] = '';
			}
			
			if (!formData) { // before add new product
				var result = {
					coreData: {
						condition: "New",
						productType: "bundle",
						cancellationPolicy: {
							description: {}
						}
					},
					productConfiguration: [
						{
							"name": "contractLength",
							"label": {
								"en": "Contract Length",
								"fr": "Contract Length"
							},
							"required": false,
							"type": "select",
							"values": []
						}
					],
					productData: {
						media: {
							images: imagesBody,
							videos: videoBody,
							resources: resourcesBody
						},
						title: {},
						shortDescription: shortDescriptionsBody,
						longDescription: longDescriptionsBody,
						crossSellPath: [],
						upsellPath: []
					},
					attributes: {
						category: [
							{"value": ""}
						],
						classification: []
					},
					bundleData: {
						items: []
					},
					rules: {
						prerequisiteProduct: [],
						incompatibility: {
							products: []
						}
					},
					merchantMeta: {
						pricing: {
							rules: [],
							penaltyFees: {
								cancellation: [],
								pause: [],
								downgrade: []
							}
						}
					},
					fulfillmentRules: {
						content: [],
						fulfillmentPlan: {
							providers: []
						}
					},
					legal: {
						content: {}
					}
				};
				return result;
			}
			else { // beyond get product
				if (!formData.coreData) {
					formData.coreData = {};
				}
				
				if (!formData.coreData.cancellationPolicy) {
					formData.coreData.cancellationPolicy = {};
				}
				
				if (!formData.coreData.cancellationPolicy.description) {
					formData.coreData.cancellationPolicy.description = {};
				}
				
				if (!formData.productData) {
					formData.productData = {};
				}
				
				if (!formData.productData.media) {
					formData.productData.media = {};
				}
				
				if (!formData.productData.media.images) {
					formData.productData.media.images = imagesBody;
				}
				
				if (!formData.productData.media.videos) {
					formData.productData.media.videos = videoBody;
				}
				
				if (!formData.productData.media.resources) {
					formData.productData.media.resources = resourcesBody;
				}
				
				if (!formData.productData.title) {
					formData.productData.title = {};
				}
				
				if (!formData.productData.shortDescription) {
					formData.productData.shortDescription = shortDescriptionsBody;
				}
				
				if (!formData.productData.longDescription) {
					formData.productData.longDescription = longDescriptionsBody;
				}
				
				if (!formData.productData.crossSellPath) {
					formData.productData.crossSellPath = [];
				}
				
				if (!formData.productData.upsellPath) {
					formData.productData.upsellPath = [];
				}
				
				if (!formData.attributes) {
					formData.attributes = {};
				}
				
				if (!formData.attributes.category) {
					formData.attributes.category = [];
				}
				
				if (!formData.attributes.classification) {
					formData.attributes.classification = [];
				}
				
				if (!formData.rules) {
					formData.rules = {};
				}
				
				if (!formData.rules.prerequisiteProduct) {
					formData.rules.prerequisiteProduct = [];
				}
				
				if (!formData.rules.incompatibility) {
					formData.rules.incompatibility = {};
				}
				
				if (!formData.rules.incompatibility.products) {
					formData.rules.incompatibility.products = [];
				}
				
				if (!formData.merchantMeta) {
					formData.merchantMeta = {};
				}
				
				if (!formData.merchantMeta.pricing) {
					formData.merchantMeta.pricing = {};
				}
				
				if (!formData.merchantMeta.pricing.rules) {
					formData.merchantMeta.pricing.rules = [];
				}
				
				if (!formData.merchantMeta.pricing.penaltyFees) {
					formData.merchantMeta.pricing.penaltyFees = {};
				}
				
				if (!formData.merchantMeta.pricing.penaltyFees.cancellation) {
					formData.merchantMeta.pricing.penaltyFees.cancellation = [];
				}
				
				if (!formData.merchantMeta.pricing.penaltyFees.pause) {
					formData.merchantMeta.pricing.penaltyFees.pause = [];
				}
				
				if (!formData.merchantMeta.pricing.penaltyFees.downgrade) {
					formData.merchantMeta.pricing.penaltyFees.downgrade = [];
				}
				
				if (!formData.fulfillmentRules) {
					formData.fulfillmentRules = {};
				}
				
				if (!formData.fulfillmentRules.content) {
					formData.fulfillmentRules.content = [];
				}
				
				if (!formData.fulfillmentRules.fulfillmentPlan) {
					formData.fulfillmentRules.fulfillmentPlan = {};
				}
				
				if (!formData.fulfillmentRules.fulfillmentPlan.providers) {
					formData.fulfillmentRules.fulfillmentPlan.providers = [];
				}
				
				if (!formData.legal) {
					formData.legal = {};
				}
				
				if (!formData.legal.content) {
					formData.legal.content = {};
				}
				
				return formData;
			}
			
		}
		
		function setFeedProfile(category, cb) {
			var params = {};
			if (category && category !== "") {
				params["category"] = category;
			}
			
			getSendDataFromServer($scope, ngDataApi, {
				"routeName": "/kbprofile/owner/feed/active/profiles",
				"method": "get",
				"params": params
			}, function (error, resp) {
				if (error) {
					console.log(error);
					cb();
				}
				else {
					cb(resp);
				}
			});
		}
		
		setFeedProfile('main', function (profile) {
			$scope.response.profile = profile;
			var response = $scope.response;
			
			bundlesModuleDevSrv.buildCategoriesFromProfile($scope, $localStorage, response.profile, function (error, categories) {
				$scope.processing = false;
				response.profile.categories = categories;
				$scope.response = response;
				$scope.treats = (response.profile.treats) ? true : false;
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				} else {
					
				}
			});
		});
		
		if ($routeParams && $routeParams.id) {
			$scope.id = $routeParams.id;
			if (window.location.href.includes("copy")) {
				$scope.mode = 'copy';
			} else {
				$scope.mode = 'edit';
			}
		} else {
			$scope.mode = 'add';
		}
		
		// init start
		var languages = ["en", "fr"];
		
		$scope.tempo = {
			classification: "",
			brand: "",
			variation: [],
			isNotPhysicalGoods: false, // sla not required
			browsedImages: {
				en: {
					data: [],
					message: '',
					applicable: false, // pictures browsed
					valid: true // status of the browsed pictures
				},
				fr: {
					data: [],
					message: '',
					applicable: false,
					valid: true
				}
			},
			availableProducts: [], // will be set in $scope.setAllAvailableProducts
			availableProductsAndBundles: [], // will be set in $scope.setAllAvailableProductsAndBundles
			availableConditions: {
				SystemKeysX: []
			}
		};
		
		$scope.setAllAvailableProducts = function () {
			var opts = {
				"method": "get",
				"routeName": "/knowledgebase/product/availableProducts",
				"params": {
					// productsOrBundles
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				"data": {}
			};
			
			bundlesModuleDevSrv.getEntriesFromApi($scope, opts, function (error, data) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
					extraUtils.refreshThumbnails($scope);
				}
				else {
					var products = data.records;
					var availableProducts = [];
					
					for (var i = 0; i < products.length; i++) {
						var temp = {
							serial: products[i].serial,
							udac: products[i].coreData.udac,
							title: products[i].productData.title.en
						};
						availableProducts.push(temp);
					}
					
					$scope.tempo.availableProducts = availableProducts;
				}
			});
		};
		
		$scope.setAllAvailableProductsAndBundles = function () {
			var opts = {
				"method": "get",
				"routeName": "/knowledgebase/product/availableProducts",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase(),
					"productsOrBundles": "both"
				}
			};
			
			bundlesModuleDevSrv.getEntriesFromApi($scope, opts, function (error, data) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
					extraUtils.refreshThumbnails($scope);
				}
				else {
					var products = data.records;
					var availableProductsAndBundles = [];
					
					for (var i = 0; i < products.length; i++) {
						var temp = {
							serial: products[i].serial,
							udac: products[i].coreData.udac,
							title: products[i].productData.title.en
						};
						availableProductsAndBundles.push(temp);
					}
					
					$scope.tempo.availableProductsAndBundles = availableProductsAndBundles;
				}
			});
		};
		
		$scope.setAllAvailableProductsAndBundles();
		$scope.setAllAvailableProducts();
		
		$scope.tempoImageFile = [];
		for (var i = 0; i < languages.length; i++) {
			$scope.tempoImageFile[languages] = {
				url: "",
				type: ""
			};
		}
		
		var tempoProduct = {
			serial: "",
			udac: ""
		};
		
		var tempoProduct2 = {
			serial: "",
			udac: "",
			quantity: 1
		};
		
		$scope.addPrerequisiteProduct = function () {
			var tempo = angular.copy(tempoProduct);
			$scope.formData.rules.prerequisiteProduct.push(tempo);
		};
		$scope.addIncompatibilityProduct = function () {
			var tempo = angular.copy(tempoProduct);
			$scope.formData.rules.incompatibility.products.push(tempo);
		};
		$scope.addCrossSellPath = function () {
			var tempo = angular.copy(tempoProduct);
			$scope.formData.productData.crossSellPath.push(tempo);
		};
		$scope.addUpsellPath = function () {
			var tempo = angular.copy(tempoProduct);
			$scope.formData.productData.upsellPath.push(tempo);
		};
		$scope.addBundleDataItem = function () {
			var tempo = angular.copy(tempoProduct2);
			$scope.formData.bundleData.items.push(tempo);
		};
		
		$scope.removeBundleDataItem = function (index) {
			$scope.formData.bundleData.items.splice(index, 1);
		};
		$scope.removePrerequisiteProduct = function (index) {
			$scope.formData.rules.prerequisiteProduct.splice(index, 1);
		};
		$scope.removeIncompatibilityProduct = function (index) {
			$scope.formData.rules.incompatibility.products.splice(index, 1);
		};
		$scope.removeCrossSellPath = function (index) {
			$scope.formData.productData.crossSellPath.splice(index, 1);
		};
		$scope.removeUpsellPath = function (index) {
			$scope.formData.productData.upsellPath.splice(index, 1);
		};
		
		// fulfillment plan - providers
		var tempoProvider = {
			name: "",
			config: {},
			tempo: [] // array of configurations => which will be used to set config object
		};
		
		var tempoProviderConfig = {
			key: "",
			value: ""
		};
		
		$scope.addNewProvider = function () {
			var tempo = angular.copy(tempoProvider);
			$scope.formData.fulfillmentRules.fulfillmentPlan.providers.push(tempo);
		};
		
		$scope.removeProvider = function (index) {
			$scope.formData.fulfillmentRules.fulfillmentPlan.providers.splice(index, 1);
		};
		
		$scope.addNewProviderConfig = function (index) {
			var tempo = angular.copy(tempoProviderConfig);
			$scope.formData.fulfillmentRules.fulfillmentPlan.providers[index].tempo.push(tempo);
		};
		
		$scope.removeProviderConfig = function (parentIndex, index) {
			$scope.formData.fulfillmentRules.fulfillmentPlan.providers[parentIndex].tempo.splice(index, 1);
		};
		
		// pricing rules
		var tempoCondition = {
			"criteria": "",
			"value": ""
		};
		var tempoOneTime = {
			"name": {
				"en": "",
				"fr": ""
			},
			"value": "",
			"savings": {}
		};
		var tempoRegistration = {
			"name": {
				"en": "",
				"fr": ""
			},
			"value": "",
			"quantity": ""
		};
		var tempoRecurring = {
			"name": {
				"en": "",
				"fr": ""
			},
			"value": "",
			"frequency": "",
			"savings": {}
		};
		
		var tempoContract = {
			"unit": "",
			"quantity": 0
		};
		
		$scope.addOneTimeFee = function (index) {
			var tempo = angular.copy(tempoOneTime);
			$scope.formData.merchantMeta.pricing.rules[index].onetime = tempo;
		};
		
		$scope.addRegistrationFee = function (index) {
			if (!$scope.formData.merchantMeta.pricing.rules[index].registration) {
				$scope.formData.merchantMeta.pricing.rules[index].registration = [];
			}
			var tempo = angular.copy(tempoRegistration);
			$scope.formData.merchantMeta.pricing.rules[index].registration.push(tempo);
		};
		
		//$scope.formData.merchantMeta.pricing.rules.recurring = [];
		$scope.addRecurringFee = function (index) {
			if (!$scope.formData.merchantMeta.pricing.rules[index].recurring) {
				$scope.formData.merchantMeta.pricing.rules[index].recurring = [];
			}
			var tempo = angular.copy(tempoRecurring);
			$scope.formData.merchantMeta.pricing.rules[index].recurring.push(tempo);
		};
		
		$scope.addContractLength = function (index) {
			if (!$scope.formData.merchantMeta.pricing.rules[index].term) {
				$scope.formData.merchantMeta.pricing.rules[index].term = {};
			}
			else {
				var tempo = angular.copy(tempoContract);
				$scope.formData.merchantMeta.pricing.rules[index].term = tempo;
			}
		};
		
		$scope.addCondition = function (index) {
			if (!$scope.formData.merchantMeta.pricing.rules[index].conditions) {
				$scope.formData.merchantMeta.pricing.rules[index].conditions = [];
			}
			var tempo = angular.copy(tempoCondition);
			$scope.formData.merchantMeta.pricing.rules[index].conditions.push(tempo);
		};
		
		$scope.addRule = function () {
			var defaultType = 'regular';
			if ($scope.formData.coreData.priceSource === 'external') {
				defaultType = 'adjustment';
			}
			$scope.formData.merchantMeta.pricing.rules.push({
				id: new Date().getTime().toString(),
				ruleType: defaultType
			});
		};
		
		$scope.removeRule = function (index) {
			$scope.formData.merchantMeta.pricing.rules.splice(index, 1);
		};
		
		$scope.removeOnetime = function (index) {
			delete $scope.formData.merchantMeta.pricing.rules[index].onetime;
		};
		
		$scope.removeRegistration = function (parentIndex, index) {
			$scope.formData.merchantMeta.pricing.rules[parentIndex].registration.splice(index, 1);
		};
		
		$scope.removeRecurring = function (parentIndex, index) {
			$scope.formData.merchantMeta.pricing.rules[parentIndex].recurring.splice(index, 1);
		};
		
		$scope.removeContractTerm = function (index) {
			delete $scope.formData.merchantMeta.pricing.rules[index].term;
		};
		
		$scope.removeCondition = function (parentIndex, index) {
			$scope.formData.merchantMeta.pricing.rules[parentIndex].conditions.splice(index, 1);
		};
		
		$scope.addCancellation = function () {
			$scope.formData.merchantMeta.pricing.penaltyFees.cancellation.push({});
		};
		
		$scope.removeCancellation = function (index) {
			$scope.formData.merchantMeta.pricing.penaltyFees.cancellation.splice(index, 1);
		};
		
		$scope.addPause = function () {
			$scope.formData.merchantMeta.pricing.penaltyFees.pause.push({});
		};
		
		$scope.removePause = function (index) {
			$scope.formData.merchantMeta.pricing.penaltyFees.pause.splice(index, 1);
		};
		
		$scope.addDowngrade = function () {
			$scope.formData.merchantMeta.pricing.penaltyFees.downgrade.push({});
		};
		
		$scope.removeDowngrade = function (index) {
			$scope.formData.merchantMeta.pricing.penaltyFees.downgrade.splice(index, 1);
		};
		
		$scope.pureEvergreenOnClick = function (index) {
			var isSelected = document.getElementById("pureEvergreenFlag" + index).checked;
			if (isSelected) { // on select clear others
				$scope.formData.merchantMeta.pricing.rules.forEach(function (rule, currentIndex) {
					if (index !== currentIndex) {
						rule.pureEvergreen = false;
					}
				});
			}
			// on deselect do nothing!
		};
		
		// images
		var imagesValidationConstant = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
		$scope.imageUrlUpdate = function (lang, index) {
			var imageUrl = $scope.formData.productData.media.images[lang][index].url;
			if (imagesValidationConstant.test(imageUrl)) { // valid
				$scope.tempoImages[lang][index].url = $scope.formData.productData.media.images[lang][index].url;
			} else {
				$scope.tempoImages[lang][index].url = '';
			}
		};
		
		// old stuff
		
		$scope.saveResources = function ($scope, productsSrv, cb) {
			var myResources = productsSrv.extractResourcesFromPostedData($scope);
			if (myResources.length !== 0) {
				productsSrv.uploadFile($scope, myResources, "/knowledgebase/products/addResource", function (error, response) {
					if (error) {
						var errorString;
						if (Array.isArray(error)) {
							errorString = error.join(", ");
						}
						else {
							errorString = error.message;
						}
						overlayLoading.hide();
						return $scope.$parent.displayAlert('danger', errorString);
					}
					else {
						var res = [];
						for (var i = 0; i < response.length; i++) {
							res.push({
								"url": response[i].original,
								"resource": response[i].resource,
								"type": response[i].type,
								"language": response[i].language
							});
						}
						for (var i = 0; i < res.length; i++) {
							var current = res[i];
							var obj = {
								url: current.url,
								type: current.type
							};
							$scope.formData.productData.media.resources[current.language].push(obj);
						}
						
						var language = ["en", "fr"];
						
						for (var i = 0; i < language.length; i++) {
							document.getElementById("browseimage_" + language[i]).value = "";
						}
						return cb();
					}
				});
			}
			else {
				return cb();
			}
		};
		
		$scope.saveDraft = function () {
			if (!$scope.formData.productData.title['en']) {
				return $scope.$parent.displayAlert('danger', 'Product Title is mandatory to Save as Draft');
			}
			$scope.saveBundle('draft');
		};
		
		$scope.save = function (draft) {
			if ($scope.response.profile.languages) {
				var selected = {};
				for (var lang in $scope.response.profile.languages) {
					if ($scope.response.profile.languages[lang].selected) {
						selected = $scope.response.profile.languages[lang];
					}
				}
				$scope.formData.sLang = selected;
			}
			var successMsg;
			
			overlayLoading.show();
			bundlesModuleDevSrv.bundleDataFormtoDb($scope.formData, $scope.tempo, function (bundle) {
				var postData = {
					"bundle": bundle
				};
				var opts = {
					"method": "send",
					"params": {
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					},
					"data": postData
				};
				
				if (draft) {
					opts.method = "send";
					opts.routeName = "/knowledgebase/product/bundlesDraft";
					
					if ($scope.mode === "edit") {
						opts.routeName = "/knowledgebase/product/bundlesDraft?id=" + $scope.id;
						successMsg = translation.productUpdatedSuccessfully[LANG];
					}
					else { // add or copy
						successMsg = translation.productAddedSuccessfully[LANG];
					}
				}
				else {
					if ($scope.mode === "edit") {
						opts.method = "put";
						successMsg = translation.productUpdatedSuccessfully[LANG];
						opts.routeName = "/knowledgebase/product/bundles/" + $scope.id;
					}
					else { // add or copy
						successMsg = translation.productAddedSuccessfully[LANG];
						opts.routeName = "/knowledgebase/product/bundles";
					}
				}
				
				
				bundlesModuleDevSrv.getEntriesFromApi($scope, opts, function (error) {
					overlayLoading.hide();
					if (error) {
						$scope.$parent.displayAlert('danger', error.message);
						extraUtils.refreshThumbnails($scope);
					}
					else {
						$scope.$parent.displayAlert('success', successMsg);
						$scope.formData = {};
						$scope.$parent.go("/discountEngine/bundles");
					}
				});
				
			});
		};
		
		$scope.removeBrowsed = function (language) {
			$scope.tempo.browsedImages[language].valid = true;
			$scope.tempo.browsedImages[language].applicable = false;
			$scope.tempo.browsedImages[language].data = [];
			$scope.tempo.browsedImages[language].message = '';
			angular.element("input[id='browseimage_" + language + "']").val(null);
		};
		
		$scope.saveBundle = function (draft) {
			
			var langs = Object.keys($scope.tempo.browsedImages);
			var invalidBrowsedImages = false;
			langs.forEach(function (lang) {
				if ($scope.tempo.browsedImages[lang].applicable && !$scope.tempo.browsedImages[lang].valid) {
					invalidBrowsedImages = true;
				}
			});
			
			if (invalidBrowsedImages) {
				$scope.$parent.displayAlert('danger', 'Invalid images browsed');
				return;
			}
			
			overlayLoading.show();
			var myFiles = bundlesModuleDevSrv.extractFilesFromPostedData($scope);
			
			if (myFiles.length !== 0) {
				bundlesModuleDevSrv.uploadFile($scope, myFiles, "/knowledgebase/products/addImage", function (error, response) {
					if (error) {
						var errorString;
						if (Array.isArray(error)) {
							errorString = error.join(", ");
						}
						else {
							errorString = error.message;
						}
						overlayLoading.hide();
						return $scope.$parent.displayAlert('danger', errorString);
					}
					else {
						var img = [];
						for (var i = 0; i < response.length; i++) {
							img.push({
								"url": response[i].original,
								"resource": response[i].resource,
								"type": response[i].type,
								"language": response[i].language
							});
						}
						for (var i = 0; i < img.length; i++) {
							var current = img[i];
							var obj = {
								url: current.url,
								type: current.type
							};
							$scope.formData.productData.media.images[current.language].push(obj);
						}
						
						var language = ["en", "fr"];
						
						for (var i = 0; i < language.length; i++) {
							document.getElementById("browseimage_" + language[i]).value = "";
						}
						
						$scope.save(draft);
					}
				});
			}
			else {
				$scope.save(draft);
			}
			
		};
		
		$scope.AddNewImage = function (language) {
			$scope.formData.productData.media.images[language].push({'url': '', 'type': ''});
			$scope.tempoImages[language].push({'url': '', 'type': ''});
		};
		
		$scope.removeImage = function (language, index) {
			$scope.formData.productData.media.images[language].splice(index, 1);
			$scope.tempoImages[language].splice(index, 1);
		};
		
		$scope.AddNewResource = function (language) {
			$scope.formData.productData.media.resources[language].push({'url': '', 'type': ''});
		};
		
		$scope.removeResource = function (language, index) {
			$scope.formData.productData.media.resources[language].splice(index, 1);
		};
		
		$scope.AddNewVideo = function (language) {
			$scope.formData.productData.media.videos[language].push({'url': ''});
		};
		
		$scope.removeVideo = function (language, index) {
			$scope.formData.productData.media.videos[language].splice(index, 1);
		};
		
		$scope.checkDisabled = function (disabledSelector, data) {
			for (var i = 0; i < disabledSelector.length; i++) {
				if (data === disabledSelector[i]) {
					return true;
				}
			}
			return false;
		};
		
		$scope.initDisabled = function (disabledSelector, data) {
			for (var i = 0; i < disabledSelector.length; i++) {
				if (data === disabledSelector[i]) {
					return;
				}
			}
			if (data) {
				disabledSelector.push(data);
			}
		};
		
		$scope.addNewVariation = function () {
			$scope.tempo.variation.push({'type': '', 'value': ''});
		};
		
		$scope.removeAvariation = function (index) {
			$scope.tempo.variation.splice(index, 1);
			$scope.disabledSelector.splice(index, 1); // todo
		};
		
		$scope.beforeRenderStartDate = function ($view, $dates, $leftDate, $upDate, $rightDate, $index) {
			if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.endDate) {
				var activeDate = moment($scope.data[$index].sale.endDate);
				for (var i = 0; i < $dates.length; i++) {
					if ($dates[i].localDateValue() >= activeDate.valueOf()) {
						$dates[i].selectable = false;
					}
				}
			}
			else if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.startDate) {
				$scope.$broadcast('RenderEndDate');
			}
			var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
			for (var i = 0; i < $dates.length; i++) {
				if ($dates[i].localDateValue() < minDate.valueOf()) {
					$dates[i].selectable = false;
				}
			}
		};
		
		$scope.beforeRenderEndDate = function ($view, $dates, $leftDate, $upDate, $rightDate, $index) {
			if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.startDate) {
				var activeDate = moment($scope.data[$index].sale.startDate).subtract(1, $view).add(1, 'minute');
				for (var i = 0; i < $dates.length; i++) {
					if ($dates[i].localDateValue() <= activeDate.valueOf()) {
						$dates[i].selectable = false;
					}
				}
			}
			else if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.endDate) {
				$scope.$broadcast('RenderStartDate');
			}
			var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
			for (var i = 0; i < $dates.length; i++) {
				if ($dates[i].localDateValue() <= minDate.valueOf()) {
					$dates[i].selectable = false;
				}
			}
		};
		
		$scope.addBundleConf = function () {
			// tempo.customConfAdd will have a sub object having the name of the bundle conf obj and the body of the additionalFields
			// the root configuration will always read from this object, and on save this will be added to the bundle configuration
			$scope.tempo.customConfAdd = {};
			$scope.tempo.customConfAdd['@temponew@'] = [];
			
			popUpConfiguration.addCustomConfiguration($scope, 'productConfiguration', '@root@', null);
		};
		
		$scope.editProdConfiguration = function (index) {
			$scope.tempo.customConfAdd = {};
			if ($scope.formData.productConfiguration[index].additionalFields) {
				$scope.tempo.customConfAdd[$scope.formData.productConfiguration[index].name] = angular.copy($scope.formData.productConfiguration[index].additionalFields);
			} else {
				$scope.tempo.customConfAdd[$scope.formData.productConfiguration[index].name] = [];
			}
			
			popUpConfiguration.addCustomConfiguration($scope, 'productConfiguration', '@root@', null, index);
		};
		
		$scope.removeProdConfiguration = function (index) {
			$scope.formData.productConfiguration.splice(index, 1);
			$scope.refreshAvailableConditions();
		};
		
		// refresh available conditions whenever the product configutaion is updated // edit, add, delete & on load
		$scope.refreshAvailableConditions = function () {
			var productConfs = $scope.formData.productConfiguration;
			
			$scope.tempo.availableConditions = {
				SystemKeysX: []
			};
			
			productConfs.forEach(function (eachConf) {
				if (eachConf.configureCondition) {
					$scope.tempo.availableConditions.SystemKeysX.push(eachConf.name);
					if (eachConf.type === 'select') {
						$scope.tempo.availableConditions[eachConf.name] = [];
						eachConf.values.forEach(function (eachValue) {
							$scope.tempo.availableConditions[eachConf.name].push(eachValue.v);
						});
					} else if (eachConf.type === 'boolean') {
						$scope.tempo.availableConditions[eachConf.name] = [true, false];
					}
				}
			});
		};
		
		function validateImageDimensions(file, callback) {
			var reader = new FileReader();
			reader.addEventListener("load", function () {
				var image = new Image();
				image.addEventListener("load", function () {
					var maximumWidth = 2000;
					var maximumHeight = 2000;
					if (image.width > maximumWidth || image.height > maximumHeight) {
						return callback('Invalid file chosen [' + file.name + '] (' + image.width + 'x' + image.height + '). Maximum dimensions accepted (' + maximumWidth + 'x' + maximumHeight + ')');
					} else {
						return callback();
					}
					
				});
				image.src = reader.result;
			});
			reader.readAsDataURL(file);
		}
		
		$scope.validateImages = function (language) {
			setTimeout(function () { // on change formData.media wont be filled right away
				var current = angular.element("input[id='browseimage_" + language + "']");
				
				if (current['0'].value === '') {
					$scope.tempo.browsedImages[language].applicable = false;
					$scope.tempo.browsedImages[language].data = [];
					$scope.tempo.browsedImages[language].message = '';
					$scope.tempo.browsedImages[language].valid = true;
					
					$scope.$apply();
					return;
				}
				
				$scope.tempo.browsedImages[language].applicable = true;
				
				$scope.$apply();
				
				$scope.tempo.browsedImages[language].data = [];
				
				if ($scope.formData.media && $scope.formData.media.upload) {
					if ($scope.formData.media.upload.images) {
						if ($scope.formData.media.upload.images[language]) {
							for (var i in $scope.formData.media.upload.images[language]) {
								if (typeof $scope.formData.media.upload.images[language][i] === 'object') {
									var obj = $scope.formData.media.upload.images[language][i];
									$scope.tempo.browsedImages[language].data.push(obj);
								}
							}
						}
					}
				}
				
				var images = $scope.tempo.browsedImages[language].data;
				
				async.each(images, function (image, callback) {
					var size = image.size;
					var maxSize = 32 * 1000 * 1000;
					if (size > maxSize) {
						callback('Invalid file chosen [' + image.name + '] (' + size + ' bytes). Maximum size allowed is ' + maxSize + ' bytes');
					} else if (!((/\.(png|jpeg|jpg)$/i).test(image.name))) {
						callback('Invalid file chosen [' + image.name + ']. Wrong file format, accepted are png, jpeg or jpg');
					} else {
						validateImageDimensions(image, callback);
					}
					
				}, function (err) {
					if (err) {
						// one of the iterations made an error
						$scope.tempo.browsedImages[language].message = err;
						$scope.tempo.browsedImages[language].valid = false;
					} else {
						$scope.tempo.browsedImages[language].message = 'File(s) browsed respect the dimensions requested';
						$scope.tempo.browsedImages[language].valid = true;
					}
					$scope.$apply();
				});
				
			}, 300);
		};
		
		// init end
		$scope.viewEntry = function () {
			
			var opts = {
				"routeName": "/knowledgebase/product/bundles/" + $routeParams.id,
				"method": "get",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			
			getSendDataFromServer($scope, ngDataApi, opts, function (error, response) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				if (response.bundle && response.bundle.bundleConfiguration) {
					if (!response.bundle.productConfiguration) {
						response.bundle.productConfiguration = response.bundle.bundleConfiguration;
					}
					delete response.bundle.bundleConfiguration;
				}
				$scope.saveDraftActive = false;
				if (response.bundle.status === 'draft') {
					$scope.saveDraftActive = true;
				}
				
				$scope.formData = response.bundle;
				$scope.formData = confirmStandardBundle($scope.formData);
				$scope.formData = bundlesModuleDevSrv.bundleDataDbToForm($scope.formData);
				
				$scope.refreshAvailableConditions();
				
				$scope.tempoImages = angular.copy($scope.formData.productData.media.images);
				
				$scope.formData.serial = '';
				
				if ($scope.mode === "copy") {
					$scope.formData.coreData.udac = "";
					delete $scope.formData._id;
					$scope.saveDraftActive = true;
				}
			});
		};
		
		if ($scope.mode === 'edit' || $scope.mode === "copy") {
			$scope.viewEntry();
		}
		else { // add new
			$scope.saveDraftActive = true;
			$scope.formData = confirmStandardBundle($scope.formData);
			$scope.tempoImages = angular.copy($scope.formData.productData.media.images);
		}
		
		
		$scope.beforeRenderStartDate = function ($view, $dates, $leftDate, $upDate, $rightDate) {
			if ($scope.formData && $scope.formData.coreData) {
				if ($scope.formData.coreData.endDate) {
					var activeDate = moment($scope.formData.coreData.endDate);
					for (var i = 0; i < $dates.length; i++) {
						if ($dates[i].localDateValue() >= activeDate.valueOf()) {
							$dates[i].selectable = false;
						}
					}
				}
				else if ($scope.formData.coreData.startDate) {
					$scope.$broadcast('RenderEndDate');
				}
			}
			var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
			for (var i = 0; i < $dates.length; i++) {
				if ($dates[i].localDateValue() < minDate.valueOf()) {
					$dates[i].selectable = false;
				}
			}
		};
		
		$scope.beforeRenderEndDate = function ($view, $dates, $leftDate, $upDate, $rightDate) {
			if ($scope.formData && $scope.formData.coreData) {
				if ($scope.formData.coreData.startDate) {
					var activeDate = moment($scope.formData.coreData.startDate).subtract(1, $view).add(1, 'minute');
					for (var i = 0; i < $dates.length; i++) {
						if ($dates[i].localDateValue() <= activeDate.valueOf()) {
							$dates[i].selectable = false;
						}
					}
				}
				else if ($scope.formData.coreData.endDate) {
					$scope.$broadcast('RenderStartDate');
				}
			}
			
			var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
			for (var i = 0; i < $dates.length; i++) {
				if ($dates[i].localDateValue() <= minDate.valueOf()) {
					$dates[i].selectable = false;
				}
			}
		};
		
		$scope.toTimeZone = function (time, zone) {
			var format = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]';
			return moment(moment(time), format).tz(zone).format(format);
		};
		
		$scope.addInput = function () {
			cbInputHelper.addInput($scope);
		};
		
		$scope.updateInput = function (type, fieldInfo, index) {
			cbInputHelper.editInput($scope, type, fieldInfo, index);
		};
		
		$scope.removeInput = function (fieldName) {
			cbInputHelper.removeInput($scope, fieldName);
		};
		
		$scope.updateContentOrder = function () {
			overlayLoading.show();
			$scope.formData.fulfillmentRules.content.sort(function (a, b) {
				return parseFloat(a.order) - parseFloat(b.order);
			});
			setTimeout(function () {
				overlayLoading.hide();
			}, 100);
		};
		
		injectFiles.injectCss(ModuleDevMpLocation + "/merchants.css");
	}]);
