"use strict";
var shCartDevApp = soajsApp.components;

shCartDevApp.controller('shoppingCartListModuleDevCtrl', ['$scope', '$timeout', '$modal', 'ngDataApi', '$cookies', 'injectFiles', function ($scope, $timeout, $modal, ngDataApi, $cookies, injectFiles) {
	$scope.$parent.isUserLoggedIn();
	
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, shoppingCartModuleDevConfig.permissions);
	$scope.startLimit = 0;
	$scope.totalCount = 1000;
	$scope.endLimit = shoppingCartModuleDevConfig.apiEndLimit;
	$scope.increment = shoppingCartModuleDevConfig.apiEndLimit;
	$scope.showNext = true;
	
	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}
	
	$scope.getCarts = function (firsCall) {
		overlayLoading.show();
		getSendDataFromServer($scope, ngDataApi, {
				"method": "get",
				"routeName": "/shoppingcart/dashboard/getCarts",
				"params": {
					"start": $scope.startLimit,
					"limit": $scope.endLimit,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			},
			function (error, response) {
				overlayLoading.hide();
				if (error) {
					$scope.$parent.displayAlert('danger', error.code, true, 'shoppingCart', error.message);
				}
				else {
					$scope.totalCount = response.count;
					
					if (firsCall) {
						$scope.showNext = $scope.totalCount > $scope.endLimit;
					}
					else {
						var nextLimit = $scope.startLimit + $scope.increment;
						if ($scope.totalCount <= nextLimit) {
							$scope.showNext = false;
						}
						
					}
					response.records.forEach(function (cart) {
						cart.userInfo = cart.user.fullName ? cart.user.fullName : "";
						cart.userInfo += cart.user.type ? ((cart.userInfo === "" ? "" : ",") + cart.user.type ) : "";
						cart.userInfo += cart.user.companyName ? ((cart.userInfo === "" ? "" : ":") + cart.user.companyName) : "";
						cart.userInfo += " ( " + cart.user.id + " )";
						cart.cartLength = 0;
						cart.totalPrice = 0;
						if (cart.salesRep) {
							cart.SalesRep = cart.salesRep.id;
						}
						if (cart.items && cart.items.length !== 0) {
							cart.items.forEach(function (item) {
								cart.cartLength = cart.cartLength + item.quantity;
							});
						}
						if (cart.totals) {
							cart.totalPrice = cart.totals.price;
						}
						if (cart.totals && cart.totals.discountedPrice) {
							cart.discountedPrice = cart.totals.discountedPrice + ' ' + cart.currency;
						}
						if (cart.totals && cart.totals.discountValue) {
							cart.discountValue = cart.totals.discountValue + ' ' + cart.currency;
						}
						if (!cart.currency) {
							cart.currency = 'CAD';
						}
						cart.totalPrice = Math.round(cart.totalPrice * 100) / 100 + ' ' + cart.currency;
					});
					
					var options = {
						grid: shoppingCartModuleDevConfig.grid,
						data: response.records,
						defaultSortField: 'ts',
						defaultSortASC: true,
						left: [],
						top: []
					};
					options.left.push({
						'label': translation.view[LANG],
						'icon': 'file-text',
						'handler': 'viewOneCart'
					});

					if ($scope.access.delete) {
						options.left.push({
							'label': translation.delete[LANG],
							'icon': 'cross',
							'msg': "Are you sure you want to delete this cart?",
							'handler': 'deleteCart'
						});

						options.top.push({
							'label': translation.delete[LANG],
							'msg': "Are you sure you want to delete cart(s)?",
							'handler': 'deleteCarts'
						});
					}
					buildGrid($scope, options);
				}
			});
	};

	$scope.deleteCart = function (data) {
		var config = {
			"method": "del",
			"routeName": "/shoppingcart/dashboard/purge/" + data._id,
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		overlayLoading.show();
		getSendDataFromServer($scope, ngDataApi, config, function (error) {
			overlayLoading.hide();
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', "Cart deleted!");
				$scope.getCarts(true);
			}
		});

	};

	$scope.deleteCarts = function () {
		var config = {
			"method": "del",
			"routeParam": true,
			"routeName": "/shoppingcart/dashboard/purge/%id%",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			},
			'msg': {
				'error': "One Or More Of The Selected Product Was Not Deleted",
				'success': "Selected Cart(s) Have Been Deleted"
			}
		};
		overlayLoading.show();
		multiRecordUpdate(ngDataApi, $scope, config, function () {
			overlayLoading.hide();
			$scope.getCarts(true);
		});
	};

	$scope.getCarts(true);
	
	$scope.viewOneCart = function (data) {
		$modal.open({
			templateUrl: ModuleDevScartLocation + "/directives/viewOneCart.html",
			size: 'lg',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewCart[LANG];
				$scope.cart = data;
				fixBackDrop();
				$scope.cancel = function () {
					$modalInstance.dismiss('cancel');
				};
			}
		});
		
	};
	
	$scope.getPrev = function () {
		$scope.startLimit = $scope.startLimit - $scope.increment;
		if (0 <= $scope.startLimit) {
			$scope.getCarts();
			$scope.showNext = true;
		}
		else {
			$scope.startLimit = 0;
		}
	};
	
	$scope.getNext = function () {
		var startLimit = $scope.startLimit + $scope.increment;
		if (startLimit < $scope.totalCount) {
			$scope.startLimit = startLimit;
			$scope.getCarts();
		}
		else {
			$scope.showNext = false;
		}
	};
	
	injectFiles.injectCss(ModuleDevScartLocation + "/shoppingCart.css");
	
}]);