"use strict";
var ModuleDevScartLocation = uiModuleDev + '/shoppingCart';

var cartModuleDevTranslation = {
	"shoppingCarts": {
		"ENG": "Shopping Carts",
		"FRA": "Les Sacs"
	},
	"cartUserInfo": {
		"ENG": "User",
		"FRA": "Utilisateur"
	},
	"nbItems": {
		"ENG": "Number of Items",
		"FRA": "Numero d'articles"
	},
	"grandTotal": {
		"ENG": "Grand Total",
		"FRA": "Grand Total"
	},
	"discountedPrice": {
		"ENG": "Discounted Price",
		"FRA": "DiscountedPrice"
	},
	"discountValue": {
		"ENG": "Discount Value",
		"FRA": "Discount Value"
	},
	"monthlyFee": {
		"ENG": "Monthly Fee",
		"FRA": "Monthly Fee"
	},
	"annualFee": {
		"ENG": "Annual Fee",
		"FRA": "Annual Fee"
	},
	"monthlyDiscountedFee": {
		"ENG": "Monthly Discounted Fee",
		"FRA": "Monthly Discounted Fee"
	},
	"annualDiscountedFee": {
		"ENG": "Annual Discounted Fee",
		"FRA": "Annual Discounted Fee"
	},
	"monthlyDiscountedValue": {
		"ENG": "Monthly Discounted Value",
		"FRA": "Monthly Discounted Fee"
	},
	"annualDiscountedValue": {
		"ENG": "Annual Discounted Value",
		"FRA": "Annual Discounted Fee"
	},
	"tenant": {
		"ENG": "Tenant",
		"FRA": "Tenant"
	},
	"prodctTitle": {
		"ENG": "Title",
		"FRA": "Titre"
	},
	"productConfiguration": {
		"ENG": "Product Configuration",
		"FRA": "Product Configuration"
	},
	"fulfillmentRulesKey": {
		"ENG": "Fulfillment RulesKey",
		"FRA": "Fulfillment RulesKey"
	},
	
	"productSerial": {
		"ENG": "Product Serial",
		"FRA": "Product Serial"
	},
	"productCount": {
		"ENG": "Product Count",
		"FRA": "Product Count"
	},
	
	"qty": {
		"ENG": "Qty",
		"FRA": "Qté"
	},
	"view": {
		"ENG": "View",
		"FRA": "View"
	},
	"viewCart": {
		"ENG": "View Cart",
		"FRA": "View Sac"
	}
};

for (var attrname in cartModuleDevTranslation) {
	translation[attrname] = cartModuleDevTranslation[attrname];
}

var cartModuleDevNav = [
	{
		'id': 'shCartList',
		'label': translation.shoppingCarts[LANG],
		'checkPermission': {
			'service': 'shoppingcart',
			'method': 'get',
			'route': '/dashboard/getCarts'
		},
		'url': '#/shoppingCart',
		'tplPath': ModuleDevScartLocation + '/directives/list.tmpl',
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'icon': 'cart',
		'mainMenu': true,
		'contentMenu': true,
		'tracker': true,
		'order': 6,
		'scripts': [ModuleDevScartLocation + '/config.js', ModuleDevScartLocation + '/controller.js'],
		'ancestor': [translation.home[LANG]]
	}
];

navigation = navigation.concat(cartModuleDevNav);

errorCodes.shoppingCart = {
	"400": {
		"ENG": "Failed to connect to Database",
		"FRA": "Échec de la connexion à la base de données"
	},
	"412": {
		"ENG": "No records found",
		"FRA": "Aucun sac trouvé"
	}
};
