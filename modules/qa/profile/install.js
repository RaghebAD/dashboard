"use strict";
var ModuleQaKbaseLocation = uiModuleQa + '/profile';

var kbModuleQaTranslation = {
	"price": {
		"ENG": "Price",
		"FRA": "Price"
	},
	"feedProfiles": {
		"ENG": "Feed Profiles",
		"FRA": "Feed Profiles"
	},
	"minimumQuantity": {
		"ENG": "Minimum Quantity",
		"FRA": "Minimum Quantity"
	},
	"defaultLanguage": {
		"ENG": "Default Language",
		"FRA": "Default Language"
	},
	"numberOfCategories": {
		"ENG": "Number of Categories",
		"FRA": "Number of Categories"
	},
	"translation": {
		"ENG": "Translation",
		"FRA": "Translation"
	},
	"parent": {
		"ENG": "Parent",
		"FRA": "Parent"
	},
	"parents": {
		"ENG": "Parents",
		"FRA": "Parents"
	},
	"children": {
		"ENG": "Children",
		"FRA": "Children"
	},
	"classification": {
		"ENG": "Classification",
		"FRA": "Classification"
	},
	"department": {
		"ENG": "Department",
		"FRA": "Department"
	},
	
	"imfv": {
		"ENG": "IMFV",
		"FRA": "IMFV"
	},
	"generalConfiguration": {
		"ENG": "General Configuration",
		"FRA": "General Configuration"
	},
	"treats": {
		"ENG": "Treats",
		"FRA": "Treats"
	},
	"addTreat": {
		"ENG": "Add Treat",
		"FRA": "Add Treat"
	},
	"editTreat": {
		"ENG": "Edit Treat",
		"FRA": "Edit Treat"
	},
	"filters": {
		"ENG": "Filters",
		"FRA": "Filters"
	},
	"addFilter": {
		"ENG": "Add Filter",
		"FRA": "Add Filter"
	},
	"editFilter": {
		"ENG": "Edit Filter",
		"FRA": "Edit Filter"
	},
	"classifications": {
		"ENG": "Classifications",
		"FRA": "Classifications"
	},
	"addClassification": {
		"ENG": "Add Classification",
		"FRA": "Add Classification"
	},
	"editClassification": {
		"ENG": "Edit Classification",
		"FRA": "Edit Classification"
	},
	"departments": {
		"ENG": "Departments",
		"FRA": "Departments"
	},
	"addDepartment": {
		"ENG": "Add Department",
		"FRA": "Add Department"
	},
	"editDepartment": {
		"ENG": "Edit Department",
		"FRA": "Edit Department"
	},
	"categories": {
		"ENG": "Categories",
		"FRA": "Categories"
	},
	"addCategory": {
		"ENG": "Add Category",
		"FRA": "Add Category"
	},
	"editCategory": {
		"ENG": "Edit Category",
		"FRA": "Edit Category"
	},
	"tokens": {
		"ENG": "Tokens",
		"FRA": "Tokens"
	},
	"addToken": {
		"ENG": "Add Token",
		"FRA": "Add Token"
	},
	"editToken": {
		"ENG": "Edit Token",
		"FRA": "Edit Token"
	},
	"viewItem": {
		"ENG": "View Item",
		"FRA": "View Item"
	},
	"required": {
		"ENG": "Required",
		"FRA": "Required"
	},
	"saveChanges": {
		"ENG": "Save Changes",
		"FRA": "Save Changes"
	},
	"viewingOneCategory": {
		"ENG": "Viewing One Category",
		"FRA": "Viewing One Category"
	},
	"viewingOneClassification": {
		"ENG": "Viewing One Classification",
		"FRA": "Viewing One Classification"
	},
	"viewingOneDepartment": {
		"ENG": "Viewing One Department",
		"FRA": "Viewing One Department"
	},
	"viewingOneFilter": {
		"ENG": "Viewing One Filter",
		"FRA": "Viewing One Filter"
	},
	"rUsRclassification": {
		"ENG": "Are you sure you want to remove this classification?",
		"FRA": "Are you sure you want to remove this classification?"
	},
	"rUsRcategory": {
		"ENG": "Are you sure you want to remove this category?",
		"FRA": "Are you sure you want to remove this category?"
	},
	"rUsRdepartment": {
		"ENG": "Are you sure you want to remove this department?",
		"FRA": "Are you sure you want to remove this department?"
	},
	"rUsRfilter": {
		"ENG": "Are you sure you want to remove this filter?",
		"FRA": "Are you sure you want to remove this filter?"
	},
	"rUsRtoken": {
		"ENG": "Are you sure you want to remove this token?",
		"FRA": "Are you sure you want to remove this token?"
	},
	"rUsRtreat": {
		"ENG": "Are you sure you want to remove this treat?",
		"FRA": "Are you sure you want to remove this treat?"
	},
	"minimumQuantityAllowed": {
		"ENG": "Minimum Quantity Allowed",
		"FRA": "Minimum Quantity Allowed"
	},
	"languages": {
		"ENG": "Languages",
		"FRA": "Languages"
	},
	"addLanguage": {
		"ENG": "Add Language",
		"FRA": "Add Language"
	},
	"removeLanguage": {
		"ENG": "Remove Language",
		"FRA": "Remove Language"
	},
	"id": {
		"ENG": "ID",
		"FRA": "ID"
	},
	"display": {
		"ENG": "Display",
		"FRA": "Display"
	},
	"default": {
		"ENG": "Default",
		"FRA": "Default"
	},
	
	"pSyDlanguage": {
		"ENG": "Please Select your Default Language",
		"FRA": "Please Select your Default Language"
	},
	"gCuSuccessfully": {
		"ENG": "General Configuration Updated Successfully.",
		"FRA": "General Configuration Updated Successfully."
	},
	"classDeleteSuccess": {
		"ENG": "Classification Deleted Successfully.",
		"FRA": "Classification Deleted Successfully."
	},
	"classUpdateSuccess": {
		"ENG": "Classification Updated Successfully.",
		"FRA": "Classification Updated Successfully."
	},
	"classAddSuccess": {
		"ENG": "Classification Added Successfully.",
		"FRA": "Classification Added Successfully."
	},
	"catDeleteSuccess": {
		"ENG": "Category Deleted Successfully.",
		"FRA": "Category Deleted Successfully."
	},
	"catUpdateSuccess": {
		"ENG": "Category Updated Successfully.",
		"FRA": "Category Updated Successfully."
	},
	"catAddSuccess": {
		"ENG": "Category Added Successfully.",
		"FRA": "Category Added Successfully."
	},
	"depDeleteSuccess": {
		"ENG": "Department Deleted Successfully.",
		"FRA": "Department Deleted Successfully."
	},
	"depUpdateSuccess": {
		"ENG": "Department Updated Successfully.",
		"FRA": "Department Updated Successfully."
	},
	"depAddSuccess": {
		"ENG": "Department Added Successfully.",
		"FRA": "Department Added Successfully."
	},
	"filterDeleteSuccess": {
		"ENG": "Filter Deleted Successfully.",
		"FRA": "Filter Deleted Successfully."
	},
	"filterUpdateSuccess": {
		"ENG": "Filter Updated Successfully.",
		"FRA": "Filter Updated Successfully."
	},
	"filterAddSuccess": {
		"ENG": "Filter Added Successfully.",
		"FRA": "Filter Added Successfully."
	},
	"treatDeleteSuccess": {
		"ENG": "Treat Deleted Successfully.",
		"FRA": "Treat Deleted Successfully."
	},
	"treatUpdateSuccess": {
		"ENG": "Treat Updated Successfully.",
		"FRA": "Treat Updated Successfully."
	},
	"treatAddSuccess": {
		"ENG": "Treat Added Successfully.",
		"FRA": "Treat Added Successfully."
	},
	"tokenDeleteSuccess": {
		"ENG": "Token Deleted Successfully.",
		"FRA": "Token Deleted Successfully."
	},
	"tokenUpdateSuccess": {
		"ENG": "Token Updated Successfully.",
		"FRA": "Token Updated Successfully."
	},
	"tokenAddSuccess": {
		"ENG": "Token Added Successfully.",
		"FRA": "Token Added Successfully."
	},
	"pleaseSelectIfFilterIsRequired": {
		"ENG": "Please Select If %filter% Is Required!",
		"FRA": "Please Select If %filter% Is Required!"
	},
	"viewingOneToken": {
		"ENG": "Viewing One Token",
		"FRA": "Viewing One Token"
	},
	"viewingOneTreat": {
		"ENG": "Viewing One Treat",
		"FRA": "Viewing One Treat"
	}
};

for (var attrname in kbModuleQaTranslation) {
	translation[attrname] = kbModuleQaTranslation[attrname];
}

var knowledgebaseModuleQaNav = [
	{
		'id': 'feedProfileList',
		'label': translation.feedProfiles[LANG],
		'checkPermission': {
			'service': 'kbprofile',
			'method': 'get',
			'route': '/owner/feed/profiles/list'
		},
		'url': '#/feedProfiles',
		'tplPath': ModuleQaKbaseLocation + '/directives/feed-profiles-list.tmpl',
		'icon': 'cloud',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 1,
		'scripts': [ModuleQaKbaseLocation + '/config.js', ModuleQaKbaseLocation + '/controller.js'],
		'ancestor': [translation.home[LANG]]
	},
	{
		'id': 'editFeedProfileList',
		'label': 'Edit Feed Profile',
		'tracker': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'url': '#/feedProfiles/edit/:id',
		'tplPath': ModuleQaKbaseLocation + '/directives/feed-profiles-edit.tmpl',
		'scripts': [ModuleQaKbaseLocation + '/config.js', ModuleQaKbaseLocation + '/controller.js',
			ModuleQaKbaseLocation + '/controller-configuration.js', ModuleQaKbaseLocation + '/controller-treats.js',
			ModuleQaKbaseLocation + '/controller-filters.js', ModuleQaKbaseLocation + '/controller-classifications.js',
			ModuleQaKbaseLocation + '/controller-departments.js', ModuleQaKbaseLocation + '/controller-categories.js',
			ModuleQaKbaseLocation + '/controller-tokens.js'],
		'ancestor': [translation.home[LANG], translation.feedProfiles[LANG]]
	}
];
navigation = navigation.concat(knowledgebaseModuleQaNav);

errorCodes.knowledgebase = {
	"400": {
		"ENG": "Failed to connect to Database",
		"FRA": "Échec de la connexion à la base de données"
	}
};