"use strict";
var knowledgebaseApp = soajsApp.components;
knowledgebaseApp.controller('feedProfileModuleQaCtrl', ['$scope', '$timeout', '$modal', '$cookies', 'ngDataApi', function ($scope, $timeout, $modal, $cookies, ngDataApi) {
	$scope.$parent.isUserLoggedIn();
	$scope.ModuleQaKbaseLocation = ModuleQaKbaseLocation;
	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.get("myEnv").code;
	}
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, nakbModuleQaConfig.permissions);

	$scope.listProfiles = function () {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/kbprofile/owner/feed/profiles/list",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				response.forEach(function (oneProfile) {
					oneProfile.lang = 'N/A';
					for (var lang = 0; lang < oneProfile.languages.length; lang++) {
						if (oneProfile.languages[lang].selected) {
							oneProfile.lang = oneProfile.languages[lang].label;
						}
					}
					oneProfile.cats = oneProfile.categories;
				});
				var options = {
					grid: nakbModuleQaConfig.feedProfiles.grid,
					data: response,
					defaultSortField: 'ts',
					defaultSortASC: true,
					left: [],
					top: []
				};
				if ($scope.access.feedProfiles.edit) {
					options.left.push({
						'label': translation.edit[LANG],
						'icon': 'pencil2',
						'handler': 'editProfile'
					});
				}
				buildGrid($scope, options);

			}
		});
	};

	$scope.editProfile = function (data) {
		var id = data._id;
		var path = '#/feedProfiles/edit/' + id;
		$cookies.put("soajs_current_route", path.replace("#", ""));
		window.open(path, '_blank'); // in new tab
	};
	
	$scope.setActiveTab = function (feedActiveTab) {
		sessionStorage.setItem("feedActiveTab", feedActiveTab);
	};
	
	$scope.getActiveTab = function () {
		return sessionStorage.getItem("feedActiveTab");
	};
	
	$scope.isActiveTab = function (tabName, index) {
		var feedActiveTab = $scope.getActiveTab();
		return (feedActiveTab === tabName || (feedActiveTab === null && index === 0));
	};
	
	if ($scope.access.feedProfiles.list) {
		$scope.listProfiles();
	}
}]);