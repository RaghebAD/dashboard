"use strict";
var ModuleQaCatalog = uiModuleQa + '/catalogProfiles';

var ctModuleQaTranslation = {
	"catalogProfiles": {
		"ENG": "Catalog Profiles",
		"FRA": "Catalog Profiles"
	}
};

for (var attrname in ctModuleQaTranslation) {
	translation[attrname] = ctModuleQaTranslation[attrname];
}

var catalogProfilesModuleQaNav = [
	{
		'id': 'catalogProfiles',
		'label': translation.catalogProfiles[LANG],
		'url': '#/catalogProfiles',
		'scripts': [
			ModuleQaCatalog + '/config.js', ModuleQaCatalog + '/service.js',
			ModuleQaCatalog + '/controller.js'
		],
		'tplPath': ModuleQaCatalog + '/directives/list.tmpl',
		'checkPermission': {
			'service': 'catalog',
			'method': 'get',
			'route': '/owner/catalog/profiles'
		},
		'icon': 'profile',
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'contentMenu': true,
		'mainMenu': true,
		'tracker': true,
		'order': 2,
		'ancestor': [translation.home[LANG]]
	}
];

navigation = navigation.concat(catalogProfilesModuleQaNav);

errorCodes.catalog = {
	"400": {
		"ENG": "Failed to connect to Database",
		"FRA": "Échec de la connexion à la base de données"
	}
};