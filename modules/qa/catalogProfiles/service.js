"use strict";
var catalogProfilesService = soajsApp.components;
catalogProfilesService.service('catalogProfilesSrv', ['$timeout', 'ngDataApi', function ($timeout, ngDataApi) {

	function callAPI(currentScope, config, callback) {
		getSendDataFromServer(currentScope, ngDataApi, config, callback);
	}

	return {
		'getEntriesFromAPI': function (currentScope, opts, callback) {
			callAPI(currentScope, opts, callback);
		},

		'printGrid': function ($scope, response) {
			for (var x = 0; x < response.length; x++) {
				response[x].feed_profile_name = response[x].config.feed_profile_name;
				response[x].feed_profile = response[x].config.feed_profile;
				response[x].sources = Object.keys(response[x].config.sources);
			}

			var options = {
				'grid': {
					recordsPerPageArray: [5, 10, 50, 100],
					'columns': [
						{'label': 'Name', 'field': 'name'},
						{'label': 'Feed Profile', 'field': 'feed_profile_name'},
						{'label': 'Sources', 'field': 'sources'}

					],
					'defaultLimit': 50
				},
				'defaultSortField': 'name',
				'data': response,
				'left': [],
				'top': []
			};

			if ($scope.access.list) {
				options.left.push({
					'icon': 'search',
					'label': 'View Item',
					'handler': 'viewEntry'
				});
			}
			if ($scope.access.build) {
				options.left.push({
					'label': 'Build',
					'icon': 'hammer',
					'msg': "Are you sure that you want to build a new catalog ?",
					'handler': 'buildCatalog'
				});
			}

			if ($scope.access.edit) {
				options.left.push({
					'label': 'Edit',
					'icon': 'pencil2',
					'handler': 'editCatalogProfile'
				});
			}

			if ($scope.access.delete) {
				options.left.push({
					'label': 'Delete',
					'icon': 'cross',
					'msg': "Are you sure you want to remove this catalog profile ?",
					'handler': 'deleteCatalogProfile'
				});
				options.top.push({
					'label': 'Delete',
					'msg': "Are you sure you want to delete the selected catalog profile(s)?",
					'handler': 'deleteCatalogProfiles'
				});
			}
			buildGrid($scope, options);
		},

		"buildCategoriesFromProfile": function (profileRecord, currentList, parentCategory) {
			var categories = [];
			var defaultLanguage = '';
			if (profileRecord) {
				for (var i = 0; i < profileRecord.languages.length; i++) {
					if (profileRecord.languages[i].selected) {
						defaultLanguage = profileRecord.languages[i].id;
					}
				}

				var parentIndex;
				for (var i = 0; i < currentList.length; i++) {
					if (currentList[i].v === parentCategory) {
						parentIndex = i;
					}
				}

				for (var cat in profileRecord.categories) {
					processCategoryLevel(cat, profileRecord.categories, currentList, parentIndex);
				}

				if (currentList && currentList.length > 0) {
					profileRecord.categories = currentList;
					profileRecord.categories.splice.apply(profileRecord.categories, [parentIndex + 1, 0].concat(categories));
				}
				else {
					profileRecord.categories = categories;
				}

			}

			function processCategoryLevel(cat, categoriesArray, currentList, parentIndex) {
				var id = cat;
				var label = (profileRecord.taxonomies[cat]) ? profileRecord.taxonomies[cat].label[defaultLanguage].value : cat;
				if (categoriesArray[cat]) {
					if (categoriesArray[cat]['deprecated-towards']) {
						//get the new value
						id = categoriesArray[cat]['deprecated-towards'];
						label += " (deprecated), becomes: " + profileRecord.taxonomies[id].label[defaultLanguage].value;
					}
					if (categoriesArray[cat].parents && categoriesArray[cat].parents.length > 0) {
						//label = getLabels(categoriesArray[cat].parents, defaultLanguage) + " / " + label;
						label = currentList[parentIndex].l + " / " + label;
					}

					//Getting level based on label string
					var labelArray = label.split(" / ");

					var t = {
						"v": id,
						"l": label,
						"level": labelArray.length - 1
					};

					/*
					 if(categoriesArray[cat].attributes) {
					 if(categoriesArray[cat].attributes.department) {
					 var departments = [];
					 for(var i = 0; i < categoriesArray[cat].attributes.department.length; i++) {
					 var depId = categoriesArray[cat].attributes.department[i];
					 if(profileRecord.taxonomies && profileRecord.taxonomies[depId] && profileRecord.taxonomies[depId].label) {
					 departments.push({
					 "v": depId,
					 "l": profileRecord.taxonomies[depId].label[defaultLanguage].value
					 });
					 }
					 }
					 t["department"] = departments;
					 }

					 if(categoriesArray[cat].attributes.classification) {
					 var classification = [];
					 for(var i = 0; i < categoriesArray[cat].attributes.classification.length; i++) {
					 var classId = categoriesArray[cat].attributes.classification[i];

					 if(profileRecord.taxonomies && profileRecord.taxonomies[classId] && profileRecord.taxonomies[classId].label) {
					 classification.push({
					 "v": classId,
					 "l": profileRecord.taxonomies[classId].label[defaultLanguage].value
					 });
					 }
					 }
					 t["classification"] = classification;
					 }
					 }
					 */

					categories.push(t);

					/*
					 if(categoriesArray[cat].children && categoriesArray[cat].children.length > 0) {
					 for(var i = 0; i < categoriesArray[cat].children.length; i++) {
					 var child = categoriesArray[cat].children[i];
					 processCategoryLevel(child, categoriesArray);
					 }
					 }*/
				}
			}

			/*
			 function getLabels(parentsArray, langCode) {
			 var out = [];
			 for(var i = 0; i < parentsArray.length; i++) {
			 var oneParentCat = parentsArray[i];
			 var label = profileRecord.taxonomies[oneParentCat].label[langCode].value;

			 if(profileRecord.categories[oneParentCat].parents && profileRecord.categories[oneParentCat].parents.length > 0) {
			 label = getLabels(profileRecord.categories[oneParentCat].parents, langCode) + " / " + label;
			 }
			 out.push(label);
			 }

			 return out.join(" / ");
			 }*/
		}
	}
}

]);
