"use strict";
var catalogModuleQaConfig = {
	permissions: {
		'list': ['catalog', '/owner/catalog/profiles', "get"],
		'add': ['catalog', '/owner/catalog/profiles', "post"],
		'edit': ['catalog', '/owner/catalog/profiles/:id', "put"],
		'delete': ['catalog', '/owner/catalog/profiles/:id', "delete"],
		'build': ['catalog', '/owner/catalog/build', "get"]
	}
};