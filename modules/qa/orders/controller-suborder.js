"use strict";
var orderApp = soajsApp.components;

orderApp.controller('subOrderListDevCtrl', ['$scope', '$timeout', '$modal', '$routeParams', '$cookies', 'ngDataApi', '$localStorage',
	function ($scope, $timeout, $modal, $routeParams, $cookies, ngDataApi, $localStorage) {
		$scope.$parent.isUserLoggedIn();
		$scope.ModuleQaOrdLocation = ModuleQaOrdLocation;
		$scope.access = {};
		constructModulePermissions($scope, $scope.access, orderModuleQaConfig.permissions);
		$scope.rows = [];
		$scope.startLimit = 0;
		$scope.totalCount = 1000;
		$scope.endLimit = orderModuleQaConfig.grid.apiEndLimit;
		$scope.increment = orderModuleQaConfig.grid.apiEndLimit;
		$scope.showNext = false;

		if (!$scope.$parent.currentSelectedEnvironment) {
			$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
		}

		$scope.getSubOrders = function (firsCall) {
			$scope.userCookie = $localStorage.soajs_user;

			overlayLoading.show();
			getSendDataFromServer($scope, ngDataApi, {
				"method": "get",
				"routeName": "/order/dashboard/merchant/subOrder/list",
				"params": {
					"start": $scope.startLimit,
					"limit": $scope.endLimit,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			}, function (error, response) {
				overlayLoading.hide();
				if (error) {
					$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
				}
				else {
					$scope.totalCount = response.count;
					$scope.rows = response.records;
					if (firsCall) {
						if ($scope.totalCount <= $scope.endLimit) {
							$scope.showNext = false;
						}
						else {
							$scope.showNext = true;
						}
					}
					else {
						var nextLimit = $scope.startLimit + $scope.increment;
						if ($scope.totalCount <= nextLimit) {
							$scope.showNext = false;
						}
					}
					response.records.forEach(function (order) {
						order.orderInfo_orderNbr = order.orderInfo.orderNbr;
						order.userInfo_fullName = order.userInfo.firstName + ' ' + order.userInfo.lastName;
						order.orderInfo_status = order.subOrder.orderInfo.status;
						order.orderInfo_totalAmount = order.subOrder.orderInfo.totalAmount;
						order.orderInfo_acquiringMode = order.subOrder.orderInfo.acquiringMode;
						order.cartLength = 0;
						order.subOrder.cart.forEach(function (item) {
							order.cartLength = order.cartLength + item.quantity;
						});
						order.merchantName = order.merchantInfo.name;
					});
					var options = {
						grid: orderModuleQaConfig.subOrder.grid,
						data: response.records,
						defaultSortField: 'ts',
						defaultSortASC: true,
						left: [],
						top: []
					};
					if ($scope.access.subOrder.view) {
						options.left.push({
							'label': translation.viewRecord[LANG],
							'icon': 'file-text',
							'handler': 'viewOneSubOrder'
						});
					}
					buildGrid($scope, options);

				}
			});
		};

		$scope.getSubOrders(true);

		$scope.viewOneSubOrder = function (order) {
			var id = order._id.toString();
			$scope.$parent.go('/subOrders/view/' + id + '/' + order.merchant);
		};

		$scope.getPrev = function () {
			$scope.startLimit = $scope.startLimit - $scope.increment;
			if (0 <= $scope.startLimit) {
				$scope.getSubOrders();
				$scope.showNext = true;
			}
			else {
				$scope.startLimit = 0;
			}
		};

		$scope.getNext = function () {
			var startLimit = $scope.startLimit + $scope.increment;
			if (startLimit < $scope.totalCount) {
				$scope.startLimit = startLimit;
				$scope.getSubOrders();
			}
			else {
				$scope.showNext = false;
			}

		};

	}]);

orderApp.controller('subOrderViewDevCtrl', ['$scope', '$timeout', '$modal', '$routeParams', '$cookies', 'ngDataApi', 'injectFiles', '$localStorage',
	function ($scope, $timeout, $modal, $routeParams, $cookies, ngDataApi, injectFiles, $localStorage) {
		$scope.$parent.isUserLoggedIn();
		$scope.ModuleQaOrdLocation = ModuleQaOrdLocation;
		$scope.showEditStatus = false;
		$scope.access = {};
		constructModulePermissions($scope, $scope.access, orderModuleQaConfig.permissions);
		$scope.order = false;

		$scope.userCookie = $localStorage.soajs_user;

		if (!$scope.$parent.currentSelectedEnvironment) {
			$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
		}

		$scope.printLabel = function () {

		};

		$scope.createLabel = function () {
			overlayLoading.show();
			getSendDataFromServer($scope, ngDataApi, {
				"method": "get",
				"routeName": "/order/dashboard/merchant/subOrder/createLabel",
				"params": {
					'orderId': $routeParams.id,
					'merchantUuid': $routeParams.merchant,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			}, function (error, response) {
				overlayLoading.hide();
				if (error) {
					$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
				}
				else {
					$scope.$parent.displayAlert('success', translation.labelCreated[LANG]);
					$scope.order = response.order;
					$scope.getSubOrder();
				}
			});
		};

		$scope.getSubOrder = function () {
			getSendDataFromServer($scope, ngDataApi, {
				"method": "get",
				"routeName": "/order/dashboard/merchant/subOrder/view",
				"params": {
					"orderId": $routeParams.id,
					'merchantUuid': $routeParams.merchant,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			}, function (error, response) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
				}
				else {
					$scope.order = {};
					$scope.order = response;
					var labelNeeded = true;

					if (response.subOrder.ypLabel) {
						if (response.merchantInfo.acquiringMode === 'pickup') {
							labelNeeded = false;
						}
						else if (response.merchantInfo.acquiringMode === 'shipping') {
							if (response.merchantInfo.shippingMethod.driver === 'easypost') {
								if (response.subOrder.postageLabel && response.subOrder.postageLabel.labelUrl) {
									labelNeeded = false;
								}
							}
							else {
								labelNeeded = false;
							}
						}
					}
					$scope.labelNeeded = labelNeeded;
					//(order.subOrder.postageLabel && order.subOrder.postageLabel.labelUrl
					response.subOrder.cart.forEach(function (item) {
						item.titleObj = {
							default: "ENG"
						};
						if (Array.isArray(item.title)) {
							item.title.forEach(function (title) {
								if (title.default === true) {
									item.titleObj.default = title.language;
								}
								item.titleObj[title.language] = title.value;
							});
						}
						else if (Array.isArray(item.titleArr)) {
							item.titleArr.forEach(function (title) {
								if (title.default === true) {
									item.titleObj.default = title.language;
								}
								item.titleObj[title.language] = title.value;
							});
						}
						else {
							item.titleObj[LANG] = title;
						}
					});
				}
			});
		};

		$scope.setDelivered = function () {
			overlayLoading.show();
			getSendDataFromServer($scope, ngDataApi, {
				"method": "send",
				"routeName": "/order/dashboard/merchant/subOrder/update",
				"data": {
					'orderId': $routeParams.id,
					'merchantUuid': $routeParams.merchant,
					'status': 'delivered',
					'updateInfo': false
				},
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			}, function (error, response) {
				overlayLoading.hide();
				if (error) {
					$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
				}
				else {
					$scope.$parent.displayAlert('success', translation.statusChangedSuccessfully[LANG]);
					$scope.getSubOrder();
				}
			});
		};

		$scope.fulfilOrder = function () {
			var options = {};
			var trackingCode = '';

			if ($scope.order.subOrder.postageLabel) {
				trackingCode = $scope.order.subOrder.postageLabel.trackingCode;
			}
			if ($scope.order.subOrder.orderInfo.acquiringMode === 'shipping') {
				options = {
					timeout: $timeout,
					form: orderModuleQaConfig.subOrder.form.shipping,
					type: 'subOrder',
					name: 'saveSubOrder',
					label: translation.fulfil[LANG],
					data: {
						carrier: $scope.order.merchantInfo.shippingMethod.config.carrier,
						trackingNumber: trackingCode
					},
					actions: [
						{
							'type': 'submit',
							'label': translation.save[LANG],
							'btn': 'primary',
							'action': function (formData) {
								var postData = {
									'orderId': $routeParams.id,
									'merchantUuid': $routeParams.merchant,
									'status': 'shipped',
									'updateInfo': true,
									'infoObject': {
										//carrier: formData.carrier,
										//trackingNumber: formData.trackingNumber,
										eta: formData.eta
									}
								};
								overlayLoading.show();
								getSendDataFromServer($scope, ngDataApi, {
									"method": "send",
									"routeName": "/order/dashboard/merchant/subOrder/update",
									"data": postData,
									"params": {
										"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
									}
								}, function (error) {
									overlayLoading.hide();
									if (error) {
										$scope.form.displayAlert('danger', error.code, true, 'order', error.message);
									}
									else {
										$scope.$parent.displayAlert('success', translation.informationSavedSuccess[LANG]);
										$scope.modalInstance.close();
										$scope.form.formData = {};
										$scope.getSubOrder();
									}
								});
							}
						},
						{
							'type': 'reset',
							'label': translation.cancel[LANG],
							'btn': 'danger',
							'action': function () {
								$scope.modalInstance.dismiss('cancel');
								$scope.form.formData = {};
							}
						}]
				};

				buildFormWithModal($scope, $modal, options);
			}
			else if ($scope.order.subOrder.orderInfo.acquiringMode === 'pickup') {
				// TODO : auto fill
				options = {
					timeout: $timeout,
					form: orderModuleQaConfig.subOrder.form.pickup,
					type: 'subOrder',
					name: 'saveSubOrder',
					label: translation.fulfil[LANG],
					actions: [
						{
							'type': 'submit',
							'label': translation.save[LANG],
							'btn': 'primary',
							'action': function (formData) {
								var postData = {
									'orderId': $routeParams.id,
									'merchantUuid': $routeParams.merchant,
									'status': 'readyForPickup',
									'updateInfo': true,
									'infoObject': {
										time: formData.time
									}
								};
								overlayLoading.show();
								getSendDataFromServer($scope, ngDataApi, {
									"method": "send",
									"routeName": "/order/dashboard/merchant/subOrder/update",
									"data": postData,
									"params": {
										"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
									}
								}, function (error) {
									overlayLoading.hide();
									if (error) {
										$scope.form.displayAlert('danger', error.code, true, 'order', error.message);
									}
									else {
										$scope.$parent.displayAlert('success', translation.informationSavedSuccess[LANG]);
										$scope.modalInstance.close();
										$scope.form.formData = {};
										$scope.getSubOrder();
									}
								});
							}
						},
						{
							'type': 'reset',
							'label': translation.cancel[LANG],
							'btn': 'danger',
							'action': function () {
								$scope.modalInstance.dismiss('cancel');
								$scope.form.formData = {};
							}
						}
					]
				};

				buildFormWithModal($scope, $modal, options);
			}

		};

		$scope.cancelOrder = function () {
			overlayLoading.show();
			getSendDataFromServer($scope, ngDataApi, {
				"method": "send",
				"routeName": "/order/dashboard/merchant/subOrder/update",
				"data": {
					'orderId': $routeParams.id,
					'merchantUuid': $routeParams.merchant,
					'status': 'rejected',
					'updateInfo': false
				},
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			}, function (error, response) {
				overlayLoading.hide();
				if (error) {
					$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
					$scope.getSubOrder();
				}
				else {
					$scope.$parent.displayAlert('success', translation.subOrderRejected[LANG]);
					$scope.getSubOrder();
				}
			});

		};

		$scope.processOrder = function () {
			overlayLoading.show();
			getSendDataFromServer($scope, ngDataApi, {
				"method": "send",
				"routeName": "/order/dashboard/merchant/subOrder/update",
				"data": {
					'orderId': $routeParams.id,
					'merchantUuid': $routeParams.merchant,
					'status': 'processing',
					'updateInfo': false
				},
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			}, function (error) {
				overlayLoading.hide();
				if (error) {
					$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
				}
				else {
					$scope.statusValue = 'processing';
					$scope.$parent.displayAlert('success', translation.statusChangedSuccessfully[LANG]);
					$scope.getSubOrder();
				}
			});

		};

		$scope.setPicked = function () {
			overlayLoading.show();
			getSendDataFromServer($scope, ngDataApi, {
				"method": "send",
				"routeName": "/order/dashboard/merchant/subOrder/update",
				"data": {
					'orderId': $routeParams.id,
					'merchantUuid': $routeParams.merchant,
					'status': 'picked',
					'updateInfo': false
				},
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			}, function (error, response) {
				overlayLoading.hide();
				if (error) {
					$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
				}
				else {
					$scope.$parent.displayAlert('success', translation.statusChangedSuccessfully[LANG]);
					$scope.getSubOrder();
				}
			});

		};

		$scope.getSubOrder();

		injectFiles.injectCss(ModuleQaOrdLocation + '/order.css');
	}]);
