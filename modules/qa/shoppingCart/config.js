var shoppingCartModuleQaConfig = {
	'permissions': {
		'list': ['shoppingcart', '/dashboard/getCarts', "get"],
		'delete': ['shoppingcart', '/dashboard/purge/:cartId', "delete"]
	},
	'apiEndLimit': 200,
	'grid': {
		recordsPerPageArray: [50, 100, 200],
		'columns': [
			{
				'label': translation.cartUserInfo[LANG], 'field': 'userInfo'
			},
			{
				'label': translation.nbItems[LANG], 'field': 'cartLength'
			},
			{
				'label': translation.dateCreated[LANG],
				'field': 'created',
				'filter': "prettyLocalDate"
			},
			{
				'label': translation.dateModified[LANG],
				'field': 'modified',
				'filter': "prettyLocalDate"
			},
			{
				'label': translation.grandTotal[LANG], 'field': 'totalPrice'
			},
			{
				'label': "status", 'field': 'status'
			}
		],
		'leftActions': [],
		'topActions': [],
		'defaultSortField': '',
		'defaultLimit': 100
	},
	'form': {}
};