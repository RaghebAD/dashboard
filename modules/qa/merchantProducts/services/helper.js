var extraUtils = {
	safeInitArray: function (arr) {
		// create array if not found
		if (arr && arr != null) {
			return arr;
		} else {
			return [];
		}
	},
	cleanMerchantPosData: function (data) {
		var output = [];
		if (!data) {
			return output;
		}
		
		for (var i = 0; i < data.length; i++) {
			output.push({
				availability: data[i].availability,
				purchase_limit: data[i].purchase_limit,
				id: data[i].id,
				sale: data[i].sale
			});
		}
		return output;
	},
	
	addAndEditCommonFunctions: function ($scope, cbInputHelper, popUpConfiguration, productsSrv) {
		var languages = ["en", "fr"];
		
		$scope.tempo = {
			classification: "",
			brand: "",
			variation: [],
			serviceAreaIsInclusive: true,
			verticalsIsInclusive: true,
			headingsIsInclusive: true,
			sensitiveHdingsIsInclusive: true,
			serviceArea: [],
			verticals: [],
			headings: [],
			sensitiveHeadings: [],
			salesChannels: {},
			accountType: {},
			browsedImages: {
				en: {
					data: [],
					message: '',
					applicable: false, // pictures browsed
					valid: true // status of the browsed pictures
				},
				fr: {
					data: [],
					message: '',
					applicable: false,
					valid: true
				}
			},
			availableProducts: [] // will be set in $scope.setAllAvailableProducts
		};
		
		$scope.setAllAvailableProducts = function () {
			
			var opts = {
				"method": "get",
				"routeName": "/knowledgebase/product/availableProducts",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				"data": {}
			};
			
			productsSrv.getEntriesFromApi($scope, opts, function (error, data) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
					extraUtils.refreshThumbnails($scope);
				}
				else {
					
					var products = data.records;
					var availableProducts = [];
					for (var i = 0; i < products.length; i++) {
						var temp = {
							serial: products[i].serial,
							udac: products[i].coreData.udac,
							title: products[i].productData.title.en
						};
						availableProducts.push(temp);
					}
					
					$scope.tempo.availableProducts = availableProducts;
				}
			});
		};
		
		$scope.setAllAvailableProducts();
		
		$scope.tempoImageFile = [];
		for (var i = 0; i < languages.length; i++) {
			$scope.tempoImageFile[languages] = {
				url: "",
				type: ""
			};
		}
		
		var tempoProduct = {
			serial: "",
			udac: ""
		};
		
		$scope.addPrerequisiteProduct = function () {
			var tempo = angular.copy(tempoProduct);
			$scope.formData.rules.prerequisiteProduct.push(tempo);
		};
		
		$scope.addIncompatibilityProduct = function () {
			var tempo = angular.copy(tempoProduct);
			$scope.formData.rules.incompatibility.products.push(tempo);
		};
		
		$scope.addCrossSellPath = function () {
			var tempo = angular.copy(tempoProduct);
			$scope.formData.productData.crossSellPath.push(tempo);
		};
		
		$scope.addUpsellPath = function () {
			var tempo = angular.copy(tempoProduct);
			$scope.formData.productData.upsellPath.push(tempo);
		};
		
		$scope.addAddOn = function () {
			$scope.formData.coreData.addOns = extraUtils.safeInitArray($scope.formData.coreData.addOns);
			var tempo = angular.copy(tempoProduct);
			$scope.formData.coreData.addOns.push(tempo);
		};
		
		$scope.removeAddOn = function (index) {
			$scope.formData.coreData.addOns.splice(index, 1);
		};
		$scope.removePrerequisiteProduct = function (index) {
			$scope.formData.rules.prerequisiteProduct.splice(index, 1);
		};
		$scope.removeIncompatibilityProduct = function (index) {
			$scope.formData.rules.incompatibility.products.splice(index, 1);
		};
		$scope.removeCrossSellPath = function (index) {
			$scope.formData.productData.crossSellPath.splice(index, 1);
		};
		$scope.removeUpsellPath = function (index) {
			$scope.formData.productData.upsellPath.splice(index, 1);
		};
		
		// pricing rules
		var tempoOneTime = {
			"name": "",
			"value": ""
		};
		var tempoRegistration = {
			"name": "",
			"value": "",
			"quantity": ""
		};
		var tempoRecurring = {
			"name": "",
			"value": "",
			"quantity": "",
			"unit": "",
			"frequency": ""
		};
		
		$scope.addOneTimeFee = function (index) {
			var tempo = angular.copy(tempoOneTime);
			$scope.formData.merchantMeta.pricing.rules[index].onetime = tempo;
		};
		
		$scope.addRegistrationFee = function (index) {
			if (!$scope.formData.merchantMeta.pricing.rules[index].registration) {
				$scope.formData.merchantMeta.pricing.rules[index].registration = [];
			}
			var tempo = angular.copy(tempoRegistration);
			$scope.formData.merchantMeta.pricing.rules[index].registration.push(tempo);
		};
		
		//$scope.formData.merchantMeta.pricing.rules.recurring = [];
		$scope.addRecurringFee = function (index) {
			if (!$scope.formData.merchantMeta.pricing.rules[index].recurring) {
				$scope.formData.merchantMeta.pricing.rules[index].recurring = [];
			}
			var tempo = angular.copy(tempoRecurring);
			$scope.formData.merchantMeta.pricing.rules[index].recurring.push(tempo);
		};
		
		$scope.addRule = function () {
			$scope.formData.merchantMeta.pricing.rules.push({});
		};
		
		$scope.removeRule = function (index) {
			$scope.formData.merchantMeta.pricing.rules.splice(index, 1);
		};
		
		$scope.removeOnetime = function (index) {
			delete $scope.formData.merchantMeta.pricing.rules[index].onetime;
		};
		
		$scope.removeRegistration = function (parentIndex, index) {
			$scope.formData.merchantMeta.pricing.rules[parentIndex].registration.splice(index, 1);
		};
		
		$scope.removeRecurring = function (parentIndex, index) {
			$scope.formData.merchantMeta.pricing.rules[parentIndex].recurring.splice(index, 1);
		};
		
		// fullfilment content stuff
		
		$scope.addInput = function () {
			cbInputHelper.addInput($scope);
		};
		
		$scope.updateInput = function (type, fieldInfo, index) {
			cbInputHelper.editInput($scope, type, fieldInfo, index);
		};
		
		$scope.removeInput = function (fieldName) {
			cbInputHelper.removeInput($scope, fieldName);
		};
		
		// images
		var imagesValidationConstant = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
		$scope.imageUrlUpdate = function (lang, index) {
			var imageUrl = $scope.formData.productData.media.images[lang][index].url;
			if (imagesValidationConstant.test(imageUrl)) { // valid
				$scope.tempoImages[lang][index].url = $scope.formData.productData.media.images[lang][index].url;
			} else {
				$scope.tempoImages[lang][index].url = '';
			}
		};
		
		$scope.productTypeOnChange = function () {
			$scope.tempo.isNotPhysicalGoods = $scope.formData.coreData.productType !== 'physicalGoods';
		};
		
		$scope.addProductConf = function () {
			// tempo.customConfAdd will have a sub object having the name of the productconf obj and the body of the additionalFields
			// the root configuration will always read from this object, and on save this will be added to the product configuration
			$scope.tempo.customConfAdd = {};
			$scope.tempo.customConfAdd['@temponew@'] = [];
			
			popUpConfiguration.addCustomConfiguration($scope, 'productConfiguration', '@root@', null);
		};
		
		$scope.editProdConfiguration = function (index) {
			$scope.tempo.customConfAdd = {};
			if ($scope.formData.productConfiguration[index].additionalFields) {
				$scope.tempo.customConfAdd[$scope.formData.productConfiguration[index].name] = angular.copy($scope.formData.productConfiguration[index].additionalFields);
			} else {
				$scope.tempo.customConfAdd[$scope.formData.productConfiguration[index].name] = [];
			}
			
			popUpConfiguration.addCustomConfiguration($scope, 'productConfiguration', '@root@', null, index);
		};
		
		$scope.removeProdConfiguration = function (index) {
			$scope.formData.productConfiguration.splice(index, 1);
		};
		
		// old stuff
		
		$scope.selectAll = function (variable) {
			var selectAll = document.getElementById(variable + "SelectAllCb").checked;
			
			for (var i = 0; i < $scope.tempo[variable].length; i++) {
				$scope.tempo[variable][i].isSelected = selectAll;
			}
		};
		
		$scope.uncheckSelectAll = function (variable) {
			document.getElementById(variable + "SelectAllCb").checked = false;
		};
		
		$scope.saveResources = function ($scope, productsSrv, cb) {
			var myResources = productsSrv.extractResourcesFromPostedData($scope);
			if (myResources.length !== 0) {
				productsSrv.uploadFile($scope, myResources, "/knowledgebase/products/addResource", function (error, response) {
					if (error) {
						var errorString;
						if (Array.isArray(error)) {
							errorString = error.join(", ");
						}
						else {
							errorString = error.message;
						}
						overlayLoading.hide();
						return $scope.$parent.displayAlert('danger', errorString);
					}
					else {
						var res = [];
						for (var i = 0; i < response.length; i++) {
							res.push({
								"url": response[i].original,
								"resource": response[i].resource,
								"type": response[i].type,
								"language": response[i].language
							});
						}
						for (var i = 0; i < res.length; i++) {
							var current = res[i];
							var obj = {
								url: current.url,
								type: current.type
							};
							$scope.formData.productData.media.resources[current.language].push(obj);
						}
						
						var language = ["en", "fr"];
						
						for (var i = 0; i < language.length; i++) {
							document.getElementById("browseimage_" + language[i]).value = "";
						}
						return cb();
					}
				});
			}
			else {
				return cb();
			}
		};
		
		$scope.saveMyProduct = function (productsSrv, mode, draft) {
			console.log("draft", draft);
			overlayLoading.show();
			if ($scope.response.profile.languages) {
				var selected = {};
				for (var lang in $scope.response.profile.languages) {
					if ($scope.response.profile.languages[lang].selected) {
						selected = $scope.response.profile.languages[lang];
					}
				}
				$scope.formData.sLang = selected;
			}
			var successMsg;
			
			console.log('Call productsSrv.dataFormtoDb');
			productsSrv.dataFormtoDb($scope, $scope.formData, {}, draft, function (msg) {
				if (msg.info || msg.productData) {
					
					// check if user is submitting the form and the status is unset => set it to inactive
					if (!draft && msg.status === 'draft') {
						msg.status = 'inactive';
					}
					
					msg.merchantMeta.pos = extraUtils.cleanMerchantPosData($scope.data);
					
					//todo clean pos
					var postData = {
						"product": msg
					};
					var opts = {
						"method": "send",
						"params": {
							"merchantId": $scope.merchantId,
							"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
						},
						"data": postData
					};
					if (mode === "edit") {
						if (draft) {
							opts.params.id = $scope.id;
							successMsg = translation.draftUpdatedSuccessfully[LANG];
							if ($scope.access.owner.products.saveDraft) {
								opts.routeName = "/knowledgebase/owner/products/saveDraft";
								opts.params.code = $scope.code;
							}
							else {
								opts.routeName = "/knowledgebase/tenant/products/saveDraft";
							}
						}
						else {
							opts.method = "put";
							successMsg = translation.productUpdatedSuccessfully[LANG];
							if ($scope.access.owner.products.saveDraft) {
								opts.routeName = "/knowledgebase/owner/products/" + $scope.id;
								opts.params.code = $scope.code;
							}
							else {
								opts.routeName = "/knowledgebase/tenant/products/" + $scope.id;
							}
						}
					}
					else if (mode === "add") {
						if (draft) {
							successMsg = translation.draftUpdatedSuccessfully[LANG];
							if ($scope.access.owner.products.saveDraft) {
								opts.routeName = "/knowledgebase/owner/products/saveDraft";
								opts.params.code = $scope.code;
							}
							else {
								opts.routeName = "/knowledgebase/tenant/products/saveDraft";
							}
						}
						else {
							successMsg = translation.productAddedSuccessfully[LANG];
							if ($scope.access.owner.products.add) {
								opts.routeName = "/knowledgebase/owner/products";
								opts.params.code = $scope.code;
							}
							else {
								opts.routeName = "/knowledgebase/tenant/products";
							}
						}
					}
					productsSrv.getEntriesFromApi($scope, opts, function (error) {
						overlayLoading.hide();
						if (error) {
							$scope.$parent.displayAlert('danger', error.message);
							extraUtils.refreshThumbnails($scope);
						}
						else {
							$scope.$parent.displayAlert('success', successMsg);
							$scope.formData = {};
							$scope.$parent.go("/merchantTenants/merchants/browse/" + $scope.merchantId);
						}
					});
				}
				else {
					overlayLoading.hide();
					$scope.$parent.displayAlert('danger', msg);
				}
			});
			
		};
		
		$scope.renderCategoriesAttributes = function () {
			$scope.formData.myDepartments = [];
			$scope.formData.myClassification = [];
			for (var i = 0; i < $scope.response.profile.categories.length; i++) {
				if ($scope.response.profile.categories[i].v === $scope.formData.attributes.category[0].value) {
					if ($scope.response.profile.categories[i].department) {
						$scope.formData.myDepartments = $scope.response.profile.categories[i].department;
					}
					if ($scope.response.profile.categories[i].classification) {
						$scope.formData.myClassification = $scope.response.profile.categories[i].classification;
					}
					if ($scope.response.profile.categories[i].filters) {
						$scope.formData.myFilters = $scope.response.profile.categories[i].filters;
					}
					else if ($scope.formData.myFilters) {
						delete $scope.formData.myFilters;
					}
					if ($scope.response.profile.categories[i].filtersObject) {
						$scope.myFiltersObject = $scope.response.profile.categories[i].filtersObject;
						$scope.myFiltersKeys = Object.keys($scope.myFiltersObject);
						if ($scope.tempo.variation) {
							for (var x = 0; x < $scope.tempo.variation.length; x++) {
								if ($scope.myFiltersKeys.indexOf($scope.tempo.variation[x].type) === -1) {
									$scope.tempo.variation.splice(x, 1);
								}
							}
							if ($scope.tempo.variation.length === 0) {
								$scope.tempo.variation.push({'type': '', 'value': ''});
							}
						}
					}
					break;
				}
			}
		};
		
		$scope.applyToAll = function (t, v) {
			for (var i = 0; i < $scope.data.length; i++) {
				$scope.data[i][t] = v;
			}
		};
		
		$scope.applySaleToAll = function (t, v) {
			for (var i = 0; i < $scope.data.length; i++) {
				if (!$scope.data[i].sale) {
					$scope.data[i].sale = {};
				}
				$scope.data[i].sale[t] = v;
			}
		};
		
		/**
		 * load image and verify width & height
		 *
		 * @param {image object}file
		 * @param {empty or error string} callback
		 */
		function validateImageDimensions(file, callback) {
			var reader = new FileReader();
			reader.addEventListener("load", function () {
				var image = new Image();
				image.addEventListener("load", function () {
					var maximumWidth = 2000;
					var maximumHeight = 2000;
					if (image.width > maximumWidth || image.height > maximumHeight) {
						return callback('Invalid file chosen [' + file.name + '] (' + image.width + 'x' + image.height + '). Maximum dimensions accepted (' + maximumWidth + 'x' + maximumHeight + ')');
					} else {
						return callback();
					}
					
				});
				image.src = reader.result;
			});
			reader.readAsDataURL(file);
		}
		
		/**
		 * check the input browse file component and update the tempo.browsedImages object
		 * if there are any files browsed => set applicable to true
		 * check files extension, size, and images dimension and set valid and message attributes appropriately
		 * wait 300 ms since the $scope wont be updated right away, and do the checks asynchronously
		 *
		 * @param {string}language
		 */
		$scope.validateImages = function (language) {
			setTimeout(function () { // on change formData.media wont be filled right away
				var current = angular.element("input[id='browseimage_" + language + "']");
				
				if (current['0'].value === '') {
					$scope.tempo.browsedImages[language].applicable = false;
					$scope.tempo.browsedImages[language].data = [];
					$scope.tempo.browsedImages[language].message = '';
					$scope.tempo.browsedImages[language].valid = true;
					
					$scope.$apply();
					return;
				}
				
				$scope.tempo.browsedImages[language].applicable = true;
				
				$scope.$apply();
				
				$scope.tempo.browsedImages[language].data = [];
				
				if ($scope.formData.media && $scope.formData.media.upload) {
					if ($scope.formData.media.upload.images) {
						if ($scope.formData.media.upload.images[language]) {
							for (var i in $scope.formData.media.upload.images[language]) {
								if (typeof $scope.formData.media.upload.images[language][i] === 'object') {
									var obj = $scope.formData.media.upload.images[language][i];
									$scope.tempo.browsedImages[language].data.push(obj);
								}
							}
						}
					}
				}
				
				var images = $scope.tempo.browsedImages[language].data;
				
				async.each(images, function (image, callback) {
					var size = image.size;
					var maxSize = 32 * 1000 * 1000;
					if (size > maxSize) {
						callback('Invalid file chosen [' + image.name + '] (' + size + ' bytes). Maximum size allowed is ' + maxSize + ' bytes');
					} else if (!((/\.(png|jpeg|jpg)$/i).test(image.name))) {
						callback('Invalid file chosen [' + image.name + ']. Wrong file format, accepted are png, jpeg or jpg');
					} else {
						validateImageDimensions(image, callback);
					}
					
				}, function (err) {
					if (err) {
						// one of the iterations made an error
						$scope.tempo.browsedImages[language].message = err;
						$scope.tempo.browsedImages[language].valid = false;
					} else {
						$scope.tempo.browsedImages[language].message = 'File(s) browsed respect the dimensions requested';
						$scope.tempo.browsedImages[language].valid = true;
					}
					$scope.$apply();
				});
				
			}, 300);
		};
		
		$scope.AddNewImage = function (language) {
			$scope.formData.productData.media.images[language].push({'url': '', 'type': ''});
			$scope.tempoImages[language].push({'url': '', 'type': ''});
		};
		
		$scope.removeImage = function (language, index) {
			$scope.formData.productData.media.images[language].splice(index, 1);
			$scope.tempoImages[language].splice(index, 1);
		};
		
		$scope.removeBrowsed = function (language) {
			$scope.tempo.browsedImages[language].valid = true;
			$scope.tempo.browsedImages[language].applicable = false;
			$scope.tempo.browsedImages[language].data = [];
			$scope.tempo.browsedImages[language].message = '';
			angular.element("input[id='browseimage_" + language + "']").val(null);
		};
		
		$scope.AddNewResource = function (language) {
			$scope.formData.productData.media.resources[language].push({'url': '', 'type': ''});
		};
		
		$scope.removeResource = function (language, index) {
			$scope.formData.productData.media.resources[language].splice(index, 1);
		};
		
		$scope.AddNewVideo = function (language) {
			$scope.formData.productData.media.videos[language].push({'url': ''});
		};
		
		$scope.removeVideo = function (language, index) {
			$scope.formData.productData.media.videos[language].splice(index, 1);
		};
		
		$scope.checkDisabled = function (disabledSelector, data) {
			for (var i = 0; i < disabledSelector.length; i++) {
				if (data === disabledSelector[i]) {
					return true;
				}
			}
			return false;
		};
		
		$scope.initDisabled = function (disabledSelector, data) {
			for (var i = 0; i < disabledSelector.length; i++) {
				if (data === disabledSelector[i]) {
					return;
				}
			}
			if (data) {
				disabledSelector.push(data);
			}
		};
		
		$scope.addNewVariation = function () {
			$scope.tempo.variation.push({'type': '', 'value': ''});
		};
		
		$scope.removeAvariation = function (index) {
			$scope.tempo.variation.splice(index, 1);
			$scope.disabledSelector.splice(index, 1); // todo
		};
		
		$scope.beforeRenderStartDate = function ($view, $dates, $leftDate, $upDate, $rightDate, $index) {
			if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.endDate) {
				var activeDate = moment($scope.data[$index].sale.endDate);
				for (var i = 0; i < $dates.length; i++) {
					if ($dates[i].localDateValue() >= activeDate.valueOf()) {
						$dates[i].selectable = false;
					}
				}
			}
			else if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.startDate) {
				$scope.$broadcast('RenderEndDate');
			}
			var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
			for (var i = 0; i < $dates.length; i++) {
				if ($dates[i].localDateValue() < minDate.valueOf()) {
					$dates[i].selectable = false;
				}
			}
		};
		
		$scope.beforeRenderEndDate = function ($view, $dates, $leftDate, $upDate, $rightDate, $index) {
			if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.startDate) {
				var activeDate = moment($scope.data[$index].sale.startDate).subtract(1, $view).add(1, 'minute');
				for (var i = 0; i < $dates.length; i++) {
					if ($dates[i].localDateValue() <= activeDate.valueOf()) {
						$dates[i].selectable = false;
					}
				}
			}
			else if ($scope.data && $scope.data[$index].sale && $scope.data[$index].sale.endDate) {
				$scope.$broadcast('RenderStartDate');
			}
			var minDate = moment(new Date()).subtract(1, $view).add(1, 'minute');
			for (var i = 0; i < $dates.length; i++) {
				if ($dates[i].localDateValue() <= minDate.valueOf()) {
					$dates[i].selectable = false;
				}
			}
		};
	},
	
	setSegmentationDataSpecificApi: function ($scope, ngDataApi, route, tempoVar, APIvar, callback) {
		var opts = {
			"method": "get",
			"routeName": route,
			"params": {}
		};
		
		getSendDataFromServer($scope, ngDataApi, opts, function (error, response) {
			if (error) {
				return callback(error.message);
			}
			
			$scope.tempo[tempoVar] = [];
			if (response) {
				for (var i = 0; i < response[APIvar].length; i++) {
					if (APIvar.toLowerCase().includes("headings")) { // headings or sensitive headings
						var temp = {
							code: response[APIvar][i].code,
							value: response[APIvar][i].code,
							isSelected: false
						};
						if (response[APIvar][i].name && response[APIvar][i].name['en']) {
							temp.value = response[APIvar][i].name['en'];
						}
					}
					else {
						var temp = {
							code: response[APIvar][i].code,
							value: response[APIvar][i]['en'],
							isSelected: false
						};
					}
					$scope.tempo[tempoVar].push(temp);
				}
			}
			return callback(null);
		});
	},
	
	importSegmentationData: function ($scope, ngDataApi, mainCallBack) {
		async.parallel({
			headings: function (callback) {
				extraUtils.setSegmentationDataSpecificApi($scope, ngDataApi, "/knowledgebase/product/headings", 'completeHeadings', 'headings', function (error) {
					return callback(error);
				});
			},
			serviceArea: function (callback) {
				extraUtils.setSegmentationDataSpecificApi($scope, ngDataApi, "/knowledgebase/product/serviceArea", 'completeServiceArea', 'serviceArea', function (error) {
					return callback(error);
				});
			},
			verticals: function (callback) {
				extraUtils.setSegmentationDataSpecificApi($scope, ngDataApi, "/knowledgebase/product/verticals", 'completeVerticals', 'verticals', function (error) {
					return callback(error);
				});
			}
		}, function (err) {
			if (err) {
				$scope.$parent.displayAlert('danger', err);
			}
			extraUtils.setSegmentationDataSpecificApi($scope, ngDataApi, "/knowledgebase/product/sensitiveHeadings", 'completeSensitiveHeadings', 'sensitiveHeadings', function (error) {
				return mainCallBack();
			});
		});
	},
	
	assignSelectedSegmentation: function (allValues, array2, include) {
		// copy FromArray2 ToArray 1 BasedOn Include AndOn IsSelected
		// allvalues coming from api, array2 coming from product
		
		var array = [];
		for (var i = 0; allValues && i < allValues.length; i++) {
			var found = false;
			
			for (var j = 0; j < array2.length; j++) {
				if (allValues[i].code === array2[j].code) {
					
					var tempo = {
						code: allValues[i].code,
						value: allValues[i].value,
						isSelected: include === array2[j].isSelected // xnor
					};
					
					array.push(tempo);
					
					found = true;
					break;
				}
			}
			
			if (!found) {
				var tempo = {
					code: allValues[i].code,
					value: allValues[i].value,
					isSelected: false
				};
				
				array.push(tempo);
			}
			
		}
		
		return array;
	},
	
	compareAndConstructSegmentationData: function ($scope) {
		$scope.tempo.serviceArea = extraUtils.assignSelectedSegmentation($scope.tempo.completeServiceArea, $scope.tempo.serviceArea, $scope.tempo.serviceAreaIsInclusive);
		$scope.tempo.verticals = extraUtils.assignSelectedSegmentation($scope.tempo.completeVerticals, $scope.tempo.verticals, $scope.tempo.verticalsIsInclusive);
		$scope.tempo.headings = extraUtils.assignSelectedSegmentation($scope.tempo.completeHeadings, $scope.tempo.headings, $scope.tempo.headingsIsInclusive);
		$scope.tempo.sensitiveHeadings = extraUtils.assignSelectedSegmentation($scope.tempo.completeSensitiveHeadings, $scope.tempo.sensitiveHeadings, $scope.tempo.sensitiveHdingsIsInclusive);
	},
	
	confirmStandardProduct: function (formData) {
		var languages = ["en", "fr"];
		var imagesBody = {};
		var videoBody = {};
		var resourcesBody = {};
		var shortDescriptionsBody = {};
		var longDescriptionsBody = {};
		
		for (var i = 0; i < languages.length; i++) {
			imagesBody[languages[i]] = [
				{url: '', altTag: ''}
			];
			videoBody[languages[i]] = [
				{url: ''}
			];
			resourcesBody[languages[i]] = [
				{url: ''}
			];
			shortDescriptionsBody[languages[i]] = '';
			longDescriptionsBody[languages[i]] = '';
		}
		
		if (!formData) { // before add new product
			var result = {
				coreData: {
					condition: "New",
					addOns: [],
					cancellationPolicy: {
						description: {}
					}
				},
				segmentation: {},
				productConfiguration: [
					{
						"name": "market",
						"priceCheck": false,
						"required": true,
						"label": {
							"en": "Market",
							"fr": "Marché"
						},
						"type": "market"
					},
					{
						"name": "heading",
						"priceCheck": false,
						"required": true,
						"label": {
							"en": "Heading",
							"fr": "Rubrique"
						},
						"type": "heading"
					},
					{
						"name": "startingDate",
						"priceCheck": false,
						"required": true,
						"label": {
							"en": "Starting Date",
							"fr": "Date de début"
						},
						"type": "date"
					},
					{
						"name": "contractLength",
						"priceCheck": false,
						"required": true,
						"label": {
							"en": "Contract Length",
							"fr": "Durée de contrat"
						},
						"type": "select",
						"values": [
							{
								"label": {
									"en": "1 year",
									"fr": "1 an"
								},
								"v": "12"
							}
						]
					}
				],
				productData: {
					media: {
						images: imagesBody,
						videos: videoBody,
						resources: resourcesBody
					},
					title: {},
					shortDescription: shortDescriptionsBody,
					longDescription: longDescriptionsBody,
					crossSellPath: [],
					upsellPath: []
				},
				attributes: {
					variations: [
						{type: '', value: ''}
					],
					category: [
						{"value": ""}
					],
					brand: [
						{"value": "yp product"}
					],
					classification: []
				},
				rules: {
					prerequisiteProduct: [],
					incompatibility: {
						products: []
					}
				},
				merchantMeta: {
					pricing: {
						rules: []
					},
					pos: []
				},
				fulfillmentRules: {
					content: [
						{
							"name": "businessName",
							"label": {
								"en": "Business Name",
								"fr": "Business Name"
							},
							"required": true,
							"type": "text"
						},
						{
							"name": "businessAddress",
							"required": true,
							"type": "address",
							"label": {
								"en": "Address",
								"fr": "Address"
							}
						},
						{
							"name": "businessPhone",
							"label": {
								"en": "Phone Number",
								"fr": "Numéro de Téléphone"
							},
							"required": true,
							"type": "phone"
						},
						{
							"name": "productAndServices",
							"required": false,
							"label": {
								"en": "Product / Services",
								"fr": "Services et Produits"
							},
							"type": "productAndServices"
						}
					]
				},
				shippingData: {
					weight: [],
					dimensions: []
				},
				legal: {
					content: {}
				}
			};
			
			return result;
		}
		else { // beyond get product
			
			if (!formData.coreData) {
				formData.coreData = {};
			}
			
			if (!formData.coreData.addOns) {
				formData.coreData.addOns = [];
			}
			
			if (!formData.coreData.cancellationPolicy) {
				formData.coreData.cancellationPolicy = {};
			}
			
			if (!formData.coreData.cancellationPolicy.description) {
				formData.coreData.cancellationPolicy.description = {};
			}
			
			if (!formData.segmentation) {
				formData.segmentation = {};
			}
			
			if (!formData.productConfiguration) {
				formData.productConfiguration = {};
			}
			
			if (!formData.productData) {
				formData.productData = {};
			}
			
			if (!formData.productData.media) {
				formData.productData.media = {};
			}
			
			if (!formData.productData.media.images) {
				formData.productData.media.images = imagesBody;
			}
			
			if (!formData.productData.media.videos) {
				formData.productData.media.videos = videoBody;
			}
			
			if (!formData.productData.media.resources) {
				formData.productData.media.resources = resourcesBody;
			}
			
			if (!formData.productData.title) {
				formData.productData.title = {};
			}
			
			if (!formData.productData.shortDescription) {
				formData.productData.shortDescription = shortDescriptionsBody;
			}
			
			if (!formData.productData.longDescription) {
				formData.productData.longDescription = longDescriptionsBody;
			}
			
			if (!formData.productData.crossSellPath) {
				formData.productData.crossSellPath = [];
			}
			
			if (!formData.productData.upsellPath) {
				formData.productData.upsellPath = [];
			}
			
			if (!formData.attributes) {
				formData.attributes = {};
			}
			
			if (!formData.attributes.variations) {
				formData.attributes.variations = [{type: '', value: ''}];
			}
			
			if (!formData.attributes.category) {
				formData.attributes.category = [];
			}
			
			if (!formData.attributes.brand) {
				formData.attributes.brand = [];
			}
			
			if (!formData.attributes.classification) {
				formData.attributes.classification = [];
			}
			
			if (!formData.rules) {
				formData.rules = {};
			}
			
			if (!formData.rules.prerequisiteProduct) {
				formData.rules.prerequisiteProduct = [];
			}
			
			if (!formData.rules.incompatibility) {
				formData.rules.incompatibility = {};
			}
			
			if (!formData.rules.incompatibility.products) {
				formData.rules.incompatibility.products = [];
			}
			
			if (!formData.merchantMeta) {
				formData.merchantMeta = {};
			}
			
			if (!formData.merchantMeta.pricing) {
				formData.merchantMeta.pricing = {};
			}
			
			if (!formData.merchantMeta.pricing.rules) {
				formData.merchantMeta.pricing.rules = [];
			}
			
			if (!formData.fulfillmentRules) {
				formData.fulfillmentRules = {};
			}
			
			if (!formData.fulfillmentRules.content) {
				formData.fulfillmentRules.content = [];
			}
			
			if (!formData.shippingData) {
				formData.shippingData = {};
			}
			
			if (!formData.shippingData.weight) {
				formData.shippingData.weight = [];
			}
			
			if (!formData.shippingData.dimensions) {
				formData.shippingData.dimensions = [];
			}
			
			if (!formData.legal) {
				formData.legal = {};
			}
			
			if (!formData.legal.content) {
				formData.legal.content = {};
			}
			
			return formData;
		}
		
	},
	
	refreshThumbnails: function ($scope) {
		var imagesValidationConstant = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
		var language = ["en", "fr"];
		
		$scope.tempoImages = angular.copy($scope.formData.productData.media.images); // initialize
		
		for (var i = 0; i < language.length; i++) {
			var currentLanguage = language[i];
			var arrayOfImages = $scope.formData.productData.media.images[currentLanguage];
			
			for (var j = 0; j < arrayOfImages.length; j++) {
				var currentImageObject = arrayOfImages[j];
				var imageUrl = currentImageObject.url;
				
				if (imagesValidationConstant.test(imageUrl)) { // valid
					$scope.tempoImages[currentLanguage][j].url = imageUrl;
				} else {
					$scope.tempoImages[currentLanguage][j].url = '';
				}
			}
			
		}
	}
};
