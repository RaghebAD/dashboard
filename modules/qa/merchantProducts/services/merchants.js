"use strict";
var merchantsService = soajsApp.components;
merchantsService.service('merchantsModuleQaSrv', ['$timeout', 'ngDataApi', '$compile', '$modal', '$localStorage', '$cookies', 'Upload',
	function ($timeout, ngDataApi, $compile, $modal, $localStorage, $cookies, Upload) {
		
		function callAPI(currentScope, config, callback) {
			getSendDataFromServer(currentScope, ngDataApi, config, callback);
		}
		
		function getEntriesFromApi(currentScope, opts, callback) {
			callAPI(currentScope, opts, callback);
		}
		
		function printGrid(currentScope, response) {
			for (var x = 0; x < response.length; x++) {
				response[x].tags = response[x].tags.join(",");
				response[x].categories = response[x].categories.join(",");
			}
			
			var options = {
				'grid': {
					recordsPerPageArray: [5, 10, 50, 100],
					'columns': [
						{'label': translation.name[LANG], 'field': 'name'},
						{'label': translation.merchantId[LANG], 'field': 'merchantId'},
						{'label': translation.domain[LANG], 'field': 'domain'},
						{'label': translation.source[LANG], 'field': 'source'},
						{'label': translation.status[LANG], 'field': 'status'}
					
					],
					'defaultLimit': 50
				},
				'defaultSortField': 'name',
				'data': response,
				'left': [],
				'top': []
			};
			if (currentScope.access.tenant.merchants.edit || currentScope.access.owner.merchants.get) {
				options.left.push({
					'icon': 'search',
					'label': translation.view[LANG],
					'handler': 'viewEntry'
				});
			}
			
			if (currentScope.access.tenant.products.list || currentScope.access.owner.products.list) {
				options.left.push({
					'label': translation.manageProducts[LANG],
					'icon': 'folder-open',
					'handler': 'manageProducts'
				});
			}
			
			if (currentScope.access.tenant.merchants.configure || currentScope.access.owner.merchants.configure) {
				options.left.push({
					'label': translation.configure[LANG],
					'icon': 'cog',
					'handler': 'configureMerchant'
				});
			}
			
			if (currentScope.access.tenant.merchants.overrideProfile || currentScope.access.owner.merchants.overrideProfile) {
				options.left.push({
					'label': translation.overrideProfile[LANG],
					'icon': 'spinner4',
					'handler': 'overrideProfile'
				});
			}
			
			if (currentScope.access.tenant.merchants.edit || currentScope.access.owner.merchants.edit) {
				options.left.push({
					'label': translation.edit[LANG],
					'icon': 'pencil2',
					'handler': 'editMerchant'
				});
			}
			
			if (currentScope.access.tenant.merchants.changeStatus || currentScope.access.owner.merchants.changeStatus) {
				options.left.push({
					'label': translation.disableActivate[LANG],
					'icon': 'spinner9',
					'msg': translation.areYouSureThatYouWantToChangeTheStatusOfThisMerchant[LANG],
					'handler': 'changeStatus'
				});
				
				options.top.push({
					'label': translation.disableMerchants[LANG],
					'msg': translation.disableMerchantsMsg[LANG],
					'handler': 'disableMerchants'
				});
				
				options.top.push({
					'label': translation.enableMerchants[LANG],
					'msg': translation.enableMerchantsMsg[LANG],
					'handler': 'enableMerchants'
				});
			}
			
			if (currentScope.access.tenant.merchants.delete || currentScope.access.owner.merchants.delete) {
				options.left.push({
					'label': translation.delete[LANG],
					'icon': 'cross',
					'msg': translation.areYouSureYouWantToDeleteThisMerchant[LANG],
					'handler': 'deleteMerchant'
				});
				
				options.top.push({
					'label': translation.deleteMerchants[LANG],
					'msg': translation.areYouSureYouWantToDeleteTheSelectedMerchant[LANG],
					'handler': 'deleteMerchants'
				});
			}
			
			buildGrid(currentScope, options);
			var e = angular.element(document.getElementById('merchantsL'));
			e.html("<nglist></nglist>");
			$compile(e.contents())(currentScope);
		}
		
		function printPos(currentScope, response) {
			for (var x = 0; x < response.length; x++) {
				if (response[x].address) {
					var add2 = "", add3 = "";
					if (response[x].address.addressLine2) {
						add2 = ", " + response[x].address.addressLine2;
					}
					if (response[x].address.addressLine3) {
						add3 = ", " + response[x].address.addressLine3;
					}
					response[x].addressString = response[x].address.addressLine1 + add2 + add3 + ", " + response[x].address.city + ", " + response[x].address.state + ", " + response[x].address.zip;
					if (response[x].address.email) {
						response[x].email = response[x].address.email;
					}
					if (response[x].address.phone) {
						response[x].phone = response[x].address.phone;
						if (response[x].address.phone.charAt(0) === "+" && response[x].address.phone.charAt(1) === "1") {
							//response[x].phone = response[x].address.phone.substring(2);
						}
					}
				}
				
			}
			
			var options = {
				'grid': {
					recordsPerPageArray: [5, 10, 50, 100],
					'columns': [
						{'label': translation.name[LANG], 'field': 'name'},
						{'label': translation.type[LANG], 'field': 'type'},
						{'label': translation.currency[LANG], 'field': 'currency'},
						{'label': translation.email[LANG], 'field': 'email'},
						{'label': translation.phone[LANG], 'field': 'phone'},
						{'label': translation.address[LANG], 'field': 'addressString'},
						//{'label': translation.ypMid[LANG], 'field': 'ypMid'},
						{'label': translation.status[LANG], 'field': 'status'}
					],
					'defaultLimit': 50
				},
				'defaultSortField': 'name',
				'data': response,
				'left': [
					{
						'icon': 'search',
						'label': translation.view[LANG],
						'handler': 'viewPos'
					}
				],
				'top': []
			};
			
			if (currentScope.access.owner.pos.edit || currentScope.access.tenant.pos.edit) {
				options.left.push({
					'label': translation.edit[LANG],
					'icon': 'pencil2',
					'handler': 'editPos'
				});
			}
			if (currentScope.access.tenant.pos.changeStatus || currentScope.access.owner.pos.changeStatus) {
				options.left.push({
					'label': translation.disableActivate[LANG],
					'icon': 'spinner9',
					'msg': translation.areYouSureThatYouWantToChangeTheStatusOfThisPOS[LANG],
					'handler': 'changeStatus'
				});
			}
			/*
			 if (currentScope.access.owner.pos.delete || currentScope.access.tenant.pos.delete) {
			 options.left.push({
			 'label': 'Delete',
			 'icon': 'cross',
			 'msg': "Are you sure you want to delete the selected merchant POS?",
			 'handler': 'deletePos'
			 });
			 }
			 */
			buildGrid(currentScope, options);
		}
		
		function renderFeedConfigurationForm(form, data) {
			var type;
			if (data.feed) {
				if (data.feed.ui) {
					type = 'ui';
					form.formData.type = 'ui';
				}
				else if (data.feed.driver) {
					type = 'driver';
					form.formData.type = 'driver';
					form.data = data.feed.driver.name;
					form.formData['driver'] = data.feed.driver.name;
					form.formData.type = 'driver';
					form.formData['token'] = data.feed.driver.token;
					
					if (form.formData.extra) {
						form.formData.extra = JSON.stringify(form.formData.extra);
					}
					else if (data.feed.driver.extra) {
						form.formData.extra = JSON.stringify(data.feed.driver.extra, null, 2);
					}
				}
				else {
					type = 'api';
					form.formData.type = 'api';
					form.formData = data.feed.api || {};
					if (form.formData.extra) {
						form.formData.extra = JSON.stringify(form.formData.extra);
					}
					form.formData.type = 'api';
				}
			}
			else {
				type = data;
			}
			
			form.entries.forEach(function (oneEntry) {
				jQuery("#feed .tr-" + oneEntry.name + "-wrapper").show();
				if (type === 'ui') {
					if (oneEntry.name !== 'type') {
						oneEntry.required = false;
						jQuery("#feed .tr-" + oneEntry.name + "-wrapper").slideUp();
					}
					
				}
				if (type === 'driver') {
					if (oneEntry.name !== 'type' && oneEntry.name !== 'driver' && oneEntry.name !== 'token' && oneEntry.name != 'extra') {
						oneEntry.required = false;
						jQuery("#feed .tr-" + oneEntry.name + "-wrapper").slideUp();
					}
				}
				if (type === 'api') {
					if (oneEntry.name !== 'type' && oneEntry.name !== 'url' && oneEntry.name !== 'token' && oneEntry.name != 'extra') {
						oneEntry.required = false;
						jQuery("#feed .tr-" + oneEntry.name + "-wrapper").slideUp();
					}
				}
			});
		}
		
		function renderBillingConfigurationForm(form, data) {
			if (data.billing) {
				var billing = data.billing[Object.keys(data.billing)[0]];
				form.entries.forEach(function (oneGroup) {
					if (oneGroup.name === 'address') {
						oneGroup.entries.forEach(function (oneEntry) {
							if (oneEntry.name === 'addressLine1' && billing.address.addressLine1) {
								form.formData[oneEntry.name] = billing.address.addressLine1;
							}
							if (oneEntry.name === 'addressLine2' && billing.address.addressLine2) {
								form.formData[oneEntry.name] = billing.address.addressLine2;
							}
							if (oneEntry.name === 'addressLine3' && billing.address.addressLine3) {
								form.formData[oneEntry.name] = billing.address.addressLine3;
							}
							if (oneEntry.name === 'country' && billing.address.country) {
								oneEntry.value.forEach(function (oneCountry) {
									if (oneCountry.v === billing.address.country) {
										oneCountry.selected = true;
									}
									else if (oneCountry.selected) {
										oneCountry.selected = false;
									}
								});
							}
							if (oneEntry.name === 'city' && billing.address.city) {
								form.formData[oneEntry.name] = billing.address.city;
							}
							if (oneEntry.name === 'state' && billing.address.state) {
								form.formData[oneEntry.name] = billing.address.state;
							}
							if (oneEntry.name === 'zip' && billing.address.zip) {
								form.formData[oneEntry.name] = billing.address.zip;
							}
							if (oneEntry.name === 'email' && billing.address.email) {
								form.formData[oneEntry.name] = billing.address.email;
							}
							if (oneEntry.name === 'phone' && billing.address.phone) {
								form.formData[oneEntry.name] = billing.address.phone;
							}
						});
					}
					if (oneGroup.name === 'cc') {
						oneGroup.entries.forEach(function (oneEntry) {
							if (oneEntry.name === 'name' && billing.name) {
								form.formData[oneEntry.name] = billing.name;
							}
							if (oneEntry.name === 'ccNumber' && billing.last4digits) {
								form.formData[oneEntry.name] = "XXXX XXXX XXXX " + billing.last4digits;
							}
							if (oneEntry.name === 'month' && billing.expirationMonth) {
								form.formData[oneEntry.name] = billing.expirationMonth;
							}
							if (oneEntry.name === 'year' && billing.expirationYear) {
								form.formData[oneEntry.name] = billing.expirationYear;
							}
							if (oneEntry.name === 'cvv' && billing.cvv) {
								form.formData[oneEntry.name] = billing.cvv;
							}
						});
					}
				});
			}
		}
		
		function refillCommonConfigTab(currentScope, config, subSection) {
			config.data = {};
			if (currentScope.myMerchant.commerce && currentScope.myMerchant.commerce[subSection]) {
				config.data['type'] = (currentScope.myMerchant.commerce[subSection].ui) ? 'no' : 'yes';
				
				if (currentScope.myMerchant.commerce[subSection].api) {
					for (var key in currentScope.myMerchant.commerce[subSection].api) {
						config.data[key] = currentScope.myMerchant.commerce[subSection].api[key];
						if (key === 'extra') {
							config.data[key] = JSON.stringify(config.data[key], null);
						}
					}
				}
				else {
					config.entries.forEach(function (oneEntry) {
						
						if (oneEntry.name !== 'extra') {
							oneEntry.required = (config.data.type === 'no') ? false : true;
						}
						if (oneEntry.name === 'url' || oneEntry.name === 'token') {
							oneEntry.type = (config.data.type === 'no') ? 'readonly' : 'text';
						}
						
						if (oneEntry.name === 'extra') {
							oneEntry.type = (config.data.type === 'no') ? 'readonly' : 'textarea';
						}
						
						if (oneEntry.name === 'type') {
							oneEntry.value = [{'v': 'no', 'selected': true}, {'v': 'yes'}];
						}
					});
				}
			}
		}
		
		function fixIMFVPropertyNames(data, labels) {
			for (var property in data) {
				if (Object.hasOwnProperty.call(data, property)) {
					var newLabel = doLabel(property);
					if (newLabel !== property) {
						data[newLabel] = angular.copy(data[property]);
						delete data[property];
					}
					
					if (Array.isArray(data[newLabel]) && data[newLabel].length > 0) {
						data[newLabel].forEach(function (oneEntry) {
							fixIMFVPropertyNames(oneEntry, labels);
						});
					}
					else if (typeof(data[newLabel]) === 'object' && Object.keys(data[newLabel]).length > 0) {
						fixIMFVPropertyNames(data[newLabel], labels);
					}
				}
			}
			
			function doLabel(propertyName) {
				if (labels.indexOf(propertyName) !== -1) {
					propertyName = "_" + propertyName;
				}
				return propertyName;
			}
		}
		
		function updateMerchantConfigurtion(currentScope, ngDataApi, formData, section) {
			var patternCc = /^[0-9]{13,16}$/;
			var patternCvv = /^[0-9]{3,4}$/;
			var postData = {
				"merchantConfig": {
					"feed": {}
				}
			};
			if (currentScope.myMerchant.feed) {
				postData.merchantConfig["feed"] = currentScope.myMerchant.feed;
			}
			switch (section) {
				case 'billing':
					postData.merchantConfig.billing = {
						"cc": {
							"name": formData.name,
							"ccNumber": formData.ccNumber,
							"expirationMonth": formData.month,
							"expirationYear": formData.year,
							"cvv": formData.cvv
						},
						"address": {
							"addressLine1": formData.addressLine1,
							"country": formData.country,
							"state": formData.state,
							"city": formData.city,
							"zip": formData.zip,
							"name": formData.name,
							"email": formData.email,
							"phone": formData.phone
						}
					};
					if (formData.addressLine2) {
						postData.merchantConfig.billing.address.addressLine2 = formData.addressLine2;
					}
					if (formData.addressLine3) {
						postData.merchantConfig.billing.address.addressLine3 = formData.addressLine3;
					}
					break;
				case 'feed':
					if (formData.type === 'ui') {
						postData.merchantConfig.feed = {
							"ui": true
						};
					}
					else if (formData.type === 'driver') {
						postData.merchantConfig.feed = {
							"driver": {
								"name": formData.driver,
								'token': formData.token
							}
						};
						if (formData.extra) {
							postData.merchantConfig.feed.driver.extra = JSON.parse(formData.extra);
						}
					}
					else {
						postData.merchantConfig.feed = {
							"api": {
								"url": formData.url,
								"token": formData.token
							}
						};
						if (formData.extra) {
							postData.merchantConfig.feed.api.extra = JSON.parse(formData.extra);
						}
					}
					break;
			}
			if (postData.merchantConfig.billing) {
				if (!patternCc.exec(formData.ccNumber)) {
					return currentScope.$parent.displayAlert('danger', translation.pleaseEnterAvalidCardNumberToProceed[LANG]);
				}
				if (!patternCvv.exec(formData.cvv)) {
					return currentScope.$parent.displayAlert('danger', translation.pleaseEnterAvalidCvv[LANG]);
				}
				overlayLoading.show();
			}
			
			var opts = {
				"method": "send",
				"routeName": "/knowledgebase/tenant/merchants/" + currentScope.merchantId + "/configure",
				"params": {
					"__env": currentScope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				"data": postData
			};
			if (currentScope.access.owner.merchants.configure) {
				opts.routeName = "/knowledgebase/owner/merchants/" + currentScope.merchantId + "/configure";
				opts.params.code = currentScope.code;
			}
			else {
				
			}
			
			getSendDataFromServer(currentScope, ngDataApi, opts, function (error) {
				overlayLoading.hide();
				if (error) {
					currentScope.$parent.displayAlert('danger', error.message);
				}
				else {
					currentScope.$parent.displayAlert('success', translation.merchantConfigurationUpdated[LANG]);
					formData = {};
					currentScope.reloadMerchantRecord();
					if (postData.merchantConfig.billing) {
						$timeout(function () {
							currentScope.populateBillingForm();
						}, 100);
					}
				}
			});
		}
		
		function migrateMerchants(currentScope) {
			var merchants = currentScope.grid.rows;
			$modal.open({
				templateUrl: "migration.html",
				size: 'lg',
				backdrop: true,
				keyboard: true,
				controller: function ($scope, $modalInstance) {
					var otherEnvs = angular.copy($localStorage.environments);
					for (var i = otherEnvs.length - 1; i >= 0; i--) {
						if (otherEnvs[i].code === currentScope.currentSelectedEnvironment.toUpperCase()) {
							otherEnvs.splice(i, 1);
						}
						else if (otherEnvs[i].code === 'DASHBOARD') {
							otherEnvs.splice(i, 1);
						}
					}
					fixBackDrop();
					$scope.title = translation.merchantsMigration[LANG];
					$scope.data = {
						all: false,
						envs: otherEnvs
					};
					
					$scope.data.merchants = [];
					merchants.forEach(function (oneMerchant) {
						$scope.data.merchants.push({
							"_id": oneMerchant._id,
							"name": oneMerchant.name
						});
					});
					
					$scope.submit = function () {
						if (!$scope.data.all && !$scope.data.selectedMerchants) {
							alert("Select at least one merchant");
							return;
						}
						
						if (!$scope.data.toEnv) {
							alert("Select a destination environment");
							return;
						}
						
						var postData = {
							"type": "merchants",
							"all": $scope.data.all,
							"toEnv": $scope.data.toEnv.toUpperCase()
						};
						if (!$scope.data.all) {
							postData.ids = Object.keys($scope.data.selectedMerchants)
						}
						else {
							postData.code = currentScope.code;
						}
						if (currentScope.access.owner.merchants.registerMigration || currentScope.access.tenant.merchants.registerMigration) {
							var opts = {
								"routeName": "/knowledgebase/migrate/data/register",
								"method": "send",
								"params": {
									"__env": currentScope.currentSelectedEnvironment.toUpperCase()
								},
								"data": postData
							};
							overlayLoading.show();
							getSendDataFromServer(currentScope, ngDataApi, opts, function (error) {
								overlayLoading.hide();
								if (error) {
									$scope.message = {
										"danger": true,
										"text": error.message
									};
								}
								else {
									$scope.message = {};
									$scope.data = {};
									currentScope.$parent.displayAlert('success', translation.merchantRegisteredForMigration[LANG]);
									$modalInstance.close();
								}
							});
						}
						else {
							$modalInstance.close();
						}
					};
					
					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};
				}
			});
		}
		
		function listMigrationMerchants(currentScope) {
			var opts = {
				"routeName": "/knowledgebase/migrate/data/list",
				"method": "get",
				"params": {
					"__env": currentScope.$parent.currentSelectedEnvironment.toUpperCase(),
					"type" : "merchants"
				}
			};
			getEntriesFromApi(currentScope, opts, function (error, response) {
				if (error) {
					currentScope.$parent.displayAlert('danger', error.message);
				}
				else {
					var options = {
						'grid': {
							recordsPerPageArray: [5, 10, 50, 100],
							'columns': [
								{'label': translation.fromEnvironment[LANG], 'field': 'fromEnv'},
								{'label': translation.toEnvironment[LANG], 'field': 'toEnv'},
								{'label': translation.type[LANG], 'field': 'collection'},
								{'label': translation.label[LANG], 'field': 'label'}
							
							],
							'defaultLimit': 50
						},
						'defaultSortField': 'toEnv',
						'data': response,
						'left': [],
						'top': []
					};
					
					if (currentScope.access.tenant.merchants.removeMigration || currentScope.access.owner.merchants.removeMigration) {
						options.top.push({
							'label': translation.deleteEntries[LANG],
							'msg': translation.areYouSureYouWantToDeleteTheSelectedEntry[LANG],
							'handler': 'removeMigrationMerchants'
						});
					}
					
					buildGrid(currentScope, options);
					var e = angular.element(document.getElementById('migrationL'));
					e.html("<nglist></nglist>");
					$compile(e.contents())(currentScope);
				}
			});
		}
		
		function removeMigrationMerchants(currentScope) {
			var config;
			if (currentScope.access.owner.merchants.removeMigration || currentScope.access.tenant.merchants.removeMigration) {
				config = {
					'routeName': "/knowledgebase/migrate/data/remove",
					"params": {
						'id': '%id%',
						"__env": currentScope.$parent.currentSelectedEnvironment.toUpperCase()
					},
					'msg': {
						'error': translation.oneOrMoreOfTheSelectedEntriesWasNotRemoved[LANG],
						'success': translation.selectedEntriesHaveBeenRemoved[LANG]
					}
				};
			}
			multiRecordUpdate(ngDataApi, currentScope, config, function () {
				currentScope.listMigrationMerchants();
			});
		}
		
		function imageUpload($scope, postData, name, cb) {
			if (postData[name].url && postData[name].url !== undefined && postData[name].url !== '') {
				if (postData[name].altTag) {
					var altTag = angular.copy(postData[name].altTag);
				}
				if (!postData[name].type || postData[name].type !== 'ypg_media_server' || postData[name].type === '') {
					if (postData[name].url.indexOf("http") !== 0 && postData[name].url.indexOf("//") === 0) {
						postData[name].url = 'http:' + postData[name].url;
					}
					else if (postData[name].url.indexOf("http") !== 0) {
						postData[name].url = 'http://' + postData[name].url;
					}
					var opt = {
						"method": "send",
						"routeName": '/knowledgebase/products/addImageUrl',
						"params": {
							"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
						},
						"data": {
							"url": postData[name].url
						}
					};
					getSendDataFromServer($scope, ngDataApi, opt, function (error, response) {
						if (error) {
							return cb(error.message);
						}
						else {
							postData[name] = {
								"url": response.original,
								"type": response.type,
								"resource": response.resource
							};
							if (altTag) {
								postData[name].altTag = altTag;
							}
							return cb(null, true);
						}
					});
				}
				else return cb(null, true);
			}
			else return cb(null, true);
		}
		
		function uploadFile($scope, file, url, cb) {
			var err = [], myResponse = [];
			var config = {
				file: file,
				url: url
			};
			doUpload(config, function (error, response) {
				if (error) {
					err.push(error.message);
				}
				else {
					myResponse.push(response[0]);
				}
				return (err.length > 0) ? cb(err) : cb(null, myResponse);
			});
			
			function doUpload(config, cb) {
				var opt = {
					"method": "send",
					"file": config.file,
					"routeName": config.url,
					"upload": true,
					"params": {
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
				
				getSendDataFromServer($scope, ngDataApi, opt, function (error, response) {
					if (error) {
						return cb(new Error(translation.errorOccurredWhileUploadingFile[LANG] + " " + config.file));
					}
					else {
						return cb(null, response);
					}
				});
			}
		}
		
		return {
			'getEntriesFromApi': getEntriesFromApi,
			
			'printGrid': printGrid,
			
			'printPos': printPos,
			
			"renderFeedConfigurationForm": renderFeedConfigurationForm,
			
			"renderBillingConfigurationForm": renderBillingConfigurationForm,
			
			"refillCommonConfigTab": refillCommonConfigTab,
			
			"updateMerchantConfigurtion": updateMerchantConfigurtion,
			
			"fixIMFVPropertyNames": fixIMFVPropertyNames,
			
			"migrateMerchants": migrateMerchants,
			
			"listMigrationMerchants": listMigrationMerchants,
			
			"removeMigrationMerchants": removeMigrationMerchants,
			
			"imageUpload": imageUpload,
			
			'uploadFile': uploadFile
		}
	}]);