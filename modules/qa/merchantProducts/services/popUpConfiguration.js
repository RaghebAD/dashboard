/*
 product Configuration & bundle configuration popup manager
 */
"use strict";
var popUpConfiguration = soajsApp.components;

popUpConfiguration.service('popUpConfiguration', ['ngDataApi', '$timeout', '$modal', '$window', function (ngDataApi, $timeout, $modal, $window) {

    function constructObject(input) {
        // input object from pop up form
        // output object to submit

        // re arrange values
        var values = [];
        var valuesLength = 0;
        var inputAsArray = Object.keys(input);
        for (var i = 0; i < inputAsArray.length; i++) {
            if (inputAsArray[i].includes('valuesValue_')) {
                valuesLength++;
            }
        }
        // todo
        for (var i = 0; i < valuesLength; i++) {
            var tempo = {
                "label": {
                    "en": input['valuesLabel_en_' + i],
                    "fr": input['valuesLabel_fr_' + i]
                },
                "v": input['valuesValue_' + i]
            };
            values.push(tempo);
        }

        // construct
        var filteredName = (input.name && input.name !== '') ? input.name : input.label_en.toLowerCase().trim().replace(/\s/g, "_");

        var output = {
            "name": filteredName,
            "priceCheck": input.priceCheck || false,
            "required": input.required || false,
            "label": {
                "en": input.label_en || '',
                "fr": input.label_fr || ''
            },
            "order": input.order,
            "type": input.type || '',
            "values": values
        };

        return output;
    }

    function getIndexOfEntry(entries, entry) {
        // find between entries.name the entry u r looking for and return its index
        for (var i = 0; i < entries.length; i++) {
            if (entries[i].name === entry) {
                return i;
            }
        }
        console.error("error not found");
        return -1;
    }

    function getCurrentFormValue(currentEntry, form) {
        var data = Object.keys(form);

        for (var j = 0; j < data.length; j++) {
            var currentData = data[j];
            if (currentEntry.name === currentData) { // found take value
                if (form[currentData]) {
                    return form[currentData];
                }
                break;
            }
        }

        // not found // exceptional case of html delete button // remove values
        if (currentEntry && currentEntry.name.includes('removeValues')) {
            return "<span class='red'><span class='icon icon-cross' title='Remove'></span></span>";
        }

        return '';
    }

    function manualUpdateEntriesFromFormData(entries, form) {
        // entries coming as originally defined [if new blank, if edit as saved in the database]
        // form coming from formData including the data as updated by the user

        for (var i = 0; i < entries.length; i++) {
            var currentEntry = entries[i];

            // required and type fields
            if (currentEntry.name.includes('required') || currentEntry.name.includes('type') || currentEntry.name.includes('priceCheck')) {
                // we need to keep as is and as the selected flag accordingly // since it contains all the possible values
                var selectedValueObj = getCurrentFormValue(currentEntry, form);
                var selectedValue = Array.isArray(selectedValueObj) ? selectedValueObj[0] : selectedValueObj;

                for (var j = 0; j < currentEntry.value.length; j++) {
                    if (currentEntry.value[j].v === selectedValue) {
                        entries[i].value[j].selected = true;
                    } else {
                        entries[i].value[j].selected = false;
                    }
                }
                continue;
            }

            // values & additionalFields
            if (currentEntry.entries) {
                for (var j = 0; j < currentEntry.entries.length; j++) {
                    var currentEntryInEntries = currentEntry.entries[j];
                    entries[i].entries[j].value = getCurrentFormValue(currentEntryInEntries, form);
                }
            } else {
                entries[i].value = getCurrentFormValue(currentEntry, form);
            }
        }

        return entries;

    }

    function addCustomConfiguration(currentScope, variable, parentName, previousForm, parentIndex, additionalFieldIndex) {
        // scope
        // variable : productConfiguration
        // parent name : can be @root@ or parents attribute name
        // previousForm : parent Form : if parent Name is different then @root we will need to save the parent temporary form
        // parentIndex : index of the parent used in edit mode
        // additionalFieldIndex : index of the additional field used in edit mode

        if (parentName === '@root@' && previousForm) { // coming back from add additional fields // reload previous state
            var entries = previousForm.entries;
            entries = manualUpdateEntriesFromFormData(entries, previousForm.formData);
        }
        else {
            var name = '';
            var label_en = '';
            var label_fr = '';
            var type = '';
            var order = 1;
            var required = false; // default
            var priceCheck = false; // default
            var values = [];
            var valuesCount = 0;
            var additionalFields = [];

            if ((parentName === '@root@' && parentIndex !== undefined) || additionalFieldIndex) { // on edit

                var objOnEdit;
                if (additionalFieldIndex) {
                    var theOnlyObject = Object.keys(currentScope.tempo.customConfAdd)[0];
                    objOnEdit = currentScope.tempo.customConfAdd[theOnlyObject][additionalFieldIndex];
                } else {
                    objOnEdit = currentScope.formData[variable][parentIndex];
                }

                name = objOnEdit.name;
                label_en = objOnEdit.label.en;
                label_fr = objOnEdit.label.fr;
                required = objOnEdit.required;
                order = objOnEdit.order;
                priceCheck = objOnEdit.priceCheck;
                type = objOnEdit.type;
                additionalFields = currentScope.tempo.customConfAdd.name || [];

                values = objOnEdit.values; // used after defining the entries object
                if (values) {
                    valuesCount = values.length; // used for the values on action buttons (add and remove)
                }
            }

            var entries = [
                {
                    'name': 'name',
                    'label': "Name",
                    "type": "select",
                    "value": [
                        {
                            "v": "market",
                            "l": "Market",
                            "selected": (name === 'market')
                        },
                        {
                            "v": "heading",
                            "l": "Heading",
                            "selected": (name === 'heading')
                        },
                        {
                            "v": "startingDate",
                            "l": "Starting Date",
                            "selected": (name === 'startingDate')
                        },
                        {
                            "v": "contractLength",
                            "l": "Contract Length",
                            "selected": (name === 'contractLength')
                        },
                        {
                            "v": "language",
                            "l": "Language",
                            "selected": (name === 'language')
                        },
                        {
                            "v": "callTracking",
                            "l": "Call Tracking",
                            "selected": (name === 'callTracking')
                        },
                        {
                            "v": "domainName", "l": "Domain Name",
                            "selected": (name === 'domainName')
                        },
                        {
                            "v": "businessLine",
                            "l": "Business Line",
                            "selected": (name === 'businessLine')
                        },
                    ],
                    'placeholder': "",
                    'required': false
                },
                {
                    'name': 'label_en',
                    'label': "English Input Label",
                    'type': 'text',
                    'placeholder': translation.title[LANG],
                    'value': label_en,
                    'tooltip': translation.cbFormStep2LabelTooltip[LANG],
                    'required': true,
                    'fieldMsg': translation.cbFormStep2LabelFieldMSG[LANG]
                },
                {
                    'name': 'label_fr',
                    'label': "French Input Label",
                    'type': 'text',
                    'placeholder': translation.title[LANG],
                    'value': label_fr,
                    'tooltip': translation.cbFormStep2LabelTooltip[LANG],
                    'required': true,
                    'fieldMsg': translation.cbFormStep2LabelFieldMSG[LANG]
                },
                {
                    'name': 'order',
                    'label': "Order",
                    'type': 'number',
                    'required': true,
                    'value': order,
                    'fieldMsg': "Enter an order"
                },
                {
                    "name": "required",
                    "label": translation.mpFields.required[LANG],
                    "type": "radio",
                    "value": [
                        {
                            "v": "false", "l": "False", "selected": !required
                        },
                        {
                            "v": "true",
                            "l": "True",
                            "selected": required
                        }
                    ],
                    "required": false,
                    'fieldMsg': 'Is this field required?'
                },
                // {
                // 	"name": "priceCheck",
                // 	"label": translation.mpFields.priceCheck[LANG],
                // 	"type": "radio",
                // 	"value": [
                // 		{
                // 			"v": "false",
                // 			"l": "False",
                // 			"selected": !priceCheck
                // 		},
                // 		{
                // 			"v": "true",
                // 			"l": "True",
                // 			"selected": priceCheck
                // 		}
                // 	],
                // 	"required": false,
                // 	'fieldMsg': 'Does this field affect the price?'
                // },
                {
                    "name": "type",
                    "label": "Type",
                    "type": "select",
                    "value": [
                        {"v": "text", "l": "Text", "selected": (type === 'text')},
                        {"v": "date", "l": "Date", "selected": (type === 'date')},
                        {"v": "boolean", "l": "Boolean", "selected": (type === 'boolean')},
                        {"v": "select", "l": "Select", "selected": (type === 'select')},
                        {"v": "multiselect", "l": "Multiselect", "selected": (type === 'multiselect')},
                        {"v": "market", "l": "Market", "selected": (type === 'market')},
                        {"v": "heading", "l": "Heading", "selected": (type === 'heading')},
                        {"v": "callTracking", "l": "Call Tracking", "selected": (type === 'callTracking')},
                        {"v": "businessSelector", "l": "Business Selector", "selected": (type === 'businessSelector')}
                    ],
                    "required": true,
                    "fieldMsg": translation.cbFormStep2FileUITypeFieldMsg[LANG],
                    'onAction': function (id, data) {
                        var arr1 = ['select', "Multiselect"];
                        if (arr1.indexOf(data) !== -1) {
                            jQuery("#addConfigInput #values-wrapper").slideDown();
                            jQuery("#addConfigInput #addValues-wrapper").slideDown();
                        }
                        else {
                            jQuery("#addConfigInput #values-wrapper").slideUp();
                            jQuery("#addConfigInput #addValues-wrapper").slideUp();
                        }
                    }
                },
                {
                    'name': 'values',
                    'label': 'Values',
                    'required': false,
                    "type": "group",
                    'collapsed': false,
                    "class": "valuesList",
                    "entries": []
                },
                {
                    "name": "addValues",
                    "type": "html",
                    "value": '<span class=""><input type="button" class="btn btn-sm btn-success" value="Add New Value"></span>',
                    "onAction": function (id, data, form) {
                        var oneClone = angular.copy(valuesRow);
                        form.entries.forEach(function (entry) {
                            if (entry.name === 'values' && entry.type === 'group') {
                                for (var i = 0; i < oneClone.length; i++) {
                                    oneClone[i].name = oneClone[i].name.replace("%count%", valuesCount);
                                }
                                entry.entries = entry.entries.concat(oneClone);
                            }
                        });
                        valuesCount++;
                    }
                },
                {
                    'name': 'additionalFields',
                    'label': 'Additional Fields',
                    'required': false,
                    "type": "group",
                    'collapsed': false,
                    "class": "addFieldsList",
                    "entries": additionalFields
                },
                {
                    "name": "addAdditionalFields",
                    "type": "html",
                    "value": '<span class=""><input type="button" class="btn btn-sm btn-success" value="Add Additional Field"></span>'
                }
            ];
        }

        if (name) {
            entries[0].type = "html";
            entries[0].value = name;
        }

        var valuesRow = [
            {
                'name': 'valuesValue_%count%',
                'label': 'Value',
                'type': 'text',
                'required': true
            },
            {
                'name': 'valuesLabel_en_%count%',
                'label': 'Label English',
                'type': 'text',
                'required': true
            },
            {
                'name': 'valuesLabel_fr_%count%',
                'label': 'Label French',
                'type': 'text',
                'required': true
            },
            {
                "name": "removeValues_%count%",
                "type": "html",
                "value": "<span class='red'><span class='icon icon-cross' title='Remove'></span></span>",
                "onAction": function (id, data, form) {
                    var number = id.replace("removeValues_", "");
                    //todo: need to decrease count
                    delete form.formData['label' + number];
                    delete form.formData['value' + number];
                    form.entries.forEach(function (oneEntry) {
                        if (oneEntry.type === 'group' && oneEntry.name === 'values') {
                            for (var i = oneEntry.entries.length - 1; i >= 0; i--) {
                                if (oneEntry.entries[i].name === 'valuesLabel_en_' + number) {
                                    oneEntry.entries.splice(i, 1);
                                }
                                else if (oneEntry.entries[i].name === 'valuesLabel_fr_' + number) {
                                    oneEntry.entries.splice(i, 1);
                                }
                                else if (oneEntry.entries[i].name === 'valuesValue_' + number) {
                                    oneEntry.entries.splice(i, 1);
                                }
                                else if (oneEntry.entries[i].name === 'removeValues_' + number) {
                                    oneEntry.entries.splice(i, 1);
                                }
                            }
                        }
                    });
                }
            }
        ];
        var additionalFieldsRow = [
            {
                'name': 'addName_%count%',
                'label': 'Name',
                'type': 'readonly'
            },
            {
                'name': 'addType_%count%',
                'label': 'Type',
                'type': 'readonly'
            },
            {
                "name": "editAddField%count%",
                "type": "html",
                "value": "<span class='red'><span class='icon icon-pencil' title='Edit'></span></span>",
                "onAction": function (id, data, form) {
                    var additionalFieldIndex = id.replace("editAddField", "");
                    currentScope.modalInstance.dismiss('cancel');
                    addCustomConfiguration(currentScope, variable, form.formData.name, angular.copy(form), parentIndex, additionalFieldIndex);
                }
            },
            {
                "name": "removeAddField%count%",
                "type": "html",
                "value": "<span class='red'><span class='icon icon-cross' title='Remove'></span></span>",
                "onAction": function (id, data, form) {

                    var number = id.replace("removeAddField", "");
                    //todo: need to decrease count
                    delete form.formData['label' + number];
                    delete form.formData['value' + number];
                    form.entries.forEach(function (oneEntry) {
                        if (oneEntry.type === 'group' && oneEntry.name === 'additionalFields') {
                            for (var i = oneEntry.entries.length - 1; i >= 0; i--) {
                                if (oneEntry.entries[i].name === 'addName_' + number) {
                                    oneEntry.entries.splice(i, 1);
                                }
                                else if (oneEntry.entries[i].name === 'addType_' + number) {
                                    oneEntry.entries.splice(i, 1);
                                }
                                else if (oneEntry.entries[i].name === 'editAddField' + number) {
                                    oneEntry.entries.splice(i, 1);
                                }
                                else if (oneEntry.entries[i].name === 'removeAddField' + number) {
                                    oneEntry.entries.splice(i, 1);
                                }
                            }
                        }
                    });
                    // delete from customConfAdd // it will always have one object
                    var theOnlyObject = Object.keys(currentScope.tempo.customConfAdd)[0];
                    currentScope.tempo.customConfAdd[theOnlyObject].splice(number, 1);
                }
            }
        ];

        var addAdditionalFieldsButtonIndex = getIndexOfEntry(entries, 'addAdditionalFields');
        entries[addAdditionalFieldsButtonIndex].onAction = function (id, data, form) {
            if (parentName === '@root@') {
                currentScope.modalInstance.dismiss('cancel');
                addCustomConfiguration(currentScope, variable, form.formData.name, angular.copy(form), parentIndex);
            } else {
                alert('Warning! Cannot add an additional field to an additional field');
            }
        };

        if (parentName === '@root@' || additionalFieldIndex) { // on load root [coming from form or from additional fields add] || edit additional fields
            for (var j = 0; j < valuesCount; j++) {
                var savedValue = values[j];
                var oneClone = angular.copy(valuesRow);

                for (var i = 0; i < oneClone.length; i++) {
                    oneClone[i].name = oneClone[i].name.replace("%count%", j);
                    if (oneClone[i].name.includes('valuesValue')) {
                        oneClone[i].value = savedValue.v;
                    }
                    if (oneClone[i].name.includes('valuesLabel_en')) {
                        oneClone[i].value = savedValue.label.en;
                    }
                    if (oneClone[i].name.includes('valuesLabel_fr')) {
                        oneClone[i].value = savedValue.label.fr;
                    }
                }
                var valuesIndex = getIndexOfEntry(entries, 'values');
                var tempVal = entries[valuesIndex].entries;
                entries[valuesIndex].entries = tempVal.concat(oneClone);
            }

            // not applicable for additional fields edit
            if (additionalFieldIndex === undefined) {
                var current;
                if (parentIndex !== undefined) {
                    current = currentScope.formData[variable][parentIndex].name;
                } else {
                    current = '@temponew@';
                }

                additionalFields = currentScope.tempo.customConfAdd[current];

                var newAdditionalFields = [];

                for (var j = 0; j < additionalFields.length; j++) {
                    var savedValue = additionalFields[j];
                    var oneClone = angular.copy(additionalFieldsRow);

                    for (var i = 0; i < oneClone.length; i++) {
                        oneClone[i].name = oneClone[i].name.replace("%count%", j);
                        if (oneClone[i].name.includes('addName')) {
                            oneClone[i].value = savedValue.name;
                        }
                        if (oneClone[i].name.includes('addType')) {
                            oneClone[i].value = savedValue.type;
                        }
                    }
                    newAdditionalFields = newAdditionalFields.concat(oneClone);
                }

                var additionalFieldsIndex = getIndexOfEntry(entries, 'additionalFields');
                entries[additionalFieldsIndex].entries = angular.copy(newAdditionalFields);
            }

        }

        var config = {
            'name': '',
            'label': '',
            'actions': {},
            "entries": entries
        };

        var popUpConfig = {};
        if (parentName === '@root@') {
            popUpConfig.name = 'addConfigInput';
            popUpConfig.label = 'Product Configuration';
        } else {
            popUpConfig.name = 'addConfigInput';
            popUpConfig.label = 'Additional Field Configuration';
        }

        var options = {
            timeout: $timeout,
            form: config,
            backdrop: false,
            'name': popUpConfig.name,
            'label': popUpConfig.label,
            "postBuild": function () {
                if (type) {
                    var arr1 = ['select'];
                    if (arr1.indexOf(type) !== -1) {
                        jQuery("#addConfigInput #values-wrapper").slideDown();
                        jQuery("#addConfigInput #addValues-wrapper").slideDown();
                    }
                    else {
                        jQuery("#addConfigInput #values-wrapper").slideUp();
                        jQuery("#addConfigInput #addValues-wrapper").slideUp();
                    }
                }
                else {
                    jQuery("#addConfigInput #values-wrapper").slideUp();
                    jQuery("#addConfigInput #addValues-wrapper").slideUp();
                }
            },
            'actions': [
                {
                    'type': 'submit',
                    'label': 'Submit',
                    'btn': 'primary',
                    'action': function (formData) {
                        var obj = constructObject(formData);

                        // check if the name exists TODO
                        if (parentName === '@root@') {
                            if (parentIndex === undefined) {
                                // add new
                                if (currentScope.tempo.customConfAdd['@temponew@'].length !== 0) {
                                    obj.additionalFields = currentScope.tempo.customConfAdd['@temponew@'];
                                }
                                currentScope.formData[variable].push(obj);
                            }
                            else {
                                if (currentScope.tempo.customConfAdd[currentScope.formData[variable][parentIndex].name].length !== 0) {
                                    obj.additionalFields = currentScope.tempo.customConfAdd[currentScope.formData[variable][parentIndex].name];
                                }
                                currentScope.formData[variable][parentIndex] = obj;
                            }
                            currentScope.formData[variable].sort(function (a, b) {
                                return parseFloat(a.order) - parseFloat(b.order);
                            });
                        }
                        else {
                            var current;
                            if (currentScope.formData[variable][parentIndex]) {
                                current = currentScope.formData[variable][parentIndex].name;
                            } else {
                                current = '@temponew@';
                            }

                            if (additionalFieldIndex === undefined) {// add new additional field
                                currentScope.tempo.customConfAdd[current].push(obj);
                            } else { // edit
                                currentScope.tempo.customConfAdd[current][additionalFieldIndex] = obj;
                            }

                            currentScope.modalInstance.dismiss('cancel');
                            currentScope.tempo.customConfAdd[current].sort(function (a, b) {
                                return parseFloat(a.order) - parseFloat(b.order);
                            });


                            addCustomConfiguration(currentScope, variable, '@root@', angular.copy(previousForm), parentIndex);
                        }

                        currentScope.modalInstance.dismiss('cancel');
                    }
                },
                {
                    'type': 'reset',
                    'label': 'Cancel',
                    'btn': 'danger',
                    'action': function () {
                        if (parentName !== '@root@') {
                            addCustomConfiguration(currentScope, variable, '@root@', angular.copy(previousForm), parentIndex);
                        }
                        currentScope.modalInstance.dismiss('cancel');
                    }
                }
            ]
        };
        buildFormWithModal(currentScope, $modal, options);
    }

    return {
        'addCustomConfiguration': addCustomConfiguration
    }

}]);
