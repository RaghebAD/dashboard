"use strict";

var kbBundlesModuleQaConfig = {
	'errors': {},
	'apiEndLimit': 1000,
	"maxLength": {},
	'permissions': {
		'bundles': {
			'list': ['knowledgebase', '/products/bundles', 'get'],
			'add': ['knowledgebase', '/tenant/products/bundles', 'post'],
			'edit': ['knowledgebase', '/products/bundles/:id', 'put'],
			'delete': ['knowledgebase', '/products/bundles/:id', 'delete'],
			'registerMigration': ['knowledgebase', '/migrate/data/register', 'post']
		}
	},

	form: {
		'entries': [
			{
				'name': 'id',
				'label': translation.id[LANG],
				'type': 'text',
				"tooltip": translation.optionalCustomId[LANG],
				'required': false
			},
			{
				'name': 'name',
				'label': translation.name[LANG],
				'type': 'text',
				'placeholder': 'Store 1',
				'maxlength': 44,
				"fieldMsg": translation.maximum44CharactersAllowed[LANG],
				'required': true
			},
			{
				'name': 'currency',
				'label': translation.currency[LANG],
				'type': 'text',
				'placeholder': 'USD',
				'maxlength': 3,
				'required': true
			},
			{
				"name": "type",
				"label": translation.type[LANG],
				"type": "select",
				"value": [
					{v: 'virtual', l: "Virtual"},
					{v: 'pos', l: translation.pos[LANG]},
					{v: 'warehouse', l: translation.warehouse[LANG]}
				]
			},
			{
				'name': 'status',
				'label': translation.status[LANG],
				'type': 'radio',
				'value': [
					{'v': 'active', 'l': translation.active[LANG]},
					{
						'v': 'disabled',
						'l': translation.disabled[LANG]
					}
				],
				'required': true
			},
			{
				'name': 'long',
				'label': translation.longitude[LANG],
				'type': 'readonly',
				'placeholder': '',
				'required': false
			},
			{
				'name': 'lat',
				'label': translation.latitude[LANG],
				'type': 'readonly',
				'placeholder': '',
				'required': false
			} 

		]
	}

};
