"use strict";
var bundlesService = soajsApp.components;

function generateGroupId(coreData) {
	if (coreData.groupId && coreData.groupId !== '') {
		return coreData.groupId;
	}
	if (coreData.udac && coreData.udac !== '') {
		return coreData.udac + '_' + Date.now();
	} else {
		return ''; // if the user didnt enter the udac yet (on draft for ex)
	}
}

bundlesService.service('bundlesModuleQaSrv', ['$timeout', 'ngDataApi', '$compile', '$modal', '$localStorage', '$cookies', 'Upload',
	function ($timeout, ngDataApi, $compile, $modal, $localStorage, $cookies, Upload) {
		
		function buildCategoriesFromProfile(currentScope, localStorage, profileRecord, cb) {
			var defaultLanguage = '';
			var getFullPath = function (category, profileRecord, path) {
				
				if (category['parents'].length === 0) { // root
					return path;
				} else { // 1 parent
					var records = Object.keys(profileRecord['categories']);
					var parent = category['parents'][0];
					for (var i = 0; i < records.length; i++) {
						if (records[i] === parent) {
							var current = profileRecord['categories'][records[i]];
							var label = profileRecord.taxonomies[records[i]].label[defaultLanguage].value;
							return getFullPath(current, profileRecord, label + " / " + path);
						}
					}
				}
			};
			
			for (var i = 0; i < profileRecord.languages.length; i++) {
				if (profileRecord.languages[i].selected) {
					defaultLanguage = profileRecord.languages[i].id;
				}
			}
			currentScope.defaultLang = defaultLanguage;
			
			function sortParent() {
				function doCategory(cat, cb) {
					catCount++;
					currentScope.progress = Math.ceil((catCount * 100) / catnames.length);
					if (!currentScope.$$phase) {
						currentScope.$apply();
					}
					if (!profileRecord.categories[cat].parents || profileRecord.categories[cat].parents.length === 0) {
						processCategoryLevel(cat, profileRecord.categories, function () {
							return cb(null, true);
						});
					}
					else {
						return cb(null, true);
					}
				}
				
				function processCategoryLevel(cat, categoriesArray, cb) {
					var label = cat;
					if (profileRecord.taxonomies[cat]) {
						label = profileRecord.taxonomies[cat].label[defaultLanguage].value;
					}
					else {
						console.log('Missing ' + cat);
					}
					var id = cat;
					if (categoriesArray[cat]) {
						if (categoriesArray[cat]['deprecated-towards']) {
							//get the new value
							id = categoriesArray[cat]['deprecated-towards'];
							label += " (deprecated), becomes: " + profileRecord.taxonomies[id].label[defaultLanguage].value;
						}
						
						if (categoriesArray[cat].parents && categoriesArray[cat].parents.length > 0) {
							label = getLabels(categoriesArray[cat].parents, defaultLanguage) + " / " + label;
						}
						
						var t = {
							"v": id,
							"l": label
						};
						
						if (categoriesArray[cat].attributes) {
							if (categoriesArray[cat].attributes.department) {
								var departments = [];
								for (var i = 0; i < categoriesArray[cat].attributes.department.length; i++) {
									var depId = categoriesArray[cat].attributes.department[i];
									if (profileRecord.taxonomies && profileRecord.taxonomies[depId] && profileRecord.taxonomies[depId].label) {
										departments.push({
											"v": depId,
											"l": profileRecord.taxonomies[depId].label[defaultLanguage].value
										});
									}
								}
								t["department"] = departments;
							}
							
							if (categoriesArray[cat].attributes.classification) {
								var classification = [];
								for (var i = 0; i < categoriesArray[cat].attributes.classification.length; i++) {
									var classId = categoriesArray[cat].attributes.classification[i];
									
									if (profileRecord.taxonomies && profileRecord.taxonomies[classId] && profileRecord.taxonomies[classId].label) {
										classification.push({
											"v": classId,
											"l": profileRecord.taxonomies[classId].label[defaultLanguage].value
										});
									}
								}
								t["classification"] = classification;
							}
							if (categoriesArray[cat].attributes.filters) {
								var filters = [];
								var filter = Object.keys(categoriesArray[cat].attributes.filters);
								for (var i = 0; i < filter.length; i++) {
									if (profileRecord.taxonomies && profileRecord.taxonomies[filter[i]] && profileRecord.taxonomies[filter[i]].label) {
										filters.push({
											"v": filter[i],
											"l": profileRecord.taxonomies[filter[i]].label[defaultLanguage].value
										});
									}
								}
								t["filters"] = filters;
								t["filtersObject"] = categoriesArray[cat].attributes.filters;
							}
						}
						
						categories.push(t);
						
						if (categoriesArray[cat].children && categoriesArray[cat].children.length > 0) {
							async.eachLimit(categoriesArray[cat].children, categoriesArray[cat].children.length, doCatChild, function () {
								return cb();
							});
						}
						else {
							return cb();
						}
					}
					else {
						return cb();
					}
					
					function doCatChild(oneChild, cb) {
						processCategoryLevel(oneChild, categoriesArray, cb);
					}
				}
				
				function getLabels(parentsArray, langCode) {
					var out = [];
					for (var i = 0; i < parentsArray.length; i++) {
						var oneParentCat = parentsArray[i];
						var label = profileRecord.taxonomies[oneParentCat].label[langCode].value;
						
						if (profileRecord.categories[oneParentCat].parents && profileRecord.categories[oneParentCat].parents.length > 0) {
							label = getLabels(profileRecord.categories[oneParentCat].parents, langCode) + " / " + label;
						}
						out.push(label);
					}
					
					
					return out.join(" / ");
				}
				
				if (profileRecord.treats) {
					var out = [];
					for (var i = 0; i < profileRecord.treats.length; i++) {
						var treatId = profileRecord.treats[i];
						var treatLabel = treatId;
						if (profileRecord.taxonomies[treatId]) {
							treatLabel = profileRecord.taxonomies[treatId].label[defaultLanguage].value;
						}
						out.push({
							'v': treatId,
							'l': treatLabel
						});
					}
					profileRecord.treats = out;
				}
				if (localStorage.profile && localStorage.profile.categories && localStorage.profile.ts && localStorage.profile.env && localStorage.profile.ts === profileRecord.ts && localStorage.profile.env === currentScope.$parent.currentSelectedEnvironment.toUpperCase()) {
					return cb(null, localStorage.profile.categories);
				}
				else {
					var categories = [];
					var catnames = [];
					if (profileRecord.categories) {
						catnames = Object.keys(profileRecord.categories);
					}
					var catCount = 0;
					async.eachSeries(catnames, doCategory, function () {
						delete profileRecord.taxonomies;
						localStorage.profile = {
							"categories": categories,
							"ts": profileRecord.ts,
							"env": currentScope.$parent.currentSelectedEnvironment.toUpperCase()
						};
						return cb(null, categories);
					});
				}
			}
			
			sortParent();
		}
		
		function cleanArray(array, operator, param1, param2, param3, param4) {
			// loop through the array and delete the params provided if they are null (based on the operator param)
			// and ==> if all of them are empty, or ==> if one of them is empty
			if (!array) {
				return;
			}
			
			for (var i = array.length - 1; i >= 0; i--) {
				var current = array[i];
				if (operator === 'and') {
					if (!current[param1] && !current[param2] && !current[param3] && !current[param4]) {
						array.splice(i, 1);
					}
				}
				else { // default OR
					if (!current[param1] || !current[param2] || !current[param3] || !current[param4]) {
						array.splice(i, 1);
					}
				}
			}
		}
		
		function getProductInfo(array, serial) {
			if (array) {
				for (var i = 0; i < array.length; i++) {
					if (array[i].serial === serial) {
						return array[i];
					}
				}
			}
			return {};
		}
		
		function bundleDataFormtoDb(obj, tempo, cb) {
			// takes form object as input, and returns the callback with the output object
			// clean empty properties
			var languages = ["en", "fr"]; // -=-=-=-=-
			
			// product's array
			var productData, media, images, imagesArr, videos, resources, resourcesArr, videosArr, crossSellPath, upsellPath;
			var attributes, variation;
			var rules, prerequisiteProduct, incompatibility, products;
			var upi;
			
			if (productData = obj.productData) {
				if (media = productData.media) {
					if (images = media.images) {
						for (var i = 0; i < languages.length; i++) {
							var currentLanguage = languages[i];
							cleanArray(imagesArr = images[currentLanguage], 'and', 'url', 'altTag');
						}
					}
					if (videos = media.videos) {
						for (var i = 0; i < languages.length; i++) {
							var currentLanguage = languages[i];
							cleanArray(videosArr = videos[currentLanguage], 'and', 'url');
						}
					}
					if (resources = media.resources) {
						for (var i = 0; i < languages.length; i++) {
							var currentLanguage = languages[i];
							cleanArray(resourcesArr = resources[currentLanguage], 'and', 'url');
						}
					}
				}
				
				cleanArray(crossSellPath = productData.crossSellPath, 'and', 'udac', 'serial');
				cleanArray(upsellPath = productData.upsellPath, 'and', 'udac', 'serial');
			}
			
			if (attributes = obj.attributes) {
				cleanArray(variation = attributes.variations, 'and', 'type', 'value');
				
				if (attributes.variations) {
					delete attributes.variations[''];
				}
			}
			
			if (rules = obj.rules) {
				cleanArray(prerequisiteProduct = rules.prerequisiteProduct, 'and', 'serial', 'udac');
				
				if (incompatibility = rules.incompatibility) {
					cleanArray(products = incompatibility.products, 'and', 'serial', 'udac');
				}
			}
			
			// clean upi data
			if (upi = obj.coreData.upi) {
				if (!upi.upc || upi.upc === '') {
					delete upi.upc;
				}
			}
			
			// reconstruct bundle data using the serial
			var bundleData = obj.bundleData.items;
			for (var i = 0; i < bundleData.length; i++) {
				var correspondingProduct = getProductInfo(tempo.availableProducts, bundleData[i].serial);
				bundleData[i].udac = correspondingProduct.udac;
			}
			
			obj.coreData.groupId = generateGroupId(obj.coreData);
			
			var output = {
				attributes: obj.attributes,
				productConfiguration: obj.productConfiguration,
				bundleData: obj.bundleData,
				coreData: obj.coreData,
				legal: obj.legal,
				merchantMeta: obj.merchantMeta,
				productData: obj.productData,
				fulfillmentRules: obj.fulfillmentRules,
				rules: obj.rules,
				status: obj.status
			};
			
			return cb(output);
		}
		
		function callAPI(currentScope, config, callback) {
			getSendDataFromServer(currentScope, ngDataApi, config, callback);
		}
		
		function getEntriesFromApi(currentScope, opts, callback) {
			callAPI(currentScope, opts, callback);
		}
		
		function printGrid(currentScope, response) {
			
			for (var i = 0; i < response.length; i++) {
				var current = response[i];
				current.bundleName = current.productData.title.en;
				current.price = current.merchantMeta.pricing.price;
				current.udac = current.coreData.udac;
			}
			
			var options = {
				'grid': {
					recordsPerPageArray: [5, 10, 50, 100],
					'columns': [
						{'label': translation.bundle.bundleName[LANG], 'field': 'bundleName'},
						{'label': translation.mpFields.serial[LANG], 'field': 'serial'},
						{'label': "UDAC", 'field': 'udac'},
						{'label': translation.mpFields.price[LANG], 'field': 'price'},
						{'label': translation.mpFields.status[LANG], 'field': 'status'}
					],
					'defaultLimit': 50
				},
				'defaultSortField': 'name',
				'data': response,
				'left': [],
				'top': []
			};
			
			options.left.push({
				'icon': 'search',
				'label': translation.view[LANG],
				'handler': 'viewBundle'
			});
			
			options.left.push({
				'label': translation.edit[LANG],
				'icon': 'pencil2',
				'handler': 'editBundle'
			});
			
			options.left.push({
				'label': translation.disableActivate[LANG],
				'icon': 'spinner9',
				'msg': translation.areYouSureThatYouWantToChangeStatusOfThisBundle[LANG],
				'handler': 'changeStatus'
			});
			
			options.left.push({
				'label': translation.delete[LANG],
				'icon': 'cross',
				'msg': translation.areYouSureYouWantToDeleteThisBundle[LANG],
				'handler': 'deleteBundle'
			});
			
			options.top.push({
				'label': translation.activateProduct[LANG],
				'msg': translation.areYouSureYouWantToActivateTheSelectedProduct[LANG],
				'handler': 'activate'
			});
			
			options.top.push({
				'label': translation.disableProduct[LANG],
				'msg': translation.areYouSureYouWantToActivateTheSelectedProduct[LANG],
				'handler': 'disable'
			});
			
			buildGrid(currentScope, options);
			var e = angular.element(document.getElementById('BundlesL'));
			e.html("<nglist></nglist>");
			$compile(e.contents())(currentScope);
		}
		
		function extractFilesFromPostedData($scope) {
			var formUrlFiles = [];
			if ($scope.response.profile.languages) {
				
			}
			var lang = ["en", "fr"];
			if ($scope.formData.media && $scope.formData.media.upload) {
				if ($scope.formData.media.upload.images) {
					lang.forEach(function (l) {
						if ($scope.formData.media.upload.images[l]) {
							for (var i in $scope.formData.media.upload.images[l]) {
								if (typeof $scope.formData.media.upload.images[l][i] === 'object') {
									var obj = $scope.formData.media.upload.images[l][i];
									obj.language = l;
									formUrlFiles.push(obj);
								}
							}
						}
					});
				}
			}
			
			return formUrlFiles;
		}
		
		function imageUpload($scope, postData, images, cb) {
			if (postData.productData && postData.productData.media.images) {
				var lang = ["en", "fr"];
				var doLang = function (l, cback) {
					images[l] = [];
					if (postData.productData.media.images[l] && postData.productData.media.images[l].length !== 0) {
						if (postData.productData.media.images[l][0].url === '') {
							postData.productData.media.images[l] = [];
							return cback();
						}
						
						async.mapSeries(postData.productData.media.images[l], function (oneImg, uCb) {
							if (oneImg.type !== 'ypg_media_server' && oneImg.url !== undefined && oneImg.url !== '') {
								// TODO: remove condition when data is cleaned
								if (oneImg.type === 'main' || oneImg.type === '') {
									var opt = {
										"method": "send",
										"routeName": '/knowledgebase/products/addImageUrl',
										"params": {
											"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
										},
										"data": {
											"url": oneImg.url
										}
									};
									getSendDataFromServer($scope, ngDataApi, opt, function (error, response) {
										if (error) {
											return uCb(error.message);
										}
										else {
											var image = {
												"url": response.original,
												"type": response.type,
												"resource": response.resource
											};
											if (oneImg.altTag) {
												image.altTag = oneImg.altTag
											}
											images[l].push(image);
											return uCb(null, true)
										}
									});
								}
							}
							else {
								if (oneImg.type === 'ypg_media_server') {
									if (oneImg.altTag && oneImg.altTag === '') {
										delete oneImg.altTag;
									}
									images[l].push(oneImg);
								}
								return uCb(null, true);
							}
						}, function () {
							return cback(null, true);
						});
					}
					else {
						cback();
					}
				};
				async.each(lang, doLang, function () {
					postData.productData.media.images = images;
					return cb(null, true);
				});
			}
			else {
				return cb(null, true);
			}
		}
		
		function uploadFile($scope, files, url, cb) {
			var err = [], myResponse = [];
			if (files && files.length > 0) {
				var max = files.length, counter = 0;
				for (var i = 0; i < files.length; i++) {
					if (files[i]) {
						var config = {
							language: files[i].language,
							url: url,
							file: files[i]
						};
						doUpload(config, function (error, response) {
							if (error) {
								err.push(error.message);
							}
							else {
								myResponse.push(response[0]);
							}
							counter++;
							if (counter === max) {
								return (err.length > 0) ? cb(err) : cb(null, myResponse);
							}
						});
					}
				}
			}
			else {
				cb(null, myResponse);
			}
			function doUpload(config, cb) {
				var opt = {
					"method": "send",
					"file": config.file,
					"routeName": config.url,
					"upload": true,
					"params": {
						"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
					}
				};
				
				getSendDataFromServer($scope, ngDataApi, opt, function (error, response) {
					if (error) {
						return cb(new Error(translation.errorOccurredWhileUploadingFile[LANG] + " " + config.file));
					}
					else {
						response[0].language = config.language;
						return cb(null, response);
					}
				});
			}
		}
		
		function migrateBundles(currentScope) {
			var bundles = currentScope.grid.rows;
			
			$modal.open({
				templateUrl: "migration.html",
				size: 'lg',
				backdrop: true,
				keyboard: true,
				controller: function ($scope, $modalInstance) {
					var otherEnvs = angular.copy($localStorage.environments);
					for (var i = otherEnvs.length - 1; i >= 0; i--) {
						if (otherEnvs[i].code === currentScope.currentSelectedEnvironment.toUpperCase()) {
							otherEnvs.splice(i, 1);
						}
						else if (otherEnvs[i].code === 'DASHBOARD') {
							otherEnvs.splice(i, 1);
						}
					}
					fixBackDrop();
					$scope.title = 'Bundles Migration';
					$scope.data = {
						all: false,
						envs: otherEnvs
					};
					
					$scope.data.bundles = [];
					var originalBundles = [];
					bundles.forEach(function (oneProduct) {
						originalBundles.push({
							"_id": oneProduct._id,
							"name": oneProduct.productData.title["en"]
						});
					});
					$scope.data.bundles = angular.copy(originalBundles);
					
					$scope.filterData = function (query) {
						if (query && query !== '' && query.length >= 3) {
							var filtered = [];
							for (var i = 0; i < $scope.data.bundles.length; i++) {
								var pattern = query.toLowerCase();
								if ($scope.data.bundles[i].name.toLowerCase().indexOf(pattern) !== -1) {
									filtered.push($scope.data.bundles[i]);
								}
							}
							$scope.data.bundles = filtered;
						}
						else {
							$scope.data.bundles = originalBundles;
						}
					};
					
					$scope.submit = function () {
						
						if (!$scope.data.all && !$scope.data.selectedBundles) {
							alert("Select at least one bundle");
							return;
						}
						
						if (!$scope.data.toEnv) {
							alert("Select a destination environment");
							return;
						}
						
						var postData = {
							"type": "bundles",
							"all": $scope.data.all,
							"toEnv": $scope.data.toEnv.toUpperCase()
						};
						if (!$scope.data.all) {
							postData.ids = Object.keys($scope.data.selectedBundles)
						}
						else {
							postData.code = currentScope.code;
							postData.merchantId = currentScope.merchantId;
						}
						// if (currentScope.access.owner.bundles.registerMigration || currentScope.access.tenant.bundles.registerMigration) {
						var opts = {
							"routeName": "/knowledgebase/migrate/data/register",
							"method": "send",
							"params": {
								"__env": currentScope.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						};
						overlayLoading.show();
						getSendDataFromServer(currentScope, ngDataApi, opts, function (error) {
							overlayLoading.hide();
							if (error) {
								$scope.message = {
									"danger": true,
									"text": error.message
								};
							}
							else {
								$scope.message = {};
								$scope.data = {};
								currentScope.$parent.displayAlert('success', 'Bundles Registered for Migration.');
								$modalInstance.close();
							}
						});
						// }
						// else {
						// 	$modalInstance.close();
						// }
					};
					
					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};
				}
			});
		}
		
		function listMigrationBundles(currentScope) {
			var opts = {
				"routeName": "/knowledgebase/migrate/data/list",
				"method": "get",
				"params": {
					"__env": currentScope.$parent.currentSelectedEnvironment.toUpperCase(),
					"type": "bundles"
				}
			};
			getEntriesFromApi(currentScope, opts, function (error, response) {
				if (error) {
					currentScope.$parent.displayAlert('danger', error.message);
				}
				else {
					var options = {
						'grid': {
							recordsPerPageArray: [5, 10, 50, 100],
							'columns': [
								{'label': translation.fromEnvironment[LANG], 'field': 'fromEnv'},
								{'label': translation.toEnvironment[LANG], 'field': 'toEnv'},
								{'label': translation.type[LANG], 'field': 'collection'},
								{'label': translation.label[LANG], 'field': 'label'}
							
							],
							'defaultLimit': 50
						},
						'defaultSortField': 'toEnv',
						'data': response,
						'left': [],
						'top': []
					};
					
					options.top.push({
						'label': translation.deleteEntries[LANG],
						'msg': translation.areYouSureYouWantToDeleteTheSelectedEntry[LANG],
						'handler': 'removeMigrationBundles'
					});
					
					buildGrid(currentScope, options);
					var e = angular.element(document.getElementById('migrationL'));
					e.html("<nglist></nglist>");
					$compile(e.contents())(currentScope);
				}
			});
		}
		
		function removeMigrationBundles(currentScope) {
			var config = {
				'routeName': "/knowledgebase/migrate/data/remove",
				"params": {
					'id': '%id%',
					"__env": currentScope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				'msg': {
					'error': translation.oneOrMoreOfTheSelectedEntriesWasNotRemoved[LANG],
					'success': translation.selectedEntriesHaveBeenRemoved[LANG]
				}
			};
			
			multiRecordUpdate(ngDataApi, currentScope, config, function () {
				currentScope.listMigrationBundles();
			});
		}
		
		return {
			getEntriesFromApi: getEntriesFromApi,
			buildCategoriesFromProfile: buildCategoriesFromProfile,
			bundleDataFormtoDb: bundleDataFormtoDb,
			printGrid: printGrid,
			extractFilesFromPostedData: extractFilesFromPostedData,
			imageUpload: imageUpload,
			uploadFile: uploadFile,
			migrateBundles: migrateBundles,
			listMigrationBundles: listMigrationBundles,
			removeMigrationBundles: removeMigrationBundles
		}
	}]
);