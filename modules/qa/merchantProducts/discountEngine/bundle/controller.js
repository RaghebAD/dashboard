"use strict";
var bundlesApp = soajsApp.components;

bundlesApp.controller('bundlesListModuleQaCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$cookies', 'bundlesModuleQaSrv',
	function ($scope, $modal, ngDataApi, injectFiles, $timeout, $cookies, bundlesModuleQaSrv) {
		
		$scope.$parent.isUserLoggedIn();
		$scope.ModuleQaMpLocation = ModuleQaMpLocation;
		if (!$scope.$parent.currentSelectedEnvironment) {
			$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
		}
		$scope.access = {};
		constructModulePermissions($scope, $scope.access, kbBundlesModuleQaConfig.permissions);
		
		
		$scope.viewBundle = function (oneDataRecord) {
			
			var config = {
				"method": "get",
				"routeName": "/knowledgebase/product/bundles/" + oneDataRecord._id,
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			
			getSendDataFromServer($scope, ngDataApi, config, function (error, response) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					var profileRecord = response.profile;
					var defaultLanguage = '';
					for (var i = 0; i < profileRecord.languages.length; i++) {
						if (profileRecord.languages[i].selected) {
							defaultLanguage = profileRecord.languages[i].id;
						}
					}
					var lang = [];
					if (response.bundle.productData.title) {
						
					}
					if (response.bundle.productData.desc_short) {
						
					}
					if (response.bundle.productData.desc_full) {
						for (var z = 0; z < response.bundle.productData.desc_full.length; z++) {
							if (lang.indexOf(response.bundle.productData.desc_full[z].language) === -1) {
								lang.push(response.bundle.productData.desc_full[z].language);
							}
						}
					}
					
					$modal.open({
						templateUrl: ModuleQaMpLocation + "/discountEngine/bundle/directives/previewBundle.tmpl",
						size: 'lg',
						backdrop: true,
						keyboard: true,
						controller: function ($scope, $modalInstance) {
							$scope.lang = lang;
							$scope.taxonomies = profileRecord.taxonomies;
							$scope.selectedLang = defaultLanguage;
							$scope.languages = profileRecord.languages;
							$scope.title = translation.viewingOneProduct[LANG];
							$scope.data = response.bundle;
							fixBackDrop();
							setTimeout(function () {
								highlightMyCode()
							}, 500);
							$scope.ok = function () {
								$modalInstance.dismiss('ok');
							};
						}
					});
				}
			});
		};
		
		$scope.editBundle = function (data) {
			var path = "#/discountEngine/bundles/edit/" + data._id;
			$cookies.put("soajs_current_route", path.replace("#", ""));
			window.open(path, '');
		};
		
		$scope.addBundle = function () {
			var path = "#/discountEngine/bundles/add";
			$cookies.put("soajs_current_route", path.replace("#", ""));
			window.open(path, '');
		};
		
		$scope.migrateBundles = function () {
			bundlesModuleQaSrv.migrateBundles($scope);
		};
		
		$scope.listMigrationBundles = function () {
			bundlesModuleQaSrv.listMigrationBundles($scope);
		};
		
		$scope.removeMigrationBundles = function (data) {
			bundlesModuleQaSrv.removeMigrationBundles($scope);
		};
		
		$scope.changeStatus = function (data) {
			var status = (data.status === 'active') ? 'inactive' : 'active';
			var opts;
			opts = {
				"method": "put",
				"routeName": "/knowledgebase/product/bundles/" + data._id + "/status",
				"params": {
					'status': status,
					'code': $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			
			overlayLoading.show();
			getSendDataFromServer($scope, ngDataApi, opts, function (error) {
				overlayLoading.hide();
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.$parent.displayAlert('success', ' Bundle Status Changed Successfully.');
					$scope.listBundles();
				}
			});
			
		};
		
		$scope.listBundles = function () {
			var opts = {
				"routeName": "/knowledgebase/product/bundles",
				"method": "get",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			
			bundlesModuleQaSrv.getEntriesFromApi($scope, opts, function (error, response) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					bundlesModuleQaSrv.printGrid($scope, response.records);
				}
			});
			
		};
		
		$scope.deleteBundle = function (data) {
			var config = {
				"method": "del",
				"routeName": "/knowledgebase/product/bundles/" + data._id,
				"params": {
					'code': $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
			
			getSendDataFromServer($scope, ngDataApi, config, function (error) {
				if (error) {
					$scope.$parent.displayAlert('danger', error.message);
				}
				else {
					$scope.$parent.displayAlert('success', ' Bundle Deleted Successfully.');
					$scope.listBundles();
				}
			});
			
		};
		
		$scope.activate = function () {
			var config = {
				"method": "put",
				"routeParam": true,
				'routeName': "/knowledgebase/product/bundles/%id%/status",
				"params": {
					'status': 'active',
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				'msg': {
					'error': translation.oneOrMoreOfTheSelectedProductStatusWasNotChanged[LANG],
					'success': translation.selectedProductStatusHaveBeenUpdated[LANG]
				}
			};
			for (var i = $scope.grid.rows.length - 1; i >= 0; i--) {
				if ($scope.grid.rows[i].selected && $scope.grid.rows[i].status === 'draft') {
					$scope.grid.rows[i].selected = false;
				}
			}
			overlayLoading.show();
			multiRecordUpdate(ngDataApi, $scope, config, function () {
				overlayLoading.hide();
				$scope.listBundles();
			});
		};
		
		$scope.disable = function () {
			var config = {
				"method": "put",
				"routeParam": true,
				'routeName': "/knowledgebase/product/bundles/%id%/status",
				"params": {
					'status': 'inactive',
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				'msg': {
					'error': translation.oneOrMoreOfTheSelectedProductStatusWasNotChanged[LANG],
					'success': translation.selectedProductStatusHaveBeenUpdated[LANG]
				}
			};
			
			for (var i = $scope.grid.rows.length - 1; i >= 0; i--) {
				if ($scope.grid.rows[i].selected && $scope.grid.rows[i].status === 'draft') {
					$scope.grid.rows[i].selected = false;
				}
			}
			overlayLoading.show();
			multiRecordUpdate(ngDataApi, $scope, config, function () {
				overlayLoading.hide();
				$scope.listBundles();
			});
		};
		
		// on init, call list Bundles
		$scope.listBundles();
		
		injectFiles.injectCss(ModuleQaMpLocation + "/merchants.css");
	}]);
