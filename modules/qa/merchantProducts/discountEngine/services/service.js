"use strict";
var myService = soajsApp.components;

myService.service('discountModuleQaSrv', ['$timeout', 'ngDataApi', '$compile', '$modal', '$localStorage', '$cookies', 'Upload',
	function ($timeout, ngDataApi, $compile, $modal, $localStorage, $cookies, Upload) {
		
		function buildCategoriesFromProfile(currentScope, localStorage, profileRecord, cb) {
			var defaultLanguage = '';
			var getFullPath = function (category, profileRecord, path) {
				
				if (category['parents'].length === 0) { // root
					return path;
				} else { // 1 parent
					var records = Object.keys(profileRecord['categories']);
					var parent = category['parents'][0];
					for (var i = 0; i < records.length; i++) {
						if (records[i] === parent) {
							var current = profileRecord['categories'][records[i]];
							var label = profileRecord.taxonomies[records[i]].label[defaultLanguage].value;
							return getFullPath(current, profileRecord, label + " / " + path);
						}
					}
				}
			};
			
			for (var i = 0; i < profileRecord.languages.length; i++) {
				if (profileRecord.languages[i].selected) {
					defaultLanguage = profileRecord.languages[i].id;
				}
			}
			currentScope.defaultLang = defaultLanguage;
			
			function sortParent() {
				function doCategory(cat, cb) {
					catCount++;
					currentScope.progress = Math.ceil((catCount * 100) / catnames.length);
					if (!currentScope.$$phase) {
						currentScope.$apply();
					}
					if (!profileRecord.categories[cat].parents || profileRecord.categories[cat].parents.length === 0) {
						processCategoryLevel(cat, profileRecord.categories, function () {
							return cb(null, true);
						});
					}
					else {
						return cb(null, true);
					}
				}
				
				function processCategoryLevel(cat, categoriesArray, cb) {
					var label = cat;
					if (profileRecord.taxonomies[cat]) {
						label = profileRecord.taxonomies[cat].label[defaultLanguage].value;
					}
					else {
						console.log('Missing ' + cat);
					}
					var id = cat;
					if (categoriesArray[cat]) {
						if (categoriesArray[cat]['deprecated-towards']) {
							//get the new value
							id = categoriesArray[cat]['deprecated-towards'];
							label += " (deprecated), becomes: " + profileRecord.taxonomies[id].label[defaultLanguage].value;
						}
						
						if (categoriesArray[cat].parents && categoriesArray[cat].parents.length > 0) {
							label = getLabels(categoriesArray[cat].parents, defaultLanguage) + " / " + label;
						}
						
						var t = {
							"v": id,
							"l": label
						};
						
						if (categoriesArray[cat].attributes) {
							if (categoriesArray[cat].attributes.department) {
								var departments = [];
								for (var i = 0; i < categoriesArray[cat].attributes.department.length; i++) {
									var depId = categoriesArray[cat].attributes.department[i];
									if (profileRecord.taxonomies && profileRecord.taxonomies[depId] && profileRecord.taxonomies[depId].label) {
										departments.push({
											"v": depId,
											"l": profileRecord.taxonomies[depId].label[defaultLanguage].value
										});
									}
								}
								t["department"] = departments;
							}
							
							if (categoriesArray[cat].attributes.classification) {
								var classification = [];
								for (var i = 0; i < categoriesArray[cat].attributes.classification.length; i++) {
									var classId = categoriesArray[cat].attributes.classification[i];
									
									if (profileRecord.taxonomies && profileRecord.taxonomies[classId] && profileRecord.taxonomies[classId].label) {
										classification.push({
											"v": classId,
											"l": profileRecord.taxonomies[classId].label[defaultLanguage].value
										});
									}
								}
								t["classification"] = classification;
							}
							if (categoriesArray[cat].attributes.filters) {
								var filters = [];
								var filter = Object.keys(categoriesArray[cat].attributes.filters);
								for (var i = 0; i < filter.length; i++) {
									if (profileRecord.taxonomies && profileRecord.taxonomies[filter[i]] && profileRecord.taxonomies[filter[i]].label) {
										filters.push({
											"v": filter[i],
											"l": profileRecord.taxonomies[filter[i]].label[defaultLanguage].value
										});
									}
								}
								t["filters"] = filters;
								t["filtersObject"] = categoriesArray[cat].attributes.filters;
							}
						}
						
						categories.push(t);
						
						if (categoriesArray[cat].children && categoriesArray[cat].children.length > 0) {
							async.eachLimit(categoriesArray[cat].children, categoriesArray[cat].children.length, doCatChild, function () {
								return cb();
							});
						}
						else {
							return cb();
						}
					}
					else {
						return cb();
					}
					
					function doCatChild(oneChild, cb) {
						processCategoryLevel(oneChild, categoriesArray, cb);
					}
				}
				
				function getLabels(parentsArray, langCode) {
					var out = [];
					for (var i = 0; i < parentsArray.length; i++) {
						var oneParentCat = parentsArray[i];
						var label = profileRecord.taxonomies[oneParentCat].label[langCode].value;
						
						if (profileRecord.categories[oneParentCat].parents && profileRecord.categories[oneParentCat].parents.length > 0) {
							label = getLabels(profileRecord.categories[oneParentCat].parents, langCode) + " / " + label;
						}
						out.push(label);
					}
					
					
					return out.join(" / ");
				}
				
				if (profileRecord.treats) {
					var out = [];
					for (var i = 0; i < profileRecord.treats.length; i++) {
						var treatId = profileRecord.treats[i];
						var treatLabel = treatId;
						if (profileRecord.taxonomies[treatId]) {
							treatLabel = profileRecord.taxonomies[treatId].label[defaultLanguage].value;
						}
						out.push({
							'v': treatId,
							'l': treatLabel
						});
					}
					profileRecord.treats = out;
				}
				if (localStorage.profile && localStorage.profile.categories && localStorage.profile.ts && localStorage.profile.env && localStorage.profile.ts === profileRecord.ts && localStorage.profile.env === currentScope.$parent.currentSelectedEnvironment.toUpperCase()) {
					return cb(null, localStorage.profile.categories);
				}
				else {
					var categories = [];
					var catnames = [];
					if (profileRecord.categories) {
						catnames = Object.keys(profileRecord.categories);
					}
					var catCount = 0;
					async.eachSeries(catnames, doCategory, function () {
						delete profileRecord.taxonomies;
						localStorage.profile = {
							"categories": categories,
							"ts": profileRecord.ts,
							"env": currentScope.$parent.currentSelectedEnvironment.toUpperCase()
						};
						return cb(null, categories);
					});
				}
			}
			
			sortParent();
		}
		
		function bundleDataFormtoDb(input, cb) {
			// takes form object as input, and returns the callback with the output object
			var output = {};
			
			return cb(output);
		}
		
		function callAPI(currentScope, config, callback) {
			getSendDataFromServer(currentScope, ngDataApi, config, callback);
		}
		
		function getEntriesFromApi(currentScope, opts, callback) {
			callAPI(currentScope, opts, callback);
		}
		
		function printGrid(currentScope, response) {
			
			var options = {
				'grid': {
					recordsPerPageArray: [5, 10, 50, 100],
					'columns': [
						{'label': "Type", 'field': 'type'},
						{'label': "Start Date", 'field': 'startDate'},
						{'label': "End Date", 'field': 'endDate'},
						{'label': "Status", 'field': 'status'},
						{'label': "Description", 'field': 'description'}
					],
					'defaultLimit': 50
				},
				'defaultSortField': '',
				'data': response,
				'left': [],
				'top': []
			};
			
			options.left.push({
				'label': translation.edit[LANG],
				'icon': 'pencil2',
				'handler': 'editRule'
			});
			
			options.left.push({
				'label': translation.disableActivate[LANG],
				'icon': 'spinner9',
				'msg': "Are you sure you want to change the status ?",
				'handler': 'changeStatus'
			});
			
			options.left.push({
				'label': translation.delete[LANG],
				'icon': 'cross',
				'msg': "Are you sure you want to delete this discount ?",
				'handler': 'deleteDiscount'
			});
			
			buildGrid(currentScope, options);
		}
		
		
		return {
			'getEntriesFromApi': getEntriesFromApi,
			"buildCategoriesFromProfile": buildCategoriesFromProfile,
			'bundleDataFormtoDb': bundleDataFormtoDb,
			'printGrid': printGrid
		}
	}]);