"use strict";
var pendingMerchants = soajsApp.components;
pendingMerchants.controller('pendingMerchantsModuleQaCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$routeParams', '$cookies', function ($scope, $modal, ngDataApi, injectFiles, $timeout, $routeParams, $cookies) {
	$scope.ModuleQaMpLocation = ModuleQaMpLocation;

	$scope.$parent.isUserLoggedIn();
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, pendingMerchantsConfig.permissions);
	
	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}
	
	$scope.formConfig = {
		"entries": [
			{
				"name": "tenant",
				"label": translation.tenant[LANG] ,
				"type": "select",
				"required": true,
				"value": [
					{
						"v": "noTenantSelected",
						"l": translation.createNewTenant[LANG]
					}
				],
				"onAction": function (id, data, form) {
					if (data === "noTenantSelected") {
						form.entries.push({
							"name": "name",
							"label": translation.newTenantName[LANG],
							"type": "text",
							"required": true
						})
					}
					else if (form.entries.length > 1) {
						form.entries.splice(1)
					}
				}
			}
		]
	};

	$scope.listEntries = function () {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/knowledgebase/owner/merchants/pending/list",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				response.records.forEach(function (oneMerchant) {
					oneMerchant.name = oneMerchant.merchant.name;
					oneMerchant.merchantId = oneMerchant.merchant.merchantId;
					oneMerchant.domain = oneMerchant.merchant.domain;
				});
				var options = {
					'grid': {
						recordsPerPageArray: [5, 10, 50, 100],
						'columns': [
							{'label': translation.merchant[LANG], 'field': 'name'},
							{'label': translation.merchantId[LANG], 'field': 'merchantId'},
							{'label': translation.domain[LANG], 'field': 'domain'},
							{'label': translation.status[LANG], 'field': 'status'},
							{'label': translation.created[LANG], 'field': 'created', 'filter': "prettyLocalDate"}

						],
						'defaultLimit': 100
					},
					'defaultSortField': 'created',
					defaultSortASC: true,
					'data': response.records,
					'left': [
						{
							'icon': 'search',
							'label': translation.viewItem[LANG],
							'handler': 'viewEntry'
						},
						{
							'label': translation.approve[LANG],
							'icon': 'checkmark',
							'handler': 'addEntry'
						},
						{
							'label': translation.reject[LANG],
							'icon': 'cross',
							'msg': translation.areYouSureYouWantToRejectThisMerchant[LANG],
							'handler': 'reject'
						}
					]
				};
				buildGrid($scope, options);
			}
		});
	};

	$scope.viewEntry = function (oneDataRecord) {
		var languages = [];
		var translated = ['description','information.customer_service','information.terms_condition','information.return_policy'];
		translated.forEach(function (oneTrans) {
			if(oneDataRecord.merchant[oneTrans] && Array.isArray(oneDataRecord.merchant[oneTrans])){
				oneDataRecord.merchant[oneTrans].forEach(function(oneVal){
					if(oneVal.language && languages.indexOf(oneVal.language) === -1){
						languages.push(oneVal.language);
					}
				});
			}
		});
		$modal.open({
			templateUrl: "infoBox.html",
			size: 'lg',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewingOneMerchant[LANG];
				$scope.data = oneDataRecord;
				$scope.languages = languages;
				fixBackDrop();
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};

	$scope.addEntry = function (data) {
		var formFields = angular.copy($scope.formConfig.entries);
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/dashboard/tenant/list"
		}, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'dashboard');
			}
			else {
				$scope.tenantsList = {
					"rows": response
				};
				response.forEach(function (oneTenant) {
					if (oneTenant.type === "client") {
						formFields[0].value.push({
							"v": oneTenant.code,
							"l": oneTenant.name
						});
					}
				});
				var options = {
					timeout: $timeout,
					form: {"entries": formFields},
					'name': 'addTenant',
					'label': translation.addTenant[LANG],
					'actions': [
						{
							'type': 'submit',
							'label': translation.submit[LANG],
							'btn': 'primary',
							'action': function (formData) {
								if (formData.tenant && formData.tenant !== "noTenantSelected" && formData.tenant.length > 0) {
									data.source = formData.tenant;
									$scope.approve(data);
								}
								else if (formData.tenant === "noTenantSelected" && formData.name) {
									var tCode = $scope.generateTenantCode(formData.name);
									var tType = "client";
									var postData = {
										'type': tType,
										'code': tCode,
										'name': formData.name,
										'email': data.merchant.information.email,
										'description': formData.name + " Tenant application",
										'tag': "Merchant"
									};
									overlayLoading.show();
									getSendDataFromServer($scope, ngDataApi, {
										"method": "send",
										"routeName": "/dashboard/tenant/add",
										"data": postData
									}, function (error, response) {
										if (error) {
											overlayLoading.hide();
											return $scope.form.displayAlert('danger', error.message);
										}
										else {
											var tId = response.id;
											var ttl = 7 * 24;
											var postData = {
												'description': 'Dashboard application for DSBRD_TNCOM package',
												'_TTL': ttl.toString(),
												'productCode': "DSBRD",
												'packageCode': "TNCOM",
												"env":$scope.$parent.currentSelectedEnvironment.toUpperCase()
											};

											getSendDataFromServer($scope, ngDataApi, {
												"method": "send",
												"routeName": "/dashboard/tenant/application/add",
												"data": postData,
												"params": {"id": tId}
											}, function (error, response) {
												if (error) {
													overlayLoading.hide();
													return $scope.form.displayAlert('danger', error.message);
												}
												else {
													var appId = response.appId;
													getSendDataFromServer($scope, ngDataApi, {
														"method": "send",
														"routeName": "/dashboard/tenant/application/key/add",
														"params": {"id": tId, "appId": appId}
													}, function (error, response) {
														if (error) {
															overlayLoading.hide();
															return $scope.form.displayAlert('danger', error.message);
														}
														else {
															var key = response.key;
															getSendDataFromServer($scope, ngDataApi, {
																"method": "send",
																"routeName": "/dashboard/tenant/application/key/ext/add",
																"data": postData,
																"params": {"id": tId, "appId": appId, "key": key}
															}, function (error) {
																if (error) {
																	overlayLoading.hide();
																	return $scope.form.displayAlert('danger', error.message);
																}
																else {
																	$scope.availableEnv = [];
																	getSendDataFromServer($scope, ngDataApi, {
																		"method": "get",
																		"routeName": "/dashboard/environment/list"
																	}, function (error, response) {
																		if (error) {
																			overlayLoading.hide();
																			return $scope.form.displayAlert('danger', error.message);
																		}
																		else {
																			response.forEach(function (oneEnv) {
																				$scope.availableEnv.push(oneEnv.code.toLowerCase());
																			});
																			$scope.updateConfiguration(tId, appId, key, $scope.availableEnv, pendingMerchantsConfig.config, function (error) {
																				if (error) {
																					overlayLoading.hide();
																					return $scope.form.displayAlert('danger', error.message);
																				}
																				else {
																					data.source = tCode;
																					$scope.approve(data);
																				}
																			});
																		}
																	});
																}
															});
														}
													});
												}
											});
										}
									});
								}
							}
						},
						{
							'type': 'reset',
							'label': translation.cancel[LANG],
							'btn': 'danger',
							'action': function () {
								$scope.modalInstance.dismiss('cancel');
								$scope.form.formData = {};
							}
						}
					]
				};
				buildFormWithModal($scope, $modal, options);
			}
		});

	};

	$scope.approve = function (data) {
		var status = "approved";
		var opts = {
			"method": "get",
			"routeName": "/knowledgebase/owner/merchants/pending/changeStatus",
			"params": {
				'id': data._id,
				'status': status,
				'code': data.source,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		overlayLoading.show();
		getSendDataFromServer($scope, ngDataApi, opts, function (error) {
			overlayLoading.hide();
			if (error) {
				return $scope.form.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.merchantIsApprovedSuccessfully[LANG]);
				$scope.listEntries();
				$scope.modalInstance.close();
				$scope.form.formData = {};
			}
		});
	};

	$scope.reject = function (data) {
		var status = "rejected";
		var opts = {
			"method": "get",
			"routeName": "/knowledgebase/owner/merchants/pending/changeStatus",
			"params": {
				'id': data._id,
				'status': status,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		overlayLoading.show();
		getSendDataFromServer($scope, ngDataApi, opts, function (error) {
			overlayLoading.hide();
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.merchantIsRejectedSuccessfully[LANG]);
				$scope.listEntries();
			}
		});
	};

	$scope.generateTenantCode = function (tName) {
		var tCode = "";
		var nameArray = tName.split(" ");
		var index = Math.ceil(4 / nameArray.length);

		for (var i = 0; i < nameArray.length && tCode.length < 4; i++) {
			nameArray[i] = nameArray[i].replace(/[!@#$%^&*()_+,.<>;'?]/, "");
			if (tCode.length === 3) {
				tCode += nameArray[i].slice(0, 1);
				break;
			}
			if (nameArray[i].length > 1) {
				tCode += nameArray[i].slice(0, index);
			} else {
				tCode += nameArray[i].slice(0, index);
				index++;
			}
		}

		if (tCode.length < 4) {
			tCode += "tenant".slice(0, 4 - tCode.length);
		} else if (tCode.length > 4) {
			tCode = tCode.slice(0, 4);
		}
		tCode = tCode.toUpperCase();

		var counter = 1;
		return $scope.checkTenantCodeAvailability(tName, tCode, counter);
	};

	$scope.checkTenantCodeAvailability = function (tName, tCode, counter) {
		$scope.tenantsList.rows.forEach(function (oneTenant) {
			if (oneTenant.code === tCode && oneTenant.name !== tName) {
				tCode = tCode.slice(0, 3) + counter++;
				return $scope.checkTenantCodeAvailability(tName, tCode, counter);
			}
		});
		return tCode;
	};

	$scope.updateConfiguration = function (tId, appId, key, env, config, cb) {
		async.map(env, function (env, uCb) {
			var postData = {
				'envCode': env,
				'config': config
			};
			getSendDataFromServer($scope, ngDataApi, {
				"method": "send",
				"routeName": "/dashboard/tenant/application/key/config/update",
				"data": postData,
				"params": {"id": tId, "appId": appId, "key": key}
			}, function (error) {
				if (error) {
					return cb(error);
				}
				return uCb(null);
			});
		}, cb);
	};

	if($scope.access.pm.list){
		$scope.listEntries();
	}
	injectFiles.injectCss(ModuleQaMpLocation + "/./merchants.css");
}]);