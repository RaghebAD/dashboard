"use strict";
var pendingMerchantsConfig = {
	'permissions': {
		'pm': {
			'list': ['knowledgebase', '/owner/merchants/pending/list','get'],
			'changeStatus': ['knowledgebase', '/owner/merchants/pending/changeStatus','get'],
			'listTenants': ['dashboard', '/tenant/list','get'],
			'listEnv': ['dashboard', '/environment/list','get'],
			'addTenant': ['dashboard', '/tenant/add','post'],
			'addApp': ['dashboard', '/tenant/application/add','post'],
			'addKey': ['dashboard', '/tenant/application/key/add','post'],
			'addExt': ['dashboard', '/tenant/application/key/ext/add','post'],
			'updateConfig': ['dashboard', '/tenant/application/key/config/update','put']
		}
	},
	'config': {
		"cdn": {
			"path": {
				"url": "http://ssms.qa.yp.ca/api/import/url",
				"file": "http://ssms.qa.yp.ca/api/import/file"
			},
			"username": "ypshopapi",
			"password": "ypshopapipw"
		},
		"geocoder": {
			"apiKey": "AIzaSyA5ERag6pMlk4jZlxH9VchT-mJw7Ylwjtk"
		},
		"knowledgebase": {},
		"mail": {
			"from": "soajstest@soajs.org",
			"transport": {
				"type": "smtp",
				"options": {
					"host": "secure.emailsrvr.com",
					"port": "587",
					"ignoreTLS": true,
					"secure": false,
					"auth": {
						"user": "soajstest@soajs.org",
						"pass": "p@ssw0rd"
					}
				}
			}
		},
		"order": {
			"websiteDomain": "agmkpl.com",
			"link": {
				"cancelOrderFromToken": "http://widget.agmkpl.com/#/myAccount/orders/cancelOrder/",
				"userViewOrder": "http://widget.agmkpl.com/#/myAccount/orders/viewOrder/",
				"merchantViewOrder": "http://dashboard.agmkpl.com/#/order/viewSubOrder/"
			},
			"mail": {
				"createOrder": {
					"ENG": {
						"subject": "YP Shopwise | Your order has been created",
						"path": "mail/ENG/createOrder.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Votre commande a été créée",
						"path": "mail/FRA/createOrder.tmpl"
					}
				},
				"createSubOrder": {
					"ENG": {
						"subject": "YP Shopwise | A new order has been created",
						"path": "mail/ENG/createSubOrder.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Une nouvelle commande a été créée",
						"path": "mail/FRA/createSubOrder.tmpl"
					}
				},
				"merchantRejected": {
					"ENG": {
						"subject": "YP Shopwise | Order rejection confirmation",
						"path": "mail/ENG/merchantRejected.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Confirmation du reject de la commande",
						"path": "mail/FRA/merchantRejected.tmpl"
					}
				},
				"merchantDelivered": {
					"ENG": {
						"subject": "YP Shopwise | Confirmation of delivery",
						"path": "mail/ENG/merchantDelivered.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Confirmation de livraison",
						"path": "mail/FRA/merchantDelivered.tmpl"
					}
				},
				"merchantReadyForPickup": {
					"ENG": {
						"subject": "YP Shopwise | Order pickup confirmation",
						"path": "mail/ENG/merchantReadyForPickup.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Confirmation de cueillette",
						"path": "mail/FRA/merchantReadyForPickup.tmpl"
					}
				},
				"merchantSentOut": {
					"ENG": {
						"subject": "YP Shopwise | Your order has been sent",
						"path": "mail/ENG/merchantSentOut.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Votre commande a été envoyée",
						"path": "mail/FRA/merchantSentOut.tmpl"
					}
				},
				"merchantPicked": {
					"ENG": {
						"subject": "YP Shopwise | Pickup confirmation",
						"path": "mail/ENG/merchantPicked.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Confirmation de cueillette",
						"path": "mail/FRA/merchantPicked.tmpl"
					}
				},
				"merchantShipped": {
					"ENG": {
						"subject": "YP Shopwise | Order shipment confirmation",
						"path": "mail/ENG/merchantShipped.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Commande a été envoyée",
						"path": "mail/FRA/merchantShipped.tmpl"
					}
				},
				"userRejected": {
					"ENG": {
						"subject": "YP Shopwise | Your order has been rejected",
						"path": "mail/ENG/userRejected.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Votre commande a été refusée",
						"path": "mail/FRA/userRejected.tmpl"
					}
				},
				"userDelivered": {
					"ENG": {
						"subject": "YP Shopwise | Your order has been delivered",
						"path": "mail/ENG/userDelivered.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Votre commande a été livrée",
						"path": "mail/FRA/userDelivered.tmpl"
					}
				},
				"userReadyForPickup": {
					"ENG": {
						"subject": "YP Shopwise | Your order is ready for pickup",
						"path": "mail/ENG/userReadyForPickup.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Votre commande est prête à être ramassée",
						"path": "mail/FRA/userReadyForPickup.tmpl"
					}
				},
				"userPicked": {
					"ENG": {
						"subject": "YP Shopwise | Your order has been pickedup",
						"path": "mail/ENG/userPicked.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Votre commande a été cueillie",
						"path": "mail/FRA/userPicked.tmpl"
					}
				},
				"userShipped": {
					"ENG": {
						"subject": "YP Shopwise | Your order has been shipped",
						"path": "mail/ENG/userShipped.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Votre commande a été envoyée",
						"path": "mail/FRA/userShipped.tmpl"
					}
				},
				"userSentOut": {
					"ENG": {
						"subject": "YP Shopwise | Order sent confirmation",
						"path": "mail/ENG/userSentOut.tmpl"
					},
					"FRA": {
						"subject": "PJ Shopwise | Votre commande est confirmée",
						"path": "mail/FRA/userSentOut.tmpl"
					}
				}
			}
		}
	}

};