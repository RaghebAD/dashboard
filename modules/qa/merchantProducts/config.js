"use strict";
var country = [
	{v: 'AF', l: 'Afghanistan'},
	{v: 'AX', l: 'Aland Islands'},
	{v: 'AL', l: 'Albania'},
	{v: 'DZ', l: 'Algeria'},
	{v: 'AS', l: 'American Samoa'},
	{v: 'AD', l: 'Andorra'},
	{v: 'AO', l: 'Angola'},
	{v: 'AI', l: 'Anguilla'},
	{v: 'AQ', l: 'Antarctica'},
	{v: 'AG', l: 'Antigua and Barbuda'},
	{v: 'AR', l: 'Argentina'},
	{v: 'AM', l: 'Armenia'},
	{v: 'AW', l: 'Aruba'},
	{v: 'AC', l: 'Ascension Island'},
	{v: 'AU', l: 'Australia'},
	{v: 'AT', l: 'Austria'},
	{v: 'AZ', l: 'Azerbaijan'},
	{v: 'BS', l: 'Bahamas'},
	{v: 'BH', l: 'Bahrain'},
	{v: 'BD', l: 'Bangladesh'},
	{v: 'BB', l: 'Barbados'},
	{v: 'BY', l: 'Belarus'},
	{v: 'BE', l: 'Belgium'},
	{v: 'BZ', l: 'Belize'},
	{v: 'BJ', l: 'Benin'},
	{v: 'BM', l: 'Bermuda'},
	{v: 'BT', l: 'Bhutan'},
	{v: 'BO', l: 'Bolivia'},
	{v: 'BA', l: 'Bosnia and Herzegovina'},
	{v: 'BW', l: 'Botswana'},
	{v: 'BV', l: 'Bouvet Island'},
	{v: 'BR', l: 'Brazil'},
	{v: 'IO', l: 'British Indian Ocean Territory'},
	{v: 'VG', l: 'British Virgin Islands'},
	{v: 'BN', l: 'Brunei Darussalam'},
	{v: 'BG', l: 'Bulgaria'},
	{v: 'BF', l: 'Burkina Faso'},
	{v: 'BI', l: 'Burundi'},
	{v: 'KH', l: 'Cambodia'},
	{v: 'CM', l: 'Cameroon'},
	{v: 'CA', l: 'Canada', 'selected': true},
	{v: 'CV', l: 'Cape Verde'},
	{v: 'KY', l: 'Cayman Islands'},
	{v: 'CF', l: 'Central African Republic'},
	{v: 'TD', l: 'Chad'},
	{v: 'CL', l: 'Chile'},
	{v: 'CN', l: 'China'},
	{v: 'CX', l: 'Christmas Island'},
	{v: 'CC', l: 'Cocos (Keeling) Islands'},
	{v: 'CO', l: 'Colombia'},
	{v: 'KM', l: 'Comoros'},
	{v: 'CG', l: 'Congo'},
	{v: 'CD', l: 'Congo, Democratic Republic'},
	{v: 'CK', l: 'Cook Islands'},
	{v: 'CR', l: 'Costa Rica'},
	{v: 'CI', l: 'Cote D\'Ivoire (Ivory Coast)'},
	{v: 'HR', l: 'Croatia (Hrvatska)'},
	{v: 'CU', l: 'Cuba'},
	{v: 'CY', l: 'Cyprus'},
	{v: 'CZ', l: 'Czech Republic'},
	{v: 'CS', l: 'Czechoslovakia (former)'},
	{v: 'DK', l: 'Denmark'},
	{v: 'DJ', l: 'Djibouti'},
	{v: 'DM', l: 'Dominica'},
	{v: 'DO', l: 'Dominican Republic'},
	{v: 'TP', l: 'East Timor'},
	{v: 'EC', l: 'Ecuador'},
	{v: 'EG', l: 'Egypt'},
	{v: 'SV', l: 'El Salvador'},
	{v: 'GQ', l: 'Equatorial Guinea'},
	{v: 'ER', l: 'Eritrea'},
	{v: 'EE', l: 'Estonia'},
	{v: 'ET', l: 'Ethiopia'},
	{v: 'EU', l: 'European Union'},
	{v: 'MK', l: 'F.Y.R.O.M. (Macedonia)'},
	{v: 'FK', l: 'Falkland Islands (Malvinas)'},
	{v: 'FO', l: 'Faroe Islands'},
	{v: 'FJ', l: 'Fiji'},
	{v: 'FI', l: 'Finland'},
	{v: 'FR', l: 'France'},
	{v: 'FX', l: 'France, Metropolitan'},
	{v: 'GF', l: 'French Guiana'},
	{v: 'PF', l: 'French Polynesia'},
	{v: 'TF', l: 'French Southern Territories'},
	{v: 'GA', l: 'Gabon'},
	{v: 'GM', l: 'Gambia'},
	{v: 'GE', l: 'Georgia'},
	{v: 'DE', l: 'Germany'},
	{v: 'GH', l: 'Ghana'},
	{v: 'GI', l: 'Gibraltar'},
	{v: 'GB', l: 'Great Britain (UK)'},
	{v: 'GR', l: 'Greece'},
	{v: 'GL', l: 'Greenland'},
	{v: 'GD', l: 'Grenada'},
	{v: 'GP', l: 'Guadeloupe'},
	{v: 'GU', l: 'Guam'},
	{v: 'GT', l: 'Guatemala'},
	{v: 'GG', l: 'Guernsey'},
	{v: 'GN', l: 'Guinea'},
	{v: 'GW', l: 'Guinea-Bissau'},
	{v: 'GY', l: 'Guyana'},
	{v: 'HT', l: 'Haiti'},
	{v: 'HM', l: 'Heard and McDonald Islands'},
	{v: 'HN', l: 'Honduras'},
	{v: 'HK', l: 'Hong Kong'},
	{v: 'HU', l: 'Hungary'},
	{v: 'IS', l: 'Iceland'},
	{v: 'IN', l: 'India'},
	{v: 'ID', l: 'Indonesia'},
	{v: 'IR', l: 'Iran'},
	{v: 'IQ', l: 'Iraq'},
	{v: 'IE', l: 'Ireland'},
	{v: 'IM', l: 'Isle of Man'},
	{v: 'IL', l: 'Israel'},
	{v: 'IT', l: 'Italy'},
	{v: 'JM', l: 'Jamaica'},
	{v: 'JP', l: 'Japan'},
	{v: 'JE', l: 'Jersey'},
	{v: 'JO', l: 'Jordan'},
	{v: 'KZ', l: 'Kazakhstan'},
	{v: 'KE', l: 'Kenya'},
	{v: 'KI', l: 'Kiribati'},
	{v: 'KP', l: 'Korea (North)'},
	{v: 'KR', l: 'Korea (South)'},
	{v: 'XK', l: 'Kosovo*'},
	{v: 'KW', l: 'Kuwait'},
	{v: 'KG', l: 'Kyrgyzstan'},
	{v: 'LA', l: 'Laos'},
	{v: 'LV', l: 'Latvia'},
	{v: 'LB', l: 'Lebanon'},
	{v: 'LS', l: 'Lesotho'},
	{v: 'LR', l: 'Liberia'},
	{v: 'LY', l: 'Libya'},
	{v: 'LI', l: 'Liechtenstein'},
	{v: 'LT', l: 'Lithuania'},
	{v: 'LU', l: 'Luxembourg'},
	{v: 'MO', l: 'Macau'},
	{v: 'MG', l: 'Madagascar'},
	{v: 'MW', l: 'Malawi'},
	{v: 'MY', l: 'Malaysia'},
	{v: 'MV', l: 'Maldives'},
	{v: 'ML', l: 'Mali'},
	{v: 'MT', l: 'Malta'},
	{v: 'MH', l: 'Marshall Islands'},
	{v: 'MQ', l: 'Martinique'},
	{v: 'MR', l: 'Mauritania'},
	{v: 'MU', l: 'Mauritius'},
	{v: 'YT', l: 'Mayotte'},
	{v: 'MX', l: 'Mexico'},
	{v: 'FM', l: 'Micronesia'},
	{v: 'MD', l: 'Moldova'},
	{v: 'MC', l: 'Monaco'},
	{v: 'MN', l: 'Mongolia'},
	{v: 'ME', l: 'Montenegro'},
	{v: 'MS', l: 'Montserrat'},
	{v: 'MA', l: 'Morocco'},
	{v: 'MZ', l: 'Mozambique'},
	{v: 'MM', l: 'Myanmar'},
	{v: 'NA', l: 'Namibia'},
	{v: 'NR', l: 'Nauru'},
	{v: 'NP', l: 'Nepal'},
	{v: 'NL', l: 'Netherlands'},
	{v: 'AN', l: 'Netherlands Antilles'},
	{v: 'NT', l: 'Neutral Zone'},
	{v: 'NC', l: 'New Caledonia'},
	{v: 'NZ', l: 'New Zealand (Aotearoa)'},
	{v: 'NI', l: 'Nicaragua'},
	{v: 'NE', l: 'Niger'},
	{v: 'NG', l: 'Nigeria'},
	{v: 'NU', l: 'Niue'},
	{v: 'NF', l: 'Norfolk Island'},
	{v: 'MP', l: 'Northern Mariana Islands'},
	{v: 'NO', l: 'Norway'},
	{v: 'OM', l: 'Oman'},
	{v: 'PK', l: 'Pakistan'},
	{v: 'PW', l: 'Palau'},
	{v: 'PS', l: 'Palestinian Territory, Occupied'},
	{v: 'PA', l: 'Panama'},
	{v: 'PG', l: 'Papua New Guinea'},
	{v: 'PY', l: 'Paraguay'},
	{v: 'PE', l: 'Peru'},
	{v: 'PH', l: 'Philippines'},
	{v: 'PN', l: 'Pitcairn'},
	{v: 'PL', l: 'Poland'},
	{v: 'PT', l: 'Portugal'},
	{v: 'PR', l: 'Puerto Rico'},
	{v: 'QA', l: 'Qatar'},
	{v: 'RE', l: 'Reunion'},
	{v: 'RO', l: 'Romania'},
	{v: 'RU', l: 'Russian Federation'},
	{v: 'RW', l: 'Rwanda'},
	{v: 'GS', l: 'S. Georgia and S. Sandwich Isls.'},
	{v: 'KN', l: 'Saint Kitts and Nevis'},
	{v: 'LC', l: 'Saint Lucia'},
	{v: 'VC', l: 'Saint Vincent & the Grenadines'},
	{v: 'WS', l: 'Samoa'},
	{v: 'SM', l: 'San Marino'},
	{v: 'ST', l: 'Sao Tome and Principe'},
	{v: 'SA', l: 'Saudi Arabia'},
	{v: 'SN', l: 'Senegal'},
	{v: 'RS', l: 'Serbia'},
	{v: 'YU', l: 'Serbia and Montenegro (former)'},
	{v: 'SC', l: 'Seychelles'},
	{v: 'SL', l: 'Sierra Leone'},
	{v: 'SG', l: 'Singapore'},
	{v: 'SK', l: 'Slovak Republic'},
	{v: 'SI', l: 'Slovenia'},
	{v: 'SB', l: 'Solomon Islands'},
	{v: 'SO', l: 'Somalia'},
	{v: 'ZA', l: 'South Africa'},
	{v: 'ES', l: 'Spain'},
	{v: 'LK', l: 'Sri Lanka'},
	{v: 'SH', l: 'St. Helena'},
	{v: 'PM', l: 'St. Pierre and Miquelon'},
	{v: 'SD', l: 'Sudan'},
	{v: 'SR', l: 'Suriname'},
	{v: 'SJ', l: 'Svalbard & Jan Mayen Islands'},
	{v: 'SZ', l: 'Swaziland'},
	{v: 'SE', l: 'Sweden'},
	{v: 'CH', l: 'Switzerland'},
	{v: 'SY', l: 'Syria'},
	{v: 'TW', l: 'Taiwan'},
	{v: 'TJ', l: 'Tajikistan'},
	{v: 'TZ', l: 'Tanzania'},
	{v: 'TH', l: 'Thailand'},
	{v: 'TG', l: 'Togo'},
	{v: 'TK', l: 'Tokelau'},
	{v: 'TO', l: 'Tonga'},
	{v: 'TT', l: 'Trinidad and Tobago'},
	{v: 'TN', l: 'Tunisia'},
	{v: 'TR', l: 'Turkey'},
	{v: 'TM', l: 'Turkmenistan'},
	{v: 'TC', l: 'Turks and Caicos Islands'},
	{v: 'TV', l: 'Tuvalu'},
	{v: 'UG', l: 'Uganda'},
	{v: 'UA', l: 'Ukraine'},
	{v: 'AE', l: 'United Arab Emirates'},
	{v: 'UK', l: 'United Kingdom'},
	{v: 'US', l: 'United States'},
	{v: 'UY', l: 'Uruguay'},
	{v: 'UM', l: 'US Minor Outlying Islands'},
	{v: 'SU', l: 'USSR (former)'},
	{v: 'UZ', l: 'Uzbekistan'},
	{v: 'VU', l: 'Vanuatu'},
	{v: 'VA', l: 'Vatican City State (Holy See)'},
	{v: 'VE', l: 'Venezuela'},
	{v: 'VN', l: 'Viet Nam'},
	{v: 'VI', l: 'Virgin Islands (U.S.)'},
	{v: 'WF', l: 'Wallis and Futuna Islands'},
	{v: 'EH', l: 'Western Sahara'},
	{v: 'YE', l: 'Yemen'},
	{v: 'ZR', l: 'Democratic Republic of the Congo (Zaire)'},
	{v: 'ZM', l: 'Zambia'},
	{v: 'ZW', l: 'Zimbabwe'}
];

var knowledgeBaseModuleQaConfig = {
	'errors': {
		100: translation["100"][LANG],
		101: translation["101"][LANG],
		102: translation["102"][LANG],
		103: translation["103"][LANG],
		104: translation["104"][LANG],
		105: translation["105"][LANG],
		106: translation["106"][LANG],
		107: translation["107"][LANG],
		108: translation["108"][LANG],
		109: translation["109"][LANG],
		
		200: translation["200"][LANG],
		201: translation["201"][LANG],
		202: translation["202"][LANG],
		203: translation["203"][LANG],
		204: translation["204"][LANG]
	},
	'apiEndLimit': 1000,
	"maxLength": {
		"product": {
			"desc_short": 420,
			"desc_full": 1000,
			"title": 140
		}
	},
	'permissions': {
		'tenant': {
			'merchants': {
				'list': ['knowledgebase', '/tenant/merchants', 'get'],
				'add': ['knowledgebase', '/tenant/merchants', 'post'],
				'edit': ['knowledgebase', '/tenant/merchants/:id', 'put'],
				'get': ['knowledgebase', '/tenant/merchants/:id', 'get'],
				'delete': ['knowledgebase', '/tenant/merchants/:id', 'delete'],
				'changeStatus': ['knowledgebase', '/tenant/merchants/:id/status', 'put'],
				'configure': ['knowledgebase', '/tenant/merchants/:id/configure', 'post'],
				'overrideProfile': ['knowledgebase', '/tenant/merchants/:id/overrideProfile', 'post'],
				'listMigration': ['knowledgebase', '/migrate/data/list', 'get'],
				'registerMigration': ['knowledgebase', '/migrate/data/register', 'post'],
				'removeMigration': ['knowledgebase', '/migrate/data/remove', 'get']
			},
			'promoCodes': {
				'list': ['knowledgebase', '/merchant/promoCodes', 'get'],
				'add': ['knowledgebase', '/merchant/promoCodes', 'post']
			},
			'pos': {
				'list': ['knowledgebase', '/tenant/merchants/:id/pos', 'get'],
				'add': ['knowledgebase', '/tenant/merchants/:id/pos', 'post'],
				'edit': ['knowledgebase', '/tenant/merchants/:id/pos/:posId', 'put'],
				'delete': ['knowledgebase', '/tenant/merchants/:id/pos/:posId', 'delete'],
				'changeStatus': ['knowledgebase', '/tenant/merchants/:id/pos/:posId/status', 'put']
			},
			'products': {
				'list': ['knowledgebase', '/tenant/products', 'get'],
				'add': ['knowledgebase', '/tenant/products', 'post'],
				'edit': ['knowledgebase', '/tenant/products/:id', 'put'],
				'get': ['knowledgebase', '/tenant/products/:id', 'get'],
				'delete': ['knowledgebase', '/tenant/products/:id', 'delete'],
				'changeStatus': ['knowledgebase', '/tenant/products/:id/status', 'put'],
				'updateStock': ['knowledgebase', '/tenant/products/updateStock', 'put'],
				'updateTreats': ['knowledgebase', '/tenant/products/updateTreats', 'post'],
				'search': ['knowledgebase', '/products/search', 'get'],
				'listMigration': ['knowledgebase', '/migrate/data/list', 'get'],
				'registerMigration': ['knowledgebase', '/migrate/data/register', 'post'],
				'removeMigration': ['knowledgebase', '/migrate/data/remove', 'get'],
				'saveDraft': ['knowledgebase', '/tenant/products/saveDraft', 'post'],
				'getProductVariations': ['knowledgebase', '/tenant/products/variations', 'get']
			},
			'sale': {
				'update': ['knowledgebase', '/merchant/sale/applyVariation', "post"]
			}
		},
		'owner': {
			'merchants': {
				'setDeliverySignature': ['knowledgebase', '/owner/merchants/setDeliverySignature'],
				'list': ['knowledgebase', '/owner/merchants', 'get'],
				'add': ['knowledgebase', '/owner/merchants', 'post'],
				'edit': ['knowledgebase', '/owner/merchants/:id', 'put'],
				'get': ['knowledgebase', '/owner/merchants/:id', 'get'],
				'delete': ['knowledgebase', '/owner/merchants/:id', 'delete'],
				'changeStatus': ['knowledgebase', '/owner/merchants/:id/status', 'put'],
				'configure': ['knowledgebase', '/owner/merchants/:id/configure', 'post'],
				'overrideProfile': ['knowledgebase', '/owner/merchants/:id/overrideProfile', 'post'],
				'listMigration': ['knowledgebase', '/migrate/data/list', 'get'],
				'registerMigration': ['knowledgebase', '/migrate/data/register', 'post'],
				'removeMigration': ['knowledgebase', '/migrate/data', 'delete']
			},
			'pos': {
				'list': ['knowledgebase', '/owner/merchants/:id/pos', 'get'],
				'add': ['knowledgebase', '/owner/merchants/:id/pos', 'post'],
				'edit': ['knowledgebase', '/owner/merchants/:id/pos/:posId', "put"],
				'delete': ['knowledgebase', '/owner/merchants/:id/pos/:posId', "delete"],
				'changeStatus': ['knowledgebase', '/owner/merchants/:id/pos/:posId/status', 'put']
			},
			'products': {
				'list': ['knowledgebase', '/owner/products', 'get'],
				'add': ['knowledgebase', '/owner/products', 'post'],
				'edit': ['knowledgebase', '/owner/products/:id', 'put'],
				'get': ['knowledgebase', '/owner/products/:id', 'get'],
				'delete': ['knowledgebase', '/owner/products/:id', 'delete'],
				'changeStatus': ['knowledgebase', '/owner/products/:id/status', 'put'],
				'updateStock': ['knowledgebase', '/owner/products/updateStock', 'put'],
				'updateTreats': ['knowledgebase', '/owner/products/updateTreats', 'post'],
				'search': ['knowledgebase', '/products/search', 'get'],
				'listMigration': ['knowledgebase', '/migrate/data/list', 'get'],
				'registerMigration': ['knowledgebase', '/migrate/data/register', 'post'],
				'removeMigration': ['knowledgebase', '/migrate/data', 'delete'],
				'saveDraft': ['knowledgebase', '/owner/products/saveDraft', 'post'],
				'getProductVariations': ['knowledgebase', '/owner/products/variations', 'get']
			}
		}
	},
	
	"merchants": {
		"form": {
			"oneTab": {
				'label': '',
				'type': 'tab',
				'entries': [
					{
						'name': 'description%lang%',
						'label': translation.description[LANG],
						'type': 'editor',
						'placeholder': translation.descriptionPlaceHolder[LANG],
						'fieldMsg': translation.descriptionFieldMsg[LANG],
						'required': true
					},
					{
						'name': 'customer_service%lang%',
						'label': translation.customerServiceContact[LANG],
						'type': 'editor',
						'fieldMsg': translation.customerServiceFieldMsg[LANG],
						'required': true
					},
					{
						'name': 'return_policy%lang%',
						'label': translation.returnPolicy[LANG],
						'type': 'editor',
						'fieldMsg': translation.returnPolicyFieldMsg[LANG],
						'required': true
					},
					{
						'name': 'terms_condition%lang%',
						'label': translation.termsAndConditions[LANG],
						'type': 'editor',
						'fieldMsg': translation.termsAndConditionsFieldMsg[LANG],
						'required': false
					}
				]
			},
			'entries': [
				//{
				//    'name': 'merchantId',
				//    'label': 'Merchant Id',
				//    'type': 'text',
				//    'placeholder': '1280',
				//    'required': true
				//},
				{
					'name': 'domain',
					'label': translation.domain[LANG],
					'type': 'text',
					'placeholder': 'mydomain.com',
					'required': true
				},
				{
					'name': 'name',
					'label': translation.name[LANG],
					'type': 'text',
					'placeholder': translation.myDomain[LANG],
					'required': true
				},
				{
					'name': 'language',
					'label': translation.preferredLanguage[LANG],
					'type': 'select',
					'value': [{v: "ENG", l: "English"}, {v: "FRA", l: "Francais"}],
					'required': true
				},
				{
					'name': 'status',
					'label': translation.status[LANG],
					'type': 'radio',
					'value': [{'v': 'active', 'l': translation.active[LANG]}, {
						'v': 'disabled',
						'l': translation.disabled[LANG]
					}],
					'required': true
				},
				{
					'name': 'logo',
					'label': translation.logo[LANG],
					'type': 'string',
					'placeholder': '//www.mydomain.com/i/t/tacticstm.png',
					'required': false
				},
				{
					'name': 'uploadLogo',
					'label': translation.uploadLogo[LANG],
					'type': 'image',
					'required': false
				},
				{
					'name': 'logoAltTag',
					'label': translation.altTagForLogo[LANG],
					'type': 'string',
					'fieldMsg': translation.optional[LANG],
					'tooltip': translation.logoAltTagTooltip[LANG],
					'required': false
				},
				{
					'name': 'image',
					'label': translation.storeImage[LANG],
					'type': 'string',
					'placeholder': '//www.mydomain.com/i/t/tacticstm.png',
					'required': false
				},
				{
					'name': 'uploadImage',
					'label': translation.uploadStoreImage[LANG],
					'type': 'image',
					'required': false
				},
				{
					'name': 'imageAltTag',
					'label': translation.altTagForStoreImage[LANG],
					'type': 'string',
					'fieldMsg': translation.optional[LANG],
					'tooltip': translation.imageAltTagTooltip[LANG],
					'required': false
				},
				{
					'name': 'homepage',
					'label': translation.homepage[LANG],
					'type': 'url',
					'placeholder': 'http://www.mydomain.com',
					'required': true
				},
				{
					'name': 'tags',
					'label': translation.tags[LANG],
					'type': 'text',
					'value': '',
					'tooltip': translation.tagsTooltip[LANG],
					'placeholder': 'tag1,tag2,tag3,tag4,tag5',
					'required': false
				},
				{
					'name': 'categories',
					'label': translation.categories[LANG],
					'type': 'text',
					'value': '',
					'tooltip': translation.categoriesTooltip[LANG],
					'placeholder': 'category1,category2,category3',
					'required': false
				},
				{
					"name": "rating",
					"label": translation.optionalRating[LANG],
					"type": "group",
					'collapsed': true,
					'required': false,
					"class": "",
					"entries": [
						{
							'name': 'average',
							'label': translation.average[LANG],
							'type': 'number',
							'placeholder': '4.5',
							'required': false
						},
						{
							'name': 'count',
							'label': translation.count[LANG],
							'type': 'number',
							'placeholder': '25',
							'required': false
						}
					]
				},
				{
					"name": "information",
					"label": translation.information[LANG],
					"type": "group",
					'collapsed': false,
					'required': false,
					"class": "",
					"entries": [
						{
							'name': 'email',
							'type': translation.email[LANG],
							'placeholder': 'bob@domain.com',
							'required': true
						},
						{
							'name': 'phone',
							'label': translation.phone[LANG],
							'type': 'phone',
							'required': true
						},
						{
							'name': 'address',
							'label': translation.address[LANG],
							'type': 'textarea',
							'rows': 4,
							'required': false
						}
					]
				},
				{
					'name': 'translations',
					'label': translation.enterTranslations[LANG],
					'type': 'tabset',
					"class": "",
					"tabs": []
				}
			]
		},
		"configure": {
			"feed": [
				{
					'name': 'type',
					'label': translation.feedingSource[LANG],
					'type': 'select',
					'value': [
						{'v': 'ui', l: translation.managementInterfaceOnly[LANG]},
						{'v': 'api', l: translation.iHaveAnApiThatSendDataToYou[LANG], 'selected': true},
						{'v': "driver", l: translation.youCreatedAdriverForMe[LANG]}
					],
					'tooltip': translation.feedingSourceTooltip[LANG]
				},
				{
					'name': 'driver',
					'label': translation.driverName[LANG],
					'type': 'text',
					'value': '',
					'tooltip': translation.driverTooltip[LANG],
					'placeholder': translation.driverTooltip[LANG],
					'required': true
				},
				{
					'name': 'url',
					'label': translation.url[LANG],
					'type': 'text',
					'value': '',
					'tooltip': translation.urlTooltip[LANG],
					'placeholder': 'http://myapi.domain.com',
					'required': true
				},
				{
					'name': 'token',
					'label': translation.token[LANG],
					'type': 'text',
					'value': '',
					'tooltip': translation.tokenTooltip[LANG],
					'placeholder': translation.tokenPlaceholder[LANG],
					'required': true
				},
				{
					'name': 'extra',
					'label': translation.additionalInformation[LANG],
					'type': 'textarea',
					'value': '',
					"rows": 10,
					'tooltip': translation.additionalInformationTooltip[LANG],
					'placeholder': JSON.stringify({"property": "value"}, null, 2),
					'required': false
				}
			],
			"billing": [
				{
					'name': 'cc',
					'label': translation.creditCard[LANG],
					'type': 'group',
					'collapsed': false,
					'class': 'cc',
					"entries": [
						{
							'name': 'name',
							'label': translation.nameOnCard[LANG],
							'type': 'text',
							'value': '',
							'required': true
						},
						{
							'name': 'ccNumber',
							'label': translation.cardNumber[LANG],
							'type': 'text',
							'value': '',
							'required': true
						},
						{
							'name': 'month',
							'label': translation.expirationMonth[LANG],
							'type': 'select',
							'required': true,
							'value': [
								{'v': '', l: 'MM'},
								{'v': '1', l: '1'},
								{'v': "2", l: '2'},
								{'v': "3", l: '3'},
								{'v': "4", l: '4'},
								{'v': "5", l: '5'},
								{'v': "6", l: '6'},
								{'v': "7", l: '7'},
								{'v': "8", l: '8'},
								{'v': "9", l: '9'},
								{'v': "10", l: '10'},
								{'v': "11", l: '11'},
								{'v': "12", l: '12'}
							]
						},
						{
							'name': 'year',
							'label': translation.expirationYear[LANG],
							'type': 'select',
							'required': true,
							'value': [{'v': '', l: 'YYYY'}]
						},
						{
							'name': 'cvv',
							'label': translation.cvvCode[LANG],
							'type': 'text',
							'value': '',
							'maxlength': 4,
							'required': true
						}
					
					]
				},
				{
					'name': 'address',
					'label': translation.billingAddress[LANG],
					'type': 'group',
					'collapsed': false,
					"entries": [
						{
							'name': 'addressLine1',
							'label': translation.addressLine1[LANG],
							'type': 'text',
							'placeholder': '',
							'required': true
						},
						{
							'name': 'addressLine2',
							'label': translation.addressLine2[LANG],
							'type': 'text',
							'placeholder': '',
							'required': false
						},
						{
							'name': 'addressLine3',
							'label': translation.addressLine3[LANG],
							'type': 'text',
							'placeholder': '',
							'required': false
						},
						{
							"name": "country",
							"label": translation.country[LANG],
							"type": "select",
							"tooltip": translation.countryTooltip[LANG],
							"value": country,
							"required": true
						},
						{
							'name': 'city',
							'label': translation.city[LANG],
							'type': 'text',
							'placeholder': '',
							'required': true
						},
						{
							'name': 'state',
							'label': translation.state[LANG],
							'type': 'text',
							'placeholder': 'QC',
							'maxlength': 2,
							'value': 'QC',
							'required': true
						},
						{
							'name': 'zip',
							'label': translation.zip[LANG],
							'type': 'text',
							'placeholder': '',
							'required': true
						},
						{
							'name': 'email',
							'label': translation.email[LANG],
							'type': 'email',
							'placeholder': 'user@domain.com',
							'required': true
						},
						{
							'name': 'phone',
							'label': translation.phone[LANG],
							'type': 'phone',
							'placeholder': '+18769762345',
							'required': true
						}
					
					
					]
				}
			]
		},
		"promoCode": {
			"entries": [
				{
					"name": "codeValue",
					"label": translation.codeValue[LANG],
					"type": "text",
					"tooltip": translation.codeValueTooltip[LANG],
					"value": "",
					"required": true
				},
				{
					"name": "promotionType",
					"label": translation.promotionType[LANG],
					"type": "select",
					"tooltip": translation.promotionTypeTooltip[LANG],
					"value": [
						{l: "discountAmount", v: "discountAmount"},
						{l: "discountPercentage", v: "discountPercentage"},
						{l: "freeShipping", v: "freeShipping"}
					],
					"required": true
				}
			]
		},
		"override": {
			"entries": [
				{
					'name': 'config',
					"label": translation.customFeedConfiguration[LANG],
					'type': 'jsoneditor',
					'options': {
						'mode': 'code'
					},
					"tooltip": translation.configTooltip[LANG],
					'height': '200px',
					"value": {},
					'required': true
				}
			]
		}
	},
	
	"pos": {
		form: {
			"oneException": [
				{
					'name': 'exceptionD%count%',
					'label': translation.date[LANG],
					'type': 'text',
					'value': 0,
					'placeholder': 'DD/MM',
					'required': false
				},
				{
					'name': 'exceptionF%count%',
					'label': translation.from[LANG],
					'type': 'text',
					'value': '',
					'placeholder': 'HH:MM',
					'required': false
				},
				{
					'name': 'exceptionT%count%',
					'label': translation.till[LANG],
					'type': 'text',
					'value': '',
					'placeholder': 'HH:MM',
					'required': false
				},
				{
					"type": "html",
					"name": "removeException%count%",
					"value": "<span class='red'><span class='icon icon-cross' title=" + translation.removeException[LANG] + "></span></span>",
					"onAction": function (id, data, form) {
						var number = id.replace("removeException", "");
						
						delete form.formData['exceptionD' + number];
						delete form.formData['exceptionF' + number];
						delete form.formData['exceptionT' + number];
						
						form.entries.forEach(function (oneEntry) {
							if (oneEntry.name === 'exception') {
								
								for (var i = 0; i < oneEntry.entries.length; i++) {
									if (oneEntry.entries[i].name === 'exceptionD' + number) {
										oneEntry.entries.splice(i, 1);
									}
									if (oneEntry.entries[i].name === 'exceptionF' + number) {
										oneEntry.entries.splice(i, 1);
									}
									if (oneEntry.entries[i].name === 'exceptionT' + number) {
										oneEntry.entries.splice(i, 1);
									}
									if (oneEntry.entries[i].name === 'removeException' + number) {
										oneEntry.entries.splice(i, 1);
									}
								}
							}
						});
					}
				}
			],
			"oneEmail": [
				{
					'name': 'email%count%',
					'label': translation.additionalEmail[LANG],
					'type': 'email',
					'placeholder': 'store@domain.com',
					'required': false
				},
				{
					"type": "html",
					"name": "removeEmail%count%",
					"value": "<span class='red'><span class='icon icon-cross' title=" + translation.removeException[LANG] + "></span></span>",
					"onAction": function (id, data, form) {
						var number = id.replace("removeEmail", "");
						
						delete form.formData['email' + number];
						
						form.entries.forEach(function (oneEntry) {
							if (oneEntry.name === 'otherEmails') {
								
								for (var i = 0; i < oneEntry.entries.length; i++) {
									if (oneEntry.entries[i].name === 'email' + number) {
										oneEntry.entries.splice(i, 1);
									}
									if (oneEntry.entries[i].name === 'removeEmail' + number) {
										oneEntry.entries.splice(i, 1);
									}
								}
							}
						});
					}
				}
			],
			'entries': [
				{
					'name': 'id',
					'label': translation.id[LANG],
					'type': 'text',
					"tooltip": translation.optionalCustomId[LANG],
					'required': false
				},
				{
					'name': 'name',
					'label': translation.name[LANG],
					'type': 'text',
					'placeholder': 'Store 1',
					'maxlength': 44,
					"fieldMsg": translation.maximum44CharactersAllowed[LANG],
					'required': true
				},
				{
					'name': 'currency',
					'label': translation.currency[LANG],
					'type': 'text',
					'placeholder': 'USD',
					'maxlength': 3,
					'required': true
				},
				{
					"name": "type",
					"label": translation.type[LANG],
					"type": "select",
					"value": [
						{v: 'virtual', l: "Virtual"},
						{v: 'pos', l: translation.pos[LANG]},
						{v: 'warehouse', l: translation.warehouse[LANG]}
					]
				},
				{
					'name': 'status',
					'label': translation.status[LANG],
					'type': 'radio',
					'value': [
						{'v': 'active', 'l': translation.active[LANG]},
						{
							'v': 'disabled',
							'l': translation.disabled[LANG]
						}
					],
					'required': true
				},
				{
					'name': 'long',
					'label': translation.longitude[LANG],
					'type': 'readonly',
					'placeholder': '',
					'required': false
				},
				{
					'name': 'lat',
					'label': translation.latitude[LANG],
					'type': 'readonly',
					'placeholder': '',
					'required': false
				},
				{
					'name': 'address',
					'label': translation.address[LANG],
					'type': 'group',
					'collapsed': false,
					"entries": [
						{
							'name': 'addressLine1',
							'label': translation.addressLine1[LANG],
							'type': 'text',
							'placeholder': '',
							'required': true
						},
						{
							'name': 'addressLine2',
							'label': translation.addressLine2[LANG],
							'type': 'text',
							'placeholder': '',
							'required': false
						},
						{
							'name': 'addressLine3',
							'label': translation.addressLine3[LANG],
							'type': 'text',
							'placeholder': '',
							'required': false
						},
						{
							"name": "country",
							"label": translation.country[LANG],
							"type": "select",
							"tooltip": translation.countryTooltip[LANG],
							"value": country,
							"required": true
						},
						{
							'name': 'city',
							'label': translation.city[LANG],
							'type': 'text',
							'placeholder': '',
							'required': true
						},
						{
							'name': 'state',
							'label': translation.state[LANG],
							'type': 'text',
							'placeholder': 'QC',
							'maxlength': 2,
							'value': 'QC',
							'required': true
						},
						{
							'name': 'zip',
							'label': translation.zip[LANG],
							'type': 'text',
							'placeholder': '',
							'required': true
						},
						{
							'name': 'email',
							'label': translation.email[LANG],
							'type': 'email',
							'placeholder': 'store@domain.com',
							'required': true
						},
						{
							'name': 'phone',
							'label': translation.phone[LANG],
							'type': 'phone',
							'placeholder': '+18769762345',
							'required': true
						}
					
					
					]
				},
				{
					'name': 'otherEmails',
					'label': translation.otherEmails[LANG],
					'type': 'group',
					'class': 'otherEmail',
					'collapsed': false,
					"entries": [
						{
							"type": "html",
							"name": "addEmail",
							"value": '<input type="button" value="' + translation.addAnother[LANG] + '" class="btn btn-success btn-sm f-right"/>',
							"onAction": function (id, data, form) {
								form.actions[2].action();
							}
						}
					]
				},
				{
					'name': 'pickup',
					'label': translation.pickup[LANG],
					'type': 'radio',
					'value': [{'v': true, 'l': translation.yes[LANG]}, {'v': false, 'l': translation.no[LANG]}],
					'required': false
				},
				{
					'name': 'shipping',
					'label': translation.shipping[LANG],
					'type': 'radio',
					'value': [{'v': true, 'l': translation.yes[LANG]}, {'v': false, 'l': translation.no[LANG]}],
					'required': false
				},
				{
					'name': 'preparation_time',
					'label': translation.preparationTime[LANG],
					'type': 'text',
					'placeholder': '00:30',
					'required': true
				},
				{
					'name': 'days',
					'label': translation.daysOfOperation[LANG],
					'type': 'group',
					'collapsed': false,
					'class': 'operation',
					"entries": [
						{
							"type": "checkbox",
							"name": "monday",
							"value": [{v: "monday", l: translation.monday[LANG]}],
							"onAction": function (id, data, form) {
								for (var i = 0; i < form.entries.length; i++) {
									if (form.entries[i].name === 'days') {
										for (var y = 0; y < form.entries[i].entries.length; y++) {
											if (form.entries[i].entries[y].name === 'monday') {
												if (form.entries[i].entries[y + 1] && form.entries[i].entries[y + 1].name === 'fromMonday') {
													form.entries[i].entries.splice(y + 1, 2);
													
													var ele1 = angular.element(document.getElementsByClassName("tr-monday-wrapper"));
													var ele2 = angular.element(document.getElementsByClassName("tr-fromMonday-wrapper"));
													var ele3 = angular.element(document.getElementsByClassName("tr-tillMonday-wrapper"));
													ele1.removeClass('myClass');
													ele2.removeClass('myClass');
													ele3.removeClass('myClass');
													
												}
												else {
													form.entries[i].entries.splice(
														y + 1, 0,
														{
															'name': 'fromMonday',
															'label': translation.from[LANG],
															'type': 'text',
															'placeholder': '09:00',
															'required': false
														},
														{
															'name': 'tillMonday',
															'label': translation.till[LANG],
															'type': 'text',
															'placeholder': '20:00',
															'required': false
														}
													);
													
													setTimeout(function () {
														var ele1 = angular.element(document.getElementsByClassName("tr-monday-wrapper"));
														var ele2 = angular.element(document.getElementsByClassName("tr-fromMonday-wrapper"));
														var ele3 = angular.element(document.getElementsByClassName("tr-tillMonday-wrapper"));
														ele1.addClass('myClass');
														ele2.addClass('myClass');
														ele3.addClass('myClass');
													}, 5);
													
												}
												break;
											}
										}
									}
								}
							}
						},
						{
							"type": "checkbox",
							"name": "tuesday",
							"value": [{v: "tuesday", l: translation.tuesday[LANG]}],
							"onAction": function (id, data, form) {
								for (var i = 0; i < form.entries.length; i++) {
									if (form.entries[i].name === 'days') {
										for (var y = 0; y < form.entries[i].entries.length; y++) {
											if (form.entries[i].entries[y].name === 'tuesday') {
												if (form.entries[i].entries[y + 1] && form.entries[i].entries[y + 1].name === 'fromTuesday') {
													form.entries[i].entries.splice(y + 1, 2);
													
													var ele1 = angular.element(document.getElementsByClassName("tr-tuesday-wrapper"));
													var ele2 = angular.element(document.getElementsByClassName("tr-fromTuesday-wrapper"));
													var ele3 = angular.element(document.getElementsByClassName("tr-tillTuesday-wrapper"));
													ele1.removeClass('myClass');
													ele2.removeClass('myClass');
													ele3.removeClass('myClass');
												}
												else {
													form.entries[i].entries.splice(
														y + 1, 0,
														{
															'name': 'fromTuesday',
															'label': translation.from[LANG],
															'type': 'text',
															'placeholder': '09:00',
															'required': false
														},
														{
															'name': 'tillTuesday',
															'label': translation.till[LANG],
															'type': 'text',
															'placeholder': '20:00',
															'required': false
														}
													);
													
													setTimeout(function () {
														var ele1 = angular.element(document.getElementsByClassName("tr-tuesday-wrapper"));
														var ele2 = angular.element(document.getElementsByClassName("tr-fromTuesday-wrapper"));
														var ele3 = angular.element(document.getElementsByClassName("tr-tillTuesday-wrapper"));
														ele1.addClass('myClass');
														ele2.addClass('myClass');
														ele3.addClass('myClass');
													}, 5);
												}
												break;
											}
										}
									}
								}
							}
						},
						{
							"type": "checkbox",
							"name": "wednesday",
							"value": [{v: "wednesday", l: translation.wednesday[LANG]}],
							"onAction": function (id, data, form) {
								for (var i = 0; i < form.entries.length; i++) {
									if (form.entries[i].name === 'days') {
										for (var y = 0; y < form.entries[i].entries.length; y++) {
											if (form.entries[i].entries[y].name === 'wednesday') {
												if (form.entries[i].entries[y + 1] && form.entries[i].entries[y + 1].name === 'fromWednesday') {
													form.entries[i].entries.splice(y + 1, 2);
													
													var ele1 = angular.element(document.getElementsByClassName("tr-wednesday-wrapper"));
													var ele2 = angular.element(document.getElementsByClassName("tr-fromWednesday-wrapper"));
													var ele3 = angular.element(document.getElementsByClassName("tr-tillWednesday-wrapper"));
													ele1.removeClass('myClass');
													ele2.removeClass('myClass');
													ele3.removeClass('myClass');
												}
												else {
													form.entries[i].entries.splice(
														y + 1, 0,
														{
															'name': 'fromWednesday',
															'label': translation.from[LANG],
															'type': 'text',
															'placeholder': '09:00',
															'required': false
														},
														{
															'name': 'tillWednesday',
															'label': translation.till[LANG],
															'type': 'text',
															'placeholder': '20:00',
															'required': false
														}
													);
													
													setTimeout(function () {
														var ele1 = angular.element(document.getElementsByClassName("tr-wednesday-wrapper"));
														var ele2 = angular.element(document.getElementsByClassName("tr-fromWednesday-wrapper"));
														var ele3 = angular.element(document.getElementsByClassName("tr-tillWednesday-wrapper"));
														ele1.addClass('myClass');
														ele2.addClass('myClass');
														ele3.addClass('myClass');
													}, 5);
												}
												break;
											}
										}
									}
								}
							}
						},
						{
							"type": "checkbox",
							"name": "thursday",
							"value": [{v: "thursday", l: translation.thursday[LANG]}],
							"onAction": function (id, data, form) {
								for (var i = 0; i < form.entries.length; i++) {
									if (form.entries[i].name === 'days') {
										for (var y = 0; y < form.entries[i].entries.length; y++) {
											if (form.entries[i].entries[y].name === 'thursday') {
												if (form.entries[i].entries[y + 1] && form.entries[i].entries[y + 1].name === 'fromThursday') {
													form.entries[i].entries.splice(y + 1, 2);
													
													var ele1 = angular.element(document.getElementsByClassName("tr-thursday-wrapper"));
													var ele2 = angular.element(document.getElementsByClassName("tr-fromThursday-wrapper"));
													var ele3 = angular.element(document.getElementsByClassName("tr-tillThursday-wrapper"));
													ele1.removeClass('myClass');
													ele2.removeClass('myClass');
													ele3.removeClass('myClass');
												}
												else {
													form.entries[i].entries.splice(
														y + 1, 0,
														{
															'name': 'fromThursday',
															'label': translation.from[LANG],
															'type': 'text',
															'placeholder': '09:00',
															'required': false
														},
														{
															'name': 'tillThursday',
															'label': translation.till[LANG],
															'type': 'text',
															'placeholder': '20:00',
															'required': false
														}
													);
													
													setTimeout(function () {
														var ele1 = angular.element(document.getElementsByClassName("tr-thursday-wrapper"));
														var ele2 = angular.element(document.getElementsByClassName("tr-fromThursday-wrapper"));
														var ele3 = angular.element(document.getElementsByClassName("tr-tillThursday-wrapper"));
														ele1.addClass('myClass');
														ele2.addClass('myClass');
														ele3.addClass('myClass');
													}, 5);
												}
												break;
											}
										}
									}
								}
							}
						},
						{
							"type": "checkbox",
							"name": "friday",
							"value": [{v: "friday", l: translation.friday[LANG]}],
							"onAction": function (id, data, form) {
								for (var i = 0; i < form.entries.length; i++) {
									if (form.entries[i].name === 'days') {
										for (var y = 0; y < form.entries[i].entries.length; y++) {
											if (form.entries[i].entries[y].name === 'friday') {
												if (form.entries[i].entries[y + 1] && form.entries[i].entries[y + 1].name === 'fromFriday') {
													form.entries[i].entries.splice(y + 1, 2);
													
													var ele1 = angular.element(document.getElementsByClassName("tr-friday-wrapper"));
													var ele2 = angular.element(document.getElementsByClassName("tr-fromFriday-wrapper"));
													var ele3 = angular.element(document.getElementsByClassName("tr-tillFriday-wrapper"));
													ele1.removeClass('myClass');
													ele2.removeClass('myClass');
													ele3.removeClass('myClass');
												}
												else {
													form.entries[i].entries.splice(
														y + 1, 0,
														{
															'name': 'fromFriday',
															'label': translation.from[LANG],
															'type': 'text',
															'placeholder': '09:00',
															'required': false
														},
														{
															'name': 'tillFriday',
															'label': translation.till[LANG],
															'type': 'text',
															'placeholder': '20:00',
															'required': false
														}
													);
													
													setTimeout(function () {
														var ele1 = angular.element(document.getElementsByClassName("tr-friday-wrapper"));
														var ele2 = angular.element(document.getElementsByClassName("tr-fromFriday-wrapper"));
														var ele3 = angular.element(document.getElementsByClassName("tr-tillFriday-wrapper"));
														ele1.addClass('myClass');
														ele2.addClass('myClass');
														ele3.addClass('myClass');
													}, 5);
												}
												break;
											}
										}
									}
								}
							}
						},
						{
							"type": "checkbox",
							"name": "saturday",
							"value": [{v: "saturday", l: translation.saturday[LANG]}],
							"onAction": function (id, data, form) {
								for (var i = 0; i < form.entries.length; i++) {
									if (form.entries[i].name === 'days') {
										for (var y = 0; y < form.entries[i].entries.length; y++) {
											if (form.entries[i].entries[y].name === 'saturday') {
												if (form.entries[i].entries[y + 1] && form.entries[i].entries[y + 1].name === 'fromSaturday') {
													form.entries[i].entries.splice(y + 1, 2);
													
													var ele1 = angular.element(document.getElementsByClassName("tr-saturday-wrapper"));
													var ele2 = angular.element(document.getElementsByClassName("tr-fromSaturday-wrapper"));
													var ele3 = angular.element(document.getElementsByClassName("tr-tillSaturday-wrapper"));
													ele1.removeClass('myClass');
													ele2.removeClass('myClass');
													ele3.removeClass('myClass');
												}
												else {
													form.entries[i].entries.splice(
														y + 1, 0,
														{
															'name': 'fromSaturday',
															'label': translation.from[LANG],
															'type': 'text',
															'placeholder': '09:00',
															'required': false
														},
														{
															'name': 'tillSaturday',
															'label': translation.till[LANG],
															'type': 'text',
															'placeholder': '20:00',
															'required': false
														}
													);
													
													setTimeout(function () {
														var ele1 = angular.element(document.getElementsByClassName("tr-saturday-wrapper"));
														var ele2 = angular.element(document.getElementsByClassName("tr-fromSaturday-wrapper"));
														var ele3 = angular.element(document.getElementsByClassName("tr-tillSaturday-wrapper"));
														ele1.addClass('myClass');
														ele2.addClass('myClass');
														ele3.addClass('myClass');
													}, 5);
												}
												break;
											}
										}
									}
								}
							}
						},
						{
							"type": "checkbox",
							"name": "sunday",
							"value": [{v: "sunday", l: translation.sunday[LANG]}],
							"onAction": function (id, data, form) {
								for (var i = 0; i < form.entries.length; i++) {
									if (form.entries[i].name === 'days') {
										for (var y = 0; y < form.entries[i].entries.length; y++) {
											if (form.entries[i].entries[y].name === 'sunday') {
												if (form.entries[i].entries[y + 1] && form.entries[i].entries[y + 1].name === 'fromSunday') {
													form.entries[i].entries.splice(y + 1, 2);
													
													var ele1 = angular.element(document.getElementsByClassName("tr-sunday-wrapper"));
													var ele2 = angular.element(document.getElementsByClassName("tr-fromSunday-wrapper"));
													var ele3 = angular.element(document.getElementsByClassName("tr-tillSunday-wrapper"));
													ele1.removeClass('myClass');
													ele2.removeClass('myClass');
													ele3.removeClass('myClass');
												}
												else {
													form.entries[i].entries.splice(
														y + 1, 0,
														{
															'name': 'fromSunday',
															'label': translation.from[LANG],
															'type': 'text',
															'placeholder': '09:00',
															'required': false
														},
														{
															'name': 'tillSunday',
															'label': translation.till[LANG],
															'type': 'text',
															'placeholder': '20:00',
															'required': false
														}
													);
													
													setTimeout(function () {
														var ele1 = angular.element(document.getElementsByClassName("tr-sunday-wrapper"));
														var ele2 = angular.element(document.getElementsByClassName("tr-fromSunday-wrapper"));
														var ele3 = angular.element(document.getElementsByClassName("tr-tillSunday-wrapper"));
														ele1.addClass('myClass');
														ele2.addClass('myClass');
														ele3.addClass('myClass');
													}, 5);
												}
												break;
											}
										}
									}
								}
							}
						}
					]
				},
				{
					'name': 'exception',
					'label': translation.exceptionalDates[LANG],
					'type': 'group',
					'class': 'exception',
					'collapsed': false,
					"entries": []
				}
				//,
				// {
				// 	'name': 'ypMid',
				// 	'label': translation.ypMerchantId[LANG],
				// 	'type': 'select',
				// 	'value': [{
				// 		v: '',
				// 		l: translation.chooseOneMid[LANG]
				// 	}],
				// 	"fieldMsg": translation.ypMidFieldMsg[LANG],
				// 	'placeholder': '',
				// 	'required': false
				// }
			
			]
		}
	},
	
	"updateStock": {
		form: {
			'entries': [
				{
					'name': 'availability',
					'label': translation.availability[LANG],
					'type': 'number',
					'value': 0,
					'placeholder': '10',
					'required': true
				},
				{
					'name': 'price',
					'label': translation.price[LANG],
					'type': 'number',
					'value': 0,
					'placeholder': '15',
					'required': true
				}
			]
			
		}
	},
	
	"updateTreats": {
		"form": {
			"entries": [
				{
					'name': "treats",
					"type": "checkbox",
					"value": []
					
				}
			]
		}
	}
};

console.log('loaded knowledgeBase ModuleQaConfig');