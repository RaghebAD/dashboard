"use strict";
var merchantsApp = soajsApp.components;

merchantsApp.controller('merchantPickupMethodsModuleQaCtrl', ['$scope', '$modal', 'ngDataApi', 'injectFiles', '$timeout', '$routeParams', '$cookies', function ($scope, $modal, ngDataApi, injectFiles, $timeout, $routeParams, $cookies) {
	$scope.ModuleQaMpLocation = ModuleQaMpLocation;
	$scope.merchantId = $routeParams.id;
	$scope.code = $cookies.get('knowledgebase_merchant') || '';
	$scope.$parent.isUserLoggedIn();
	
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, knowledgeBaseModuleQaConfig.permissions);
	
	$scope.grid = {rows: []};
	$scope.formConfig = {
		"entries": [
			{
				'name': 'driver',
				'label': translation.driverName[LANG],
				'type': 'select',
				'value': [],
				'tooltip': translation.driverNameTooltip[LANG],
				'required': true
			},
			{
				'name': 'label',
				'label': translation.driverLabel[LANG],
				'type': 'text',
				'value': '',
				'tooltip': translation.driverLabelTooltip[LANG],
				'placeholder': translation.driverLabelPlaceholder[LANG],
				'required': true
			},
			{
				'name': 'active',
				'label': translation.active[LANG],
				'type': 'radio',
				'value': [{'v': true, 'l': translation.yes[LANG], 'selected': true}, {
					'v': false,
					'l': translation.no[LANG]
				}],
				'required': true
			},
			{
				"name": "config",
				"label": translation.configuration[LANG],
				"type": "group",
				"entries": []
			}
		]
	};
	
	//function that lists the Delivery Methods in a grid
	$scope.listEntries = function () {
		$scope.noAdd = false;
		
		var opts;
		if ($scope.access.owner.merchants.get) {
			opts = {
				"routeName": "/knowledgebase/owner/merchants" + $scope.merchantId,
				"method": "get",
				"params": {
					"code": $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		else {
			opts = {
				"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId,
				"method": "get",
				"params": {
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				}
			};
		}
		
		getSendDataFromServer($scope, ngDataApi, opts, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				if (!response.pickup) {
					response.pickup = {};
				}
				$scope.myMerchant = response;
				var options = {
					'grid': {
						recordsPerPageArray: [5, 10, 50, 100],
						'columns': [
							{'label': translation.driver[LANG], 'field': 'label'},
							{'label': translation.active[LANG], 'field': 'active'},
							{'label': translation.merchantSpecific[LANG], 'field': 'custom'}
						],
						'defaultLimit': 50
					},
					'defaultSortField': 'active',
					'defaultSortASC': true,
					'data': response.pickup.methods || [],
					'left': [
						{
							'icon': 'search',
							'label': translation.viewItem[LANG],
							'handler': 'viewEntry'
						},
						{
							'label': translation.edit[LANG],
							'icon': 'pencil2',
							'handler': 'editEntry'
						},
						{
							'label': translation.edit[LANG],
							'icon': 'spinner9',
							'handler': 'changeEntryStatus'
						},
						{
							'label': translation.delete[LANG],
							'icon': 'cross',
							'msg': translation.areYouSureYouWantToRemoveThisMethod[LANG],
							'handler': 'deleteEntry'
						}
					]
				};
				buildGrid($scope, options);
				
				$scope.checkAdd();
			}
		});
	};
	
	$scope.checkAdd = function () {
		//check if merchant pickup methods is the same as the drivers, then disable add
		if ($scope.myMerchant.pickup && $scope.myMerchant.pickup.methods) {
			$scope.noAdd = Object.keys($scope.drivers).length <= $scope.myMerchant.pickup.methods.length;
		}
	};
	
	$scope.viewEntry = function (oneDataRecord) {
		$modal.open({
			templateUrl: "infoBoxDriver.html",
			size: 'dialog',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewingOnePickupMethod[LANG];
				$scope.data = oneDataRecord;
				fixBackDrop();
				setTimeout(function () {
					highlightMyCode()
				}, 500);
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};
	
	$scope.addEntry = function () {
		var formFields = angular.copy($scope.formConfig.entries);
		resetFormFields();
		
		////push into formFields the config.from returned per driver
		formFields[0].onAction = function (id, data, form) {
			form.entries[3].entries = [];
			if (data.indexOf("_owner") === -1) {
				var clones = angular.copy($scope.drivers[data]);
				clones.entries.forEach(function (oneEntry) {
					form.entries[3].entries.push(oneEntry);
				});
			}
			else {
				var ownerDriver = angular.copy($scope.ownerDriver);
				ownerDriver.forEach(function (oneDriver) {
					if (oneDriver.driver === data.replace("_owner", "")) {
						form.entries[1].value = oneDriver.label;
						form.formData.label = oneDriver.label;
					}
				});
			}
		};
		
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'addpickupMethod',
			'label': translation.addPickupMethod[LANG],
			'actions': [
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var postData = {
							custom: true,
							config: {}
						};
						
						var predefined = [];
						$scope.formConfig.entries.forEach(function (oneField) {
							predefined.push(oneField.name);
						});
						
						for (var i in formData) {
							if (predefined.indexOf(i) !== -1) {
								postData[i] = formData[i];
							}
							else {
								postData.config[i] = formData[i];
							}
						}
						
						if (postData.driver.indexOf("_owner") !== -1) {
							postData.driver = postData.driver.replace("_owner", "");
							postData.custom = false;
							var ownerDriver = angular.copy($scope.ownerDriver);
							ownerDriver.forEach(function (oneDriver) {
								if (oneDriver.driver === postData.driver) {
									postData.config = oneDriver.config;
								}
							});
						}
						if (!$scope.myMerchant.pickup) {
							$scope.myMerchant.pickup = {
								methods: []
							};
						}
						if (!$scope.myMerchant.pickup.methods) {
							$scope.myMerchant.pickup.methods = [];
						}
						if (postData.driver && postData.label && Object.keys(postData.config).length > 0) {
							$scope.myMerchant.pickup.methods.push(postData);
							var opts = {
								"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId + "/configure",
								"method": "send",
								"params": {
									"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
								},
								"data": {
									"merchantConfig": {
										"pickup": $scope.myMerchant.pickup
									}
								}
							};
							if ($scope.access.owner.merchants.configure) {
								opts = {
									"routeName": "/knowledgebase/owner/merchants/" + $scope.merchantId + "/configure",
									"method": "send",
									"params": {
										"code": $scope.code,
										"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
									},
									"data": {
										"merchantConfig": {
											"pickup": $scope.myMerchant.pickup
										}
									}
								};
							}
							else {
								
							}
							
							getSendDataFromServer($scope, ngDataApi, opts, function (error) {
								if (error) {
									$scope.form.displayAlert('danger', error.message);
								}
								else {
									$scope.$parent.displayAlert('success', translation.merchantPickupStatusChangedSuccessfully[LANG]);
									$scope.modalInstance.close();
									$scope.form.formData = {};
									$scope.checkAdd();
									$scope.listEntries();
								}
							});
						}
						else {
							$scope.form.displayAlert('danger', translation.someInputsAreStillMissing[LANG]);
						}
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);
		
		//custom function to reset form fields
		function resetFormFields() {
			var myArray = formFields[0];
			myArray.value = [];
			
			if ($scope.drivers) {
				var ownerDriver = angular.copy($scope.ownerDriver);
				
				var driverNames = Object.keys($scope.drivers);
				driverNames.forEach(function (oneDriverName) {
					
					if ($scope.grid.filteredRows.length === 0) {
						myArray.value.push({
							"v": oneDriverName,
							"l": oneDriverName
						});
					}
					else {
						for (var i = 0; i < $scope.grid.filteredRows.length; i++) {
							//skip existing drivers
							if ($scope.grid.filteredRows[i].driver === oneDriverName) {
								continue;
							}
							myArray.value.push({
								"v": oneDriverName,
								"l": oneDriverName
							});
							
						}
					}
				});
				
				ownerDriver.forEach(function (oneDriver) {
					if ($scope.grid.filteredRows.length === 0) {
						myArray.value.push({
							"v": oneDriver.driver + "_owner",
							"l": oneDriver.label + " (" + translation.useMarketplaceDefault[LANG] + ")"
						});
					}
					else {
						for (var i = 0; i < $scope.grid.filteredRows.length; i++) {
							//skip existing drivers
							if ($scope.grid.filteredRows[i].driver === oneDriver.driver) {
								continue;
							}
							myArray.value.push({
								"v": oneDriver.driver + "_owner",
								"l": oneDriver.label + " (" + translation.useMarketplaceDefault[LANG] + ")"
							});
						}
					}
					
				});
			}
			
		}
	};
	
	$scope.editEntry = function (data) {
		var formFields = angular.copy($scope.formConfig.entries);
		resetFormFields(data);
		
		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'editpickupMethod',
			'label': 'Edit pickup Method',
			'data': data,
			'actions': [
				{
					'type': 'submit',
					'label': translation.saveChanges[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var postData = {
							custom: data.custom,
							config: {}
						};
						
						var predefined = [];
						$scope.formConfig.entries.forEach(function (oneField) {
							predefined.push(oneField.name);
						});
						
						for (var i in formData) {
							if (predefined.indexOf(i) !== -1) {
								postData[i] = formData[i];
							}
							else {
								postData.config[i] = formData[i];
							}
						}
						
						for (var i = 0; i < $scope.myMerchant.pickup.methods.length; i++) {
							if ($scope.myMerchant.pickup.methods[i].driver === postData.driver) {
								$scope.myMerchant.pickup.methods[i] = postData;
								break;
							}
						}
						
						if (postData.driver && postData.label && Object.keys(postData.config).length > 0) {
							var opts = {
								"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId + "/configure",
								"method": "send",
								"params": {
									"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
								},
								"data": {
									"merchantConfig": {
										"pickup": $scope.myMerchant.pickup
									}
								}
							};
							if ($scope.access.owner.merchants.configure) {
								opts = {
									"routeName": "/knowledgebase/owner/merchants/" + $scope.merchantId + "/configure",
									"method": "send",
									"params": {
										"code": $scope.code,
										"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
									},
									"data": {
										"merchantConfig": {
											"pickup": $scope.myMerchant.pickup
										}
									}
								};
							}
							else {
							}
							
							getSendDataFromServer($scope, ngDataApi, opts, function (error) {
								if (error) {
									$scope.form.displayAlert('danger', error.message);
								}
								else {
									$scope.$parent.displayAlert('success', translation.merchantPickupStatusChangedSuccessfully[LANG]);
									$scope.modalInstance.close();
									$scope.form.formData = {};
									$scope.listEntries();
								}
							});
						}
						else {
							$scope.form.displayAlert('danger', translation.someInputsAreStillMissing[LANG]);
						}
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		
		buildFormWithModal($scope, $modal, options, function () {
			//push into formFields the config.from returned per driver
			$scope.form.entries[3].entries = [];
			var clones = angular.copy($scope.drivers[data.driver].entries);
			clones.forEach(function (oneEntry) {
				for (var i in data.config) {
					if (i === oneEntry.name) {
						if (Array.isArray(oneEntry.value)) {
							oneEntry.value.forEach(function (sV) {
								if (Array.isArray(data.config[i])) {
									if (data.config[i].indexOf(sV.v) !== -1) {
										sV.selected = true;
										$scope.form.formData[i] = data.config[i];
									}
								}
								else if (data.config[i].toString() === sV.v.toString()) {
									sV.selected = true;
									$scope.form.formData[i] = data.config[i];
								}
							})
						}
						else {
							if (!data.custom) {
								oneEntry.type = 'readonly';
							}
							oneEntry.value = data.config[i];
							$scope.form.formData[i] = data.config[i];
						}
					}
				}
				$scope.form.entries[3].entries.push(oneEntry);
			});
		});
		
		//custom function to reset form fields
		function resetFormFields(data) {
			var myArray = formFields[0];
			myArray.value = data.driver;
			myArray.type = "readonly";
			
			if (!data.custom) {
				formFields[1].type = 'readonly';
			}
		}
	};
	
	$scope.deleteEntry = function (data) {
		for (var i = $scope.myMerchant.pickup.methods.length - 1; i >= 0; i--) {
			if ($scope.myMerchant.pickup.methods[i].driver === data.driver) {
				$scope.myMerchant.pickup.methods.splice(i, 1);
			}
		}
		
		var opts = {
			"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId + "/configure",
			"method": "send",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			},
			"data": {
				"merchantConfig": {
					"pickup": $scope.myMerchant.pickup
				}
			}
		};
		if ($scope.access.owner.merchants.configure) {
			opts = {
				"routeName": "/knowledgebase/owner/merchants/" + $scope.merchantId + "/configure",
				"method": "send",
				"params": {
					"code": $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				"data": {
					"merchantConfig": {
						"pickup": $scope.myMerchant.pickup
					}
				}
			};
		}
		else {
		}
		
		getSendDataFromServer($scope, ngDataApi, opts, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.merchantPickupDeletedSuccessfully[LANG]);
				$scope.checkAdd();
				$scope.listEntries();
			}
		});
	};
	
	$scope.changeEntryStatus = function (data) {
		for (var i = $scope.myMerchant.pickup.methods.length - 1; i >= 0; i--) {
			if ($scope.myMerchant.pickup.methods[i].driver === data.driver) {
				$scope.myMerchant.pickup.methods[i].active = !$scope.myMerchant.pickup.methods[i].active;
			}
		}
		
		var opts = {
			"routeName": "/knowledgebase/tenant/merchants/" + $scope.merchantId + "/configure",
			"method": "send",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			},
			"data": {
				"merchantConfig": {
					"pickup": $scope.myMerchant.pickup
				}
			}
		};
		if ($scope.access.owner.merchants.configure) {
			opts = {
				"routeName": "/knowledgebase/owner/merchants/" + $scope.merchantId + "/configure",
				"method": "send",
				"params": {
					"code": $scope.code,
					"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				},
				"data": {
					"merchantConfig": {
						"pickup": $scope.myMerchant.pickup
					}
				}
			};
		}
		else {
		}
		
		getSendDataFromServer($scope, ngDataApi, opts, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.merchantPickupStatusChangedSuccessfully[LANG]);
			}
		});
		
	};
	
	function listMethods() {
		var opts = {
			"routeName": "/order/owner/pickupMethods/list",
			"method": "get",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		getSendDataFromServer($scope, ngDataApi, opts, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.drivers = response.drivers;
				$scope.ownerDriver = response.list;
				$scope.listEntries();
			}
		});
	}
	
	listMethods();
	
}]);