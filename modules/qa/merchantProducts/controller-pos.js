"use strict";
var posApp = soajsApp.components;

posApp.controller('merchantSummaryModuleQaCtrl', ['$scope', '$routeParams', 'ngDataApi', '$cookies', 'merchantsModuleQaSrv', function ($scope, $routeParams, ngDataApi, $cookies, merchantsModuleQaSrv) {
	$scope.ModuleQaMpLocation = ModuleQaMpLocation;
	$scope.merchantName = '';
	$scope.$parent.isUserLoggedIn();
	
	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}
	$scope.access = {};
	constructModulePermissions($scope, $scope.access, knowledgeBaseModuleQaConfig.permissions);
	
	$scope.code = $cookies.get('knowledgebase_merchant') || '';
	
	$scope.viewEntry = function () {
		var opts = {
			"routeName": "/knowledgebase/tenant/merchants/" + $routeParams.id,
			"method": "get",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		if ($scope.access.owner.merchants.list) {
			opts.routeName = "/knowledgebase/owner/merchants/" + $routeParams.id;
			opts.params.code = $scope.code;
		}
		else {
		}
		
		merchantsModuleQaSrv.getEntriesFromApi($scope, opts, function (error, data) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.merchantName = data.name;
			}
		});
	};
	
	$scope.viewEntry();
}]);

posApp.controller('posModuleQaCtrl', ['$scope', '$modal', '$routeParams', 'ngDataApi', 'injectFiles', '$timeout', 'merchantsModuleQaSrv', '$cookies', function ($scope, $modal, $routeParams, ngDataApi, injectFiles, $timeout, merchantsModuleQaSrv, $cookies) {
	$scope.pos = angular.extend($scope);
	$scope.pos.$parent.isUserLoggedIn();
	$scope.pos.merchantId = $routeParams.id;
	$scope.pos.code = $cookies.get('knowledgebase_merchant') || '';
	
	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}
	$scope.pos.access = {};
	constructModulePermissions($scope.pos, $scope.pos.access, knowledgeBaseModuleQaConfig.permissions);
	
	$scope.pos.listPosS = function () {
		var opts = {
			"routeName": "/knowledgebase/tenant/merchants/" + $scope.pos.merchantId + "/pos",
			"method": "get",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		if ($scope.pos.access.owner.pos.list) {
			opts.routeName = "/knowledgebase/owner/merchants/" + $scope.pos.merchantId + "/pos";
			opts.params.code = $scope.pos.code;
		}
		else {
		}
		
		merchantsModuleQaSrv.getEntriesFromApi($scope.pos, opts, function (error, response) {
			if (error) {
				$scope.pos.$parent.displayAlert('danger', error.message);
			}
			else {
				merchantsModuleQaSrv.printPos($scope.pos, response);
			}
		});
	};
	
	$scope.pos.viewPos = function (oneDataRecord) {
		$modal.open({
			templateUrl: "infoBox.html",
			size: 'dialog',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				var data = angular.copy(oneDataRecord);
				delete data.phone;
				delete data.addressString;
				delete data.email;
				delete data.name;
				delete data.operates;
				var exc = {};
				for (var oneExc in oneDataRecord.operates.excluded) {
					exc[oneExc] = (oneDataRecord.operates.excluded[oneExc].length > 0) ? oneDataRecord.operates.excluded[oneExc] : "All Day";
				}
				if (Object.keys(exc).length > 0) {
					data["exceptional dates"] = exc;
				}
				if (Object.keys(oneDataRecord.operates.days).length > 0) {
					data["days of operation"] = oneDataRecord.operates.days;
				}
				$scope.title = translation.viewingOnePOS[LANG];
				$scope.data = data;
				fixBackDrop();
				setTimeout(function () {
					highlightMyCode()
				}, 500);
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};
	
	$scope.changeStatus = function (data) {
		var status = (data.status === 'active') ? 'disabled' : 'active';
		var opts = {
			"method": "put",
			"routeName": "/knowledgebase/tenant/merchants/" + $scope.pos.merchantId + "/pos/" + data.id + "/status",
			"params": {
				"status": status,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		if ($scope.pos.access.owner.pos.changeStatus) {
			opts.routeName = "/knowledgebase/owner/merchants/" + $scope.pos.merchantId + "/pos/" + data.id + "/status";
			opts.params.code = $scope.pos.code;
		}
		
		overlayLoading.show();
		getSendDataFromServer($scope.pos, ngDataApi, opts, function (error) {
			overlayLoading.hide();
			if (error) {
				$scope.pos.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.pos.$parent.displayAlert('success', translation.posStatusChangedSuccessfully[LANG]);
				$scope.pos.listPosS();
			}
		});
		
	};
	
	$scope.pos.deletePos = function (data) {
		var opts = {
			"method": "get",
			"routeName": "/knowledgebase/tenant/merchants/" + $scope.pos.merchantId + "/pos/" + data.id,
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		if ($scope.pos.access.owner.pos.delete) {
			opts.routeName = "/knowledgebase/owner/merchants/" + $scope.pos.merchantId + "/pos/" + data.id;
			opts.params.code = $scope.pos.code;
		}
		else {
		}
		getSendDataFromServer($scope.pos, ngDataApi, opts, function (error) {
			if (error) {
				$scope.pos.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.pos.$parent.displayAlert('success', translation.posDeletedSuccessfully[LANG]);
				$scope.pos.listPosS();
			}
		});
	};
	
	$scope.pos.addPos = function () {
		var config = {
			'name': '',
			'label': '',
			'actions': {},
			"entries": angular.copy(knowledgeBaseModuleQaConfig.pos.form.entries)
		};
		var ct = 0;
		var count = 0;
		
		var day = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
		var Day = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
		
		config.entries.forEach(function (oneEntry) {
			if ($scope.pos.myMerchant.pos && Array.isArray($scope.pos.myMerchant.pos) && $scope.pos.myMerchant.pos.length > 0) {
				if (oneEntry.name === 'currency') {
					oneEntry.value = $scope.pos.myMerchant.pos[0].currency;
				}
			}
			
			if (oneEntry.name === 'type') {
				oneEntry.value[0].selected = true;
			}
			
			if (oneEntry.name === 'address') {
				oneEntry.entries.forEach(function (oneField) {
					if (oneField.name === 'email') {
						oneField.value = ($scope.pos.myMerchant.information && $scope.pos.myMerchant.information.email) ? $scope.pos.myMerchant.information.email : "";
					}
					if (oneField.name === 'phone') {
						if ($scope.pos.myMerchant.information && $scope.pos.myMerchant.information.phone) {
							if ($scope.pos.myMerchant.information.phone.charAt(0) === "+" && $scope.pos.myMerchant.information.phone.charAt(1) === "1") {
								oneField.value = $scope.pos.myMerchant.information.phone.substring(2);
							}
							else {
								oneField.value = $scope.pos.myMerchant.information.phone;
							}
						}
						else {
							oneField.value = "";
						}
					}
				});
			}
			
			if (oneEntry.name === 'exception') {
				var clone = angular.copy(knowledgeBaseModuleQaConfig.pos.form.oneException);
				for (var i = 0; i < clone.length; i++) {
					clone[i].name = clone[i].name.replace("%count%", ct);
				}
				oneEntry.entries = oneEntry.entries.concat(clone);
				ct++;
			}
			if (oneEntry.name === 'otherEmails') {
				var clone = angular.copy(knowledgeBaseModuleQaConfig.pos.form.oneEmail);
				for (var i = 0; i < clone.length; i++) {
					clone[i].name = clone[i].name.replace("%count%", count);
				}
				oneEntry.entries = oneEntry.entries.concat(clone);
				count++;
			}
			
		});
		
		var options = {
			timeout: $timeout,
			form: config,
			'name': 'addPos',
			'label': translation.addPOS[LANG],
			'actions': [
				// {
				// 	'type': 'submit',
				// 	'label': translation.getYpMidOptions[LANG],
				// 	'btn': 'primary',
				// 	'action': function (formData) {
				// 		getYpMids(formData, function (error, response) {
				// 			if (error) {
				// 				$scope.pos.form.displayAlert('danger', error.message);
				// 			}
				// 			else {
				// 				$scope.pos.form.entries[x].value = response;
				// 			}
				// 		});
				// 	}
				// },
				{
					'type': 'button',
					'label': translation.addExceptionalDate[LANG],
					'btn': 'success',
					'action': function () {
						$scope.pos.form.entries.forEach(function (oneEntry) {
							if (oneEntry.name === 'exception') {
								var clone = angular.copy(knowledgeBaseModuleQaConfig.pos.form.oneException);
								for (var i = 0; i < clone.length; i++) {
									clone[i].name = clone[i].name.replace("%count%", ct);
								}
								oneEntry.entries = oneEntry.entries.concat(clone);
								ct++;
							}
						});
					}
				},
				{
					'type': 'hidden',
					'label': translation.addOtherEmail[LANG],
					'btn': 'success',
					'action': function () {
						$scope.pos.form.entries.forEach(function (oneEntry) {
							if (oneEntry.name === 'otherEmails' && oneEntry.entries.length < 11) {
								var clone = angular.copy(knowledgeBaseModuleQaConfig.pos.form.oneEmail);
								for (var i = 0; i < clone.length; i++) {
									clone[i].name = clone[i].name.replace("%count%", count);
								}
								oneEntry.entries = oneEntry.entries.concat(clone);
								count++;
							}
						});
					}
				},
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formData) {
						var pickup = true;
						var shipping = true;
						if (formData.pickup === false) {
							pickup = false;
						}
						if (formData.shipping === false) {
							pickup = shipping;
						}
						var postData = {
							"id": formData.id,
							"name": formData.name,
							"currency": formData.currency.toUpperCase(),
							'type': formData.type,
							"address": {
								"addressLine1": formData.addressLine1,
								"state": formData.state,
								"city": formData.city,
								"zip": formData.zip,
								"country": formData.country,
								"name": formData.name,
								"email": formData.email,
								"phone": formData.phone
							},
							"pickup": pickup,
							"shipping": shipping,
							"preparation_time": formData.preparation_time,
							"operates": {
								'days': {},
								'excluded': {}
							}
						};
						
						if (formData.status) {
							postData.status = formData.status;
						}
						else {
							postData.status = 'disabled';
						}
						if (formData.addressLine2) {
							postData.address.addressLine2 = formData.addressLine2;
						}
						if (formData.addressLine3) {
							postData.address.addressLine3 = formData.addressLine3;
						}
						if (formData.id) {
							postData.id = formData.id;
						}
						
						for (var x = 0; x < day.length; x++) {
							if (formData['from' + Day[x]] && formData['till' + Day[x]] && formData[day[x]][0] === day[x]) {
								if (formData['from' + Day[x]] && formData['till' + Day[x]]) {
									var from = formData['from' + Day[x]];
									var till = formData['till' + Day[x]];
									from = parseInt(from.replace(":", ""));
									till = parseInt(till.replace(":", ""));
									if (from < till) {
										postData.operates.days[day[x]] = [formData['from' + Day[x]], formData['till' + Day[x]]];
									}
									else {
										return $scope.pos.form.displayAlert('danger', translation.fromCantBeEqualOrGreaterThanTillInDaysOfOperation[LANG] + " ( " + translation[day[x]][LANG] + " )");
									}
								}
							}
						}
						for (var y = 0; y < ct; y++) {
							if (formData['exceptionF' + y] && formData['exceptionT' + y]) {
								var from = formData['exceptionF' + y];
								var till = formData['exceptionT' + y];
								from = parseInt(from.replace(":", ""));
								till = parseInt(till.replace(":", ""));
								if (from < till) {
									postData.operates.excluded[formData['exceptionD' + y]] = [formData['exceptionF' + y], formData['exceptionT' + y]];
								}
								else {
									return $scope.pos.form.displayAlert('danger', translation.fromCantBeEqualOrGreaterThanTillInExceptionalDate[LANG] + " " + formData['exceptionD' + y]);
								}
							}
							else if (formData['exceptionD' + y]) {
								postData.operates.excluded[formData['exceptionD' + y]] = [];
							}
						}
						var otherEmails = [];
						for (var z = 0; z < count; z++) {
							if (formData['email' + z]) {
								otherEmails.push(formData['email' + z]);
							}
						}
						if (otherEmails.length > 0) {
							postData.otherEmails = otherEmails;
						}
						
						var opts = {
							"method": "send",
							"routeName": "/knowledgebase/tenant/merchants/" + $scope.pos.merchantId + "/pos",
							"params": {
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": {
								"pos": postData
							}
						};
						if ($scope.pos.access.owner.pos.add) {
							opts.routeName = "/knowledgebase/owner/merchants/" + $scope.pos.merchantId + "/pos";
							opts.params.code = $scope.pos.code;
						}
						else {
						}
						overlayLoading.show();
						getSendDataFromServer($scope.pos, ngDataApi, opts, function (error) {
							overlayLoading.hide();
							if (error) {
								$scope.pos.form.displayAlert('danger', error.message);
							}
							else {
								$scope.pos.$parent.displayAlert('success', translation.posAddedSuccessfully[LANG]);
								$scope.pos.modalInstance.close();
								$scope.pos.form.formData = {};
								$scope.pos.listPosS();
							}
						});
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.pos.modalInstance.dismiss('cancel');
						$scope.pos.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope.pos, $modal, options);
	};
	
	function getYpMids(formData, cb) {
		var postData = {
			"name": formData.name,
			"merchantName": $scope.pos.myMerchant.name
		};
		if (formData.address) {
			postData.address = formData.address;
		}
		else {
			postData.address = {
				"addressLine1": formData.addressLine1,
				"state": formData.state,
				"city": formData.city,
				"zip": formData.zip,
				"country": formData.country,
				"email": formData.email,
				"phone": formData.phone
			};
		}
		opts = {
			"method": "send",
			"routeName": "/knowledgebase/merchants/getYpMid",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			},
			"data": {
				"pos": postData
			}
		};
		
		overlayLoading.show();
		getSendDataFromServer($scope.pos, ngDataApi, opts, function (error, response) {
			overlayLoading.hide();
			if (error) {
				return cb(error);
			}
			else {
				var arrValues = [{
					v: '',
					l: translation.chooseOneMid[LANG]
				}];
				response.forEach(function (store) {
					arrValues.push(
						{
							v: store.mId,
							l: store.name + ': ' + store.address.street + ', ' + store.address.city + ', ' + store.address.prov + ". ph:" + store.phone + " (" + store.mId + ")"
						}
					);
				});
				return cb(null, arrValues);
			}
		});
		
	}
	
	$scope.pos.editPos = function (data) {
		var config = {
			'name': '',
			'label': '',
			'actions': {},
			"entries": angular.copy(knowledgeBaseModuleQaConfig.pos.form.entries)
		};
		var currentScope = $scope;
		var day = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
		var Day = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
		var selected = [];
		var Selected = [];
		var ct = 0;
		var count = 0;
		var arrMids = [{
			v: '',
			l: translation.chooseOneMid[LANG]
		}];
		
		function fillEntries() {
			config.entries.forEach(function (oneEntry) {
				if (oneEntry.name === 'status') {
					oneEntry.type = 'readonly';
					oneEntry.value = data.status;
				}
				if (oneEntry.name === 'address') {
					oneEntry.entries.forEach(function (oneField) {
						if (data.address) {
							if (oneField.name === "addressLine1" && data.address.addressLine1) {
								oneField.value = data.address.addressLine1;
							}
							if (oneField.name === "addressLine2" && data.address.addressLine2) {
								oneField.value = data.address.addressLine2;
							}
							if (oneField.name === "addressLine3" && data.address.addressLine3) {
								oneField.value = data.address.addressLine3;
							}
							if (oneField.name === "country" && data.address.country) {
								oneField.value.forEach(function (oneCountry) {
									if (oneCountry.v === data.address.country) {
										oneCountry.selected = true;
									}
								});
							}
							if (oneField.name === "state" && data.address.state) {
								oneField.value = data.address.state;
							}
							if (oneField.name === "city" && data.address.city) {
								oneField.value = data.address.city;
							}
							if (oneField.name === "zip" && data.address.zip) {
								oneField.value = data.address.zip;
							}
							if (oneField.name === "email" && data.address.email) {
								oneField.value = data.address.email;
							}
							if (oneField.name === "phone" && data.address.phone) {
								if (data.address.phone.charAt(0) === "+" && data.address.phone.charAt(1) === "1") {
									oneField.value = data.address.phone.substring(2);
									//data.address.phone = oneField.value;
								}
								else {
									oneField.value = data.address.phone;
								}
							}
						}
					});
				}
				else if (oneEntry.name === 'exception') {
					if (data.operates.excluded) {
						var exc = Object.keys(data.operates.excluded);
						for (var i = 0; i < exc.length; i++) {
							var clone = angular.copy(knowledgeBaseModuleQaConfig.pos.form.oneException);
							clone.forEach(function (oneField) {
								oneField.name = oneField.name.replace("%count%", ct);
								
								if (oneField.name === 'exceptionD' + ct) {
									oneField.value = exc[i];
								}
								if (oneField.name === 'exceptionF' + ct) {
									oneField.value = data.operates.excluded[exc[i]][0];
								}
								if (oneField.name === 'exceptionT' + ct) {
									oneField.value = data.operates.excluded[exc[i]][1];
								}
								if (oneField.type === 'html') {
									oneField.value = oneField.value.replace("%count%", ct);
								}
								
								oneEntry.entries.push(oneField);
							});
							ct++;
						}
					}
				}
				else if (oneEntry.name === 'otherEmails') {
					if (data.otherEmails) {
						for (var x = 0; x < data.otherEmails.length; x++) {
							var clone = angular.copy(knowledgeBaseModuleQaConfig.pos.form.oneEmail);
							clone.forEach(function (oneField) {
								oneField.name = oneField.name.replace("%count%", count);
								if (oneField.name === 'email' + count) {
									oneField.value = data.otherEmails[x]
								}
								if (oneField.type === 'html') {
									oneField.value = oneField.value.replace("%count%", count);
								}
								oneEntry.entries.push(oneField);
							});
							count++;
						}
					}
					else {
						var clone = angular.copy(knowledgeBaseModuleQaConfig.pos.form.oneEmail);
						for (var i = 0; i < clone.length; i++) {
							clone[i].name = clone[i].name.replace("%count%", count);
						}
						oneEntry.entries = oneEntry.entries.concat(clone);
						count++;
					}
				}
				else if (oneEntry.name === 'id') {
					oneEntry.type = "readonly";
				}
				else if (oneEntry.name === 'ypMid') {
					oneEntry.value = arrMids;
				}
			});
			for (var z = 0; z < day.length; z++) {
				for (var i = 0; i < config.entries.length; i++) {
					if (config.entries[i].name === 'days') {
						for (var y = 0; y < config.entries[i].entries.length; y++) {
							if (config.entries[i].entries[y].name === day[z] && data.operates.days[day[z]]) {
								config.entries[i].entries[y].value[0].selected = true;
								selected.push(day[z]);
								Selected.push(Day[z]);
								config.entries[i].entries.splice(
									y + 1, 0,
									{
										'name': 'from' + Day[z],
										'label': translation.from[LANG],
										'type': 'text',
										'placeholder': '09:00',
										'value': data.operates.days[day[z]][0],
										'required': false
									},
									{
										'name': 'till' + Day[z],
										'label': translation.till[LANG],
										'type': 'text',
										'placeholder': '20:00',
										'value': data.operates.days[day[z]][1],
										'required': false
									}
								);
							}
						}
					}
				}
			}
			
			var options = {
				timeout: $timeout,
				form: config,
				'name': 'editPos',
				'label': translation.editPos[LANG],
				'data': data,
				'actions': [
					// {
					// 	'type': 'submit',
					// 	'label': translation.getYpMidOptions[LANG],
					// 	'btn': 'primary',
					// 	'action': function (formData) {
					// 		getYpMids(formData, function (error, response) {
					// 			if (error) {
					// 				$scope.pos.form.displayAlert('danger', error.message);
					// 			}
					// 			else {
					// 				for (var x = 0; x < $scope.pos.form.entries.length; x++) {
					// 					if ($scope.pos.form.entries[x].name === 'ypMid') {
					// 						$scope.pos.form.entries[x].value = response;
					// 						break;
					// 					}
					// 				}
					// 			}
					// 		});
					// 	}
					// },
					{
						'type': 'button',
						'label': translation.addExceptionalDate[LANG],
						'btn': 'success',
						'action': function () {
							$scope.pos.form.entries.forEach(function (oneEntry) {
								if (oneEntry.name === 'exception') {
									var clone = angular.copy(knowledgeBaseModuleQaConfig.pos.form.oneException);
									for (var i = 0; i < clone.length; i++) {
										clone[i].name = clone[i].name.replace("%count%", ct);
									}
									oneEntry.entries = oneEntry.entries.concat(clone);
									ct++;
								}
							});
						}
					},
					{
						'type': 'hidden',
						'label': translation.addOtherEmail[LANG],
						'btn': 'success',
						'action': function () {
							$scope.pos.form.entries.forEach(function (oneEntry) {
								if (oneEntry.name === 'otherEmails' && oneEntry.entries.length < 11) {
									var clone = angular.copy(knowledgeBaseModuleQaConfig.pos.form.oneEmail);
									for (var i = 0; i < clone.length; i++) {
										clone[i].name = clone[i].name.replace("%count%", count);
									}
									oneEntry.entries = oneEntry.entries.concat(clone);
									count++;
								}
							});
						}
					},
					{
						'type': 'submit',
						'label': translation.submit[LANG],
						'btn': 'primary',
						'action': function (formData) {
							if (formData.phone.charAt(0) === "+" && formData.phone.charAt(1) === "1") {
								formData.phone = formData.phone.substring(2);
							}
							var postData = {
								"ypMid": formData.ypMid,
								"name": formData.name,
								"currency": formData.currency.toUpperCase(),
								'type': formData.type,
								"address": {
									"addressLine1": formData.addressLine1,
									"country": formData.country,
									"state": formData.state,
									"city": formData.city,
									"zip": formData.zip,
									"name": formData.name,
									"email": formData.email,
									"phone": formData.phone
								},
								"pickup": formData.pickup,
								"shipping": formData.shipping,
								"preparation_time": formData.preparation_time,
								"operates": {
									'days': {},
									'excluded': {}
								}
							};
							if (data.status) {
								postData.status = data.status;
							}
							
							if (formData.addressLine2) {
								postData.address.addressLine2 = formData.addressLine2;
							}
							if (formData.addressLine3) {
								postData.address.addressLine3 = formData.addressLine3;
							}
							if (formData.id) {
								postData.id = formData.id;
							}
							for (var x = 0; x < day.length; x++) {
								if (formData['from' + Day[x]] && formData['till' + Day[x]] && formData[day[x]][0] === day[x]) {
									if (formData['from' + Day[x]] && formData['till' + Day[x]]) {
										var from = formData['from' + Day[x]];
										var till = formData['till' + Day[x]];
										from = parseInt(from.replace(":", ""));
										till = parseInt(till.replace(":", ""));
										if (from < till) {
											postData.operates.days[day[x]] = [formData['from' + Day[x]], formData['till' + Day[x]]];
										}
										else {
											return $scope.pos.form.displayAlert('danger', translation.fromCantBeEqualOrGreaterThanTillInDaysOfOperation[LANG] + " ( " + translation[day[x]][LANG] + " )");
										}
									}
								}
							}
							for (var y = 0; y < ct; y++) {
								if (formData['exceptionF' + y] && formData['exceptionT' + y]) {
									var from = formData['exceptionF' + y];
									var till = formData['exceptionT' + y];
									from = parseInt(from.replace(":", ""));
									till = parseInt(till.replace(":", ""));
									if (from < till) {
										postData.operates.excluded[formData['exceptionD' + y]] = [formData['exceptionF' + y], formData['exceptionT' + y]];
									}
									else {
										return $scope.pos.form.displayAlert('danger', translation.fromCantBeEqualOrGreaterThanTillInExceptionalDate[LANG] + " " + formData['exceptionD' + y]);
									}
								}
								else if (formData['exceptionD' + y]) {
									postData.operates.excluded[formData['exceptionD' + y]] = [];
								}
							}
							var otherEmails = [];
							for (var z = 0; z < count; z++) {
								if (formData['email' + z]) {
									otherEmails.push(formData['email' + z]);
								}
							}
							if (otherEmails.length > 0) {
								postData.otherEmails = otherEmails;
							}
							var opts = {
								"method": "put",
								"routeName": "/knowledgebase/tenant/merchants/" + $scope.pos.merchantId + "/pos/" + data.id,
								"params": {
									"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
								},
								"data": {
									"pos": postData
								}
							};
							if ($scope.pos.access.owner.pos.edit) {
								opts.params.code = $scope.pos.code;
								opts.routeName = "/knowledgebase/owner/merchants/" + $scope.pos.merchantId + "/pos/" + data.id;
							}
							else {
							}
							overlayLoading.show();
							getSendDataFromServer($scope.pos, ngDataApi, opts, function (error) {
								overlayLoading.hide();
								if (error) {
									$scope.pos.form.displayAlert('danger', error.message);
								}
								else {
									$scope.pos.$parent.displayAlert('success', translation.posUpdatedSuccessfully[LANG]);
									$scope.pos.modalInstance.close();
									$scope.pos.form.formData = {};
									$scope.pos.listPosS();
								}
							});
						}
					},
					{
						'type': 'reset',
						'label': translation.cancel[LANG],
						'btn': 'danger',
						'action': function () {
							$scope.pos.modalInstance.dismiss('cancel');
							$scope.pos.form.formData = {};
						}
					}
				]
			};
			buildFormWithModal($scope.pos, $modal, options, function () {
				setTimeout(function () {
					for (var z = 0; z < selected.length; z++) {
						var ele1 = angular.element(document.getElementsByClassName("tr-" + selected[z] + "-wrapper"));
						var ele2 = angular.element(document.getElementsByClassName("tr-from" + Selected[z] + "-wrapper"));
						var ele3 = angular.element(document.getElementsByClassName("tr-till" + Selected[z] + "-wrapper"));
						ele1.addClass('myClass');
						ele2.addClass('myClass');
						ele3.addClass('myClass');
					}
				}, 300);
			});
		}
		
		// getYpMids(data, function (error, response) {
		// 	if (error) {
		// 		$scope.pos.$parent.displayAlert('danger', error.message);
		// 	}
		// 	else {
		// 		arrMids = response;
		// 	}
		// });
		fillEntries();
		
	};
	
	//get current merchant info
	var opts;
	if ($scope.pos.access.owner.merchants.get || $scope.pos.access.tenant.merchants.get) {
		opts = {
			"routeName": "/knowledgebase/owner/merchants/" + $scope.pos.merchantId,
			"method": "get",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		if ($scope.pos.access.owner.merchants.get) {
			opts.routeName = "/knowledgebase/owner/merchants/" + $scope.pos.merchantId;
			opts.params.code = $scope.pos.code;
		}
		else {
			opts.routeName = "/knowledgebase/tenant/merchants/" + $scope.pos.merchantId;
		}
		
		merchantsModuleQaSrv.getEntriesFromApi($scope.pos, opts, function (error, response) {
			if (error) {
				$scope.pos.$parent.displayAlert('danger', error.message);
			}
			else {
				$scope.pos.myMerchant = response;
				if ($scope.pos.access.owner.pos.list || $scope.pos.access.tenant.pos.list) {
					$scope.pos.listPosS();
				}
				//get delivery methods
				// merchantsModuleQaSrv.getEntriesFromApi($scope.pos, {
				// 	"routeName": "/order/owner/deliveryMethods/list",
				// 	"method": "get",
				// 	"params": {
				// 		"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
				// 	}
				// }, function (error, response) {
				// 	if (error) {
				// 		$scope.pos.$parent.displayAlert('danger', error.message);
				// 	}
				// 	else {
				// 		$scope.pos.deliveryMethods = response;
				//
				// 		if ($scope.pos.access.owner.pos.list || $scope.pos.access.tenant.pos.list) {
				// 			$scope.pos.listPosS();
				// 		}
				// 	}
				// });
			}
		});
	}
	
	injectFiles.injectCss(ModuleQaMpLocation + "/merchants.css");
	
}]);