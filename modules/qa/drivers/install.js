"use strict";
var ModuleQaDrvLocation = uiModuleQa + '/drivers';

var ModuleQaDrTranslation = {
	"shippingMethods": {
		"ENG": "Shipping Methods",
		"FRA": "Méthodes de Livraison"
	},
	"shippingMethod": {
		"ENG": "Shipping Method",
		"FRA": "Méthode de Livraison"
	},
	"taxCalculator": {
		"ENG": "Tax Calculator",
		"FRA": "Tax Calculator"
	},
	"addNewTaxRate": {
		"ENG": "Add New Tax Rate",
		"FRA": "Add New Tax Rate"
	},
	"addressValidation": {
		"ENG": "Address Validation",
		"FRA": "Validation de l’adresse"
	},
	"currencyConverter": {
		"ENG": "Currency Converter",
		"FRA": "Currency Converter"
	},
	"deliveryMethods": {
		"ENG": "Delivery Methods",
		"FRA": "Delivery Methods"
	},
	"deliveryMethod": {
		"ENG": "Delivery Method",
		"FRA": "Delivery Method"
	},
	"addDeliveryInterval": {
		"ENG": "Add New Delivery Interval",
		"FRA": "Add New Delivery Interval"
	},
	"paymentMethods": {
		"ENG": "Payment Methods",
		"FRA": "Méthodes de Paiement"
	},
	"paymentMethod": {
		"ENG": "Payment Method",
		"FRA": "Méthode de Paiement"
	},
	"pickupMethods": {
		"ENG": "Pickup Methods",
		"FRA": "Pickup Methods"
	},
	"pickupMethod": {
		"ENG": "Pickup Method",
		"FRA": "Pickup Method"
	}
};

for (var attrname in ModuleQaDrTranslation) {
	translation[attrname] = ModuleQaDrTranslation[attrname];
}

var driversModuleQaNav = [
	{
		//main information
		'id': 'currencyConverter',
		'label': translation.currencyConverter[LANG],
		'url': '#/currencyConverter',
		'scripts': [ModuleQaDrvLocation + '/currencyConverter/service.js', ModuleQaDrvLocation + '/currencyConverter/controller.js', ModuleQaDrvLocation + '/currencyConverter/config.js'],
		'tplPath': ModuleQaDrvLocation + '/currencyConverter/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/currencyConverter/list'
		},
		//menu & tracker information
		'icon': 'calculator',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	}
];

navigation = navigation.concat(driversModuleQaNav);

var driversNav_BU = [
	{
		//main information
		'id': 'addressValidation',
		'label': translation.addressValidation[LANG],
		'url': '#/addressValidation',
		'scripts': [ModuleQaDrvLocation + '/addressValidation/service.js', ModuleQaDrvLocation + '/addressValidation/controller.js', ModuleQaDrvLocation + '/addressValidation/config.js'],
		'tplPath': ModuleQaDrvLocation + '/addressValidation/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/addressValidation/list'
		},
		//menu & tracker information
		'icon': 'address-book',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'currencyConverter',
		'label': translation.currencyConverter[LANG],
		'url': '#/currencyConverter',
		'scripts': [ModuleQaDrvLocation + '/currencyConverter/service.js', ModuleQaDrvLocation + '/currencyConverter/controller.js', ModuleQaDrvLocation + '/currencyConverter/config.js'],
		'tplPath': ModuleQaDrvLocation + '/currencyConverter/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/currencyConverter/list'
		},
		//menu & tracker information
		'icon': 'calculator',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'paymentMethods',
		'label': translation.paymentMethods[LANG],
		'url': '#/paymentMethods',
		'scripts': [ModuleQaDrvLocation + '/paymentMethods/service.js', ModuleQaDrvLocation + '/paymentMethods/controller.js', ModuleQaDrvLocation + '/paymentMethods/config.js'],
		'tplPath': ModuleQaDrvLocation + '/paymentMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/paymentMethods/list'
		},
		//menu & tracker information
		'icon': 'credit-card',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'shippingMethods',
		'label': translation.shippingMethods[LANG],
		'url': '#/shippingMethods',
		'scripts': [ModuleQaDrvLocation + '/shippingMethods/config.js', ModuleQaDrvLocation + '/shippingMethods/service.js', ModuleQaDrvLocation + '/shippingMethods/controller.js'],
		'tplPath': ModuleQaDrvLocation + '/shippingMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/shippingMethods/list'
		},
		//menu & tracker information
		'icon': 'airplane',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'deliveryMethods',
		'label': translation.deliveryMethods[LANG],
		'url': '#/deliveryMethods',
		'scripts': [ModuleQaDrvLocation + '/deliveryMethods/service.js', ModuleQaDrvLocation + '/deliveryMethods/controller.js', ModuleQaDrvLocation + '/deliveryMethods/config.js'],
		'tplPath': ModuleQaDrvLocation + '/deliveryMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/deliveryMethods/list'
		},
		//menu & tracker information
		'icon': 'truck',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'taxValidation',
		'label': translation.taxCalculator[LANG],
		'url': '#/taxValidation',
		'scripts': [ModuleQaDrvLocation + '/taxValidation/service.js', ModuleQaDrvLocation + '/taxValidation/controller.js', ModuleQaDrvLocation + '/taxValidation/config.js'],
		'tplPath': ModuleQaDrvLocation + '/taxValidation/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/taxValidation/list'
		},
		//menu & tracker information
		'icon': 'coin-dollar',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	},
	{
		//main information
		'id': 'pickupMethods',
		'label': translation.pickupMethods[LANG],
		'url': '#/pickupMethods',
		'scripts': [ModuleQaDrvLocation + '/pickupMethods/config.js', ModuleQaDrvLocation + '/pickupMethods/service.js', ModuleQaDrvLocation + '/pickupMethods/controller.js'],
		'tplPath': ModuleQaDrvLocation + '/pickupMethods/directives/list.tmpl',
		//permissions information
		'checkPermission': {
			'service': 'order',
			'route': '/owner/pickupMethods/list'
		},
		//menu & tracker information
		'icon': 'home2',
		'contentMenu': true,
		'pillar': {
			'name': 'operate',
			'label': translation.operate[LANG],
			'position': 4
		},
		'mainMenu': true,
		'tracker': true,
		'order': 10,
		'ancestor': [translation.home[LANG]]
	}
];