"use strict";
var paymentMethodsService = soajsApp.components;
paymentMethodsService.service('paymentMethodsSrv', ['$timeout', 'ngDataApi', function ($timeout, ngDataApi) {

	function callAPI(currentScope, config, callback) {
		getSendDataFromServer(currentScope, ngDataApi, config, callback);
	}

	return {
		'getEntriesFromAPI': function (currentScope, opts, callback) {
			callAPI(currentScope, opts, callback);
		},

		'printGrid': function ($scope, response) {
			var options = {
				'grid': {
					recordsPerPageArray: [5, 10, 50, 100],
					'columns': [
						{'label': translation.driver[LANG], 'field': 'label'},
						{'label': translation.active[LANG], 'field': 'active', 'filter': "translateFields"}
					],
					'defaultLimit': 50
				},
				'defaultSortField': 'active',
				'defaultSortASC': true,
				'data': response,
				'left': []
			};

			if ($scope.access.list) {
				options.left.push({
					'icon': 'search',
					'label': translation.viewItem[LANG],
					'handler': 'viewEntry'
				});
			}

			if ($scope.access.edit) {
				options.left.push({
					'label': translation.edit[LANG],
					'icon': 'pencil2',
					'handler': 'editEntry'
				});
			}

			if ($scope.access.changeStatus) {
				options.left.push({
					'label': translation.disableActivate[LANG],
					'icon': 'spinner9',
					'handler': 'changeEntryStatus'
				});
			}

			if ($scope.access.delete) {
				options.left.push({
					'label': translation.delete[LANG],
					'icon': 'cross',
					'msg': translation.rUsureUwant2removeMethod[LANG],
					'handler': 'deleteEntry'
				});
			}

			buildGrid($scope, options);
		}
	}
}]);