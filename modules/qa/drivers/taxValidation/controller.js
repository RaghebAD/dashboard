"use strict";

var taxValidationApp = soajsApp.components;
taxValidationApp.controller('taxValidationModuleQaCtrl', ['$scope', '$modal', 'ngDataApi', '$cookies', 'injectFiles', '$timeout', 'taxValidationSrv', function ($scope, $modal, ngDataApi, $cookies, injectFiles, $timeout, taxValidationSrv) {
	$scope.$parent.isUserLoggedIn();
	
	//define the permissions
	var permissions = taxValidationModuleQaConfig.permissions;
	
	$scope.access = {};
	//call the method and compare the permissions with the ACL
	//allowed permissions are then stored in scope.access
	constructModulePermissions($scope, $scope.access, permissions);

	if (!$scope.$parent.currentSelectedEnvironment) {
		$scope.$parent.currentSelectedEnvironment = $cookies.getObject("myEnv").code;
	}

	$scope.drivers = {};

	//function that lists the Delivery Methods in a grid
	$scope.listEntries = function () {
		$scope.noAdd = false;

		var opts = {
			"routeName": "/order/owner/taxValidation/list",
			"method": "get",
			"params": {
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		};
		taxValidationSrv.getEntriesFromAPI($scope, opts, function (error, response) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.drivers = response.drivers;
				if (Object.keys(response.drivers).length === response.list.length) {
					$scope.noAdd = true;
				}

				taxValidationSrv.printGrid($scope, response.list);
			}
		});
	};
	
	//function that prints one data record to the console
	$scope.viewEntry = function (oneDataRecord) {
		$modal.open({
			templateUrl: "infoBox.html",
			size: 'dialog',
			backdrop: true,
			keyboard: true,
			controller: function ($scope, $modalInstance) {
				$scope.title = translation.viewingOne[LANG] + translation.taxCalculator[LANG];
				$scope.data = oneDataRecord;
				fixBackDrop();
				setTimeout(function () {
					highlightMyCode()
				}, 500);
				$scope.ok = function () {
					$modalInstance.dismiss('ok');
				};
			}
		});
	};

	function removeTax(id, data, form) {
		var number = id.replace("removeTax", "");
		delete form.formData['to' + number];
		delete form.formData['rate' + number];
		delete form.formData['type' + number];
		delete form.formData['priority' + number];
		delete form.formData['specialTax' + number];
		form.entries.forEach(function (oneEntry) {
			if (oneEntry.type === 'group') {
				for (var i = 0; i < oneEntry.entries.length; i++) {
					if (oneEntry.entries[i].name === 'to' + number) {
						oneEntry.entries.splice(i, 1);
					}
					if (oneEntry.entries[i].name === 'rate' + number) {
						oneEntry.entries.splice(i, 1);
					}
					if (oneEntry.entries[i].name === 'type' + number) {
						oneEntry.entries.splice(i, 1);
					}
					if (oneEntry.entries[i].name === 'priority' + number) {
						oneEntry.entries.splice(i, 1);
					}
					if (oneEntry.entries[i].name === 'specialTax' + number) {
						oneEntry.entries.splice(i, 1);
					}

					if (oneEntry.entries[i].name === 'removeTax' + number) {
						oneEntry.entries.splice(i, 1);
					}
				}
			}
		});
	}

	$scope.addEntry = function () {
		var formFields = angular.copy(taxValidationModuleQaConfig.form.entries);
		resetFormFields();
		var oneClone;
		var count = 0;

		//push into formFields the config.from returned per driver
		formFields[0].onAction = function (id, data, form) {
			form.entries[3].entries = [];
			if (!oneClone) {
				oneClone = angular.copy($scope.drivers[data].entries);
			}
			$scope.drivers[data].entries.forEach(function (oneEntry) {
				oneEntry.name = angular.copy(oneEntry.name.replace("%count%", count));
				if (oneEntry.type === 'html') {
					oneEntry.value = oneEntry.value.replace("%count%", count);
					oneEntry["onAction"] = removeTax;
				}
				form.entries[3].entries = form.entries[3].entries.concat(oneEntry);
			});
			count++;
		};


		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'addTaxValidation',
			'label': translation.addTaxCalculator[LANG],
			'actions': [
				{
					'type': 'button',
					'label': translation.addNewTaxRate[LANG],
					'btn': 'success',
					'action': function () {
						var cloneAgain = angular.copy(oneClone);
						cloneAgain.forEach(function (oneEntry) {
							oneEntry.name = oneEntry.name.replace("%count%", count);
							if (oneEntry.type === 'html') {
								oneEntry.value = oneEntry.value.replace("%count%", count);
								oneEntry['onAction'] = removeTax;
							}
							$scope.form.entries[3].entries = $scope.form.entries[3].entries.concat(oneEntry);
						});
						count++;
					}
				},
				{
					'type': 'submit',
					'label': translation.submit[LANG],
					'btn': 'primary',
					'action': function (formUI) {
						var formData = angular.copy(formUI);

						var postData = {
							config: {
								rates: []
							}
						};

						var predefined = [];
						taxValidationModuleQaConfig.form.entries.forEach(function (oneField) {
							predefined.push(oneField.name);
						});

						for (var i in formData) {
							if (predefined.indexOf(i) !== -1) {
								postData[i] = formData[i];
								delete formData[i];
							}
						}

						var status;
						if ($scope.drivers[postData.driver].customMapping) {
							var customMapping = new Function("formData", "postData", "count", $scope.drivers[postData.driver].customMapping);
							status = customMapping(formData, postData, count);
						}

						if (status === false) {
							return false;
						}

						getSendDataFromServer($scope, ngDataApi, {
							"method": "send",
							"routeName": "/order/owner/taxValidation/add",
							"params": {
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						}, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.code, true, 'order', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.methodAddedSuccessfully[LANG]);
								$scope.modalInstance.close();
								$scope.form.formData = {};
								$scope.listEntries();
							}
						});
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);

		//custom function to reset form fields
		function resetFormFields() {
			var myArray = formFields[0];
			myArray.value = [];

			var driverNames = Object.keys($scope.drivers);
			driverNames.forEach(function (oneDriverName) {

				if ($scope.grid.filteredRows.length === 0) {
					myArray.value.push({
						"v": oneDriverName,
						"l": oneDriverName
					});
				}
				else {
					for (var i = 0; i < $scope.grid.filteredRows.length; i++) {

						//skip existing drivers
						if ($scope.grid.filteredRows[i].driver === oneDriverName) {
							continue;
						}

						myArray.value.push({
							"v": oneDriverName,
							"l": oneDriverName
						});
					}
				}
			});
		}
	};

	$scope.editEntry = function (data) {
		var formFields = angular.copy(taxValidationModuleQaConfig.form.entries);
		var oneClone = angular.copy($scope.drivers[data.driver].entries);
		var count = data.config.rates.length;
		resetFormFields(data);

		var options = {
			timeout: $timeout,
			form: {"entries": formFields},
			'name': 'editTaxValidation',
			'label': translation.edit[LANG] + ' ' + translation.taxCalculator[LANG],
			'data': data,
			'actions': [
				{
					'type': 'button',
					'label': translation.addNewTaxRate[LANG],
					'btn': 'success',
					'action': function () {
						var cloneAgain = angular.copy(oneClone);
						cloneAgain.forEach(function (oneEntry) {
							oneEntry.name = oneEntry.name.replace("%count%", count);
							if (oneEntry.type === 'html') {
								oneEntry.value = oneEntry.value.replace("%count%", count);
								oneEntry['onAction'] = removeTax;
							}
							$scope.form.entries[3].entries = $scope.form.entries[3].entries.concat(oneEntry);
						});
						count++;
					}
				},
				{
					'type': 'submit',
					'label': translation.save[LANG],
					'btn': 'primary',
					'action': function (formUI) {
						var formData = angular.copy(formUI);

						var postData = {
							config: {
								rates: []
							}
						};

						var predefined = [];
						taxValidationModuleQaConfig.form.entries.forEach(function (oneField) {
							predefined.push(oneField.name);
						});

						for (var i in formData) {
							if (predefined.indexOf(i) !== -1) {
								postData[i] = formData[i];
								delete formData[i];
							}
						}

						var status;
						if ($scope.drivers[postData.driver].customMapping) {
							var customMapping = new Function("formData", "postData", "count", $scope.drivers[postData.driver].customMapping);
							status = customMapping(formData, postData, count);
						}

						if (status === false) {
							return false;
						}

						getSendDataFromServer($scope, ngDataApi, {
							"method": "send",
							"routeName": "/order/owner/taxValidation/edit",
							"params": {
								'id': data._id,
								"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
							},
							"data": postData
						}, function (error) {
							if (error) {
								$scope.form.displayAlert('danger', error.code, true, 'order', error.message);
							}
							else {
								$scope.$parent.displayAlert('success', translation.methodUpdatedSuccessfully[LANG]);
								$scope.modalInstance.close();
								$scope.form.formData = {};
								$scope.listEntries();
							}
						});
					}
				},
				{
					'type': 'reset',
					'label': translation.cancel[LANG],
					'btn': 'danger',
					'action': function () {
						$scope.modalInstance.dismiss('cancel');
						$scope.form.formData = {};
					}
				}
			]
		};
		buildFormWithModal($scope, $modal, options);

		function resetFormFields(data) {
			var myArray = formFields[0];
			myArray.value = data.driver;
			myArray.type = "readonly";

			for (var i = 0; i < count; i++) {
				var subClone = angular.copy(oneClone);
				subClone.forEach(function (oneEntry) {
					if (oneEntry.type === 'html') {
						oneEntry.value = oneEntry.value.replace("%count%", i);
						oneEntry["onAction"] = removeTax;
					}
					else if (oneEntry.type === 'select') {
						oneEntry.value.forEach(function (oneOption) {
							if (oneOption.v === data.config.rates[i][oneEntry.name.replace("%count%", "")]) {
								oneOption.selected = true;
							}
						});
					}
					else {
						oneEntry.value = data.config.rates[i][oneEntry.name.replace("%count%", "")];
					}
					oneEntry.name = oneEntry.name.replace("%count%", i);
					if (oneEntry.name.indexOf('specialTax') === 0) {
						oneEntry.value = (oneEntry.value) ? oneEntry.value.toString() : '';
					}

				});
				formFields[3].entries = formFields[3].entries.concat(subClone);
			}
		}
	};

	$scope.deleteEntry = function (data) {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/order/owner/taxValidation/delete",
			"params": {
				'id': data._id,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.methodDeletedSuccessfully[LANG]);
				$scope.listEntries();
			}
		});
		
	};

	$scope.changeEntryStatus = function (data) {
		getSendDataFromServer($scope, ngDataApi, {
			"method": "get",
			"routeName": "/order/owner/taxValidation/changeStatus",
			"params": {
				'id': data._id,
				"__env": $scope.$parent.currentSelectedEnvironment.toUpperCase()
			}
		}, function (error) {
			if (error) {
				$scope.$parent.displayAlert('danger', error.code, true, 'order', error.message);
			}
			else {
				$scope.$parent.displayAlert('success', translation.statusChangedSuccessfully[LANG]);
				$scope.listEntries();
			}
		});

	};

	//if scope.access.list is allowed, call listEntries
	if ($scope.access.list) {
		$scope.listEntries();
	}
	injectFiles.injectCss(ModuleQaDrvLocation + "/taxValidation.css");
}]);